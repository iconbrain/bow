<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('password/email',array(
	'as'	=> 'password.email',
	'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'));
Route::get('password/reset/{token}',array(
	'as'	=> 'password.reset',
	'uses' => 'Auth\ResetPasswordController@showResetForm'));
Route::get('passwords/reset',array(
	'as'	=> 'auth.passwords.reset',
	'uses' => 'Frontend\UserController@showResetPassword'));
Route::get('password/reset',array(
	'as'	=> 'password.request',
	'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'));
Route::post('password/reset',array(
	'uses' => 'Auth\ResetPasswordController@reset'));

Route::group(array('namespace'=>'Frontend'), function()
{
	// HOME, LOGIN, LOGOUT
	Route::get('/',array(
		'as'	=> 'home',
		'uses' => 'HomeController@index'));
	Route::post('login', 'UserController@login');
	Route::post('signup', 'UserController@signup');
	Route::get('logout', 'UserController@logout');
	Route::get('logoutProfile', 'UserController@logoutProfile');

	Route::get('bantuan', 'HomeController@getHowToUse');
	Route::get('credits', 'HomeController@getCredits');
	Route::get('tentang', 'HomeController@getTentang');
	Route::get('forgot-password', 'HomeController@getForgotPassword');

	// RECOMMENDED
	Route::get('recommended', 'HomeController@getRecommended');

	// REVIEW OF THE WEEK
	Route::get('weekly-review', 'HomeController@getWeeklyReview');

	// NEW PLACE
	Route::get('weekly-added', 'HomeController@getWeeklyAdded');

	// NOT REVIEWED YET
	Route::get('not-reviewed-yet', 'HomeController@getNotReviewedYet');

	// CATEGORY
	Route::get('category/{prefix}', 'CategoryController@placeByCategory');

	// PLACE
	Route::get('place/{prefix}', 'PlaceController@place');
	Route::post('rating', 'PlaceController@storeReview');

	// ADD COMMENT
	Route::post('comment','CommentController@store');

	// SEARCH
	Route::post('search/results', 'HomeController@searchResults');

	// PLACES C
	Route::get('places/add', 'PlaceController@add');
	Route::post('addPlaces', 'PlaceController@addPlaces');

	// BOOKMARK
	Route::post('bookmark/place',array(
		'as'	=> 'bookmark.place',
		'uses' => 'PlaceController@bookmarkPlace'));
	Route::post('account/bookmark', 'PlaceController@deleteBookmark');

	// PROFILE RIGHT SIDEBAR
	Route::get('account/profile', 'ProfileController@getProfile');
	Route::post('updateUserProfile', 'ProfileController@editUserProfile');
	Route::get('account/password', 'ProfileController@getPassword');
	Route::post('updateUserPassword', 'ProfileController@editUserPassword');
	Route::get('account/review', 'ProfileController@getReview');
	Route::get('account/comment', 'ProfileController@getComment');
	Route::get('account/bookmark', 'ProfileController@getBookmark');

	Route::post('review/delete', 'ProfileController@postDeleteReview');
	Route::get('account/edit-review/{reviewId}', 'ProfileController@getEditReview');
	Route::post('review/edit', 'ProfileController@postEditReview');
	Route::post('comment/delete', 'ProfileController@postDeleteComment');
	Route::get('account/edit-comment/{commentId}', 'ProfileController@getEditComment');
	Route::post('comment/edit', 'ProfileController@postEditComment');

	Route::post('category/filter-city', 'CategoryController@postFilterCity');
	Route::post('category/filter-recommended', 'CategoryController@postFilterRecommended');
	Route::post('category/filter-nry', 'CategoryController@postFilterNry');
	Route::post('category/filter-wr', 'CategoryController@postFilterWr');
	Route::post('category/filter-wa', 'CategoryController@postFilterWa');
});

Route::group(array('namespace'=>'Backend','prefix' => 'adminpanel'), function()
{
	// HOME, LOGIN, LOGOUT
	Route::get('/','HomeController@check');
    Route::get('login', 'HomeController@login');
    Route::post('login', 'HomeController@postLogin');
    Route::group(['middleware' => ['auth', 'admin']], function() {
    	Route::get('logout', 'HomeController@logout');
		Route::get('home', 'HomeController@index');

		// TASKS DATATABLES
		Route::get('tasks', 'HomeController@getTasksList');
		Route::get('tasksList', 
			array('as' => 'adminpanel.tasks', 
				'uses' =>'HomeController@getTasksData')
		);

		// TASKS CRUD
		Route::get('tasks/add',
			'TaskController@addTask'
		);
		Route::post('tasks/add',
			'TaskController@postAddTask'
		);
		Route::get('tasks/edit/{id}',
			'TaskController@editTask'
		);
		Route::post('tasks/edit/{id}',
			'TaskController@postEditTask'
		);
		Route::post('tasks/delete/task',array(
			'as'	=> 'tasks.delete.task',
			'uses' => 'TaskController@deleteTask'));

		// PROFILE RU
		Route::get('profile', 'ProfileController@getProfile');
		Route::post('profile/admin/edit',
			'ProfileController@postEditProfile'
		);
		Route::post('password/admin/edit',
			'ProfileController@postEditPassword'
		);

		// CATEGORIES DATATABLES
		Route::get('categories', 'HomeController@getCategoriesList');
		Route::get('categoriesList', 
			array('as' => 'adminpanel.categories', 
				'uses' =>'HomeController@getCategoriesData')
		);

		// CATEGORIES RU
		Route::get('categories/edit/{category_id}',
			'CategoryController@editCategory'
		);
		Route::post('categories/edit/{category_id}',
			'CategoryController@postEditCategory'
		);

		// LOCATIONS DATATABLES
		Route::get('locations', 'HomeController@getLocationsList');
		Route::get('locationsList', 
			array('as' => 'adminpanel.locations', 
				'uses' =>'HomeController@getLocationsData')
		);

		// LOCATIONS RU
		Route::get('locations/edit/{location_id}',
			'LocationController@editLocation'
		);
		Route::post('locations/edit/{location_id}',
			'LocationController@postEditLocation'
		);
		
		// PLACES DATATABLES
		Route::get('places', 'HomeController@getPlacesList');
		Route::get('placesList', 
			array('as' => 'adminpanel.places', 
				'uses' =>'HomeController@getPlacesData')
		);

		// REQUEST DATATABLES
		Route::get('request', 'ReqController@getRequestsList');
		Route::get('requestList',
			array('as' => 'adminpanel.request', 
				'uses' =>'ReqController@getRequestData')
		);

		// PROCEED REQUEST
		Route::get('request/view/{id}',
			'ReqController@viewRequest'
		);
		Route::post('places/addPlaceFromUser',
			'PlaceController@postAddPlaceFromUser'
		);
		Route::post('request/delete/request',array(
			'as'	=> 'request.delete.request',
			'uses' => 'ReqController@deleteRequest'));

		// PLACES CRUD
		Route::get('places/add',
			'PlaceController@addPlace'
		);
		Route::post('places/add',
			'PlaceController@postAddPlace'
		);
		Route::get('places/edit/{place_id}',
			'PlaceController@editPlace'
		);
		Route::post('rating/places/edit/{place_id}',
			'PlaceController@postEditPlaceRating'
		);
		Route::post('home/places/edit/{place_id}',
			'PlaceController@postEditPlaceHome'
		);
		Route::post('assets/places/edit/{place_id}',
			'PlaceController@postEditPlaceAssets'
		);
		Route::post('places/delete/place',array(
			'as'	=> 'places.delete.place',
			'uses' => 'PlaceController@deletePlace'));

		// REVIEWS DATATABLES
		Route::get('reviews', 'HomeController@getReviewsList');
		Route::get('reviewsList', 
			array('as' => 'adminpanel.reviews', 
				'uses' =>'HomeController@getReviewsData')
		);
		Route::get('reviews/view/{id}',
			'HomeController@viewReviews'
		);

		// COMMENTS DATATABLES
		Route::get('comments', 'HomeController@getCommentsList');
		Route::get('commentsList', 
			array('as' => 'adminpanel.comments', 
				'uses' =>'HomeController@getCommentsData')
		);
		Route::get('comments/view/{id}',
			'HomeController@viewComments'
		);

		// COMMENTS DATATABLES
		Route::get('users', 'HomeController@getUsersList');
		Route::get('usersList', 
			array('as' => 'adminpanel.users', 
				'uses' =>'HomeController@getUsersData')
		);

		// USERS DATATABLES
		Route::get('users', 'HomeController@getUsersList');
		Route::get('usersList', 
			array('as' => 'adminpanel.users', 
				'uses' =>'HomeController@getUsersData')
		);

		// USERS RU
		Route::get('users/view/{id}',
			'UserController@viewUser'
		);
		Route::get('users/edit/{id}',
			'UserController@editUser'
		);
		Route::post('home/users/edit/{id}',
			'UserController@postEditUser'
		);
		Route::post('password/users/edit/{id}',
			'UserController@postEditUserPassword'
		);

		// RATING
		Route::get('/rating',array(
			'as' => 'rating', 
			'uses' => 'HomeController@rating'));

		// AHP
		Route::get('/ahp',array(
			'uses' => 'HomeController@ahp'));
		// AHP + TOPSIS
		Route::get('/ahp-topsis/wisata-alam',array(
			'as'	=> 'algo.wisataAlam',
			'uses' => 'HomeController@wisataAlam'));
		Route::get('/ahp-topsis/wisata-religi',array(
			'as'	=> 'algo.wisataReligi',
			'uses' => 'HomeController@wisataReligi'));
		Route::get('/ahp-topsis/wisata-sejarah',array(
			'as'	=> 'algo.wisataSejarah',
			'uses' => 'HomeController@wisataSejarah'));
		Route::get('/ahp-topsis/wisata-belanja',array(
			'as'	=> 'algo.wisataBelanja',
			'uses' => 'HomeController@wisataBelanja'));
		Route::get('/ahp-topsis/wisata-satwa',array(
			'as'	=> 'algo.wisataSatwa',
			'uses' => 'HomeController@wisataSatwa'));
		Route::get('/ahp-topsis/wisata-kuliner',array(
			'as'	=> 'algo.wisataKuliner',
			'uses' => 'HomeController@wisataKuliner'));
		Route::get('/ahp-topsis/agrowisata',array(
			'as'	=> 'algo.agrowisata',
			'uses' => 'HomeController@agrowisata'));
		Route::get('/ahp-topsis/wisata-keluarga',array(
			'as'	=> 'algo.wisataKeluarga',
			'uses' => 'HomeController@wisataKeluarga'));
		Route::get('/ahp-topsis/all',array(
			'as'	=> 'algo.all',
			'uses' => 'HomeController@all'));

		Route::get('ahp-topsis/all/list', 
			array('as' => 'ahptopsis.all', 
				'uses' =>'HomeController@getDecisionMatriksData')
		);

		// REQUEST API
		Route::get('/request-api', array(
			'as'	=> 'request-api',
			'uses' => 'UserController@requestAPI'));
		Route::post('/request-api-post', array(
			'as'	=> 'request-api-post',
			'uses' => 'UserController@requestAPIPost'));
    });
	
});

