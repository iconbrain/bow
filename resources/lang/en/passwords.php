<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi minimal 6 karakter dan sesuai dengan konfirmasi.',
    'reset' => 'Kata sandi Anda telah di ganti.',
    'sent' => 'Kami telah mengirimkan instruksi penggantian kata sandi ke email Anda.',
    'token' => 'Token untuk penggantian kata sandi ini tidak valid.',
    'user' => "Alamat email tidak ditemukan dalam sistem kami.",

];
