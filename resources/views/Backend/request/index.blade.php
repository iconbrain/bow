@extends('Backend.layouts.blank')

@push('stylesheets')
  <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Permintaan Wisata</h3>
      </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      @if (Session::has('message'))
        <div class="alert alert-success"><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
      @endif
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Permintaan Wisata <small>BOW</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="requestWisata" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
          <thead>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog" style="color: white">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#F92A00">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-trash"></i>&nbsp;&nbsp;Anda yakin ingin menghapus request ini?</h4>
            </div>
            <div class="modal-footer">
              <form id="delete_form" method="post" action="/adminpanel/tasks/delete/task">
                  {{ csrf_field() }}
                  <input type="button" class="btn btn-danger pull-right" value="Ya, Hapus Sekarang" style="background-color:#F92A00" id="submitDelete" data-dismiss="modal"></input>
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
              </form>
            </div>
        </div>
    </div>
  </div>
  
  <!-- SCRIPT -->
  @push('scripts')
  <script>
    var table;
    function deleteRequest(id) 
    {
      $('#delete_modal').modal('show');
      document.getElementById('submitDelete').onclick = function() {
        $.ajax({
        type: "POST",
        url: "{{ URL::to('adminpanel/request/delete/request') }}",
        data: {id:id, _token:"<?php echo csrf_token(); ?>"},
        success:
        function(success)
        {
          if(success)
          {
            table.draw(false);
            // $('#delete_modal').modal('hide');
            // alert('Data has been deleted');
          }
          else alert('Failed');
        }
        })
      };
    }

    $(function() {
      table = $('#requestWisata').DataTable({
        processing: false,
        serverSide: true,
        ajax: '{!! route('adminpanel.request') !!}',
        aaSorting: [[6,"desc"]],

        columns: [
          { data: 'id', name: 'id', title:'ID', "width": "1%" },
          { data: 'username', name: 'username', title:'Dari User', "width": "15%" },
          { data: 'name', name: 'name', title:'Nama', "width": "15%" },
          { data: 'category_name', name: 'category_name', title:'Kategori', "width": "15%" },
          { data: 'address', name: 'address', title:'Alamat', "width": "25%" },
          { data: 'description', name: 'description', title:'Deskripsi', "width": "10%", render: function(data, type, row) {
            function getExcerpt($str, $startPos=0, $maxLength=100) {
              if(($str).length > $maxLength) {
                $excerpt   = ($str).substring($startPos, $maxLength-3);
                $lastSpace = ($str).lastIndexOf(" ");
                $excerpt   = ($excerpt).substring(0, $lastSpace);
                $excerpt  += '...';
              } else {
                $excerpt = $str;
              }
              
              return $excerpt;
            }

            return getExcerpt(row.description, 0, 40);
          } },
          { data: 'created_at', name: 'created_at', title:'Tanggal', "width": "25%" },
          { className: "dt-center", width:"10%", name: 'actions', title: 'Aksi', render: function(data, type, row) {
            var data = "`" + row.name + "`";
            return '<a class="btn btn_primary btn_action" href=request/view/'+row.id+'>' + '<i class="fa fa-eye"></i> Lihat' + '</a>'  +
                   '<a class="btn btn_danger btn_action" onclick="deleteRequest(' + row.id + ')" >' + '<i class="fa fa-trash"></i> Hapus' + '</a>';
          } }
        ]
      });
    });
  </script>
  @endpush

  @include('Backend.includes.footer')
@endsection
