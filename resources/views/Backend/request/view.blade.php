@extends('Backend.layouts.blank')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset("css/custom.css")}}"/>
@endpush

@section('main_container')

<div class="right_col" role="main">
  <div class="page-title">
    <div class="title_left">
      <h3>Lihat Permintaan Wisata</h3>
    </div>
  </div>

  @foreach($requests as $req)
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Form Permintaan Wisata <small>BOW</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <p>Jika Anda menekan tombol <code>submit</code>, maka form ini akan langsung <code>dimasukkan sebagai objek wisata baru.</code></p><br>

          @foreach($requests as $r)
          <form id="demo-form2" data-parsley-validate=""  class="form-horizontal form-label-left input_mask" novalidate="" action="{{ URL::to('/adminpanel/places/addPlaceFromUser') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
            <input type="hidden" name="id" value="{{ $r->id }}">

            @if (count($errors->placeByUserRequest) > 0)
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                @foreach ($errors->placeByUserRequest->all() as $error)
                <P>{{ $error }}</p>
                  @endforeach
              </div>
            @endif

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Dari User</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="from_user" required="required" class="form-control col-md-7 col-xs-12" value="{{ $r->username }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Objek Wisata</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="name" required="required" class="form-control col-md-7 col-xs-12" value="{{ $r->name }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Telepon</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" required="required" type="text" name="phone_number" value="{{ $r->phone_number }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea rows="3" class="form-control col-md-7 col-xs-12" required="required" type="text" name="address">{{ $r->address }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Jam Operasional</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea rows="3" class="form-control col-md-7 col-xs-12" required="required" type="text" name="operational_hours">{!! $r->operational_hours !!}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Range HTM</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" type="text" required="required" name="price_range" value="{{ $r->price_range }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Tiket Masuk</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea rows="3" class="form-control col-md-7 col-xs-12" required="required" type="text" name="price">{{ $r->price }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Lokasi</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" type="text" required="required" name="location" value="{{ $r->location_name }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" type="text" required="required" name="category" value="{{ $r->category_name }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea rows="10" class="form-control col-md-7 col-xs-12" required="required" type="text" name="description">{{ $r->description }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <button class="btn btn-danger" type="reset">Reset</button>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
          @endforeach
        </div>

      </div>
    </div>
  </div>
  @endforeach
</div>

<!-- SCRIPT -->
@push('scripts')
<!-- jQuery -->
<script async="" src="https://www.google-analytics.com/analytics.js"></script>

<!-- FastClick -->
<script src="{{ asset("js/fastclick.js") }}"></script>

<!-- NProgress -->
<script src="{{ asset("js/nprogress.js") }}"></script>

<!-- validator -->
<script src="{{ asset("js/validator.js") }}"></script>

<!-- bootstrap-progressbar -->
<script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

<!-- iCheck -->
<script src="{{ asset("js/icheck.min.js") }}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset("js/moment.min.js") }}"></script>
<script src="{{ asset("js/daterangepicker.js") }}"></script>

<!-- bootstrap-wysiwyg -->
<script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
<script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
<script src="{{ asset("js/prettify.js") }}"></script>

<!-- jQuery tags input -->
<script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

<!-- Switchery -->
<script src="{{ asset("js/switchery.min.js") }}"></script>

<!-- Select2 -->
<script src="{{ asset("js/select2.full.min.js") }}"></script>

<!-- Autosize -->
<script src="{{ asset("js/autosize.min.js") }}"></script>

<!-- jQuery autocomplete -->
<script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

<!-- starrr -->
<script src="{{ asset("js/starrr.js") }}"></script>

<!-- PNotify -->
<script src="{{ asset("js/pnotify.js") }}"></script>
<script src="{{ asset("js/pnotify.buttons.js") }}"></script>
<script src="{{ asset("js/pnotify.nonblock.js") }}"></script>

<!-- Parsley -->
<script src="{{ asset("js/parsley.min.js") }}"></script>
@endpush

@include('Backend.includes.footer')
@endsection
