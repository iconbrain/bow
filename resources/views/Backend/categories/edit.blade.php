@extends('Backend.layouts.blank')

@push('stylesheets')
  <!-- Switchery -->
  <link href="{{ asset("css/switchery.min.css") }}" rel="stylesheet">
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Kategori Wisata</h3>
      </div>
    </div>

    @if (Session::has('message'))
    <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
    @endif

    @foreach($categories as $item)
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
        <div class="x_panel">
          <div class="x_title">
            <h2>Ubah <small>Kategori Wisata</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a><i class="fa fa-wrench"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/categories/edit/{{ $item->category_id }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
              @if (count($errors->editCategories) > 0)
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                @foreach ($errors->editCategories->all() as $error)
                <P>{{ $error }}</p>
                @endforeach
              </div>
              @endif 

              <p>Silahkan ubah <code>kategori wisata</code> sesuai dengan yang Anda inginkan.</p>
              <span class="section">Informasi</span>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ID</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="category_id" class="form-control col-md-7 col-xs-12" type="text" value="{{ $item->category_id }}" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Prefix</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="prefix" class="form-control col-md-7 col-xs-12" type="text" value="{{ $item->prefix }}" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Kategori</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input name="category_name" class="form-control col-md-7 col-xs-12" type="text" value="{{ $item->category_name }}" required="required" >
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gambar</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" id="image" name="image" class="form-control col-md-7 col-xs-12"></input><br><br>
                  <img src="{{ URL::to('/') }}/images/{{$item->icon}}" class="img-responsive" alt="" width="140px">
                </div>
                <input type="hidden" name="iconOld" value="{{ $item->icon }}"></input>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button class="btn btn-danger" type="reset">Reset</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
  @endforeach

  @push('scripts')
  <!-- jQuery -->
  <script async="" src="https://www.google-analytics.com/analytics.js"></script>

  <!-- FastClick -->
  <script src="{{ asset("js/fastclick.js") }}"></script>

  <!-- NProgress -->
  <script src="{{ asset("js/nprogress.js") }}"></script>

  <!-- validator -->
  <script src="{{ asset("js/validator.js") }}"></script>

  <!-- bootstrap-progressbar -->
  <script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

  <!-- iCheck -->
  <script src="{{ asset("js/icheck.min.js") }}"></script>

  <!-- bootstrap-daterangepicker -->
  <script src="{{ asset("js/moment.min.js") }}"></script>
  <script src="{{ asset("js/daterangepicker.js") }}"></script>

  <!-- bootstrap-wysiwyg -->
  <script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
  <script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
  <script src="{{ asset("js/prettify.js") }}"></script>

  <!-- jQuery tags input -->
  <script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

  <!-- Switchery -->
  <script src="{{ asset("js/switchery.min.js") }}"></script>

  <!-- Select2 -->
  <script src="{{ asset("js/select2.full.min.js") }}"></script>

  <!-- Parsley -->
  <script src="{{ asset("js/parsley.min.js") }}"></script>

  <!-- Autosize -->
  <script src="{{ asset("js/autosize.min.js") }}"></script>

  <!-- jQuery autocomplete -->
  <script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

  <!-- starrr -->
  <script src="{{ asset("js/starrr.js") }}"></script>

  <!-- PNotify -->
  <script src="{{ asset("js/pnotify.js") }}"></script>
  <script src="{{ asset("js/pnotify.buttons.js") }}"></script>
  <script src="{{ asset("js/pnotify.nonblock.js") }}"></script>
  @endpush

  @include('Backend.includes.footer')
@endsection
