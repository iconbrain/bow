@extends('Backend.layouts.blank')

@push('stylesheets')
  <!-- Switchery -->
  <link href="{{ asset("css/switchery.min.css") }}" rel="stylesheet">
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Objek Wisata</h3>
      </div>
    </div>

    @foreach($places as $item)
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
        <div class="x_panel">
          <div class="x_title">
            <h2>{{$item->name}}<small>Keterangan</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_home" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Home</a>
                </li>
                <li role="presentation" class=""><a href="#tab_rating" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Default Rating</a>
                </li>
                <li role="presentation" class=""><a href="#tab_assets" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Assets</a>
                </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_home" aria-labelledby="home-tab">
                  <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/home/places/edit/{{ $item->place_id }}" method="post">
                    {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                    @if (count($errors->editPlaceHome) > 0)
                    <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      @foreach ($errors->editPlaceHome->all() as $error)
                      <P>{{ $error }}</p>
                      @endforeach
                    </div>
                    @endif 
                    @if (Session::has('message'))
                    <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
                    @endif

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control col-md-7 col-xs-12" value="{{ $item->place_id }}" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Prefix
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="place_prefix" name="place_prefix" required="required" class="form-control col-md-7 col-xs-12" value="{{$item->place_prefix}}" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Objek Wisata
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12" value="{{$item->name}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Lokasi
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" value="{{ isset($location) ? $location->location_name : '-' }}" class="form-control col-md-7 col-xs-12" readonly></input>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <select class="form-control" name="lokasi">
                          <option>Pilih Lokasi...</option>
                          @foreach($locations as $loc)
                            <option value="{{ $loc->location_id }}">{{ $loc->location_name }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori Wisata
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" value="{{ isset($category) ? $category->category_name : '-' }}" class="form-control col-md-7 col-xs-12" readonly></input>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <select class="form-control" name="kategori">
                          <option>Pilih Kategori...</option>
                          @foreach($categories as $cat)
                            <option value="{{ $cat->category_id }}">{{ $cat->category_name }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operational_hours">Jam Operasional
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea rows="3" id="operational_hours" name="operational_hours" required="required" class="form-control col-md-7 col-xs-12">{!! $item->operational_hours !!}</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Harga
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea rows="3" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12">{{$item->price}}</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone_number">Nomor Telepon
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="phone_number" name="phone_number" required="required" class="form-control col-md-7 col-xs-12" value="{{$item->phone_number}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Alamat
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea rows="3" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12">{{$item->address}}</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Deskripsi
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="x_content">
                          <div id="alerts"></div>
                          <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#descriptionEdit">
                            <div class="btn-group">
                              <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li>
                                  <a data-edit="fontSize 5">
                                    <p style="font-size:17px">Huge</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit="fontSize 3">
                                    <p style="font-size:14px">Normal</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit="fontSize 1">
                                    <p style="font-size:11px">Small</p>
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div class="btn-group">
                              <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                              <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                              <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                              <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                            </div>

                            <div class="btn-group">
                              <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                              <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                              <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                              <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                            </div>

                            <div class="btn-group">
                              <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                              <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                              <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                              <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                            </div>
                            
                            <div class="btn-group">
                              <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                              <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                            </div>
                          </div>
                          
                          <textarea name="description" id="description" style="display:none;">{{ Request::old('description') }}</textarea>
                          <div id="descriptionEdit" name="descriptionEdit" class="editor-wrapper placeholderText" contenteditable="true" required="required">{!! $item->description !!}</div>
                        </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary" name="submitHome" id="submitHome">Submit</button>
                      </div>
                    </div>

                  </form>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_rating" aria-labelledby="profile-tab">
                  <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/rating/places/edit/{{ $item->place_id }}" method="post">
                    {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                    @if (count($errors->editPlaceRating) > 0)
                    <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      @foreach ($errors->editPlaceRating->all() as $error)
                      <P>{{ $error }}</p>
                      @endforeach
                    </div>
                    @endif 
                    @if (Session::has('message'))
                    <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
                    @endif

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Fasilitas
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="facility" name="facility" required="required" class="form-control col-md-7 col-xs-12" value="{{ $item->facility_criteria }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kebersihan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="cleanliness" name="cleanliness" required="required" class="form-control col-md-7 col-xs-12" value="{{ $item->cleanliness_criteria }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kenyamanan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="comfort" name="comfort" required="required" class="form-control col-md-7 col-xs-12" value="{{ $item->comfort_criteria }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Pelayanan
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="service" name="service" required="required" class="form-control col-md-7 col-xs-12" value="{{ $item->service_criteria }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Harga
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12" value="{{ $item->price_criteria }}">
                      </div>
                    </div>                    
                    <input type="hidden" id="name" name="name" value="{{ $item->name }}">
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>

                  </form>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tab_assets" aria-labelledby="profile-tab">
                  <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/assets/places/edit/{{ $item->place_id }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                    @if (count($errors->editPlaceAssets) > 0)
                    <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      @foreach ($errors->editPlaceAssets->all() as $error)
                      <P>{{ $error }}</p>
                      @endforeach
                    </div>
                    @endif 
                    @if (Session::has('message'))
                    <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
                    @endif

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gambar
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="file" id="image" name="image" class="form-control col-md-7 col-xs-12"></input><br><br><br>
                        @if(strpos($item->image,'https://') !== false)
                          <img src="{{$item->image}}" class="img-responsive" alt="">
                        @else
                          <img src="{{ URL::to('/') }}/images/{{$item->image}}" class="img-responsive" alt="">
                        @endif
                      </div>
                      <input type="hidden" name="iconOld" value="{{ $item->image }}"></input>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Video
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="video_link" name="video_link" class="form-control col-md-7 col-xs-12" value="{{ $item->video_link }}"></input><br><br><br>
                        @if($item->video_link != NULL)
                          <?php
                            $embed = Embed::make( $item->video_link )->parseUrl();
                            
                            if ($embed) {
                              $embed->setAttribute(['width' => 300]);
                              echo $embed->getHtml();
                            }
                          ?>
                          @endif
                      </div>
                    </div>
                    <input type="hidden" id="name" name="name" value="{{ $item->name }}">
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
  @endforeach

  @push('scripts')
  <!-- jQuery -->
  <script async="" src="https://www.google-analytics.com/analytics.js"></script>

  <!-- FastClick -->
  <script src="{{ asset("js/fastclick.js") }}"></script>

  <!-- NProgress -->
  <script src="{{ asset("js/nprogress.js") }}"></script>

  <!-- validator -->
  <script src="{{ asset("js/validator.js") }}"></script>

  <!-- bootstrap-progressbar -->
  <script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

  <!-- iCheck -->
  <script src="{{ asset("js/icheck.min.js") }}"></script>

  <!-- bootstrap-daterangepicker -->
  <script src="{{ asset("js/moment.min.js") }}"></script>
  <script src="{{ asset("js/daterangepicker.js") }}"></script>

  <!-- bootstrap-wysiwyg -->
  <script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
  <script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
  <script src="{{ asset("js/prettify.js") }}"></script>

  <!-- jQuery tags input -->
  <script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

  <!-- Switchery -->
  <script src="{{ asset("js/switchery.min.js") }}"></script>

  <!-- Select2 -->
  <script src="{{ asset("js/select2.full.min.js") }}"></script>

  <!-- Parsley -->
  <script src="{{ asset("js/parsley.min.js") }}"></script>

  <!-- Autosize -->
  <script src="{{ asset("js/autosize.min.js") }}"></script>

  <!-- jQuery autocomplete -->
  <script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

  <!-- starrr -->
  <script src="{{ asset("js/starrr.js") }}"></script>

  <!-- PNotify -->
  <script src="{{ asset("js/pnotify.js") }}"></script>
  <script src="{{ asset("js/pnotify.buttons.js") }}"></script>
  <script src="{{ asset("js/pnotify.nonblock.js") }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
      });
      var activeTab = localStorage.getItem('activeTab');
      if(activeTab){
        $('#myTab a[href="' + activeTab + '"]').tab('show');
      }
    });

    $(document).ready(function(){
      $('#submitHome').click(function(){
        $('#description').val($('#descriptionEdit').html())
      });
    });
  </script>
  @endpush

  @include('Backend.includes.footer')
@endsection
