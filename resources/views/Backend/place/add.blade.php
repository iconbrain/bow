@extends('Backend.layouts.blank')

@push('stylesheets')
<!-- PNotify -->
<link href="{{ asset("css/pnotify.css") }}" rel="stylesheet">
<link href="{{ asset("css/pnotify.buttons.css") }}" rel="stylesheet">
<link href="{{ asset("css/pnotify.nonblock.css") }}" rel="stylesheet">
@endpush

@section('main_container')

<div class="right_col" role="main" style="min-height: 1134px;">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Objek Wisata</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-xs-12" style="margin-top: 20px;">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah<small>Objek Wisata Baru</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="display: block;">
						<br>
						<form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left input_mask" novalidate="" action="/adminpanel/places/add" method="post" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
							@if (count($errors->addPlaceHome) > 0)
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								@foreach ($errors->addPlaceHome->all() as $error)
								<P>{{ $error }}</p>
									@endforeach
								</div>
								@endif 
								@if (Session::has('message'))
								<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
								@endif

								<div class="col-md-6 col-xs-12">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Objek Wisata</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<input type="text" class="form-control" name="name" id="name" required="required" value="{{ Request::old('name') }}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Lokasi
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="lokasi">
												<option>Pilih Lokasi...</option>
												@foreach($locations as $loc)
												<option value="{{ $loc->location_id }}">{{ $loc->location_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori Wisata
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="kategori">
												<option>Pilih Kategori...</option>
												@foreach($categories as $cat)
												<option value="{{ $cat->category_id }}">{{ $cat->category_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Range Harga
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="range_harga">
												<option>Pilih Range HTM...</option>
												@foreach($rangeHarga as $rh)
												<option value="{{ $rh->id }}">{{ $rh->price_range }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Jam Operasional</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<textarea rows="2" id="operational_hours" name="operational_hours" required="required" class="form-control col-md-7 col-xs-12" value="{{ Request::old('operational_hours') }}"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<textarea rows="2" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12" value="{{ Request::old('price') }}"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Telepon</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<input type="text" class="form-control" name="phone_number" id="phone_number" required="required" value="{{ Request::old('phone_number') }}">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Alamat
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<textarea rows="2" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" value="{{ Request::old('address') }}"></textarea>
										</div>
									</div>
								</div>

								<div class="col-md-6 col-xs-12">
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="rating">Rating</label>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="text" class="form-control has-feedback-left" id="facility" name="facility" placeholder="Fasilitas" required="required" value="{{ Request::old('facility') }}">
											<span class="fa fa-trophy form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="text" class="form-control" id="cleanliness" name="cleanliness" placeholder="Kebersihan" required="required" value="{{ Request::old('cleanliness') }}">
											<span class="fa fa-shield form-control-feedback right" aria-hidden="true"></span>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="text" class="form-control has-feedback-left" id="comfort" name="comfort" placeholder="Kenyamanan" required="required" value="{{ Request::old('comfort') }}">
											<span class="fa fa-heart form-control-feedback left" aria-hidden="true"></span>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="text" class="form-control" id="service" name="service" placeholder="Pelayanan" required="required" value="{{ Request::old('service') }}">
											<span class="fa fa-gears form-control-feedback right" aria-hidden="true"></span>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
											<input type="text" class="form-control has-feedback-left" id="price_criteria" name="price_criteria" placeholder="Harga" required="required" value="{{ Request::old('price_criteria') }}">
											<span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Gambar
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<input type="file" id="image" name="image" required="required" class="form-control col-md-7 col-xs-12" value="{{ Request::old('image') }}"></input>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="video">Video
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<input type="text" id="video_link" name="video_link" class="form-control col-md-7 col-xs-12" value="https://www.youtube.com/embed/SOr8WSfeti0"></input>
										</div>
									</div>
								</div>

								<div class="col-md-12 col-xs-12">
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="description">Deskripsi
										</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<div class="x_content">
												<div id="alerts"></div>
												<div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#descriptionEdit">
													<div class="btn-group">
														<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
														<ul class="dropdown-menu">
															<li>
																<a data-edit="fontSize 5">
																	<p style="font-size:17px">Huge</p>
																</a>
															</li>
															<li>
																<a data-edit="fontSize 3">
																	<p style="font-size:14px">Normal</p>
																</a>
															</li>
															<li>
																<a data-edit="fontSize 1">
																	<p style="font-size:11px">Small</p>
																</a>
															</li>
														</ul>
													</div>

													<div class="btn-group">
														<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
														<a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
														<a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
														<a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
													</div>

													<div class="btn-group">
														<a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
														<a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
														<a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
														<a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
													</div>

													<div class="btn-group">
														<a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
														<a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
														<a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
														<a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
													</div>
													
													<div class="btn-group">
														<a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
														<a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
													</div>
												</div>
												
												<textarea name="description" id="description" style="display:none;">{{ Request::old('description') }}</textarea>
												<div id="descriptionEdit" name="descriptionEdit" class="editor-wrapper placeholderText" contenteditable="true" required="required"></div>
											</div>
										</div>
									</div>
									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
											<button class="btn btn-danger" type="reset">Reset</button>
											<button type="submit" class="btn btn-primary" id="submitHome">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>

	@push('scripts')
	<!-- jQuery -->
	<script async="" src="https://www.google-analytics.com/analytics.js"></script>

	<!-- FastClick -->
	<script src="{{ asset("js/fastclick.js") }}"></script>

	<!-- NProgress -->
	<script src="{{ asset("js/nprogress.js") }}"></script>

	<!-- validator -->
	<script src="{{ asset("js/validator.js") }}"></script>

	<!-- bootstrap-progressbar -->
	<script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

	<!-- iCheck -->
	<script src="{{ asset("js/icheck.min.js") }}"></script>

	<!-- bootstrap-daterangepicker -->
	<script src="{{ asset("js/moment.min.js") }}"></script>
	<script src="{{ asset("js/daterangepicker.js") }}"></script>

	<!-- bootstrap-wysiwyg -->
	<script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
	<script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
	<script src="{{ asset("js/prettify.js") }}"></script>

	<!-- jQuery tags input -->
	<script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

	<!-- Select2 -->
	<script src="{{ asset("js/select2.full.min.js") }}"></script>

	<!-- Parsley -->
	<script src="{{ asset("js/parsley.min.js") }}"></script>

	<!-- Autosize -->
	<script src="{{ asset("js/autosize.min.js") }}"></script>

	<!-- jQuery autocomplete -->
	<script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

	<!-- starrr -->
	<script src="{{ asset("js/starrr.js") }}"></script>

	<script type="text/javascript">
		$(document).ready(function(){
	      $('#submitHome').click(function(){
	        $('#description').val($('#descriptionEdit').html())
	      });
	    });
	</script>
	@endpush

	@include('Backend.includes.footer')
	@endsection
