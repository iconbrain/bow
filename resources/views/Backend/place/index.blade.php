@extends('Backend.layouts.blank')

@push('stylesheets')
  <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Objek Wisata</h3>
      </div>

      <div class="title_right">
        <div class="col-md-2 col-sm-2 col-xs-22 form-group pull-right">
          <div class="input-group">
            <a class="btn btn-info" href="places/add"><i class="fa fa-plus-circle"></i>&nbsp;Tambah</a>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      @if (Session::has('message'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
      @endif
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Objek Wisata <small>BOW</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="objekWisata" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
          <thead>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog" style="color: white;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#F92A00">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-trash"></i>&nbsp;&nbsp;Anda yakin ingin menghapus objek wisata ini?</h4>
            </div>
            <div class="modal-footer">
              <form id="delete_form" method="post" action="/adminpanel/places/delete/place">
                  {{ csrf_field() }}
                  <input type="button" class="btn btn-danger pull-right" value="Ya, Hapus Sekarang" style="background-color:#F92A00" id="submitDelete" data-dismiss="modal"></input>
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
              </form>
            </div>
        </div>
    </div>
  </div>
  
  <!-- SCRIPT -->
  @push('scripts')
  <script>
    var table;
    function deletePlaces(name, place_id) 
    {
      $('#delete_modal').modal('show');
      document.getElementById('submitDelete').onclick = function() {
        $.ajax({
        type: "POST",
        url: "{{ URL::to('adminpanel/places/delete/place') }}",
        data: {place_id:place_id, name: name, _token:"<?php echo csrf_token(); ?>"},
        success:
        function(success)
        {
          if(success)
          {
            table.draw(false);
            // $('#delete_modal').modal('hide');
            // alert('Data has been deleted');
          }
          else alert('Failed');
        }
        })
      };
    }

    function deletePlace() 
    {
      $.ajax({
        type: "POST",
        url: "{{ URL::to('adminpanel/places/delete/place') }}",
        data: {place_id:place_id, name: name, _token:"<?php echo csrf_token(); ?>"},
        success:
        function(success)
        {
          if(success)
          {
            table.draw(false);
            // $('#delete_modal').modal('hide');
            // alert('Data has been deleted');
          }
          else alert('Failed');
        }
      })
    }

    $(function() {
      table = $('#objekWisata').DataTable({
        processing: false,
        serverSide: true,
        ajax: '{!! route('adminpanel.places') !!}',

        columns: [
          { data: 'place_id', name: 'place_id', title:'ID', "width": "1%" },
          { data: 'name', name: 'name', title:'Nama', "width": "15%" },
          { data: 'address', name: 'address', title:'Alamat', "width": "15%" , render: function(data, type, row) {
            function getExcerpt($str, $startPos=0, $maxLength=100) {
              if(($str).length > $maxLength) {
                $excerpt   = ($str).substring($startPos, $maxLength-3);
                $lastSpace = ($str).lastIndexOf(" ");
                $excerpt   = ($excerpt).substring(0, $lastSpace);
                $excerpt  += '...';
              } else {
                $excerpt = $str;
              }
              
              return $excerpt;
            }

            return getExcerpt(row.address, 0, 20);
          } },
          { data: 'description', name: 'description', title:'Deskripsi', "width": "5%" , render: function(data, type, row) {
            function getExcerpt($str, $startPos=0, $maxLength=100) {
              if(($str).length > $maxLength) {
                $excerpt   = ($str).substring($startPos, $maxLength-3);
                $lastSpace = ($str).lastIndexOf(" ");
                $excerpt   = ($excerpt).substring(0, $lastSpace);
                $excerpt  += '...';
              } else {
                $excerpt = $str;
              }
              
              return $excerpt;
            }

            return getExcerpt(row.description, 0, 30);
          } },
          // { data: 'operational_hours', name: 'operational_hours', title:'Jam', "width": "10%" },

          { data: 'operational_hours', name: 'operational_hours', title:'Jam', "width": "10%", render: function(data, type, row) {
            function getExcerpt($str, $startPos=0, $maxLength=100) {
              if(($str).length > $maxLength) {
                $excerpt   = ($str).substring($startPos, $maxLength-3);
                $lastSpace = ($str).lastIndexOf(" ");
                $excerpt   = ($excerpt).substring(0, $lastSpace);
                $excerpt  += '...';
              } else {
                $excerpt = $str;
              }
              
              return $excerpt;
            }
            return getExcerpt(row.operational_hours, 0, 40);
          } },
          { data: 'phone_number', name: 'phone_number', title:'Telepon', "width": "5%" , render: function(data, type, row) {
            if(row.phone_number) return row.phone_number;
            else return '-';
          } },
          { data: 'image', name: 'image', title:'Gambar', "width": "5%" , render: function(data, type, row) {
            var str = row.image;
            if(str.indexOf('https://') !== -1)
                return '<img src="'+row.image+'" alt="" width="100px">';
            else
                return '<img src="{{ URL::to('/') }}/images/'+row.image+'" alt="" width="100px">';
          } },
          { data: 'video_link', name: 'video_link', title:'Video', "width": "5%" , render: function(data, type, row) {
            if(row.video_link) return '<iframe id="video1" width="100" height="75" src="'+row.video_link+'" frameborder="0" allowtransparency="true" allowfullscreen></iframe>';
            else return '-';
          } },
          { className: "dt-center", width:"12%", name: 'actions', title: 'Aksi', render: function(data, type, row) {
            var data = "`" + row.name + "`";
            return '<a class="btn btn_warning btn_action" href=places/edit/'+row.place_id+'>' + '<i class="fa fa-edit"></i> Ubah' + '</a> &nbsp;' +
                   '<a class="btn btn_danger btn_action" onclick="deletePlaces(' + data + ', ' + row.place_id + ')" >' + '<i class="fa fa-trash"></i> Hapus' + '</a>';
          } }
        ]
      });
    });
  </script>
  @endpush

  @include('Backend.includes.footer')
@endsection
