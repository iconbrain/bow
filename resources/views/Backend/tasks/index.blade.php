@extends('Backend.layouts.blank')

@push('stylesheets')
  <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Daftar Tugas</h3>
      </div>

      @if(count($tasks) < 9)
        <div class="title_right" style="margin-bottom: -20px;">
          <div class="col-md-2 col-sm-2 col-xs-22 form-group pull-right">
            <div class="input-group">
              <a class="btn btn-info" href="tasks/add"><i class="fa fa-plus-circle"></i>&nbsp;Tambah</a>
            </div>
          </div>
        </div>
      @elseif(count($tasks) >= 9)
      @endif
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      @if (Session::has('message'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
      @endif
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar Tugas <small>BOW</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="task" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
          <thead>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog" style="color: white">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#F92A00">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-trash"></i>&nbsp;&nbsp;Anda yakin ingin menghapus tugas ini?</h4>
            </div>
            <div class="modal-footer">
              <form id="delete_form" method="post" action="/adminpanel/tasks/delete/task">
                  {{ csrf_field() }}
                  <input type="button" class="btn btn-danger pull-right" value="Ya, Hapus Sekarang" style="background-color:#F92A00" id="submitDelete" data-dismiss="modal"></input>
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
              </form>
            </div>
        </div>
    </div>
  </div>
  
  <!-- SCRIPT -->
  @push('scripts')
  <script>
    var table;
    function deleteTasks(task_name, id) 
    {
      $('#delete_modal').modal('show');
      document.getElementById('submitDelete').onclick = function() {
        $.ajax({
        type: "POST",
        url: "{{ URL::to('adminpanel/tasks/delete/task') }}",
        data: {id:id, task_name: task_name, _token:"<?php echo csrf_token(); ?>"},
        success:
        function(success)
        {
          if(success)
          {
            table.draw(false);
            // $('#delete_modal').modal('hide');
            // alert('Data has been deleted');
          }
          else alert('Failed');
        }
        })
      };
    }

    function deleteTask() 
    {
      $.ajax({
        type: "POST",
        url: "{{ URL::to('adminpanel/tasks/delete/task') }}",
        data: {place_id:place_id, name: name, _token:"<?php echo csrf_token(); ?>"},
        success:
        function(success)
        {
          if(success)
          {
            table.draw(false);
            // $('#delete_modal').modal('hide');
            // alert('Data has been deleted');
          }
          else alert('Failed');
        }
      })
    }

    $(function() {
      table = $('#task').DataTable({
        processing: false,
        serverSide: true,
        ajax: '{!! route('adminpanel.tasks') !!}',
        aaSorting: [[2,"asc"]],

        columns: [
          { data: 'id', name: 'id', title:'ID', "width": "1%" },
          { data: 'task_name', name: 'task_name', title:'Nama Tugas', "width": "30%" },
          { data: 'ordering', name: 'ordering', title:'Urutan', "width": "1%" },
          { className: "dt-center", width:"10%", name: 'actions', title: 'Aksi', render: function(data, type, row) {
            var data = "`" + row.name + "`";
            return '<a class="btn btn_warning btn_action" href=tasks/edit/'+row.id+'>' + '<i class="fa fa-edit"></i> Ubah' + '</a> &nbsp;'  +
                   '<a class="btn btn_danger btn_action" onclick="deleteTasks(' + data + ', ' + row.id + ')" >' + '<i class="fa fa-trash"></i> Hapus' + '</a>';
          } }
        ]
      });
    });
  </script>
  @endpush

  @include('Backend.includes.footer')
@endsection
