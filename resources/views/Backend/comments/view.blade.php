@extends('Backend.layouts.blank')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset("css/custom.css")}}"/>
@endpush

@section('main_container')

<div class="right_col" role="main">
  <div class="page-title">
    <div class="title_left">
      <h3>Komentar</h3>
    </div>
  </div>

  @foreach($comments as $c)
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      <div class="x_panel">
        <div class="x_title">
          <h2>Komentar Pengguna <small>{{ $c->placename }}</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <div class="profile_img">
              <div id="crop-avatar">
                <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/images/{{ $c->avatar }}" alt="Avatar" style="border-radius: 6px;">
              </div>
            </div>
            <h3>{{ $c->username }}</h3>

            <ul class="list-unstyled user_data">
              <li><i class="fa fa-envelope user-profile-icon"></i> {{ $c->email }}
              </li>
            </ul>

          </div>
          <div class="col-md-9 col-sm-9 col-xs-12">

            <div class="profile_title">
              <div class="col-md-6">
                <h2>Isi Komentar</h2>
              </div>
            </div>

            <div class="col-md-12">
              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Pada Post
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! $c->title !!}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Komentar
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! $c->body !!}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Tanggal Komentar
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $c->created_at }}
                </div>
              </div>
            </div>
            
          </div>

        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

@include('Backend.includes.footer')
@endsection
