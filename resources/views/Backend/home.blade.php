@extends('Backend.layouts.blank')

@push('stylesheets')
<!-- JQVMap -->
<link href="{{ asset("css/jqvmap.min.css") }}" rel="stylesheet"/>
<!-- iCheck -->
<link href="{{ asset("css/green.css") }}" rel="stylesheet"/>
@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="row top_tiles">
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-location-arrow" style="color: #08d1c0"></i></div>
					<div class="count">{{ $totalObjekWisata }}</div>
					<h3>Objek Wisata</h3>
					<p>Objek wisata yang tersedia.</p>
				</div>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-edit" style="color: #06ad3e"></i></div>
					<div class="count">{{ $totalReview }}</div>
					<h3>Ulasan</h3>
					<p>Ulasan pada objek wisata yang ada.</p>
				</div>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-user" style="color: #178ced"></i></div>
					<div class="count">{{ $totalUser }}</div>
					<h3>Pengguna</h3>
					<p>Total pengguna Bandung Objek Wisata.</p>
				</div>
			</div>
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-comments-o" style="color: orange"></i></div>
					<div class="count">{{ $totalKomentar }}</div>
					<h3>Komentar</h3>
					<p>Komentar pada ulasan yang diberikan.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Peta Lokasi<small>Dunia</small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="dashboard-widget-content">
									<div class="col-md-4 hidden-small">
										<h2 class="line_30">Objek wisata berdasarkan lokasi.</h2>

										<table class="countries_list">
											<tbody>
												<tr>
													<td>Bandung Selatan</td>
													<td class="fs15 fw700 text-right">{{ $tbSelatan }}</td>
												</tr>
												<tr>
													<td>Bandung Barat</td>
													<td class="fs15 fw700 text-right">{{ $tbBarat }}</td>
												</tr>
												<tr>
													<td>Bandung Utara</td>
													<td class="fs15 fw700 text-right">{{ $tbUtara }}</td>
												</tr>
												<tr>
													<td>Bandung Kota</td>
													<td class="fs15 fw700 text-right">{{ $tbKota }}</td>
												</tr>
												<tr>
													<td>Bandung Timur</td>
													<td class="fs15 fw700 text-right">{{ $tbTimur }}</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="col-md-3">
				<div class="x_panel">
					<div class="x_title">
						<h2>Pengguna</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					@if($sumNewSignUp->sum == 1)
					<ul class="list-unstyled">
						<li class="media event">
							<a class="pull-left border-aero profile_thumb">
								<i class="fa fa-user aero"></i>
							</a>
							<div class="media-body">
								<a class="title" href="#">{{ $newSignUp[0]->name }}</a>
								<p>{{ $newSignUp[0]->email }}</p>
								<p> <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newSignUp[0]->created_at)->format('M d, Y \a\t h:i a') }}</small>
								</p>
							</div>
						</li>
					</ul>
					@elseif($sumNewSignUp->sum == 2)
					<ul class="list-unstyled">
						<li class="media event">
							<a class="pull-left border-aero profile_thumb">
								<i class="fa fa-user aero"></i>
							</a>
							<div class="media-body">
								<a class="title" href="#">{{ $newSignUp[0]->name }}</a>
								<p>{{ $newSignUp[0]->email }}</p>
								<p> <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newSignUp[0]->created_at)->format('M d, Y \a\t h:i a') }}</small>
								</p>
							</div>
						</li>
						<li class="media event">
							<a class="pull-left border-green profile_thumb">
								<i class="fa fa-user green"></i>
							</a>
							<div class="media-body">
								<a class="title" href="#">{{ $newSignUp[1]->name }}</a>
								<p>{{ $newSignUp[1]->email }}</p>
								<p> <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newSignUp[1]->created_at)->format('M d, Y \a\t h:i a') }}</small>
								</p>
							</div>
						</li>
					</ul>
					@elseif($sumNewSignUp->sum >= 3)
					<ul class="list-unstyled">
						<li class="media event">
							<a class="pull-left border-aero profile_thumb">
								<i class="fa fa-user aero"></i>
							</a>
							<div class="media-body">
								<a class="title" href="#">{{ $newSignUp[0]->name }}</a>
								<p>{{ $newSignUp[0]->email }}</p>
								<p> <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newSignUp[0]->created_at)->format('M d, Y \a\t h:i a') }}</small>
								</p>
							</div>
						</li>
						<li class="media event">
							<a class="pull-left border-green profile_thumb">
								<i class="fa fa-user green"></i>
							</a>
							<div class="media-body">
								<a class="title" href="#">{{ $newSignUp[1]->name }}</a>
								<p>{{ $newSignUp[1]->email }}</p>
								<p> <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newSignUp[1]->created_at)->format('M d, Y \a\t h:i a') }}</small>
								</p>
							</div>
						</li>
						<li class="media event">
							<a class="pull-left border-blue profile_thumb">
								<i class="fa fa-user blue"></i>
							</a>
							<div class="media-body">
								<a class="title" href="#">{{ $newSignUp[2]->name }}</a>
								<p>{{ $newSignUp[2]->email }}</p>
								<p> <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newSignUp[2]->created_at)->format('M d, Y \a\t h:i a') }}</small>
								</p>
							</div>
						</li>
					</ul>
					@endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="x_panel">
					<div class="x_title">
						<h2>Ulasan Terkini<small>BOW</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@foreach($recentReview as $key => $rr)
						<article class="media event">
							<?php
							$datetime = new \DateTime($rr->rcreatedat);
							$day = $datetime->format('d');
							$month = $datetime->format('M');
							?>
							@if($key %2 == 1)
							<a class="pull-left date" style="background-color: teal">
								<p class="month">{{ $month }}</p>
								<p class="day">{{ $day }}</p>
							</a>
							@else
							<a class="pull-left date" style="background-color: grey">
								<p class="month">{{ $month }}</p>
								<p class="day">{{ $day }}</p>
							</a>
							@endif
							<div class="media-body">
								<a class="title" href="#">{{ $rr->name }}</a>
								<p><?php $excerpt = getExcerpt($rr->body, 0, 60); ?>
									{!! $excerpt !!}
								</p>
							</div>
						</article>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="x_panel">
					<div class="x_title">
						<h2>Komentar Terkini<small>BOW</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@foreach($recentComment as $key => $rc)
						<article class="media event">
							<?php
							$datetime = new \DateTime($rc->ccreatedat);
							$day = $datetime->format('d');
							$month = $datetime->format('M');
							?>
							@if($key %2 == 1)
							<a class="pull-left date" style="background-color: red">
								<p class="month">{{ $month }}</p>
								<p class="day">{{ $day }}</p>
							</a>
							@else
							<a class="pull-left date" style="background-color: green">
								<p class="month">{{ $month }}</p>
								<p class="day">{{ $day }}</p>
							</a>
							@endif
							<div class="media-body">
								<a class="title" href="#">{{ $rc->name }}</a>
								<p><?php $excerpt = getExcerpt($rc->body, 0, 60); ?>
									{!! $excerpt !!}
								</p>
							</div>
						</article>
						@endforeach
					</div>
				</div>
			</div>

			<!-- Start to do list -->
            <div class="col-md-4">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Daftar Tugas<small>BOW</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/adminpanel/tasks') }}">Pengaturan</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="">
                    <ul class="to_do">
                      @foreach($tasks as $t)
                      <li>
                        <p>
                          <input type="checkbox" class="flat" checked="true"> {{ $t->task_name }} </p>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- End to do list -->
		</div>
	</div>
</div>
<!-- /page content -->

@push('scripts')
<!-- jQuery -->
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<!-- FastClick -->
<script src="{{ asset("js/fastclick.js") }}"></script>
<!-- NProgress -->
<script src="{{ asset("js/nprogress.js") }}"></script>
<!-- Chart.js -->
<script src="{{ asset("js/Chart.min.js") }}"></script>
<!-- jQuery Sparklines -->
<script src="{{ asset("js/jquery.sparkline.min.js") }}"></script>
<!-- Flot -->
<script src="{{ asset("js/jquery.flot.js") }}"></script>
<script src="{{ asset("js/jquery.flot.pie.js") }}"></script>
<script src="{{ asset("js/jquery.flot.time.js") }}"></script>
<script src="{{ asset("js/jquery.flot.stack.js") }}"></script>
<script src="{{ asset("js/jquery.flot.resize.js") }}"></script>
<!-- Flot plugins -->
<script src="{{ asset("js/jquery.flot.orderBars.js") }}"></script>
<script src="{{ asset("js/jquery.flot.spline.min.js") }}"></script>
<script src="{{ asset("js/curvedLines.js") }}"></script>
<!-- DateJS -->
<script src="{{ asset("js/date.js") }}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{ asset("js/moment.min.js") }}"></script>
<script src="{{ asset("js/daterangepicker.js") }}"></script>
<!-- JQVMap -->
<script src="{{ asset("js/jquery.vmap.js") }}"></script>
<script src="{{ asset("js/jquery.vmap.world.js") }}"></script>
<script src="{{ asset("js/jquery.vmap.sampledata.js") }}"></script>
<!-- iCheck -->
<script src="{{ asset("js/icheck.min.js") }}"></script>
@endpush

@include('Backend.includes.footer')
@endsection

<?php
function getExcerpt($str, $startPos=0, $maxLength=100) {
	if(strlen($str) > $maxLength) {
		$excerpt   = substr($str, $startPos, $maxLength-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '...';
	} else {
		$excerpt = $str;
	}

	return $excerpt;
}
?>