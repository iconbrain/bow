@extends('Backend.layouts.blank')

@push('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
	  	<div class="page-title">
		    <div class="title_left">
		      <h3>Rating</h3>
		    </div>
	  	</div>

	    <div class="x_panel" style="margin-top: 20px;">
	        <div class="x_title">
	          <h2>Rating<small>Objek Wisata</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	          <table id="rating" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
	            </thead>
	            <tbody>
	            </tbody>
	          </table>

	        </div>
	    </div>
    </div>
    <!-- /page content -->

    <!-- SCRIPT -->
	@push('scripts')
	<script>
    	var table;
		$(function() {
	      table = $('#rating').DataTable({
	        processing: false,
	        serverSide: true,
	        ajax: '{!! route('ahptopsis.all') !!}',

	        columns: [
	          { data: 'place_id', name: 'place_id', title:'ID', "width": "1%" },
	          { data: 'name', name: 'name', title:'Nama', "width": "15%" },
	          { data: 'facility_attr', name: 'facility_attr', title:'Fasilitas', "width": "15%" },
	          { data: 'cleanliness_attr', name: 'cleanliness_attr', title:'Kebersihan', "width": "15%" },
	          { data: 'comfort_attr', name: 'comfort_attr', title:'Kenyamanan', "width": "15%" },
	          { data: 'service_attr', name: 'service_attr', title:'Pelayanan', "width": "15%" },
	          { data: 'price_attr', name: 'price_attr', title:'Harga', "width": "15%" }
	        ]
	      });
	    });
	</script>
  	@endpush

    @include('Backend.includes.footer')
@endsection