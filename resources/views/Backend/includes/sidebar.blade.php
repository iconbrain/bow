<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/adminpanel/home') }}" class="site_title"><i class="fa fa-dashboard"></i> <span>BOW</span></a>
        </div>
        
        <div class="clearfix"></div>
        
        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="Avatar of {{ Auth::user()->name }}" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>{{ Auth::user()->name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        
        <br />
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Umum</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{{ url('/adminpanel/home') }}}">
                            <i class="fa fa-home"></i>
                            Beranda
                        </a>
                    </li>
                    <li><a><i class="fa fa-building-o"></i> Wisata <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{{ url('/adminpanel/categories') }}}">Kategori Wisata</a></li>
                            <li><a href="{{{ url('/adminpanel/locations') }}}">Lokasi Wisata</a></li>
                            <li><a href="{{{ url('/adminpanel/places') }}}">Objek Wisata</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{{ url('/adminpanel/rating') }}}">
                            <i class="fa fa-star"></i>
                            Penilaian
                        </a>
                    </li>
                    <li>
                        <a href="{{{ url('/adminpanel/reviews') }}}">
                            <i class="fa fa-edit"></i>
                            Ulasan
                        </a>
                    </li>
                    <li>
                        <a href="{{{ url('/adminpanel/comments') }}}">
                            <i class="fa fa-comments"></i>
                            Komentar
                        </a>
                    </li>
                    <li>
                        <a href="{{{ url('/adminpanel/users') }}}">
                            <i class="fa fa-users"></i>
                            Pengguna
                        </a>
                    </li>
                    <li>
                        <a href="{{{ url('/adminpanel/request') }}}">
                            <i class="fa fa-file-text"></i>
                            Permintaan
                            @if($notifSum->totalNotif > 0)
                                <span class="label label-success pull-right">New</span>
                            @endif
                        </a>
                    </li>
                </ul>
            </div>
            <div class="menu_section">
                <h3>Tambahan</h3>
                <ul class="nav side-menu">
                    <li>
                        <a><i class="fa fa-sitemap"></i> Algoritma <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{{ url('/adminpanel/ahp') }}}">AHP</a>
                                <!-- <li>
                                    <a>AHP + TOPSIS<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li class="sub_menu"><a href="{{{ url('/adminpanel/ahp-topsis/wisata-alam') }}}">Wisata Alam</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/wisata-religi') }}}">Wisata Religi</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/wisata-sejarah') }}}">Wisata Sejarah</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/wisata-belanja') }}}">Wisata Belanja</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/wisata-satwa') }}}">Wisata Satwa</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/wisata-kuliner') }}}">Wisata Kuliner</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/agrowisata') }}}">Agrowisata</a></li>
                                        <li><a href="{{{ url('/adminpanel/ahp-topsis/wisata-keluarga') }}}">Wisata Keluarga</a></li>
                                    </ul>
                                </li> -->
                            <li>
                                <a href="{{{ url('/adminpanel/ahp-topsis/all') }}}">AHP + TOPSIS</a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a href="{{{ url('/adminpanel/request-api') }}}">
                            <i class="fa fa-wpforms"></i>
                            Request API
                        </a>
                    </li> -->
                </ul>
            </div>
        
        </div>
        <!-- /sidebar menu -->
        
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('adminpanel/logout') }}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>