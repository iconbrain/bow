<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
        </nav>
            
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="Avatar of {{ Auth::user()->name }}">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{{ url('/adminpanel/home') }}}"> Beranda</a></li>
                        <li><a href="{{{ url('/adminpanel/profile') }}}"><i class="fa fa-user pull-right"></i> Profil</a></li>
                        <li><a href="{{ url('/adminpanel/logout') }}"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                    </ul>
                </li>
                
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-green">{{ $notifSum->totalNotif }}</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        @foreach($notif as $n)
                        <li>
                            <a href="{{ URL::to('adminpanel/request/view/') }}/{{ $n->id }}">
                                <span class="image"><img src="{{ URL::to('/') }}/images/{{ $n->avatar }}" alt="Profile Image" /></span>
                                <span>
                                  <span>{{ $n->username }}</span>
                                  <span class="time">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $n->created_at)->format('d M \a\t h:i a') }}</span>
                                </span>
                                <span class="message">
                                    <?php $rest = substr($n->description, 0, 50); ?>
                                    {{ $rest . '...' }}
                                </span>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <div class="text-center">
                                <a href="{{ URL::to('adminpanel/request') }}">
                                    <strong>Semua Notifikasi</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->