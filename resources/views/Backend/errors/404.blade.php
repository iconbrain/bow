<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>404 | BOW</title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">
    <link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
</head>

<body class="nav-md" style="background-color: #F1F1F1;">
<div class="container body">
    <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h1 class="error-number" style="color: black; font-weight: 100;">404</h1>
                    <br><br>
                    <h2 style="color: grey; font-weight: bold;">Halaman tidak ditemukan.</h2>
                    <br>
                    <div class="animated rubberBand">
                        <p style="color: grey; font-size: 16px; width: 600px; margin: 0 auto;">Kami tidak dapat melanjutkan permintaan ini, karena halaman yang Anda cari tidak ditemukan. Silahkan kembali ke <a id="back-home" href="{{ URL::to('/') }}" style="color: #2b6aa0;">halaman utama</a>.</p>
                    </div><br>
                    <p class="mid_center" style="color: grey; font-size: 14px;">Bandung Objek Wisata - BOW</p><br>
                    <img src="{{ URL::to('/') }}/images/logo_404.png"></div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<style type="text/css">
    #back-home:hover {
        color: white;
    }
</style>

<!-- jQuery -->
<script src="{{ asset("js/jquery.min.js") }}"></script>
<!-- Bootstrap -->
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
<!-- Custom Theme Scripts -->
<script src="{{ asset("js/gentelella.min.js") }}"></script>

</body>
</html>