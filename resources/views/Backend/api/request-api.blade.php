@extends('Backend.layouts.blank')

@push('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
	  	<div class="page-title">
		    <div class="title_left">
		      <h3>Request API</h3>
		    </div>
	  	</div>

	  	<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px; margin-bottom: -25px;">
		  	@if (count($errors->addAPI) > 0)
				<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				@foreach ($errors->addAPI->all() as $error)
				<P>{{ $error }}</p>
				@endforeach
				</div>
			@endif 

		    @if (Session::has('message'))
		        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
		    @endif
	    </div>

	  	<div class="row">
	      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
	        <div class="x_panel">
	          <div class="x_title">
	            <h2>Request <small>Data API</small></h2>
	            <ul class="nav navbar-right panel_toolbox">
	              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	              </li>
	              <li><a><i class="fa fa-wrench"></i></a></li>
	              <li><a class="close-link"><i class="fa fa-close"></i></a>
	              </li>
	            </ul>
	            <div class="clearfix"></div>
	          </div>
	          <div class="x_content">

	            <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/request-api-post" method="POST">
	              	{{ csrf_field() }}

	              	<p>Silahkan masukkan <code>data API</code> yang valid.</p>

					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea rows="5" name="api" required="required" class="form-control col-md-7 col-xs-12" value="{{ Request::old('api') }}"></textarea>
						</div>
					</div>
		  			<div class="form-group">
		                <div class="col-md-6">
		                  <button type="submit" class="btn btn-primary">Submit</button>
		                </div>
		             </div>
	            </form>
	          </div>
	        </div>
	      </div>
	    </div>
    </div>
    <!-- /page content -->

    @include('Backend.includes.footer')
@endsection