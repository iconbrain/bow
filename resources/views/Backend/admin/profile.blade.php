@extends('Backend.layouts.blank')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset("css/custom.css")}}"/>
@endpush

@section('main_container')

<div class="right_col" role="main">
  <div class="page-title">
    <div class="title_left">
      <h3>Admin</h3>
    </div>
  </div>

  @if (Session::has('message'))
    <div class="page-title" style="margin-top:40px; margin-bottom:5px;">
      <div class="alert alert-success"><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
    </div>
  @endif

  @foreach($admin as $ad)
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ubah Admin <small>Bandung Objek Wisata</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <div class="profile_img">
              <div id="crop-avatar">
                <!-- Current avatar -->
                <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/images/{{ $ad->avatar }}" alt="Avatar" title="Change the avatar"></img>
              </div>
            </div>
          </div>
          <div class="col-md-9 col-sm-9 col-xs-12">  
            <div class="row">

              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel">
                  <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <h4 class="panel-title">Profil</h4>
                  </a>
                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
                    <div class="panel-body">

                      <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/profile/admin/edit" method="post">
                        {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                        @if (count($errors->editAdmin) > 0)
                        <div class="alert alert-danger">
                          @foreach ($errors->editAdmin->all() as $error)
                          <p>{{ $error }}</p>
                          @endforeach
                        </div>
                        @endif 

                        @foreach($roles as $r)
                        <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Status</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="category_id" class="form-control col-md-8 col-xs-12" type="text" value="{{ $r->display_name }}" readonly>
                          </div>
                        </div>
                        @endforeach
                        <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Diperbaharui</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="category_id" class="form-control col-md-8 col-xs-12" type="text" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ad->updated_at)->format('M d, Y \a\t h:i a') }}" readonly>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Nama</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="name" class="form-control col-md-7 col-xs-12" type="text" value="{{ $ad->name }}" required="required" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Email</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="email" class="form-control col-md-7 col-xs-12" type="email" value="{{ $ad->email }}" required="required" >
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-2">
                            <button class="btn btn-danger" type="reset">Reset</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
                <div class="panel">
                  <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <h4 class="panel-title">Kata Sandi</h4>
                  </a>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                    <div class="panel-body">
                      
                      <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/password/admin/edit" method="post">
                        {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                        @if (count($errors->editAdminPassword) > 0)
                        <div class="alert alert-danger">
                          @foreach ($errors->editAdminPassword->all() as $error)
                          <p>{{ $error }}</p>
                          @endforeach
                        </div>
                        @endif

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kata Sandi Lama</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="old_password" class="form-control col-md-7 col-xs-12" type="password" required="required" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kata Sandi Baru</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="new_password" class="form-control col-md-7 col-xs-12" type="password" required="required" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Konfirmasi Kata Sandi Baru</label>
                          <div class="col-md-7 col-sm-7 col-xs-12">
                            <input name="new_password_confirm" class="form-control col-md-7 col-xs-12" type="password" required="required">
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                            <button class="btn btn-danger" type="reset">Reset</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

<!-- SCRIPT -->
@push('scripts')
<!-- jQuery -->
<script async="" src="https://www.google-analytics.com/analytics.js"></script>

<!-- FastClick -->
<script src="{{ asset("js/fastclick.js") }}"></script>

<!-- NProgress -->
<script src="{{ asset("js/nprogress.js") }}"></script>

<!-- validator -->
<script src="{{ asset("js/validator.js") }}"></script>

<!-- bootstrap-progressbar -->
<script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

<!-- iCheck -->
<script src="{{ asset("js/icheck.min.js") }}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset("js/moment.min.js") }}"></script>
<script src="{{ asset("js/daterangepicker.js") }}"></script>

<!-- bootstrap-wysiwyg -->
<script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
<script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
<script src="{{ asset("js/prettify.js") }}"></script>

<!-- jQuery tags input -->
<script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

<!-- Switchery -->
<script src="{{ asset("js/switchery.min.js") }}"></script>

<!-- Select2 -->
<script src="{{ asset("js/select2.full.min.js") }}"></script>

<!-- Autosize -->
<script src="{{ asset("js/autosize.min.js") }}"></script>

<!-- jQuery autocomplete -->
<script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

<!-- starrr -->
<script src="{{ asset("js/starrr.js") }}"></script>

<!-- Parsley -->
<script src="{{ asset("js/parsley.min.js") }}"></script>

<!-- PNotify -->
<script src="{{ asset("js/pnotify.js") }}"></script>
<script src="{{ asset("js/pnotify.buttons.js") }}"></script>
<script src="{{ asset("js/pnotify.nonblock.js") }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
      localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
      $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
  });
</script>
@endpush

@include('Backend.includes.footer')
@endsection
