@extends('Backend.layouts.blank')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset("css/custom.css")}}"/>
@endpush

@section('main_container')

<div class="right_col" role="main">
  <div class="page-title">
    <div class="title_left">
      <h3>Users</h3>
    </div>
  </div>

  @foreach($users as $user)
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      <div class="x_panel">
        <div class="x_title">
          <h2>Laporan Pengguna <small>Laporan Aktivitas</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <div class="profile_img">
              <div id="crop-avatar">
                <!-- Current avatar -->
                <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/images/{{ $user->avatar }}" alt="Avatar" title="Change the avatar">
              </div>
            </div>
            <h3>{{ $user->name }}</h3>

            <ul class="list-unstyled user_data">
              <li><i class="fa fa-envelope user-profile-icon"></i> {{ $user->email }}
              </li>

              <li>
                <i class="fa fa-sign-in user-profile-icon"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('M d, Y \a\t h:i a') }}
              </li>

              @foreach($roles as $r)
              <li class="m-top-xs">
                <i class="fa fa-flag user-profile-icon"></i> {{ $r->display_name }}
              </li>
              @endforeach
            </ul>
            <!-- end of skills -->

          </div>
          <div class="col-md-9 col-sm-9 col-xs-12">

            <div class="profile_title">
              <div class="col-md-6">
                <h2>Laporan Aktivitas User</h2>
              </div>
            </div>
            <!-- start of user-activity-graph -->
            <div class="row user_top">
              @foreach($reviews as $rvw)
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-pencil" style="color:#41eef4"></i></div>
                  <div class="count">{{ $rvw->total_review }}</div>
                  <h3>Review</h3>
                  <p>Pemberian review dan rating pada objek wisata.</p>
                </div>
              </div>
              @endforeach
              @foreach($comments as $cmt)
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-comments-o" style="color:orange"></i></div>
                  <div class="count">{{ $cmt->total_comment }}</div>
                  <h3>Comment</h3>
                  <p>Pemberian komentar pada suatu review.</p>
                </div>
              </div>
              @endforeach
              @foreach($bookmark as $bmk)
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-bookmark" style="color:#55beef"></i></div>
                  <div class="count">{{ $bmk->total_bookmark }}</div>
                  <h3>Bookmark</h3>
                  <p>Bookmark pada objek wisata yang tersedia.</p>
                </div>
              </div>
              @endforeach
            </div>
          <!-- end of user-activity-graph -->

          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
              <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Ulasan Terbaru</a>
              </li>
              <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Komentar Terbaru</a>
              </li>
              <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profil</a>
              </li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                <!-- start latest review -->
                <ul class="messages">
                  @foreach($reviews as $rvw)
                    @if ($rvw->total_review > 0)
                      @foreach($latest_review as $lr)
                      <li>
                        <img src="{{ URL::to('/') }}/images/{{ $user->avatar }}" class="avatar" alt="Avatar">
                        <div class="message_date">
                          <h3 class="date text-info">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lr->rca)->format('d') }}</h3>
                          <p class="month">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lr->rca)->format('M') }}</p>
                        </div>
                        <div class="message_wrapper">
                          <h4 class="heading">{{ $lr->title }}</h4>
                          <blockquote class="message" style="font-size: 14px;">{!! $lr->body !!}</blockquote>
                          <br>
                          <p class="url">
                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                            <i class="fa fa-map-marker"></i> {{ $lr->place_name }}
                          </p>
                        </div>
                      </li>
                      @endforeach
                    @else
                      {{ $user->name }} belum pernah memberikan review.
                    @endif
                  @endforeach

                </ul>
                <!-- end latest review -->

              </div>
              <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                <!-- start latest comment -->
                <ul class="messages">
                  @foreach($comments as $cmt)
                    @if ($cmt->total_comment > 0)
                      @foreach($latest_comment as $lc)
                      <li>
                        <i class="fa fa-reply"></i>
                        <div class="message_date">
                          <h3 class="date text-info">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lc->cca)->format('d') }}</h3>
                          <p class="month">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lc->cca)->format('M') }}</p>
                        </div>
                        <div class="message_wrapper">
                          <h4 class="heading">{{ $lc->title }}</h4>
                          <blockquote class="message" style="font-size: 14px;">{!! $lc->body !!}</blockquote>
                          <br>
                        </div>
                      </li>
                      @endforeach
                    @else
                      {{ $user->name }} belum pernah memberikan comment.
                    @endif
                  @endforeach

                </ul>
                <!-- end latest comment -->

              </div>
              <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                @foreach($reviews as $rvw)
                  @foreach($comments as $cmt)
                    @foreach($roles as $r)
                      <p>{{ $user->name }} mendaftar di Bandung Objek Wisata pada {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d F Y \p\u\k\u\l h:i a') }} sebagai {{ $r->display_name }}. {{ $user->name }} telah memberikan <b>{{ $rvw->total_review }}</b> review, <b>{{ $cmt->total_comment }}</b> komentar dan <b>{{ $bmk->total_bookmark }}</b> bookmark.</p>
                      @if($bmk->total_bookmark > 0)
                        <p>{{ $user->name }} telah mem-bookmark objek wisata:</p>
                        @foreach($bookmarkPlace as $bp)
                          <p>- {{ $bp->name }}</p>
                        @endforeach
                      @endif
                    @endforeach
                  @endforeach
                @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

<!-- SCRIPT -->
@push('scripts')
<!-- jQuery -->
<script async="" src="https://www.google-analytics.com/analytics.js"></script>

<!-- FastClick -->
<script src="{{ asset("js/fastclick.js") }}"></script>

<!-- NProgress -->
<script src="{{ asset("js/nprogress.js") }}"></script>

<!-- validator -->
<script src="{{ asset("js/validator.js") }}"></script>

<!-- bootstrap-progressbar -->
<script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

<!-- iCheck -->
<script src="{{ asset("js/icheck.min.js") }}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset("js/moment.min.js") }}"></script>
<script src="{{ asset("js/daterangepicker.js") }}"></script>

<!-- bootstrap-wysiwyg -->
<script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
<script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
<script src="{{ asset("js/prettify.js") }}"></script>

<!-- jQuery tags input -->
<script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

<!-- Switchery -->
<script src="{{ asset("js/switchery.min.js") }}"></script>

<!-- Select2 -->
<script src="{{ asset("js/select2.full.min.js") }}"></script>

<!-- Autosize -->
<script src="{{ asset("js/autosize.min.js") }}"></script>

<!-- jQuery autocomplete -->
<script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

<!-- starrr -->
<script src="{{ asset("js/starrr.js") }}"></script>

<!-- PNotify -->
<script src="{{ asset("js/pnotify.js") }}"></script>
<script src="{{ asset("js/pnotify.buttons.js") }}"></script>
<script src="{{ asset("js/pnotify.nonblock.js") }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
      localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
      $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
  });
</script>
@endpush

@include('Backend.includes.footer')
@endsection
