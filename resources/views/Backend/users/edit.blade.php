@extends('Backend.layouts.blank')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset("css/custom.css")}}"/>
@endpush

@section('main_container')

<div class="right_col" role="main">
  <div class="page-title">
    <div class="title_left">
      <h3>User</h3>
    </div>
  </div>

  @foreach($users as $user)
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ubah Pengguna <small>BOW</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <div class="profile_img">
              <div id="crop-avatar">
                <!-- Current avatar -->
                <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/images/{{ $user->avatar }}" alt="Avatar" title="Change the avatar"></img>
              </div>
            </div>
          </div>
          <div class="col-md-9 col-sm-9 col-xs-12">  
            <div class="row">
              <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#tab_home" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Keterangan</a></li>
                  <li role="presentation" class=""><a href="#tab_password" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Kata Sandi</a></li>
                </ul>

                <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane fade active in" id="tab_home" aria-labelledby="home-tab">

                    <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/home/users/edit/{{ $user->id }}" method="post">
                      {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                      @if (count($errors->editUser) > 0)
                      <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        @foreach ($errors->editUser->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                      </div>
                      @endif 
                      @if (Session::has('message'))
                      <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
                      @endif

                      <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">ID</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <input name="category_id" class="form-control col-md-8 col-xs-12" type="text" value="{{ $user->id }}" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Diperbaharui</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <input name="category_id" class="form-control col-md-8 col-xs-12" type="text" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->updated_at)->format('M d, Y \a\t h:i a') }}" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Nama</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <input name="name" class="form-control col-md-7 col-xs-12" type="text" value="{{ $user->name }}" required="required" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Email</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <input name="email" class="form-control col-md-7 col-xs-12" type="email" value="{{ $user->email }}" required="required" >
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                          <button class="btn btn-danger" type="reset">Reset</button>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div role="tabpanel" class="tab-pane fade in" id="tab_password" aria-labelledby="home-tab">
                    <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" action="/adminpanel/password/users/edit/{{ $user->id }}" method="post">
                      {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                      @if (count($errors->editUserPassword) > 0)
                      <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        @foreach ($errors->editUserPassword->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                      </div>
                      @endif 
                      @if (Session::has('message'))
                      <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
                      @endif

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kata Sandi Baru</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <input name="new_password" class="form-control col-md-7 col-xs-12" type="password" required="required" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Konfirmasi Kata Sandi Baru</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <input name="new_password_confirm" class="form-control col-md-7 col-xs-12" type="password" required="required">
                        </div>
                      </div>
                      <input type="hidden" id="name" name="name" value="{{ $user->name }}">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button class="btn btn-danger" type="reset">Reset</button>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

<!-- SCRIPT -->
@push('scripts')
<!-- jQuery -->
<script async="" src="https://www.google-analytics.com/analytics.js"></script>

<!-- FastClick -->
<script src="{{ asset("js/fastclick.js") }}"></script>

<!-- NProgress -->
<script src="{{ asset("js/nprogress.js") }}"></script>

<!-- validator -->
<script src="{{ asset("js/validator.js") }}"></script>

<!-- bootstrap-progressbar -->
<script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>

<!-- iCheck -->
<script src="{{ asset("js/icheck.min.js") }}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset("js/moment.min.js") }}"></script>
<script src="{{ asset("js/daterangepicker.js") }}"></script>

<!-- bootstrap-wysiwyg -->
<script src="{{ asset("js/bootstrap-wysiwyg.min.js") }}"></script>
<script src="{{ asset("js/jquery.hotkeys.js") }}"></script>
<script src="{{ asset("js/prettify.js") }}"></script>

<!-- jQuery tags input -->
<script src="{{ asset("js/jquery.tagsinput.js") }}"></script>

<!-- Switchery -->
<script src="{{ asset("js/switchery.min.js") }}"></script>

<!-- Select2 -->
<script src="{{ asset("js/select2.full.min.js") }}"></script>

<!-- Autosize -->
<script src="{{ asset("js/autosize.min.js") }}"></script>

<!-- jQuery autocomplete -->
<script src="{{ asset("js/jquery.autocomplete.min.js") }}"></script>

<!-- starrr -->
<script src="{{ asset("js/starrr.js") }}"></script>

<!-- Parsley -->
<script src="{{ asset("js/parsley.min.js") }}"></script>

<!-- PNotify -->
<script src="{{ asset("js/pnotify.js") }}"></script>
<script src="{{ asset("js/pnotify.buttons.js") }}"></script>
<script src="{{ asset("js/pnotify.nonblock.js") }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
      localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
      $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
  });
</script>
@endpush

@include('Backend.includes.footer')
@endsection
