@extends('Backend.layouts.blank')

@push('stylesheets')
  <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Pengguna</h3>
      </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      @if (Session::has('message'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
      @endif
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Pengguna <small>BOW</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="pengguna" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
          <thead>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>

  @include('Backend.includes.footer')
  
  <!-- SCRIPT -->
  @push('scripts')
  <script>
    var table;
    $(function() {
      table = $('#pengguna').DataTable({
        processing: false,
        serverSide: true,
        ajax: '{!! route('adminpanel.users') !!}',

        columns: [
          { data: 'id', name: 'id', title:'ID', "width": "1%" },
          { data: 'avatar', name: 'avatar', title:'Gambar Profil', "width": "15%" , render: function(data, type, row) {
            return '<img src="{{ URL::to('/') }}/images/'+row.avatar+'" alt="" width="60px">';
          } },
          { data: 'name', name: 'name', title:'Nama', "width": "15%" },
          { data: 'email', name: 'email', title:'Email', "width": "15%" },
          { data: 'created_at', name: 'created_at', title:'Daftar', "width": "15%" },
          { className: "dt-center", width:"5%", name: 'actions', title: 'Aksi', render: function(data, type, row) {
            var data = "`" + row.name + "`";
            return '<a class="btn btn_primary btn_action" href=users/view/'+row.id+'>' + '<i class="fa fa-eye"></i> Lihat' + '</a> &nbsp;' + '<a class="btn btn_warning btn_action" href=users/edit/'+row.id+'>' + '<i class="fa fa-edit"></i> Ubah' + '</a> &nbsp;';
          } }
        ]
      });
    });
  </script>
  @endpush
@endsection
