@extends('Backend.layouts.blank')

@push('stylesheets')
  <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

  <div class="right_col" role="main">
    <div class="page-title">
      <div class="title_left">
        <h3>Ulasan</h3>
      </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      @if (Session::has('message'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="font-size: 14px">{{ Session::get('message') }}</p></div>
      @endif
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Ulasan <small>BOW</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="review" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
          <thead>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>

  @include('Backend.includes.footer')
  
  <!-- SCRIPT -->
  @push('scripts')
  <script>
    var table;
    $(function() {
      table = $('#review').DataTable({
        processing: false,
        serverSide: true,
        ajax: '{!! route('adminpanel.reviews') !!}',
        "aaSorting": [[6,'asc']],

        columns: [
          { data: 'id', name: 'id', title:'ID', "width": "1%" },
          { data: 'username', name: 'username', title:'Pengguna', "width": "10%" },
          { data: 'placename', name: 'placename', title:'Objek Wisata', "width": "15%" },
          { data: 'title', name: 'title', title:'Judul', "width": "10%" },
          { data: 'body', name: 'body', title:'Ulasan', "width": "20%" , render: function(data, type, row) {
            function getExcerpt($str, $startPos=0, $maxLength=100) {
              if(($str).length > $maxLength) {
                $excerpt   = ($str).substring($startPos, $maxLength-3);
                $lastSpace = ($str).lastIndexOf(" ");
                $excerpt   = ($excerpt).substring(0, $lastSpace);
                $excerpt  += '...';
              } else {
                $excerpt = $str;
              }
              
              return $excerpt;
            }

            return getExcerpt(row.body, 0, 60);
          } },
          { data: 'created_at', name: 'created_at', title:'Tanggal', "width": "5%" },
          { className: "dt-center", width:"5%", name: 'actions', title: 'Aksi', render: function(data, type, row) {
            var data = "`" + row.name + "`";
            return '<a class="btn btn_primary btn_action" href=reviews/view/'+row.id+'>' + '<i class="fa fa-eye"></i> Lihat' + '</a> &nbsp;';
          } }
        ]
      });
    });
  </script>
  @endpush
@endsection
