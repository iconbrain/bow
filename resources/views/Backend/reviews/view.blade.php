@extends('Backend.layouts.blank')

@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
<link rel="stylesheet" type="text/css" href="{{asset("css/custom.css")}}"/>
@endpush

@section('main_container')

<div class="right_col" role="main">
  <div class="page-title">
    <div class="title_left">
      <h3>Ulasan</h3>
    </div>
  </div>

  @foreach($reviews as $r)
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ulasan Pengguna <small>{{ $r->placename }}</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <div class="profile_img">
              <div id="crop-avatar">
                <img class="img-responsive avatar-view" src="{{ URL::to('/') }}/images/{{ $r->avatar }}" alt="Avatar" style="border-radius: 6px;">
              </div>
            </div>
            <h3>{{ $r->username }}</h3>

            <ul class="list-unstyled user_data">
              <li><i class="fa fa-envelope user-profile-icon"></i> {{ $r->email }}
              </li>
            </ul>

          </div>
          <div class="col-md-9 col-sm-9 col-xs-12">

            <div class="profile_title">
              <div class="col-md-6">
                <h2>Hasil Ulasan</h2>
              </div>
            </div>

            <div class="col-md-12">
              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Objek Wisata
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->placename }}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Judul
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->title }}
                </div>
              </div>
              
              <hr><div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Ulasan
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div style="text-align: justify;">{!! $r->body !!}</div>
                </div>
              </div><hr>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Fasilitas
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->facility }}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Kebersihan
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->cleanliness }}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Kenyamanan
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->comfort }}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Pelayanan
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->service }}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Harga
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->price }}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Tanggal Ulasan
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ $r->created_at }}
                </div>
              </div>
            </div>
            
          </div>

        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

@include('Backend.includes.footer')
@endsection
