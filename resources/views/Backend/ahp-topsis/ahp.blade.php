@extends('Backend.layouts.blank')

@push('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
	  	<div class="page-title">
		    <div class="title_left">
		      <h3>Metode AHP</h3>
		    </div>
	  	</div>

	    <div class="x_panel" style="margin-top: 20px;">
	        <div class="x_title">
	          <h2>Pairwise Comparasion Matrix</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="pcm">
	            <thead>
		            <tr>
				      <th></th>
				      <th>Fasilitas</th>
				      <th>Kebersihan</th>
				      <th>Kenyamanan</th>
				      <th>Pelayanan</th>
				      <th>Rp (Harga)</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
	            		<th>{{ $q->name }}</th>
						<td>{{ $q->facility }}</td>
						<td>{{ $q->cleanliness }}</td>
						<td>{{ $q->comfort }}</td>
						<td>{{ $q->service }}</td>
						<td>{{ $q->price }}</td>
	            	</tr>
	            	@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 1<small>Jumlahkan setiap baris dari pairwise comparasion matrix.</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="step1">
	            <thead>
		            <tr>
				      <th></th>
				      <th>Fasilitas</th>
				      <th>Kebersihan</th>
				      <th>Kenyamanan</th>
				      <th>Pelayanan</th>
				      <th>Rp (Harga)</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
	            		<th>{{ $q->name }}</th>
						<td>{{ $q->facility }}</td>
						<td>{{ $q->cleanliness }}</td>
						<td>{{ $q->comfort }}</td>
						<td>{{ $q->service }}</td>
						<td>{{ $q->price }}</td>
	            	</tr>
	            	@endforeach
	            	<tr>
					    <th>Sum</th>
					    <td>{{ $sumFacility }}</td>
					    <td>{{ $sumCleanliness }}</td>
					    <td>{{ $sumComfort }}</td>
					    <td>{{ $sumService }}</td>
					    <td>{{ $sumPrice }}</td>
					</tr>
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 2<small>Bagi setiap nilai dengan jumlah kolomnya, kemudian jumlahkan setiap barisnya.</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="step2">
	            <thead>
		            <tr>
				      <th></th>
				      <th>Fasilitas</th>
				      <th>Kebersihan</th>
				      <th>Kenyamanan</th>
				      <th>Pelayanan</th>
				      <th>Rp (Harga)</th>
				      <th>Jumlah</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
					    <th>{{ $q->name }}</th>
					    <td>{{ round($q->facility/$sumFacility,3) }}</td>
					    <td>{{ round($q->cleanliness/$sumCleanliness,3) }}</td>
					    <td>{{ round($q->comfort/$sumComfort,3) }}</td>
					    <td>{{ round($q->service/$sumService,3) }}</td>
					    <td>{{ round($q->price/$sumPrice,3) }}</td>
					    <td>{{ round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3) }}</td>
					</tr>
					@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 3<small>Jumlah setiap baris dibagi dengan n (jumlah kolom yang ada), yang kemudian dinamakan <b>vektor prioritas</b>.</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="step3">
	            <thead>
		            <tr>
				      <th></th>
				      <th>Jumlah</th>
				      <th>Vektor Prioritas</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
					    <th>{{ $q->name }}</th>
					    <td>{{ round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3) }}</td>
					    <td>{{ round((round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3))/5,3) }}</td>
					</tr>
					@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 4<small>Kalikan pairwise comparasion matrix dengan matriks vektor prioritas.</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table width="25%" cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
						<td width="15%">{{ $q->facility }}</td>
						<td width="15%">{{ $q->cleanliness }}</td>
						<td width="15%">{{ $q->comfort }}</td>
						<td width="15%">{{ $q->service }}</td>
						<td width="15%">{{ $q->price }}</td>
	            	</tr>
	            	@endforeach
	            </tbody>
	        </table>

	        <table cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<tr><td>&nbsp;</td></tr>
	            	<tr><td>&nbsp;</td></tr>
	            	<tr><td width="15%">x</td></tr>
	            	<tr><td>&nbsp;</td></tr>
	            	<tr><td>&nbsp;</td></tr>
	            </tbody>
	        </table>

	        <table width="10%" cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<?php $vp = array() ?>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $vp[] = round((round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3))/5,3) }}</td>
					</tr>
					@endforeach
	            </tbody>
	        </table>

	        <table cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<tr><td>&nbsp;</td></tr>
	            	<tr><td>&nbsp;</td></tr>
	            	<tr><td width="15%">=</td></tr>
	            	<tr><td>&nbsp;</td></tr>
	            	<tr><td>&nbsp;</td></tr>
	            </tbody>
	        </table>

	        <table width="10%" cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<?php $hasilkali = array() ?>
	            	@foreach($query as $q)
	            	<tr>
	            		<td>{{ $hasilkali[] = round(($q->facility*$vp[0])+($q->cleanliness*$vp[1])+($q->comfort*$vp[2])+($q->service*$vp[3])+($q->price*$vp[4]),3) }}</td>
	            	</tr>
					@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 5<small>Bagi hasil dari <b>step 4</b> dengan nilai dari vektor prioritas.</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<?php
	            		$hasilkali_length = count($hasilkali);
	            	?>
            		<tr>
            			<td>&nbsp;&nbsp;&nbsp;[</td>
	            		@for($i=0;$i<$hasilkali_length-1;$i++)
            			<td>{{ $hasilkali[$i].' / '.$vp[$i] }}</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						@endfor
						<?php $a= $hasilkali_length-1 ?>
						<td>{{ $hasilkali[$a].' / '.$vp[$a] }}]</td>
            		</tr>
            		<tr>
	            		<td>=&nbsp;[</td>
	            		@for($i=0;$i<$hasilkali_length-1;$i++)
            			<td>{{ round($hasilkali[$i]/$vp[$i],3) }}</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						@endfor
						<td>{{ round($hasilkali[$a]/$vp[$a],3) }}]</td>
            		</tr>
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 6<small>Cari λ maks</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<?php
	            		$hasilkali_length = count($hasilkali);
	            		$sumLambdaMaks = 0;
	            		for($i=0;$i<$hasilkali_length;$i++) {
							$sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
	            		}
	            	?>
            		<tr>
            			<td>λ maks =&nbsp;(</td>
	            		@for($i=0;$i<$hasilkali_length-1;$i++)
            			<td>{{ round($hasilkali[$i]/$vp[$i],3) }}</td><td>+</td>
						@endfor
						<?php $i= $hasilkali_length-1 ?>
						<td>{{ round($hasilkali[$i]/$vp[$i],3) }})/{{ $i+1 }}</td>
						<td>&nbsp;=&nbsp;{{ round($sumLambdaMaks/5,3) }}</td>
            		</tr>
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 7<small>Cari CI</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table cellspacing="0" style="display: inline-block;">
	            <tbody>
	            	<?php
	            		$hasilkali_length = count($hasilkali);
	            		$sumLambdaMaks = 0;
	            		for($i=0;$i<$hasilkali_length;$i++) {
							$sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
	            		}
	            	?>
            		<tr>
            			<td>CI = (λ maks - n)/(n - 1)</td>
            		</tr>
            		<tr>
            			<td>CI = ({{ round($sumLambdaMaks/5,3) }} - {{ $i }})/({{ $i }} - 1)</td>
            		</tr>
            		<tr>
            			<td>CI = {{ $ci = round((round($sumLambdaMaks/5,3)-$i)/($i-1),3) }}</td>
            		</tr>
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 8<small>Cari CR</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="step8">
	        	<thead>
            		<tr>
            			<th>n</th>
            			@for($b=1;$b<16;$b++)
            			<td>{{ $b }}</td>
            			@endfor
            		</tr>
	        	</thead>
	            <tbody>
            		<tr>
            			<th>RI</td>
            			<td>0</td>
            			<td>0</td>
            			<td>0.58</td>
            			<td>0.9</td>
            			<td>1.12</td>
            			<td>1.24</td>
            			<td>1.52</td>
            			<td>1.41</td>
            			<td>1.45</td>
            			<td>1.49</td>
            			<td>1.51</td>
            			<td>1.48</td>
            			<td>1.56</td>
            			<td>1.57</td>
            			<td>1.59</td>
            		</tr>
	            </tbody>
	        </table>

	        <table cellspacing="0">
	            <tbody>
	            	<?php
	            		$hasilkali_length = count($hasilkali);
	            		$sumLambdaMaks = 0;
	            		for($i=0;$i<$hasilkali_length;$i++) {
							$sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
	            		}
	            	?>
            		<tr>
            			<td>CR = CI/RI</td>
            		</tr>
            		<tr>
            			<td>CR = {{ $ci }}/1.12</td>
            		</tr>
            		<tr>
            			<td>CR = {{ $cr = round($ci/1.12,3) }}</td>
            		</tr>
	            </tbody>
	        </table>

		    @if($cr <= 0.1)
		    	<p style="color: #2FBA0B"><b>Karena CR <= 0.1, maka pairwise comparasion matrix tersebut dianggap konsisten dan dapat digunakan.</b></p>
		    @else
		    	<p style="color: red"><b>Karena CR > 0.1, maka pairwise comparasion matrix tersebut tidak dapat digunakan.</b></p>
		    @endif

	        </div>
	    </div>

		@if($cr < 0.1)
	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Step 9<small>Hasil pembobotan.</small></h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <tbody>
	            	<tr>
	            		<th>Kriteria</th>
	            		<th>Bobot</th>
	            	</tr>
	            	@foreach($query as $q)
	            	<tr>
	            		<td>{{ $q->name }}</td>
	            		<td>{{ round((round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3))/5,3) }}</td>
	            	</tr>
	            	@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>
	    @endif
    </div>
    <!-- /page content -->

    <!-- SCRIPT -->
	@push('scripts')
	<script>
	$(document).ready(function() {
	    $('#pcm').DataTable( {
	        "scrollX": true
	    } );
	    $('#step1').DataTable( {
	        "scrollX": true
	    } );
	    $('#step2').DataTable( {
	        "scrollX": true
	    } );
	    $('#step3').DataTable( {
	        "scrollX": true
	    } );
	    $('#step8').DataTable( {
	        "scrollX": true
	    } );
	} );
	</script>
  	@endpush

    @include('Backend.includes.footer')
@endsection