@extends('Backend.layouts.blank')

@push('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{asset("css/dataTables.bootstrap.min.css")}}"/>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
	  	<div class="page-title">
		    <div class="title_left">
		      <h3>Metode AHP + TOPSIS</h3>
		    </div>
	  	</div>

	    <div class="x_panel" style="margin-top: 20px;">
	        <div class="x_title">
	          <h2>Matriks Keputusan</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	          <table id="matriksKeputusan" class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
	            </thead>
	            <tbody>
	            </tbody>
	          </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Matriks Ternormalisasi</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	          <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="matriksTernormalisasi">
	            <thead>
		            <tr>
				      <th>ID</th>
				      <th>Nama</th>
				      <th>Fasilitas</th>
				      <th>Kebersihan</th>
				      <th>Kenyamanan</th>
				      <th>Pelayanan</th>
				      <th>Harga</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $q->facility_attr/$sumFacility }}</td>
					    <td>{{ $q->cleanliness_attr/$sumCleanliness }}</td>
					    <td>{{ $q->comfort_attr/$sumComfort }}</td>
					    <td>{{ $q->service_attr/$sumService }}</td>
					    <td>{{ $q->price_attr/$sumPrice }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	          </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Matriks Ternormalisasi Terbobot</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="matriksTernormalisasiTerbobot">
	            <thead>
		            <tr>
				      <th>ID</th>
				      <th>Nama</th>
				      <th>Fasilitas</th>
				      <th>Kebersihan</th>
				      <th>Kenyamanan</th>
				      <th>Pelayanan</th>
				      <th>Harga</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	<?php
	            		$itemsFacility = array();
	            		$itemsCleanliness = array();
	            		$itemsComfort = array();
	            		$itemsService = array();
	            		$itemsPrice = array();
	            	?>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas }}</td>
					    <td>{{ $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan }}</td>
					    <td>{{ $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan }}</td>
					    <td>{{ $itemsService[] = $q->service_attr/$sumService * $pelayanan }}</td>
					    <td>{{ $itemsPrice[] = $q->price_attr/$sumPrice * $harga }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Solusi Ideal Positif dan Negatif</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="solusiIdeal">
	            <thead>
		            <tr>
				      <th></th>
				      <th>Fasilitas</th>
				      <th>Kebersihan</th>
				      <th>Kenyamanan</th>
				      <th>Pelayanan</th>
				      <th>Harga</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	<tr>
					    <td>A*</td>
					    <td>{{ max($itemsFacility) }}</td>
					    <td>{{ max($itemsCleanliness) }}</td>
					    <td>{{ max($itemsComfort) }}</td>
					    <td>{{ max($itemsService) }}</td>
					    <td>{{ max($itemsPrice) }}</td>
					</tr>
	            	<tr>
					    <td>A-</td>
					    <td>{{ min($itemsFacility) }}</td>
					    <td>{{ min($itemsCleanliness) }}</td>
					    <td>{{ min($itemsComfort) }}</td>
					    <td>{{ min($itemsService) }}</td>
					    <td>{{ min($itemsPrice) }}</td>
					</tr>
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Hasil Perhitungan</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="hasilPerhitungan">
	            <thead>
		            <tr>
				      <th>ID</th>
				      <th>Nama</th>
				      <th>Si *</th>
				      <th>Si -</th>
				      <th>Ci *</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	<?php $rank = array() ?>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*0.101) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*0.202) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*0.151) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*0.234) - max($itemsPrice)),2)) }}</td>
					    <td>{{ $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*0.101) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*0.202) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*0.151) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*0.234) - min($itemsPrice)),2)) }}</td>
						<td>{{ $rank[] = $negSolution/($negSolution+$posSolution) }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>

	    <div class="x_panel">
	        <div class="x_title">
	          <h2>Ranking</h2>
	          <ul class="nav navbar-right panel_toolbox">
	            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	            </li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            </li>
	            <li><a class="close-link"><i class="fa fa-close"></i></a>
	            </li>
	          </ul>
	          <div class="clearfix"></div>
	        </div>
	        <div class="x_content">

	        <table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0" id="ranking">
	            <thead>
		            <tr>
				      <th>ID</th>
				      <th>Nama</th>
				      <th>Kategori</th>
				      <th>Ci *</th>
				      <th>Rank</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query2 as $q2)
	            	<tr>
					    <td>{{ $q2->place_id }}</td>
					    <td>{{ $q2->name }}</td>
					    <td>{{ $q2->category_name }}</td>
					    <td>{{ $q2->result }}</td>
					    <td>{{ $q2->row }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>

	        </div>
	    </div>
    </div>
    <!-- /page content -->

    <!-- SCRIPT -->
	@push('scripts')
	<script>
    	var table;
		$(function() {
	      table = $('#matriksKeputusan').DataTable({
	        processing: false,
	        serverSide: true,
	        ajax: '{!! route('ahptopsis.all') !!}',

	        columns: [
	          { data: 'place_id', name: 'place_id', title:'ID', "width": "1%" },
	          { data: 'name', name: 'name', title:'Nama', "width": "15%" },
	          { data: 'facility_attr', name: 'facility_attr', title:'Fasilitas', "width": "15%" },
	          { data: 'cleanliness_attr', name: 'cleanliness_attr', title:'Kebersihan', "width": "15%" },
	          { data: 'comfort_attr', name: 'comfort_attr', title:'Kenyamanan', "width": "15%" },
	          { data: 'service_attr', name: 'service_attr', title:'Pelayanan', "width": "15%" },
	          { data: 'price_attr', name: 'price_attr', title:'Harga', "width": "15%" }
	        ]
	      });
	    });
	    $(document).ready(function() {
		    $('#matriksTernormalisasi').DataTable( {
		        "scrollX": true
		    } );
		    $('#matriksTernormalisasiTerbobot').DataTable( {
		        "scrollX": true
		    } );
		    $('#solusiIdeal').DataTable( {
		        "scrollX": true
		    } );
		    $('#hasilPerhitungan').DataTable( {
		        "scrollX": true
		    } );
		    $('#ranking').DataTable( {
		        "scrollX": true,
	        	aaSorting: [[4,"asc"]]
		    } );
		} );
	</script>
  	@endpush

    @include('Backend.includes.footer')
@endsection