@extends('Backend.layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div class="col-md-12 col-sm-12 col-xs-12">
    		<h1>Decision Matrix</h1>
			<table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
		            <tr>
				      <th>Place ID</th>
				      <th>Place Name</th>
				      <th>Facility</th>
				      <th>Cleanliness</th>
				      <th>Comfort</th>
				      <th>Service</th>
				      <th>Price</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $q->facility_attr }}</td>
					    <td>{{ $q->cleanliness_attr }}</td>
					    <td>{{ $q->comfort_attr }}</td>
					    <td>{{ $q->service_attr }}</td>
					    <td>{{ $q->price_attr }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>

	        <h1>Normalized Matrix</h1>
			<table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
		            <tr>
				      <th>Place ID</th>
				      <th>Place Name</th>
				      <th>Facility</th>
				      <th>Cleanliness</th>
				      <th>Comfort</th>
				      <th>Service</th>
				      <th>Price</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $q->facility_attr/$sumFacility }}</td>
					    <td>{{ $q->cleanliness_attr/$sumCleanliness }}</td>
					    <td>{{ $q->comfort_attr/$sumComfort }}</td>
					    <td>{{ $q->service_attr/$sumService }}</td>
					    <td>{{ $q->price_attr/$sumPrice }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>


    		<h1>Weighted Normalized Matrix</h1>
			<table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
		            <tr>
				      <th>Place ID</th>
				      <th>Place Name</th>
				      <th>Facility</th>
				      <th>Cleanliness</th>
				      <th>Comfort</th>
				      <th>Service</th>
				      <th>Price</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	<?php
	            		$itemsFacility = array();
	            		$itemsCleanliness = array();
	            		$itemsComfort = array();
	            		$itemsService = array();
	            		$itemsPrice = array();
	            	?>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $itemsFacility[] = $q->facility_attr/$sumFacility*0.101 }}</td>
					    <td>{{ $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312 }}</td>
					    <td>{{ $itemsComfort[] = $q->comfort_attr/$sumComfort*0.202 }}</td>
					    <td>{{ $itemsService[] = $q->service_attr/$sumService*0.151 }}</td>
					    <td>{{ $itemsPrice[] = $q->price_attr/$sumPrice*0.234 }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>


    		<h1>Positive and Negative Ideal Solution</h1>
			<table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
		            <tr>
				      <th></th>
				      <th>Facility</th>
				      <th>Cleanliness</th>
				      <th>Comfort</th>
				      <th>Service</th>
				      <th>Price</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	<tr>
					    <td>A*</td>
					    <td>{{ max($itemsFacility) }}</td>
					    <td>{{ max($itemsCleanliness) }}</td>
					    <td>{{ max($itemsComfort) }}</td>
					    <td>{{ max($itemsService) }}</td>
					    <td>{{ min($itemsPrice) }}</td>
					</tr>
	            	<tr>
					    <td>A-</td>
					    <td>{{ min($itemsFacility) }}</td>
					    <td>{{ min($itemsCleanliness) }}</td>
					    <td>{{ min($itemsComfort) }}</td>
					    <td>{{ min($itemsService) }}</td>
					    <td>{{ max($itemsPrice) }}</td>
					</tr>
	            </tbody>
	        </table>


    		<h1>Ranking</h1>
			<table class="table table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
	            <thead>
		            <tr>
				      <th>Place ID</th>
				      <th>Place Name</th>
				      <th>Si *</th>
				      <th>Si -</th>
				      <th>Ci *</th>
				    </tr>
	    		</thead>
	            <tbody>
	            	<?php $rank = array() ?>
	            	@foreach($query as $q)
	            	<tr>
					    <td>{{ $q->place_id }}</td>
					    <td>{{ $q->name }}</td>
					    <td>{{ $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*0.101) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*0.202) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*0.151) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*0.234) - min($itemsPrice)),2)) }}</td>
					    <td>{{ $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*0.101) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*0.202) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*0.151) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*0.234) - max($itemsPrice)),2)) }}</td>
						<td>{{ $rank[] = $negSolution/($negSolution+$posSolution) }}</td>
					</tr>
	        		@endforeach
	            </tbody>
	        </table>
        </div>
    </div>
    <!-- /page content -->

    @include('Backend.includes.footer')
@endsection