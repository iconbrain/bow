@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">

@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
<title>Ganti Kata Sandi | BOW</title>
@endsection

@section('content')
<div style="padding: 20px;">
    <div class="row" style="margin-top: 40px; margin-bottom: 40px;">
        <div class="col s12 m8 offset-m2 l4 offset-l4">
            <div class="card z-depth-3">
              <div class="card-title" style="color: white; font-size: 24px; font-weight: 300; background-color: #2dbc0d;">
                <h4 style="margin-left: 20px;">Ganti Kata Sandi</h4>
              </div>
              <div class="card-content">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif

                <p class="grey-text text-muted" style="font-size: 14px; font-family: 'Arial'; text-align: justify;">Silahkan isi kata sandi baru. Untuk alasan keamanan, mohon gunakan kata sandi yang tidak mudah ditebak.</p><br>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="input-field">
                        <input type="email" name="email" placeholder="Email" id="email" value="{{ $email or old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <p style="color: red; font-size: 14px; font-family: 'Arial'; text-align: justify;">{{ $errors->first('email') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="input-field">
                        <input type="password" name="password" placeholder="Kata Sandi" id="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <p style="color: red; font-size: 14px; font-family: 'Arial'; text-align: justify;">{{ $errors->first('password') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="input-field">
                        <input type="password" name="password_confirmation" placeholder="Konfirmasi Kata Sandi" id="password-confirm" required>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <p style="color: red; font-size: 14px; font-family: 'Arial'; text-align: justify;">{{ $errors->first('password_confirmation') }}</p>
                            </span>
                        @endif
                    </div>
                  <br>
                  <button class="btn waves-effect waves-light primary-color" style="color: white; width: 175px;" type="submit" name="action">Ganti Kata Sandi</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection