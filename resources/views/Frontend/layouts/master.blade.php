<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
	    <!-- Fonts -->
	    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="shortcut icon" href="{{URL::asset('/')}}images/favicon.ico">

		@yield('title')
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @stack('stylesheets')
        <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
        <link rel="stylesheet" href="{{ asset("css/bootstrap.css") }}">
        <link rel="stylesheet" href="{{ asset("css/w3.css") }}">
        <link rel="stylesheet" href="{{ asset("css/custom.css") }}">
    </head>

    <body>
        @yield('content')
        @include('Frontend.includes.footer')

        <!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        @stack('scripts')
    </body>
</html>