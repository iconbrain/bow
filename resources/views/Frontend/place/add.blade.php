@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>

@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
    <title>Tambah Objek Wisata | BOW</title>
@endsection

@section('content')
  <div class="container container-fluid cont">
    <div class="row">
      <div class="row subcontent">
        <div class="col-xs-12 col-lg-5">
          <div class="card-panel cyan z-depth-4 side-info">
            <img src="{{ URL::to('/') }}/images/lamps.png" class="img-responsive img-info" alt="">
            <ol style="list-style-type: none;">
              <span class="white-text opening"><li class="info-header">Aturan dalam menambahkan objek wisata di BOW</li></span>
            </ol>
            <hr>
            <div class="place-card-subtitle">
              <ol class="rule">
                <span class="white-text"><li>Pastikan Anda sudah login.</li></span>
                <span class="white-text"><li>Anda tidak dapat menambahkan objek wisata yang telah terdapat di BOW.</li></span>
                <span class="white-text"><li>Semua bidang isian yang tersedia wajib diisi.</li></span>
                <span class="white-text"><li>Form yang Anda kirim akan kami proses dalam waktu maksimal 2x24 jam.</li></span>
                <span class="white-text"><li>Setiap objek wisata yang Anda minta (request) akan kami evaluasi keberadaannya.</li></span>
              </ol>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-lg-7 z-depth-2 form-info">
          <p class="form-heading">Form Penambahan Objek Wisata</p><hr class="form-hd">

          @if(Auth::check())
          <form id="demo-form2" data-parsley-validate="" class="col s12 form-horizontal form-label-left form-add" novalidate="" action="/addPlaces" method="post">
            {{ csrf_field() }} <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
            @if (count($errors->addPlace) > 0)
            <div class="alert alert-danger" style="margin-bottom: 30px;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              @foreach ($errors->addPlace->all() as $error)
              <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif 
            @if (Session::has('message'))
            <div class="alert alert-success" style="margin-bottom: 30px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
            @endif
            <input type="hidden" name="from_user" value="{{ Auth::user()->id }}">

            <div class="input-field">
              <i class="material-icons prefix" style="color: black;">mode_edit</i>
              <input name="name" type="text" value="{{ old('name') }}" required="required" style="color: black;">
              <label for="icon_prefix" style="font-size: 14px;">Nama Objek Wisata</label>
            </div>
            <div class="input-field">
              <i class="material-icons prefix" style="color: black;">phone</i>
              <input name="phone_number" type="text" value="{{ old('phone_number') }}" required="required" style="color: black;">
              <label for="icon_telephone" style="font-size: 14px;">Nomor Telepon</label>
            </div>
            <div class="input-field">
              <i class="material-icons prefix" style="color: black;">location_on</i>
              <input name="address" type="text" value="{{ old('address') }}" required="required" style="color: black;">
              <label for="icon_telephone" style="font-size: 14px;">Alamat</label>
            </div>
            <div class="input-field">
              <i class="material-icons prefix" style="color: black;">av_timer</i>
              <textarea style="color: black" name="operational_hours" class="materialize-textarea" value="{{ old('operational_hours') }}" required="required" style="color: black;"></textarea>
              <label for="icon_telephone" style="font-size: 14px;">Jam Operasional</label>
            </div>
            <div class="input-field" style="color: black;">
              <select class="browser-default" style="font-size: 14px;" name="price_range">
                <option>Pilih Rentang HTM...</option>
                @foreach($priceRange as $pr)
                <option value="{{ $pr->id }}">{{ $pr->price_range }}</option>
                @endforeach
              </select>
            </div>
            <div class="input-field">
              <i class="material-icons prefix" style="color: black;">payment</i>
              <textarea style="color: black" name="price" class="materialize-textarea" value="{{ old('price') }}" required="required" style="color: black;"></textarea>
              <label for="icon_telephone" style="font-size: 14px;">Harga Tiket Masuk</label>
            </div>
            <div class="input-field" style="color: black;">
              <select class="browser-default" style="font-size: 14px;" name="lokasi">
                <option>Pilih Lokasi...</option>
                @foreach($locations as $loc)
                <option value="{{ $loc->location_id }}">{{ $loc->location_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="input-field" style="color: black;">
              <select class="browser-default" style="font-size: 14px;" name="kategori">
                <option>Pilih Kategori...</option>
                @foreach($categories as $cat)
                <option value="{{ $cat->category_id }}">{{ $cat->category_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="input-field">
              <i class="material-icons prefix" style="color: black;">assessment</i>
              <textarea name="description" class="materialize-textarea" value="{{ old('description') }}" required="required" style="color: black;"></textarea>
              <label for="icon_telephone" style="font-size: 14px;">Deskripsi singkat tentang objek wisata (minimal 20 karakter)</label>
            </div>
            <div class="row">
              <div class="col s12 right-align">
                  <button class="btn waves-effect waves-black more-item" type="submit" name="action" name="submitPlace" style="color: white;">Kirim<i class="material-icons right top-4">send</i></button>
              </div>
            </div>
          </form>
          @endif
          @unless(Auth::check())
          <div class="input-field">
            <i class="material-icons prefix">mode_edit</i>
            <input name="name" type="text">
            <label for="icon_prefix" style="font-size: 14px;">Nama Objek Wisata</label>
          </div>
          <div class="input-field">
            <i class="material-icons prefix">phone</i>
            <input name="phone" type="text">
            <label for="icon_telephone" style="font-size: 14px;">Nomor Telepon</label>
          </div>
          <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input name="address" type="text">
            <label for="icon_telephone" style="font-size: 14px;">Alamat</label>
          </div>
          <div class="input-field">
            <i class="material-icons prefix" style="color: black;">av_timer</i>
            <textarea style="color: black" name="operational_hours" class="materialize-textarea" value="{{ old('operational_hours') }}" required="required" style="color: black;"></textarea>
            <label for="icon_telephone" style="font-size: 14px;">Jam Operasional</label>
          </div>
          <div class="input-field" style="color: black;">
            <select class="browser-default" style="font-size: 14px;" name="price_range">
              <option>Pilih Rentang HTM...</option>
              @foreach($priceRange as $pr)
              <option value="{{ $pr->id }}">{{ $pr->price_range }}</option>
              @endforeach
            </select>
          </div>
          <div class="input-field">
            <i class="material-icons prefix" style="color: black;">payment</i>
            <textarea style="color: black" name="price" class="materialize-textarea" value="{{ old('price') }}" required="required" style="color: black;"></textarea>
            <label for="icon_telephone" style="font-size: 14px;">Harga Tiket Masuk</label>
          </div>
          <div class="input-field">
            <select class="browser-default" style="font-size: 14px;" name="lokasi">
              <option>Pilih Lokasi...</option>
              @foreach($locations as $loc)
              <option value="{{ $loc->location_id }}">{{ $loc->location_name }}</option>
              @endforeach
            </select>
          </div>
          <div class="input-field">
            <select class="browser-default" style="font-size: 14px;" name="kategori">
              <option>Pilih Kategori...</option>
              @foreach($categories as $cat)
              <option value="{{ $cat->category_id }}">{{ $cat->category_name }}</option>
              @endforeach
            </select>
          </div>
          <div class="input-field">
            <i class="material-icons prefix">assessment</i>
            <textarea name="description" class="materialize-textarea"></textarea>
            <label for="icon_telephone" style="font-size: 14px;">Deskripsi singkat tentang objek wisata (minimal 20 karakter)</label>
          </div>
          <div class="row">
            <div class="col s12 right-align">
                <button class="btn waves-effect waves-black more-item" id="auth" onclick="document.getElementById('authentication').style.display='block'" style="color: white;">Kirim<i class="material-icons right top-4">send</i>
                </button>
            </div>
          </div>
          @endunless

        </div>
      </div>
    </div>
  </div>

  @push('scripts')
  <!-- Parsley -->
  <script src="{{ asset("js/parsley.min.js") }}"></script>
  <script>
    $(document).ready(function(){
      $('.carousel').carousel();
      $('select').material_select();
    });
  </script>
  @endpush
@endsection