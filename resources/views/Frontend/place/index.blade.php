@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/jquery.rateyo.css") }}"/>
@include('Frontend.includes.header')

@section('title')
    <title>{{ $places[0]->name }} | BOW</title>
@endsection

@section('content')
  <div class="w3-light-grey">
    <div class="container medium-div">
    <!-- <div style="margin-left: 10px; margin-right: 10px;"> -->
      <div class="row">
        @foreach($places as $item)
          <!-- <div class="col s12 m8"> -->
          <div class="col-xs-12 col-lg-8">
            <div class="card w3-round-large">
              <div class="card-image">
                @if(strpos($item->image,'https://') !== false)
                  <img src="{{$item->image}}" class="tabs-top img-places-top responsive-img">
                @else
                  <img src="{{ URL::to('/') }}/images/{{$item->image}}" class="tabs-top img-places-top responsive-img">
                @endif
              </div>
              <div class="card-content">
                <p class="middle place-title">{{ $item->name }}</p>
                <p style="color:grey">{!! $item->address !!}</p><hr>
              </div>
              <div class="card-content card-desc-1">
                <div class="row">
                  <div class="col s6">
                    <br>
                    @if(Auth::check())
                      @if( $checkBookmark == 0 OR ($checkBookmark == 1 AND $checkBookmarkStatus == 0))
                        <form id="form-bookmark" method="post">
                          <button class="btn waves-effect waves-light more-item" type="submit" id="submitBookmark" style="color: white; background-color: #2795EC;"><i id="loading" class="material-icons left top-4">turned_in_not</i>Arsip</button>
                        </form>
                      @elseif( $checkBookmark == 1 AND $checkBookmarkStatus == 1)
                        <button class="btn waves-effect waves-light more-item" type="submit" style="color: white; background-color: #63a7dd; pointer-events: none;"><i class="material-icons left top-4">turned_in</i>Ditandai</button>
                      @endif
                    @endif

                    @unless(Auth::check())
                      <button class="btn waves-effect waves-light more-item" onclick="document.getElementById('authentication').style.display='block'" style="color: white; background-color: #2795EC"><i class="material-icons left top-4">turned_in_not</i>Arsip</button>
                    @endunless

                  </div>
                  <div class="fixed-action-btn btn-review horizontal">
                    <a class="btn-floating btn-large red">
                      <i class="large material-icons">assignment</i>
                    </a>
                    <ul>
                      <li><a class="btn-floating yellow darken-2 tooltipped" data-position="top" data-delay="1" data-tooltip="Ulasan" rel="tooltip-top" onclick="Materialize.toast('<?php
                        if($counts == 0) {
                          echo "Belum terdapat ulasan.";
                        } else if($counts == 1) {
                          echo "Telah terdapat $counts ulasan";
                        } else if($counts > 1 AND $days >= 1) {
                          echo "$counts ulasan selama $days hari";
                        } else if($counts > 1 AND $days < 1 AND $hours >= 1) {
                          echo "$counts ulasan selama $hours jam";
                        } else if($counts > 1 AND $days < 1 AND $hours < 1 AND $minutes >= 1) {
                          echo "$counts ulasan selama $minutes menit";
                        } else if($counts > 1 AND $days < 1 AND $hours < 1 AND $minutes < 1 AND $seconds >= 1) {
                          echo "$counts ulasan selama $seconds detik";
                        }
                      ?>', 3000, 'rounded')"><i class="material-icons">chat</i></a></li>
                      <li><a class="btn-floating tooltipped" style="background-color: #0A9E42;" data-position="top" data-delay="1" data-tooltip="Penilaian" rel="tooltip-top" onclick="Materialize.toast('<?php
                        if($avgRate > 0 AND $avgRate < 1) {
                          echo "Unsatisfactory";
                        } else if($avgRate >= 1 AND $avgRate < 2) {
                          echo "Sangat Buruk";
                        } else if($avgRate >= 2 AND $avgRate < 3) {
                          echo "Buruk";
                        } else if($avgRate >= 3 AND $avgRate < 4) {
                          echo "Sedang";
                        } else if($avgRate >= 4 AND $avgRate < 5) {
                          echo "Baik";
                        } else if($avgRate == 5) {
                          echo "Sangat Baik";
                        }
                      ?>', 3000, 'rounded')"><i class="material-icons">grade</i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <ul class="tabs tabs-top" id="myTab">
              <li class="tab col s4"><a class="active" href="#ringkasan"><b style="font-family: 'Arial'">Ringkasan</b></a></li>
              <li class="tab col s4"><a href="#rating"><b style="font-family: 'Arial'"">Penilaian</b></a></li>
              <li class="tab col s4"><a href="#ulasan"><b style="font-family: 'Arial'"">Ulasan ({{ $counts }})</b></a></li>
            </ul>
            <div class="margin-btm-5"></div>

            <div id="ringkasan" class="col s12 white tabs-bottom">
              <div class="tabs-div">
                <div class="col s12">
                  <p class="tabs-heading">Deskripsi Singkat</p><p align="justify">{!! $item->description !!}</p><br>
                </div>

                <div class="col m4" style="position: relative;">
                  <p class="tabs-heading">Harga&nbsp;
                  <i class="material-icons b-custom tooltipped" data-position="right" data-delay="1" data-tooltip="Harga yang dimaksud adalah Harga Tiket Masuk (HTM) pada masing-masing objek wisata.&#10;Untuk kategori wisata kuliner, harga yang dimaksud adalah harga makanan. &#10;Untuk kategori wisata belanja, harga yang dimaksud adalah harga barang yang dijual." style="font-size: 17px;" rel="tooltip">info</i></p><p class="place-card-subtitle" style="color: grey"><?php echo $item->price ?></p><br>
                </div>

                <div class="col m5">
                  <p class="tabs-heading">Jam Operasional</p>
                  <p class="place-card-subtitle" style="color: grey">
                  <?php echo $item->operational_hours ?></p>
                  <br />
                </div>

                <div class="col m3">
                  <p class="tabs-heading">Nomor Telepon</p>
                    @if($item->phone_number == NULL)<p class="place-card-subtitle" style="color: grey">(Belum Tersedia)</p>
                    @else<p class="place-card-subtitle" style="color: grey">{{ $item->phone_number }}</p>@endif
                </div>

                <table class="table-padding">
                  <style>
                    .table-padding td{
                      padding: 3px 8px;
                      font-size: 13.5px;
                    }
                  </style>
                  <tbody>
                    <tr>
                      <td><h5 style="font-family: 'Comic Sans MS'">Fasilitas</h5></td>
                      <td><div id="fac" class="tooltipped" data-position="right" data-delay="1" data-tooltip="{{ $fattr }}" style="font-size: 17px;" rel="tooltip"></div></td>
                    </tr>
                    <tr>
                      <td><h5 style="font-family: 'Comic Sans MS'">Kebersihan</h5></td>
                      <td><div id="cle" class="tooltipped" data-position="right" data-delay="1" data-tooltip="{{ $cattr }}" style="font-size: 17px;" rel="tooltip"></div></td>
                    </tr>
                    <tr>
                      <td><h5 style="font-family: 'Comic Sans MS'">Kenyamanan</h5></td>
                      <td><div id="com" class="tooltipped" data-position="right" data-delay="1" data-tooltip="{{ $comattr }}" style="font-size: 17px;" rel="tooltip"></div></td>
                    </tr>
                    <tr>
                      <td><h5 style="font-family: 'Comic Sans MS'">Pelayanan</h5></td>
                      <td><div id="ser" class="tooltipped" data-position="right" data-delay="1" data-tooltip="{{ $sattr }}" style="font-size: 17px;" rel="tooltip"></div></td>
                    </tr>
                    <tr>
                      <td><h5 style="font-family: 'Comic Sans MS'">Harga</h5></td>
                      <td><div id="pri" class="tooltipped" data-position="right" data-delay="1" data-tooltip="{{ $pattr }}" style="font-size: 17px;" rel="tooltip"></div></td>
                    </tr>
                  </tbody>
                </table><br>

                @if($item->video_link != NULL)
                <div class="col s12">
                  <p class="tabs-heading">Video</p>
                    <div class="media">
                      <div class="media-body">
                        <?php
                        $embed = Embed::make( $item->video_link )->parseUrl();
                        
                        if ($embed) {
                          $embed->setAttribute(['width' => '100%', 'height' => '335']);
                          echo $embed->getHtml();
                        }
                        ?>
                      </div>
                    </div>
                  <br><br>
                </div>
                @endif
              </div>
            </div>

            <div id="rating" class="col s12 white tabs-bottom">
              <div class="tabs-div">
                @if(Auth::check())
                  @if(in_array(Auth::user()->id, $q3_authorId))
                    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button><i class="material-icons left">done_all</i>Anda telah menilai objek wisata ini.</div>
                    
                    <p class="tabs-heading">Kriteria</p>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="place_id" value="{{ $places[0]->place_id }}">
                    <input type="hidden" name="place_prefix" value="{{ $places[0]->place_prefix }}">

                    <div class="rating-block">
                      <div class="col s4 criteria">Fasilitas</div>

                      <div class="stars">
                        <input class="star star-fasilitas-5" id="star-fasilitas-5" type="radio" name="fasilitas" value="5" disabled />
                        <label class="star star-fasilitas-5" for="star-fasilitas-5"></label>
                        <input class="star star-fasilitas-4" id="star-fasilitas-4" type="radio" name="fasilitas" value="4" disabled />
                        <label class="star star-fasilitas-4" for="star-fasilitas-4"></label>
                        <input class="star star-fasilitas-3" id="star-fasilitas-3" type="radio" name="fasilitas" value="3" disabled />
                        <label class="star star-fasilitas-3" for="star-fasilitas-3"></label>
                        <input class="star star-fasilitas-2" id="star-fasilitas-2" type="radio" name="fasilitas" value="2" disabled />
                        <label class="star star-fasilitas-2" for="star-fasilitas-2"></label>
                        <input class="star star-fasilitas-1" id="star-fasilitas-1" type="radio" name="fasilitas" value="1" disabled />
                        <label class="star star-fasilitas-1" for="star-fasilitas-1"></label>
                      </div>
                    </div>

                    <div class="rating-block">
                      <div class="col s4 criteria">Kebersihan
                      </div>

                      <div class="stars">
                        <input class="star star-kebersihan-5" id="star-kebersihan-5" type="radio" name="kebersihan" value="5" disabled />
                        <label class="star star-kebersihan-5" for="star-kebersihan-5"></label>
                        <input class="star star-kebersihan-4" id="star-kebersihan-4" type="radio" name="kebersihan" value="4" disabled />
                        <label class="star star-kebersihan-4" for="star-kebersihan-4"></label>
                        <input class="star star-kebersihan-3" id="star-kebersihan-3" type="radio" name="kebersihan" value="3" disabled />
                        <label class="star star-kebersihan-3" for="star-kebersihan-3"></label>
                        <input class="star star-kebersihan-2" id="star-kebersihan-2" type="radio" name="kebersihan" value="2" disabled />
                        <label class="star star-kebersihan-2" for="star-kebersihan-2"></label>
                        <input class="star star-kebersihan-1" id="star-kebersihan-1" type="radio" name="kebersihan" value="1" disabled />
                        <label class="star star-kebersihan-1" for="star-kebersihan-1"></label>
                      </div>
                    </div>

                    <div class="rating-block">
                      <div class="col s4 criteria">Kenyamanan
                      </div>

                      <div class="stars">
                        <input class="star star-kenyamanan-5" id="star-kenyamanan-5" type="radio" name="kenyamanan" value="5" disabled />
                        <label class="star star-kenyamanan-5" for="star-kenyamanan-5"></label>
                        <input class="star star-kenyamanan-4" id="star-kenyamanan-4" type="radio" name="kenyamanan" value="4" disabled />
                        <label class="star star-kenyamanan-4" for="star-kenyamanan-4"></label>
                        <input class="star star-kenyamanan-3" id="star-kenyamanan-3" type="radio" name="kenyamanan" value="3" disabled />
                        <label class="star star-kenyamanan-3" for="star-kenyamanan-3"></label>
                        <input class="star star-kenyamanan-2" id="star-kenyamanan-2" type="radio" name="kenyamanan" value="2" disabled />
                        <label class="star star-kenyamanan-2" for="star-kenyamanan-2"></label>
                        <input class="star star-kenyamanan-1" id="star-kenyamanan-1" type="radio" name="kenyamanan" value="1" disabled />
                        <label class="star star-kenyamanan-1" for="star-kenyamanan-1"></label>
                      </div>
                    </div>

                    <div class="rating-block">
                      <div class="col s4 criteria">Pelayanan
                      </div>

                      <div class="stars">
                        <input class="star star-pelayanan-5" id="star-pelayanan-5" type="radio" name="pelayanan" value="5" disabled />
                        <label class="star star-pelayanan-5" for="star-pelayanan-5"></label>
                        <input class="star star-pelayanan-4" id="star-pelayanan-4" type="radio" name="pelayanan" value="4" disabled />
                        <label class="star star-pelayanan-4" for="star-pelayanan-4"></label>
                        <input class="star star-pelayanan-3" id="star-pelayanan-3" type="radio" name="pelayanan" value="3" disabled />
                        <label class="star star-pelayanan-3" for="star-pelayanan-3"></label>
                        <input class="star star-pelayanan-2" id="star-pelayanan-2" type="radio" name="pelayanan" value="2" disabled />
                        <label class="star star-pelayanan-2" for="star-pelayanan-2"></label>
                        <input class="star star-pelayanan-1" id="star-pelayanan-1" type="radio" name="pelayanan" value="1" disabled />
                        <label class="star star-pelayanan-1" for="star-pelayanan-1"></label>
                      </div>
                    </div>

                    <div class="rating-block">
                      <div class="col s4 criteria">Harga
                      </div>

                      <div class="stars">
                        <input class="star star-harga-5" id="star-harga-5" type="radio" name="harga" value="5" disabled />
                        <label class="star star-harga-5" for="star-harga-5"></label>
                        <input class="star star-harga-4" id="star-harga-4" type="radio" name="harga" value="4" disabled />
                        <label class="star star-harga-4" for="star-harga-4"></label>
                        <input class="star star-harga-3" id="star-harga-3" type="radio" name="harga" value="3" disabled />
                        <label class="star star-harga-3" for="star-harga-3"></label>
                        <input class="star star-harga-2" id="star-harga-2" type="radio" name="harga" value="2" disabled />
                        <label class="star star-harga-2" for="star-harga-2"></label>
                        <input class="star star-harga-1" id="star-harga-1" type="radio" name="harga" value="1" disabled />
                        <label class="star star-harga-1" for="star-harga-1"></label>
                      </div>
                    </div>
                    
                    <div class="row">
                       <div class="input-field col s9">
                        <i class="material-icons prefix">title</i>
                        <input type="text" class="validate" value="{{ old('title') }}" name="title" disabled>
                        <label>Judul Ulasan</label>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="input-field col s10">
                        <i class="material-icons prefix">mode_edit</i>
                        <textarea class="materialize-textarea" name="komentar" id="komentar" disabled>{{ old('komentar') }}</textarea>
                        @foreach($places as $item)
                        <label>Ulasan Anda tentang objek wisata {{ $item->name }} (minimal 20 karakter).</label>
                        @endforeach
                      </div>
                    </div>
                  @endif
                  @unless(in_array(Auth::user()->id, $q3_authorId))
                    <form action="/rating" method="post">
                      @if (count($errors->postReview) > 0)
                      <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        @foreach ($errors->postReview->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                      </div>
                      @endif 
                      @if (Session::has('message'))
                      <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
                      @endif

                      <p class="tabs-heading">Kriteria</p>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="place_id" value="{{ $places[0]->place_id }}">
                      <input type="hidden" name="place_prefix" value="{{ $places[0]->place_prefix }}">

                      <div class="rating-block">
                        <div class="col s4 criteria">Fasilitas</div>

                        <div class="stars">
                          <input class="star star-fasilitas-5" id="star-fasilitas-5" type="radio" name="fasilitas" value="5" />
                          <label class="star star-fasilitas-5" for="star-fasilitas-5"></label>
                          <input class="star star-fasilitas-4" id="star-fasilitas-4" type="radio" name="fasilitas" value="4" />
                          <label class="star star-fasilitas-4" for="star-fasilitas-4"></label>
                          <input class="star star-fasilitas-3" id="star-fasilitas-3" type="radio" name="fasilitas" value="3" />
                          <label class="star star-fasilitas-3" for="star-fasilitas-3"></label>
                          <input class="star star-fasilitas-2" id="star-fasilitas-2" type="radio" name="fasilitas" value="2" />
                          <label class="star star-fasilitas-2" for="star-fasilitas-2"></label>
                          <input class="star star-fasilitas-1" id="star-fasilitas-1" type="radio" name="fasilitas" value="1" />
                          <label class="star star-fasilitas-1" for="star-fasilitas-1"></label>
                        </div>
                      </div>

                      <div class="rating-block">
                        <div class="col s4 criteria">Kebersihan
                        </div>

                        <div class="stars">
                          <input class="star star-kebersihan-5" id="star-kebersihan-5" type="radio" name="kebersihan" value="5" />
                          <label class="star star-kebersihan-5" for="star-kebersihan-5"></label>
                          <input class="star star-kebersihan-4" id="star-kebersihan-4" type="radio" name="kebersihan" value="4" />
                          <label class="star star-kebersihan-4" for="star-kebersihan-4"></label>
                          <input class="star star-kebersihan-3" id="star-kebersihan-3" type="radio" name="kebersihan" value="3" />
                          <label class="star star-kebersihan-3" for="star-kebersihan-3"></label>
                          <input class="star star-kebersihan-2" id="star-kebersihan-2" type="radio" name="kebersihan" value="2" />
                          <label class="star star-kebersihan-2" for="star-kebersihan-2"></label>
                          <input class="star star-kebersihan-1" id="star-kebersihan-1" type="radio" name="kebersihan" value="1" />
                          <label class="star star-kebersihan-1" for="star-kebersihan-1"></label>
                        </div>
                      </div>

                      <div class="rating-block">
                        <div class="col s4 criteria">Kenyamanan
                        </div>

                        <div class="stars">
                          <input class="star star-kenyamanan-5" id="star-kenyamanan-5" type="radio" name="kenyamanan" value="5" />
                          <label class="star star-kenyamanan-5" for="star-kenyamanan-5"></label>
                          <input class="star star-kenyamanan-4" id="star-kenyamanan-4" type="radio" name="kenyamanan" value="4" />
                          <label class="star star-kenyamanan-4" for="star-kenyamanan-4"></label>
                          <input class="star star-kenyamanan-3" id="star-kenyamanan-3" type="radio" name="kenyamanan" value="3" />
                          <label class="star star-kenyamanan-3" for="star-kenyamanan-3"></label>
                          <input class="star star-kenyamanan-2" id="star-kenyamanan-2" type="radio" name="kenyamanan" value="2" />
                          <label class="star star-kenyamanan-2" for="star-kenyamanan-2"></label>
                          <input class="star star-kenyamanan-1" id="star-kenyamanan-1" type="radio" name="kenyamanan" value="1" />
                          <label class="star star-kenyamanan-1" for="star-kenyamanan-1"></label>
                        </div>
                      </div>

                      <div class="rating-block">
                        <div class="col s4 criteria">Pelayanan
                        </div>

                        <div class="stars">
                          <input class="star star-pelayanan-5" id="star-pelayanan-5" type="radio" name="pelayanan" value="5" />
                          <label class="star star-pelayanan-5" for="star-pelayanan-5"></label>
                          <input class="star star-pelayanan-4" id="star-pelayanan-4" type="radio" name="pelayanan" value="4" />
                          <label class="star star-pelayanan-4" for="star-pelayanan-4"></label>
                          <input class="star star-pelayanan-3" id="star-pelayanan-3" type="radio" name="pelayanan" value="3" />
                          <label class="star star-pelayanan-3" for="star-pelayanan-3"></label>
                          <input class="star star-pelayanan-2" id="star-pelayanan-2" type="radio" name="pelayanan" value="2" />
                          <label class="star star-pelayanan-2" for="star-pelayanan-2"></label>
                          <input class="star star-pelayanan-1" id="star-pelayanan-1" type="radio" name="pelayanan" value="1" />
                          <label class="star star-pelayanan-1" for="star-pelayanan-1"></label>
                        </div>
                      </div>

                      <div class="rating-block">
                        <div class="col s4 criteria">Harga
                        </div>

                        <div class="stars">
                          <input class="star star-harga-5" id="star-harga-5" type="radio" name="harga" value="5" />
                          <label class="star star-harga-5" for="star-harga-5"></label>
                          <input class="star star-harga-4" id="star-harga-4" type="radio" name="harga" value="4" />
                          <label class="star star-harga-4" for="star-harga-4"></label>
                          <input class="star star-harga-3" id="star-harga-3" type="radio" name="harga" value="3" />
                          <label class="star star-harga-3" for="star-harga-3"></label>
                          <input class="star star-harga-2" id="star-harga-2" type="radio" name="harga" value="2" />
                          <label class="star star-harga-2" for="star-harga-2"></label>
                          <input class="star star-harga-1" id="star-harga-1" type="radio" name="harga" value="1" />
                          <label class="star star-harga-1" for="star-harga-1"></label>
                        </div>
                      </div>
                      
                      <div class="row">
                         <div class="input-field col s9">
                          <i class="material-icons prefix">title</i>
                          <input type="text" class="validate" value="{{ old('title') }}" name="title">
                          <label>Judul Ulasan</label>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="input-field col s10">
                          <i class="material-icons prefix">mode_edit</i>
                          <textarea class="materialize-textarea" name="komentar" id="komentar">{{ old('komentar') }}</textarea>
                          @foreach($places as $item)
                          <label>Ulasan Anda tentang objek wisata {{ $item->name }} (minimal 20 karakter).</label>
                          @endforeach
                        </div>
                      </div>
          
                      <div class="row">
                        <div class="col s12 right-align">
                          <button class="btn waves-effect waves-light more-item" type="submit" name="action" name="submitReview" style="color: white">Kirim<i class="material-icons right" style="margin-top: -4px">send</i>
                          </button>
                        </div>
                      </div>
                    </form>
                  @endunless
                @endif
                @unless(Auth::check())
                  <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button><i class="material-icons left">info</i>Anda perlu masuk untuk dapat memberikan penilaian.</div>
                  
                  <p class="tabs-heading">Kriteria</p>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="place_id" value="{{ $places[0]->place_id }}">
                  <input type="hidden" name="place_prefix" value="{{ $places[0]->place_prefix }}">

                  <div class="rating-block">
                    <div class="col s4 criteria">Fasilitas</div>

                    <div class="stars">
                      <input class="star star-fasilitas-5" id="star-fasilitas-5" type="radio" name="fasilitas" value="5" disabled />
                      <label class="star star-fasilitas-5" for="star-fasilitas-5"></label>
                      <input class="star star-fasilitas-4" id="star-fasilitas-4" type="radio" name="fasilitas" value="4" disabled />
                      <label class="star star-fasilitas-4" for="star-fasilitas-4"></label>
                      <input class="star star-fasilitas-3" id="star-fasilitas-3" type="radio" name="fasilitas" value="3" disabled />
                      <label class="star star-fasilitas-3" for="star-fasilitas-3"></label>
                      <input class="star star-fasilitas-2" id="star-fasilitas-2" type="radio" name="fasilitas" value="2" disabled />
                      <label class="star star-fasilitas-2" for="star-fasilitas-2"></label>
                      <input class="star star-fasilitas-1" id="star-fasilitas-1" type="radio" name="fasilitas" value="1" disabled />
                      <label class="star star-fasilitas-1" for="star-fasilitas-1"></label>
                    </div>
                  </div>

                  <div class="rating-block">
                    <div class="col s4 criteria">Kebersihan
                    </div>

                    <div class="stars">
                      <input class="star star-kebersihan-5" id="star-kebersihan-5" type="radio" name="kebersihan" value="5" disabled />
                      <label class="star star-kebersihan-5" for="star-kebersihan-5"></label>
                      <input class="star star-kebersihan-4" id="star-kebersihan-4" type="radio" name="kebersihan" value="4" disabled />
                      <label class="star star-kebersihan-4" for="star-kebersihan-4"></label>
                      <input class="star star-kebersihan-3" id="star-kebersihan-3" type="radio" name="kebersihan" value="3" disabled />
                      <label class="star star-kebersihan-3" for="star-kebersihan-3"></label>
                      <input class="star star-kebersihan-2" id="star-kebersihan-2" type="radio" name="kebersihan" value="2" disabled />
                      <label class="star star-kebersihan-2" for="star-kebersihan-2"></label>
                      <input class="star star-kebersihan-1" id="star-kebersihan-1" type="radio" name="kebersihan" value="1" disabled />
                      <label class="star star-kebersihan-1" for="star-kebersihan-1"></label>
                    </div>
                  </div>

                  <div class="rating-block">
                    <div class="col s4 criteria">Kenyamanan
                    </div>

                    <div class="stars">
                      <input class="star star-kenyamanan-5" id="star-kenyamanan-5" type="radio" name="kenyamanan" value="5" disabled />
                      <label class="star star-kenyamanan-5" for="star-kenyamanan-5"></label>
                      <input class="star star-kenyamanan-4" id="star-kenyamanan-4" type="radio" name="kenyamanan" value="4" disabled />
                      <label class="star star-kenyamanan-4" for="star-kenyamanan-4"></label>
                      <input class="star star-kenyamanan-3" id="star-kenyamanan-3" type="radio" name="kenyamanan" value="3" disabled />
                      <label class="star star-kenyamanan-3" for="star-kenyamanan-3"></label>
                      <input class="star star-kenyamanan-2" id="star-kenyamanan-2" type="radio" name="kenyamanan" value="2" disabled />
                      <label class="star star-kenyamanan-2" for="star-kenyamanan-2"></label>
                      <input class="star star-kenyamanan-1" id="star-kenyamanan-1" type="radio" name="kenyamanan" value="1" disabled />
                      <label class="star star-kenyamanan-1" for="star-kenyamanan-1"></label>
                    </div>
                  </div>

                  <div class="rating-block">
                    <div class="col s4 criteria">Pelayanan
                    </div>

                    <div class="stars">
                      <input class="star star-pelayanan-5" id="star-pelayanan-5" type="radio" name="pelayanan" value="5" disabled />
                      <label class="star star-pelayanan-5" for="star-pelayanan-5"></label>
                      <input class="star star-pelayanan-4" id="star-pelayanan-4" type="radio" name="pelayanan" value="4" disabled />
                      <label class="star star-pelayanan-4" for="star-pelayanan-4"></label>
                      <input class="star star-pelayanan-3" id="star-pelayanan-3" type="radio" name="pelayanan" value="3" disabled />
                      <label class="star star-pelayanan-3" for="star-pelayanan-3"></label>
                      <input class="star star-pelayanan-2" id="star-pelayanan-2" type="radio" name="pelayanan" value="2" disabled />
                      <label class="star star-pelayanan-2" for="star-pelayanan-2"></label>
                      <input class="star star-pelayanan-1" id="star-pelayanan-1" type="radio" name="pelayanan" value="1" disabled />
                      <label class="star star-pelayanan-1" for="star-pelayanan-1"></label>
                    </div>
                  </div>

                  <div class="rating-block">
                    <div class="col s4 criteria">Harga
                    </div>

                    <div class="stars">
                      <input class="star star-harga-5" id="star-harga-5" type="radio" name="harga" value="5" disabled />
                      <label class="star star-harga-5" for="star-harga-5"></label>
                      <input class="star star-harga-4" id="star-harga-4" type="radio" name="harga" value="4" disabled />
                      <label class="star star-harga-4" for="star-harga-4"></label>
                      <input class="star star-harga-3" id="star-harga-3" type="radio" name="harga" value="3" disabled />
                      <label class="star star-harga-3" for="star-harga-3"></label>
                      <input class="star star-harga-2" id="star-harga-2" type="radio" name="harga" value="2" disabled />
                      <label class="star star-harga-2" for="star-harga-2"></label>
                      <input class="star star-harga-1" id="star-harga-1" type="radio" name="harga" value="1" disabled />
                      <label class="star star-harga-1" for="star-harga-1"></label>
                    </div>
                  </div>
                  
                  <div class="row">
                     <div class="input-field col s9">
                      <i class="material-icons prefix">title</i>
                      <input type="text" class="validate" value="{{ old('title') }}" name="title" disabled>
                      <label>Judul Ulasan</label>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="input-field col s10">
                      <i class="material-icons prefix">mode_edit</i>
                      <textarea class="materialize-textarea" name="komentar" id="komentar" disabled>{{ old('komentar') }}</textarea>
                      @foreach($places as $item)
                      <label>Ulasan Anda tentang objek wisata {{ $item->name }} (minimal 20 karakter).</label>
                      @endforeach
                    </div>
                  </div>
      
                  <div class="row">
                    <div class="col s12 right-align">
                      <button class="btn waves-effect waves-light more-item" id="auth" onclick="document.getElementById('authentication').style.display='block'" style="color: white;">Kirim<i class="material-icons right" style="margin-top: -4px;">send</i>
                      </button>
                    </div>
                  </div>
                @endunless
              </div>
            </div>

            <div id="ulasan" class="col s12 white tabs-bottom">
              <div class="tabs-div">
                <div class="col s12">
                  @if ( !$reviews->count() )
                  <p>Belum ada ulasan pada objek wisata {{ $item->name }}. Jadilah yang pertama mengulas.</p>
                  @else
                  <div>
                    @foreach( $reviews as $rvw )
                    <div class="list-group" style="margin-left: -35px; margin-right: -35px;">
                      <div class="list-group-item">
                        <div style="display: inline-block;">
                          <img class="circle responsive-img comment-img" src="{{ URL::to('/') }}/images/{{ $rvw->author->avatar }}" alt="{{ $rvw->author->name }}" style="float: left;"></img>
                          <div class="comment-name"><b>{{ $rvw->author->name }}</b></div>
                          <div class="comment-date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $rvw->created_at)->format('M d, Y \p\u\k\u\l h:i a') }}</div>
                          <?php
                            $totalRating = round((($rvw->facility * $fasilitas) + ($rvw->cleanliness * $kebersihan) + ($rvw->comfort * $kenyamanan) + ($rvw->service * $pelayanan) + ($rvw->price * $harga)),1)
                          ?>

                          @if($totalRating > 0 AND $totalRating < 1)
                            <div class="rated" style="float: left;">MENILAI<span class="badge rate" style="color: #fff; background-color: #d11717;">{{ $totalRating }}</span></div>
                          @elseif($totalRating >= 1 AND $totalRating < 2)
                            <div class="rated" style="float: left;">MENILAI<span class="badge rate" style="color: #fff; background-color: red;">{{ $totalRating }}</span></div>
                          @elseif($totalRating >= 2 AND $totalRating < 3)
                            <div class="rated" style="float: left;">MENILAI<span class="badge rate" style="color: #fff; background-color: #F62;">{{ $totalRating }}</span></div>
                          @elseif($totalRating >= 3 AND $totalRating < 4)
                            <div class="rated" style="float: left;">MENILAI<span class="badge rate" style="color: #fff; background-color: #ff9e20;">{{ $totalRating }}</span></div>
                          @elseif($totalRating >= 4 AND $totalRating < 5)
                            <div class="rated" style="float: left;">MENILAI<span class="badge rate" style="color: #fff; background-color: #efb915;">{{ $totalRating }}</span></div>
                          @elseif($totalRating == 5)
                            <div class="rated" style="float: left;">MENILAI<span class="badge rate" style="color: #fff; background-color: #F9BC00;">{{ $totalRating }}</span></div>
                          @endif

                        </div>
                      </div>
                      <div class="list-group-item">
                        <p style="font-size: 20px; overflow-wrap: break-word;"><b><i>{!! $rvw->title !!}</i></b></p>
                        <p style="overflow-wrap: break-word;">{!! $rvw->body !!}</p>
                      </div>

                      <?php
                        $comments = $rvw->comments->where('prefix',$rvw->prefix)->sortByDesc('created_at');
                      ?>
                      @if(count($comments) > 0)
                      <div class="list-group-item comment-backgroud" style="background-color: #F9FAFC;">
                        <ul style="list-style: none; padding: 0">
                          @foreach($comments as $comment)
                            <img class="responsive-img comment-img-sm" src="{{ URL::to('/') }}/images/{{ $comment->author->avatar }}" alt="{{ $comment->author->name }}"></img>
                            <div class="comment-name-sm"><b>{{ $comment->author->name }}</b></div>
                            <div class="comment-date-sm">{{ $comment->created_at->format('M d, Y \p\u\k\u\l h:i a') }}</div>
                            <div class="comment-content-sm" style="overflow-wrap: break-word;">{!! $comment->body !!}</div>
                            <br>
                          @endforeach
                        </ul>
                      </div>
                      @endif

                      <div class="list-group-item">
                        <div class="panel-body review-body">
                          <form method="post" action="/comment" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left input_mask textarea-cmt" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="on_post" value="{{ $rvw->id }}">
                            <input type="hidden" name="prefix" value="{{ $rvw->prefix }}">
                            <input type="hidden" name="place_prefix" value="{{ $places[0]->place_prefix }}">
                            <div class="form-group">
                              <textarea required="required" placeholder="Tulis komentar Anda disini..." name = "body" class="form-control" id="txtComment" style="resize: none;"></textarea>
                            </div>

                            @if(Auth::check())
                              <button type="submit" class="btn right send-btn" name="post_comment" id="post_comment" style="color: white">
                                <i class="material-icons">send</i>
                              </button>
                            @endif
                            @unless(Auth::check())
                              <button type="submit" class="btn right send-btn" name="post_comment" id="post_comment" style="color: white" disabled>
                                <i class="material-icons">send</i>
                              </button>
                            @endunless
                          </form>
                        </div>
                        
                      </div>

                    </div>
                    @endforeach
                    
                  </div>
                  @endif
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="col s12 m4"> -->
          <div class="col-xs-12 col-lg-4">
            <div class="card-panel white w3-round-large">
              <p style="font-family: 'Comic Sans MS'; font-size: 24px;">Ringkasan</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">
              <p>Penilaian Keseluruhan</p>
              <div style="display: inline-block;">
                @if($avgRate > 0 AND $avgRate < 1)
                  <span  data-position="right" data-delay="1" data-tooltip="{{ $avgRate }} dari 5" class="badge2 rate tooltipped" style="color: #fff; background-color: #d11717;">{{ $avgRate }}</span>
                @elseif($avgRate >= 1 AND $avgRate < 2)
                  <span  data-position="right" data-delay="1" data-tooltip="{{ $avgRate }} dari 5" class="badge2 rate tooltipped" style="color: #fff; background-color: red;">{{ $avgRate }}</span>
                @elseif($avgRate >= 2 AND $avgRate < 3)
                  <span  data-position="right" data-delay="1" data-tooltip="{{ $avgRate }} dari 5" class="badge2 rate tooltipped" style="color: #fff; background-color: #F62;">{{ $avgRate }}</span>
                @elseif($avgRate >= 3 AND $avgRate < 4)
                  <span  data-position="right" data-delay="1" data-tooltip="{{ $avgRate }} dari 5" class="badge2 rate tooltipped" style="color: #fff; background-color: #ff9e20;">{{ $avgRate }}</span>
                @elseif($avgRate >= 4 AND $avgRate < 5)
                  <span  data-position="right" data-delay="1" data-tooltip="{{ $avgRate }} dari 5" class="badge2 rate tooltipped" style="color: #fff; background-color: #efb915;">{{ $avgRate }}</span>
                @elseif($avgRate == 5)
                  <span  data-position="right" data-delay="1" data-tooltip="{{ $avgRate }} dari 5" class="badge2 rate tooltipped" style="color: #fff; background-color: #F9BC00;">{{ $avgRate }}<small><sup>&#11089;</sup></small></span>
                @endif
              </div>
              @foreach($location as $l)
              <div style="padding-top: 15px;">
                <i class="fa fa-map-marker"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ $l->location_name }}
              </div>
              @endforeach
              @foreach($category as $c)
              <div>
                <i class="fa fa-tags"></i>&nbsp;&nbsp;{{ $c->category_name }}
              </div>
              @endforeach
              @if($counts == 0)
                <p style="padding-top: 10px">Objek wisata ini belum pernah diulas.</p>
              @else
                <p style="padding-top: 10px">Diulas {{ $counts }} kali oleh
                  
                  <a class="tooltipped" style="color: blue;" data-position="right" data-delay="1" data-tooltip="<?php $reviewerArray = array(); foreach($reviewerName as $rn) { $reviewerArray[] = $rn->name; } $ok = implode(', ', $reviewerArray) ?> {{ $ok }}." rel="tooltip" >{{ $totalReviewer }} orang.</a>
                </p>
              @endif
            </div>

            @if(count($recommended) > 0)
              <div class="card-panel white w3-round-large">
                <p style="font-family: 'Comic Sans MS'; font-size: 20px; overflow-wrap: break-word;">Direkomendasikan untuk Pergi</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">

                <div class="row">
                  @foreach($recommended as $rcm)
                    <div class="col s4">
                      <a href="{{ URL::to('/place/'.$rcm->place_prefix) }}">
                        @if(strpos($rcm->image,'https://') !== false)
                          <img src="{{$rcm->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @else
                          <img src="{{ URL::to('/') }}/images/{{$rcm->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @endif
                      </a>
                    </div>
                  @endforeach

                  <a href="{{ URL::to('/recommended') }}" style="float: right; margin-top: 20px; margin-bottom: -30px; font-weight: bold; font-family: 'Arial'">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                </div>
                
              </div>
            @endif

            @if(count($weeklyReview) > 0)
              <div class="card-panel white w3-round-large">
                <p style="font-family: 'Comic Sans MS'; font-size: 20px;">Ulasan Minggu Ini</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">

                <div class="row">
                  @foreach($weeklyReview as $wr)
                    <div class="col s4">
                      <a href="{{ URL::to('/place/'.$wr->place_prefix) }}">
                        @if(strpos($wr->image,'https://') !== false)
                          <img src="{{$wr->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @else
                          <img src="{{ URL::to('/') }}/images/{{$wr->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @endif
                      </a>
                    </div>
                  @endforeach

                  <a href="{{ URL::to('/weekly-review') }}" style="float: right; margin-top: 20px; margin-bottom: -30px; font-weight: bold; font-family: 'Arial'">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                </div>
                
              </div>
            @endif

            @if(count($weeklyAdded) > 0)
              <div class="card-panel white w3-round-large">
                <p style="font-family: 'Comic Sans MS'; font-size: 20px;">Objek Wisata Baru</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">

                <div class="row">
                  @foreach($weeklyAdded as $wa)
                    <div class="col s4">
                      <a href="{{ URL::to('/place/'.$wa->place_prefix) }}">
                        @if(strpos($wa->image,'https://') !== false)
                          <img src="{{$wa->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @else
                          <img src="{{ URL::to('/') }}/images/{{$wa->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @endif
                      </a>
                    </div>
                  @endforeach

                  <a href="{{ URL::to('/weekly-added') }}" style="float: right; margin-top: 20px; margin-bottom: -30px; font-weight: bold; font-family: 'Arial'">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                </div>
                
              </div>
            @endif

            @if(count($notReviewedYet) > 0)
              <div class="card-panel white w3-round-large">
                <p style="font-family: 'Comic Sans MS'; font-size: 20px;">Belum Diulas</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">

                <div class="row">
                  @foreach($notReviewedYet as $nry)
                    <div class="col s4">
                      <a href="{{ URL::to('/place/'.$nry->place_prefix) }}">
                        @if(strpos($nry->image,'https://') !== false)
                          <img src="{{$nry->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @else
                          <img src="{{ URL::to('/') }}/images/{{$nry->image}}" class="w3-round-large responsive-img" style="width:90px;height:90px;">
                        @endif
                      </a>
                    </div>
                  @endforeach

                  <a href="{{ URL::to('/not-reviewed-yet') }}" style="float: right; margin-top: 20px; margin-bottom: -30px; font-weight: bold; font-family: 'Arial'">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                </div>
                
              </div>
            @endif

            @if($countbmk > 0)
              @if(Auth::check())
                <div class="card-panel white w3-round-large">
                  <p style="font-family: 'Comic Sans MS'; font-size: 18px;">Objek wisata yang Anda tandai</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">
                    @foreach($bmk as $bk)
                      <div class="chip">
                        @if(strpos($bk->image,'https://') !== false)
                          <img src="{{$bk->image}}">
                        @else
                          <img src="{{ URL::to('/') }}/images/{{$bk->image}}">
                        @endif
                        {{ $bk->name }}
                      </div>
                    @endforeach
                </div>
              @endif
            @else
              @if(Auth::check())
                <div class="card-panel white w3-round-large">
                  <p style="font-family: 'Comic Sans MS'; font-size: 18px;">Objek wisata yang Anda tandai</p><hr style="margin-top: -1px; border: solid 1px dimgrey; ">
                    <p>Tidak ada objek wisata yang Anda tandai.</p>
                </div>
              @endif
            @endif

          </div>
        @endforeach
      </div>
    </div>
  </div>

  @push('scripts')

  <script type="text/javascript" src="{{ asset("js/jquery.autogrow-textarea.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.cookie-text-size.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.dom-builder.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.input-hint.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.linify.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.rollover.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.text-effects.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/jquery.unselectable.js") }}"></script>
  <script src="{{ asset("js/jquery.rateyo.js") }}"></script>

  <!-- Parsley -->
  <script src="{{ asset("js/parsley.min.js") }}"></script>

  <script>
    $(document).ready(function(){
        $("[rel=tooltip]").tooltip({ placement: 'right'});
        $("[rel=tooltip-top]").tooltip({ placement: 'top'});
    });

    $(function () {
      $("#fac").rateYo({
        rating: {{ $fattr }},
        readOnly: true,
        spacing: "2px",
        starWidth: "25px",
        starHeight: "25px"
      });
      $("#cle").rateYo({
        rating: {{ $cattr }},
        readOnly: true,
        spacing: "2px",
        starWidth: "25px",
        starHeight: "25px"
      });
      $("#com").rateYo({
        rating: {{ $comattr }},
        readOnly: true,
        spacing: "2px",
        starWidth: "25px",
        starHeight: "25px"
      });
      $("#ser").rateYo({
        rating: {{ $sattr }},
        readOnly: true,
        spacing: "2px",
        starWidth: "25px",
        starHeight: "25px"
      });
      $("#pri").rateYo({
        rating: {{ $pattr }},
        readOnly: true,
        spacing: "2px",
        starWidth: "25px",
        starHeight: "25px"
      });
    });

    $(function() {
      $('#txtComment').autogrow();
      $('#txtComment').css('overflow', 'hidden').autogrow();
    });

    $('#form-bookmark').submit(function(event) {
      event.preventDefault();
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer" style="border-color: #ffffff;"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: "POST",
        url: "{{ URL::to('bookmark/place') }}",
        <?php if(Auth::user()) {?>
        data: {user_id: '{{ Auth::user()->id }}', place_id: '{{ $id_place }}', _token:"<?php echo csrf_token(); ?>"},
        <?php } ?>
        success: function (response) {
          // var message = '';
          
          // if(response.error){
          // }else if(response.success){
          //   isProcessing = false;
          // }
        },
        error:function(xhr, status, error){
          var response = xhr.responseJSON;
          redirect(response.error.message.warning);
        },
        complete:function(){
          isProcessing = false;
          Materialize.toast('Ditandai.', 4000, 'rounded');
          $('.preloader-wrapper').fadeOut();
          document.getElementById("submitBookmark").style.pointerEvents = "none";
          document.getElementById("submitBookmark").style.background ='#63a7dd';
          // $('#submitBookmark').attr('disabled','disabled');
          $('#submitBookmark').html('<i class="material-icons left top-4">turned_in</i>Ditandai');
        }
      });
    });
  </script>
  @endpush
@endsection