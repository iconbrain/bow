@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>

@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
    <title>Tidak Ditemukan | BOW</title>
@endsection

@section('content')
  <div class="container container-fluid">
    <div class="row">
      <div class="top110">
        <div class="col s12 m8">
          <div class="carousel top-inv110">
            <a class="carousel-item" href="#one!"><img src="{{ URL::to('/') }}/images/carousel_farmhouse.jpeg" class="img-responsive" alt=""></a>
            <a class="carousel-item" href="#two!"><img src="{{ URL::to('/') }}/images/carousel_kawahputih.jpeg" class="img-responsive" alt=""></a>
            <a class="carousel-item" href="#three!"><img src="{{ URL::to('/') }}/images/carousel_transstudio.jpg" class="img-responsive" alt=""></a>
            <a class="carousel-item" href="#four!"><img src="{{ URL::to('/') }}/images/carousel_deranch.jpg" class="img-responsive" alt=""></a>
            <a class="carousel-item" href="#five!"><img src="{{ URL::to('/') }}/images/carousel_tebingkraton.jpeg" class="img-responsive" alt=""></a>
          </div>
        </div>
        <div class="col s12 m4 bot110">
            <h2 class="text-muted">Tidak Ditemukan...</h2><br>
            <p class="place-card-subtitle">Objek wisata yang Anda cari tidak ditemukan dalam sistem kami. Silahkan mencari objek wisata lain.</p>
            <p class="place-card-subtitle">Jika terdapat objek wisata Bandung yang tidak ada dalam sistem kami, Anda dapat menambahkan objek wisata tersebut.</p><br>
            <a class="place-card-subtitle" style="color: #ff5c33;" href="{{ URL::to('/places/add') }}">Tambah Objek Wisata</a>
        </div>
      </div>
    </div>

    <div class="notfound-2">
      <div class="row">
        <div class="col s12 m6 notfound-2-left">
          <h2 class="text-muted">Bingung mencari objek wisata di Bandung?</h2><br>
          <p class="place-card-subtitle">BOW adalah solusi yang tepat atas permasalahan Anda.</p><br>
          <p class="place-card-subtitle">BOW menyediakan rekomendasi objek-objek wisata di Bandung yang menarik untuk dikunjungi, lengkap dengan detail dan review dari masing-masing objek wisata.</p>
        </div>
        <div class="col s12 m6">
          <div class="carousel carousel-slider center" data-indicators="true">
            <div class="carousel-fixed-item center">
            </div>
            <div class="carousel-item blue white-text" href="#one!">
              <h2>Direkomendasikan Untuk Pergi</h2>
              <p class="white-text">Temukan objek wisata favorit di BOW</p><br><br><br>
              <p class="white-text">Bingung memilih objek wisata mana yang menarik untuk dikunjungi ketika Anda berlibur di Bandung? BOW memberikan rekomendasi untuk Anda, dan diurutkan berdasarkan rating yang paling tinggi.</p><br><br>
              <a class="btn waves-effect white black-text darken-text-2" href="{{ URL::to('/recommended') }}">telusuri</a>
            </div>
            @if(count($weeklyReview) > 0)
            <div class="carousel-item amber white-text" href="#two!">
              <h2>Ulasan Minggu Ini</h2>
              <p class="white-text">Objek wisata yang diulas dalam minggu ini</p><br><br><br>
              <p class="white-text">Temukan semua objek wisata yang diulas dalam minggu ini. Anda juga dapat memberikan komentar pada setiap ulasan yang terdapat di BOW. Tunggu apalagi?</p><br><br>
              <a class="btn waves-effect white black-text darken-text-2" href="{{ URL::to('/weekly-review') }}">telusuri</a>
            </div>
            @endif
            @if(count($weeklyAdded) > 0)
            <div class="carousel-item green white-text" href="#three!">
              <h2>Objek Wisata Baru</h2>
              <p class="white-text">Objek wisata yang baru ditambahkan di BOW</p><br><br><br>
              <p class="white-text">Temukan objek wisata di Bandung yang baru kami tambahkan di BOW pada 1 minggu terakhir. Berilah ulasan jika Anda sudah mengunjungi objek wisata tersebut.</p><br><br>
              <a class="btn waves-effect white black-text darken-text-2" href="{{ URL::to('/weekly-added') }}">telusuri</a>
            </div>
            @endif
            @if(count($notReviewedYet) > 0)
            <div class="carousel-item red white-text" href="#four!">
              <h2>Belum Diulas</h2>
              <p class="white-text">Objek wisata yang belum pernah diulas</p><br><br><br>
              <p class="white-text">Terdapat sejumlah objek wisata yang belum pernah diulas di BOW di minggu ini. Anda familiar dan pernah berkunjung ke objek wisata tersebut? Berikan ulasan Anda!</p><br><br>
              <a class="btn waves-effect white black-text darken-text-2" href="{{ URL::to('/not-reviewed-yet') }}">telusuri</a>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>

  </div>

  @push('scripts')
  <script>
    $(document).ready(function(){
      $('.carousel').carousel();
    });
    $('.carousel.carousel-slider').carousel({fullWidth: true});
  </script>
  @endpush
@endsection