@extends('Frontend.layouts.master')

<link rel="stylesheet" href="../../css/materialize.css">
<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css"  media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">
@include('Frontend.includes.header')

@section('title')
<title>{{ $categories[0]->category_name }} | BOW</title>
@endsection

@section('content')
  <div class="w3-light-grey">
    <div class="container little-div">
      <div class="row">
        <h2 class="middle top-title comicms">{{ $categories[0]->category_name }} di Bandung</h2>
        
        <div class="col-xs-12 col-lg-3">
          <div class="card-panel white w3-round-large">
            <h3 class="middle">Saring <i id="loading" class="material-icons"></i></h3><hr>
            <b class="saringan">Pilih Kota</b><br>
            <div class="top10">
              <?php $i=1; ?>
              @foreach($locations as $loc)
                @if($categories[0]->category_id ==1)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocAlam" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==2)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocReligi" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==3)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocSejarah" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==4)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocBelanja" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==5)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocSatwa" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==6)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocKuliner" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==7)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocAgro" . $i} }})</small></label><br>
                  </div>
                @elseif($categories[0]->category_id ==8)
                  <div class="left10">
                    <input type="checkbox" name="cities[]" value="{{ $loc->location_id }}" id="filter-city-{{ $loc->location_id }}" />
                    <label for="filter-city-{{ $loc->location_id }}" class="fil-city">{{ $loc->location_name }} <small class="greycolor"> ({{ ${"sumlocKeluarga" . $i} }})</small></label><br>
                  </div>
                @endif
                <?php $i++ ?>
              @endforeach
            </div>
            
            <button id="btnCityAll" style="border: none; background: none;" onclick="filterCity(100)">Semua <small class="greycolor">({{ $totalCity->totalCity }})</small></button><br><br>

            <b class="saringan">Pilih Harga</b><br>
            <div class="top10">
              @foreach($price_range as $pr)
                <div class="left10">
                  <input type="checkbox" name="prices[]" value="{{ $pr->id }}" id="filter-price-{{ $pr->id }}" />
                  <label for="filter-price-{{ $pr->id }}" class="fil-city">{{ $pr->price_range }}</label><br>
                </div>
              @endforeach
            </div>
            <br><br>

            <h3 class="middle">Sortir <i id="loading" class="material-icons"></i></h3><hr>
            <b class="saringan">Ulasan</b><br>
            <button id="btnFilterCity" style="border: none; background: none; margin-top: 10px;" onclick="sortReview(1)">Banyak ke Sedikit</button><br>
            <button id="btnFilterCity2" style="border: none; background: none;" onclick="sortReview(2)">Sedikit ke Banyak</button><br><br>

            <b class="saringan">Kriteria</b><br>
            <button id="btnFilterCity3" style="border: none; background: none; margin-top: 10px;" onclick="sortCriteria(1)">Fasilitas <small class="greycolor">Tinggi ke Rendah</small></button><br>
            <button id="btnFilterCity4" style="border: none; background: none;" onclick="sortCriteria(2)">Kebersihan <small class="greycolor">Tinggi ke Rendah</small></button><br>
            <button id="btnFilterCity5" style="border: none; background: none;" onclick="sortCriteria(3)">Kenyamanan <small class="greycolor">Tinggi ke Rendah</small></button><br>
            <button id="btnFilterCity6" style="border: none; background: none;" onclick="sortCriteria(4)">Pelayanan <small class="greycolor">Tinggi ke Rendah</small></button><br>
            <button id="btnFilterCity7" style="border: none; background: none;" onclick="sortCriteria(5)">Harga <small class="greycolor">Tinggi ke Rendah</small></button><br>
          </div>
        </div>

        @if(count($places) > 0)
          <div id="cityAll">
            <div class="col-xs-12 col-lg-6">
              @foreach($places as $item)
                <div class="card horizontal w3-round-large">
                  <div class="card-stacked">
                      <div class="card-content top-custom-card">
                        <a href="{{ URL::to('/place/'.$item->place_prefix) }}">
                          @if(strpos($item->image,'https://') !== false)
                            <img src="{{$item->image}}" class="w3-round-large img-filtered">
                          @else
                            <img src="{{ URL::to('/') }}/images/{{$item->image}}" class="w3-round-large img-filtered">
                          @endif
                        </a>

                        @if($item->avgRating > 0 AND $item->avgRating < 1)
                          <span  data-position="right" class="badge" style="color: #fff; background-color: #d11717; border-radius: 5px;">{{ $item->avgRating }}</span>
                        @elseif($item->avgRating >= 1 AND $item->avgRating < 2)
                          <span  data-position="right" class="badge" style="color: #fff; background-color: red; border-radius: 5px;">{{ $item->avgRating }}</span>
                        @elseif($item->avgRating >= 2 AND $item->avgRating < 3)
                          <span  data-position="right" class="badge" style="color: #fff; background-color: #F62; border-radius: 5px;">{{ $item->avgRating }}</span>
                        @elseif($item->avgRating >= 3 AND $item->avgRating < 4)
                          <span  data-position="right" class="badge" style="color: #fff; background-color: #ff9e20; border-radius: 5px;">{{ $item->avgRating }}</span>
                        @elseif($item->avgRating >= 4 AND $item->avgRating < 5)
                          <span  data-position="right" class="badge" style="color: #fff; background-color: #efb915; border-radius: 5px;">{{ $item->avgRating }}</span>
                        @elseif($item->avgRating == 5)
                          <span  data-position="right" class="badge" style="color: #fff; background-color: #F9BC00; border-radius: 5px;">{{ $item->avgRating }}</span>
                        @endif

                        <h4 class="categories-title left120"><a href="{{ URL::to('/place/'.$item->place_prefix) }}" class="a-custom">{{ $item->name }}</a></h4>
                        <a href="{{ URL::to('/place/'.$item->place_prefix) }}"><p style="color:gray;margin-left: 120px;">{{ $item->address }}</p></a>
                        
                        @if(Auth::check())
                          @foreach ($bmk as $bm)
                            @if($bm->place_id == $item->place_id)
                              <i class="material-icons" style="float: right; color: #019987">turned_in</i>
                            @endif
                          @endforeach
                        @endif
                      </div>
                      <div class="card-action">
                        <p align="justify">
                          <?php
                            $excerpt = getExcerpt($item->description, 0, 290);
                          ?>
                          {!! $excerpt !!}
                        </p>
                      </div>
                      <div class="card-action">
                        {{ $item->location_name }}
                        <i class="material-icons left greycolor">location_on</i><br>
                        <div style="display: inline-flex;">
                          <i class="material-icons left greycolor">schedule</i>
                          <div class="col s12" style="margin-left:-10px;">{!! $item->operational_hours !!}</div>
                        </div><br>
                        @if($item->phone_number == NULL) <i class="material-icons left greycolor">perm_phone_msg</i>(Belum Tersedia)
                        @else<i class="material-icons left greycolor">perm_phone_msg</i>{{ $item->phone_number }}@endif<br>
                        <div class="nulpad">
                          <i class="material-icons left greycolor">attach_money</i>
                        </div>
                        <div class="col s10" style="margin-left:-10px;">{!! $item->price !!}</div><br>
                      </div>
                      <div class="card-action">
                        <div style="float: left;">
                          <i class="material-icons left greycolor">rate_review</i>{{ $item->sumReview }} ulasan
                        </div>
                        <div style="float: right;">
                          <i class="material-icons left greycolor">bookmark</i>{{ $item->sumBookmark }} ditandai
                        </div>
                      </div>
                  </div>
                </div>
              @endforeach

              {{ $places->links() }}
            </div>
          </div>

          <div class="col-xs-12 col-lg-3">
            <div class="card horizontal w3-round-large">
              <div class="card-stacked">
                <div class="card-content top-custom-card">
                  <h2 style="font-size: 18px; font-family: 'Comic Sans MS'">Bandung Objek Wisata</h2>
                  <p style="color: grey; font-size: 13px;">Merekomendasikan objek-objek wisata di Bandung.</p>
                  <hr>

                  <div class="col s12">
                    <div class="row">
                      <img src="{{ URL::to('/') }}/images/bow_box.png" style="width:40px;height:40px; float: right; margin-top: 25px; margin-bottom: 5px;" id="an">
                      <img src="{{ URL::to('/') }}/images/category_sidebar.png" class="responsive-img">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
            @if(count($recommended) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Direkomendasikan untuk Pergi</h2>
                    <p style="color: grey; font-size: 13px;">Temukan objek wisata menarik untuk dikunjungi di Bandung.</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($recommended as $rcm)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$rcm->place_prefix) }}">
                              @if(strpos($rcm->image,'https://') !== false)
                                <img src="{{$rcm->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @else
                                <img src="{{ URL::to('/') }}/images/{{$rcm->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @endif
                            </a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/recommended') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          
            @if(count($weeklyReview) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Ulasan Minggu Ini</h2>
                    <p style="color: grey; font-size: 13px;">Temukan objek wisata yang diulas dalam minggu ini (H-7 dari hari ini).</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($weeklyReview as $wr)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$wr->place_prefix) }}">
                              @if(strpos($wr->image,'https://') !== false)
                                <img src="{{$wr->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @else
                                <img src="{{ URL::to('/') }}/images/{{$wr->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @endif
                            </a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/weekly-review') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          
            @if(count($weeklyAdded) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Objek Wisata Baru</h2>
                    <p style="color: grey; font-size: 13px;">Objek wisata yang baru ditambahkan dalam satu minggu terakhir.</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($weeklyAdded as $wa)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$wa->place_prefix) }}">
                              @if(strpos($wa->image,'https://') !== false)
                                <img src="{{$wa->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @else
                                <img src="{{ URL::to('/') }}/images/{{$wa->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @endif
                            </a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/weekly-added') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          
            @if(count($notReviewedYet) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Belum Diulas</h2>
                    <p style="color: grey; font-size: 13px;">Berikan ulasan pada objek wisata yang belum pernah diulas di BOW.</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($notReviewedYet as $nry)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$nry->place_prefix) }}">
                              @if(strpos($nry->image,'https://') !== false)
                                <img src="{{$nry->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @else
                                <img src="{{ URL::to('/') }}/images/{{$nry->image}}" class="w3-round-large" style="width:60px;height:60px;">
                              @endif
                            </a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/not-reviewed-yet') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          </div>
        @else
          <div class="col-xs-12 col-lg-6">
            <div class="row">
              <div class="col s12 m9 offset-l2">
                <div class="card-panel teal">
                  <span class="white-text">
                    <h3 style="text-align:center;">Tidak Tersedia</h3>
                    <p style="text-align:center;">Mohon pilih kategori lain.</p>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-lg-3">
            <div class="card horizontal w3-round-large">
              <div class="card-stacked">
                <div class="card-content top-custom-card">
                  <h2 style="font-size: 18px; font-family: 'Comic Sans MS';">Bandung Objek Wisata</h2>
                  <p style="color: grey; font-size: 13px;">Merekomendasikan objek-objek wisata di Bandung.</p>
                  <hr>
                  <img src="{{ URL::to('/') }}/images/bow_box.png" style="width:40px;height:40px; float: right; margin-top: 25px; margin-bottom: 5px;" id="an">
                  <img src="{{ URL::to('/') }}/images/category_sidebar.png" style="width:220px;height:140px;">
                </div>
              </div>
            </div>
          
            @if(count($recommended) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Direkomendasikan untuk Pergi</h2>
                    <p style="color: grey; font-size: 13px;">Temukan objek wisata menarik untuk dikunjungi di Bandung.</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($recommended as $rcm)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$rcm->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$rcm->image}}" class="w3-round-large" style="width:60px;height:60px;"></a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/recommended') }}" style="float: right; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          
            @if(count($weeklyReview) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Ulasan Minggu Ini</h2>
                    <p style="color: grey; font-size: 13px;">Temukan objek wisata yang diulas dalam minggu ini (H-7 dari hari ini).</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($weeklyReview as $wr)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$wr->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$wr->image}}" class="w3-round-large" style="width:60px;height:60px;"></a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/weekly-review') }}" style="float: right; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          
            @if(count($weeklyAdded) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Objek Wisata Baru</h2>
                    <p style="color: grey; font-size: 13px;">Objek wisata yang baru ditambahkan dalam satu minggu terakhir.</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($weeklyAdded as $wa)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$wa->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$wa->image}}" class="w3-round-large" style="width:60px;height:60px;"></a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/weekly-added') }}" style="float: right; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          
            @if(count($notReviewedYet) > 0)
              <div class="card horizontal w3-round-large">
                <div class="card-stacked">
                  <div class="card-content top-custom-card">
                    <h2 class="saringan">Belum Diulas</h2>
                    <p style="color: grey; font-size: 13px;">Berikan ulasan pada objek wisata yang belum pernah diulas di BOW.</p>
                    <hr>

                    <div class="col s12">
                      <div class="row" style="margin-left: -22px;">
                        @foreach($notReviewedYet as $nry)
                          <div class="col s4">
                            <a href="{{ URL::to('/place/'.$nry->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$nry->image}}" class="w3-round-large" style="width:60px;height:60px;"></a>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="{{ URL::to('/not-reviewed-yet') }}" style="float: right; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
                  </div>
                </div>
              </div>
            @endif
          </div>
        @endif

      </div>
    </div>
  </div>

  <style type="text/css">
    #btnFilterCity:focus {
        color: red;
    }
    #btnFilterCity2:focus {
        color: red;
    }
    #btnFilterCity3:focus {
        color: red;
    }
    #btnFilterCity4:focus {
        color: red;
    }
    #btnFilterCity5:focus {
        color: red;
    }
    #btnFilterCity6:focus {
        color: red;
    }
    #btnFilterCity7:focus {
        color: red;
    }
    #btnCityAll:focus {
        color: red;
    }
    #btnFilterCity:hover {
        color: teal;
    }
    #btnFilterCity2:hover {
        color: red;
    }
    #btnFilterCity3:hover {
        color: red;
    }
    #btnFilterCity4:hover {
        color: red;
    }
    #btnFilterCity5:hover {
        color: red;
    }
    #btnFilterCity6:hover {
        color: red;
    }
    #btnFilterCity7:hover {
        color: red;
    }
    hr {
      display: block;
      height: 1px;
      border: 0;
      border-top: 1px solid #ccc;
      margin: 0.5em 0;
      padding: 0; 
    }

    #an {
      -webkit-animation-duration: 3s;
      -webkit-animation-delay: 2s;
      -webkit-animation-iteration-count: infinite;

      -moz-animation-duration: 3s;
      -moz-animation-delay: 2s;
      -moz-animation-iteration-count: infinite;

      -ms-animation-duration: 3s;
      -ms-animation-delay: 2s;
      -ms-animation-iteration-count: infinite;

      -o-animation-duration: 3s;
      -o-animation-delay: 2s;
      -o-animation-iteration-count: infinite;

      animation-duration: 3s;
      animation-delay: 2s;
      animation-iteration-count: infinite;
    }
  </style>

  @push('scripts')
  <script>
    $('#an').addClass('animated infinite bounce');
  </script>

  <script>
    // $(document).ready(function () {
      var cities = [];
      var prices = [];

      $('input[name="cities[]"]').on('change', function (e) {
        e.preventDefault();
        cities = []; // reset 

        $('input[name="cities[]"]:checked').each(function()
        {
            cities.push($(this).val());
        });

        $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

        $.ajax({
          type: 'POST',
          url: "{{ URL::to('/category/filter-city') }}",
          data: { cities : cities, prices : prices, prefix : '{{ $categories[0]->prefix }}', _token : <?php echo "'" . csrf_token() . "'"; ?> },
          success: function(data) {
            $('#cityAll').html(data);
          },
          error: function(data) {
            console.log(data);
          },
          complete:function(){
            $('.preloader-wrapper').fadeOut();
          }
        });
      });

      $('input[name="prices[]"]').on('change', function (e) {
        e.preventDefault();
        prices = []; // reset 

        $('input[name="prices[]"]:checked').each(function()
        {
            prices.push($(this).val());
        });

        $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

        $.ajax({
          type: 'POST',
          url: "{{ URL::to('/category/filter-city') }}",
          data: { cities : cities, prices : prices, prefix : '{{ $categories[0]->prefix }}', _token : <?php echo "'" . csrf_token() . "'"; ?> },
          success: function(data) {
            $('#cityAll').html(data);
          },
          error: function(data) {
            console.log(data);
          },
          complete:function(){
            $('.preloader-wrapper').fadeOut();
          }
        });
      });

      $("#btnCityAll").click(function(e){
        $("#filter-city-1").prop("checked", true);
        $('#filter-city-2').prop('checked', true);
        $('#filter-city-3').prop('checked', true);
        $('#filter-city-4').prop('checked', true);
        $('#filter-city-5').prop('checked', true);
        $('#filter-price-1').prop('checked', true);
        $('#filter-price-2').prop('checked', true);
        $('#filter-price-3').prop('checked', true);
        $('#filter-price-4').prop('checked', true);
        $('#filter-price-5').prop('checked', true);
        $('#filter-price-6').prop('checked', true);
        $('#filter-price-7').prop('checked', true);
        $('#filter-price-8').prop('checked', true);
      });
    // });


    function filterCity(id) {
      cities = [1,2,3,4,5];
      prices = [1,2,3,4,5,6,7,8];
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-city') }}",
        data: { location_id : id, prefix : '{{ $categories[0]->prefix }}', _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#cityAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    function sortReview(id) {
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-city') }}",
        data: { cities: cities, prices: prices, sort_review_id : id, prefix : '{{ $categories[0]->prefix }}', _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#cityAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    function sortCriteria(id) {
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-city') }}",
        data: { cities: cities, prices: prices, sort_criteria_id : id, prefix : '{{ $categories[0]->prefix }}', _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#cityAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    $(document).ready(function() {
      document.getElementById('btnCityAll').focus();
    });
  </script>
  @endpush
@endsection
<?php
function getExcerpt($str, $startPos=0, $maxLength=100) {
  if(strlen($str) > $maxLength) {
    $excerpt   = substr($str, $startPos, $maxLength-3);
    $lastSpace = strrpos($excerpt, ' ');
    $excerpt   = substr($excerpt, 0, $lastSpace);
    $excerpt  .= '...';
    } else {
        $excerpt = $str;
    }

    return $excerpt;
}
?>