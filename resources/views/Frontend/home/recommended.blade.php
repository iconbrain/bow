@extends('Frontend.layouts.master')

<link rel="stylesheet" href="../../css/materialize.css">
<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css"  media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">
@include('Frontend.includes.header')

@section('title')
<title>Direkomendasikan | BOW</title>
@endsection

@section('content')
<div class="w3-light-grey">
  <div class="container little-div">
    <div class="row">
      <h2 class="middle top-title">Direkomendasikan untuk Pergi</h2>

      <div class="col-xs-12 col-lg-3">
        <div class="card-panel white w3-round-large">
          <h3 class="middle">Saring <i id="loading" class="material-icons"></i></h3><hr>
          <b style="font-size: 15px; font-family: 'Comic Sans MS';">Pilih Kota</b><br>
          <div style="margin-top: 10px;">
          <?php $i=1; ?>
          @foreach($locations as $loc)
            <div style="margin-left: 10px;">
              <input type="checkbox" name="citiesRcm[]" value="{{ $loc->location_id }}" id="filter-cityRcm-{{ $loc->location_id }}" />
              <label for="filter-cityRcm-{{ $loc->location_id }}" style="font-weight: normal; color: black;">{{ $loc->location_name }} <small style="color: grey"> ({{ ${"sumloc" . $i} }})</small></label><br>
            </div>
            <?php $i++ ?>
          @endforeach
          </div>
            
          <button id="btnRcmAll" style="border: none; background: none;" onclick="filterRecommended(100)">Semua <small style="color: grey;">({{ $totalCity->totalCity }})</small></button><br><br>

          <b style="font-size: 15px; font-family: 'Comic Sans MS';">Pilih Harga</b><br>
          <div style="margin-top: 10px;">
          @foreach($price_range as $pr)
            <div style="margin-left: 10px;">
              <input type="checkbox" name="pricesRcm[]" value="{{ $pr->id }}" id="filter-priceRcm-{{ $pr->id }}" />
              <label for="filter-priceRcm-{{ $pr->id }}" style="font-weight: normal; color: black;">{{ $pr->price_range }}</label><br>
            </div>
          @endforeach
          </div>
          <br><br>
          
          <h3 class="middle">Sortir <i id="loading" class="material-icons"></i></h3><hr>
          <b style="font-size: 15px; font-family: 'Comic Sans MS';">Ulasan</b><br>
          <button id="btnFilterRcm" style="border: none; background: none; margin-top: 10px;" onclick="sortReview(1)">Banyak ke Sedikit</button><br>
          <button id="btnFilterRcm2" style="border: none; background: none;" onclick="sortReview(2)">Sedikit ke Banyak</button><br><br>

          <b style="font-size: 15px; font-family: 'Comic Sans MS';">Kriteria</b><br>
          <button id="btnFilterRcm3" style="border: none; background: none; margin-top: 10px;" onclick="sortCriteria(1)">Fasilitas <small style="color: grey;">Tinggi ke Rendah</small></button><br>
          <button id="btnFilterRcm4" style="border: none; background: none;" onclick="sortCriteria(2)">Kebersihan <small style="color: grey;">Tinggi ke Rendah</small></button><br>
          <button id="btnFilterRcm5" style="border: none; background: none;" onclick="sortCriteria(3)">Kenyamanan <small style="color: grey;">Tinggi ke Rendah</small></button><br>
          <button id="btnFilterRcm6" style="border: none; background: none;" onclick="sortCriteria(4)">Pelayanan <small style="color: grey;">Tinggi ke Rendah</small></button><br>
          <button id="btnFilterRcm7" style="border: none; background: none;" onclick="sortCriteria(5)">Harga <small style="color: grey;">Tinggi ke Rendah</small></button><br>
        </div>
      </div>
      
      <div id="recommendedAll">
        <div class="col-xs-12 col-lg-6">
          @foreach($recommended as $item)
            <div class="card horizontal w3-round-large">
              <div class="card-stacked">
                <div class="card-content top-custom-card">
                  <a href="{{ URL::to('/place/'.$item->place_prefix) }}">
                    @if(strpos($item->image,'https://') !== false)
                      <img src="{{$item->image}}" class="w3-round-large" style="width:120px;height:120px;margin:-10 -10 -10 -10;float: left;">
                    @else
                      <img src="{{ URL::to('/') }}/images/{{$item->image}}" class="w3-round-large" style="width:120px;height:120px;margin:-10 -10 -10 -10;float: left;">
                    @endif
                  </a>

                  @if($item->avgRating > 0 AND $item->avgRating < 1)
                    <span  data-position="right" class="badge" style="color: #fff; background-color: #d11717; border-radius: 5px;">{{ $item->avgRating }}</span>
                  @elseif($item->avgRating >= 1 AND $item->avgRating < 2)
                    <span  data-position="right" class="badge" style="color: #fff; background-color: red; border-radius: 5px;">{{ $item->avgRating }}</span>
                  @elseif($item->avgRating >= 2 AND $item->avgRating < 3)
                    <span  data-position="right" class="badge" style="color: #fff; background-color: #F62; border-radius: 5px;">{{ $item->avgRating }}</span>
                  @elseif($item->avgRating >= 3 AND $item->avgRating < 4)
                    <span  data-position="right" class="badge" style="color: #fff; background-color: #ff9e20; border-radius: 5px;">{{ $item->avgRating }}</span>
                  @elseif($item->avgRating >= 4 AND $item->avgRating < 5)
                    <span  data-position="right" class="badge" style="color: #fff; background-color: #efb915; border-radius: 5px;">{{ $item->avgRating }}</span>
                  @elseif($item->avgRating == 5)
                    <span  data-position="right" class="badge" style="color: #fff; background-color: #F9BC00; border-radius: 5px;">{{ $item->avgRating }}</span>
                  @endif
                  
                  <h4 class="categories-title" style="margin-left: 120px;"><a href="{{ URL::to('/place/'.$item->place_prefix) }}" class="a-custom">{{ $item->name }}</a></h4>
                  <a href="{{ URL::to('/place/'.$item->place_prefix) }}"><p style="color:gray;margin-left: 120px;">{{ $item->address }}</p></a>

                  @if(Auth::check())
                    @foreach ($bmk as $bm)
                      @if($bm->place_id == $item->place_id)
                        <i class="material-icons" style="float: right; color: #019987">turned_in</i>
                      @endif
                    @endforeach
                  @endif
                </div>
                <div class="card-action">
                  <p align="justify">
                    <?php
                      $excerpt = getExcerpt($item->description, 0, 290);
                    ?>
                    {!! $excerpt !!}
                  </p>
                </div>
                <div class="card-action">
                  {{ $item-> location_name }}
                  <i class="material-icons left" style="color:gray">location_on</i><br>
                  <div style="display: inline-flex;">
                    <i class="material-icons left" style="color:gray">schedule</i>
                    <div class="col s12" style="margin-left:-10px;">{!! $item->operational_hours !!}</div>
                  </div><br>
                  @if($item->phone_number == NULL) <i class="material-icons left" style="color:gray">perm_phone_msg</i>(Belum Tersedia)
                  @else<i class="material-icons left" style="color:gray">perm_phone_msg</i>{{ $item->phone_number }}@endif<br>
                  <div class="nulpad">
                    <i class="material-icons left" style="color:gray">attach_money</i>
                  </div>
                  <div class="col s10" style="margin-left:-10px;">{!! $item->price !!}</div><br>
                </div>
                <div class="card-action">
                  <div style="float: left;">
                    <i class="material-icons left greycolor">rate_review</i>{{ $item->sumReview }} ulasan
                  </div>
                  <div style="float: right;">
                    <i class="material-icons left greycolor">bookmark</i>{{ $item->sumBookmark }} ditandai
                  </div>
                </div>
              </div>
            </div>
          @endforeach

          {{ $recommended->links() }}
        </div>
      </div>

      <div class="col-xs-12 col-lg-3">
        <div class="card horizontal w3-round-large">
          <div class="card-stacked">
            <div class="card-content top-custom-card">
              <h2 style="font-size: 18px; font-family: 'Comic Sans MS'">Bandung Objek Wisata</h2>
              <p style="color: grey; font-size: 13px;">Merekomendasikan objek-objek wisata di Bandung.</p>
              <hr>
              <img src="{{ URL::to('/') }}/images/bow_box.png" style="width:40px;height:40px; float: right; margin-top: 25px; margin-bottom: 5px;" id="an">
              <img src="{{ URL::to('/') }}/images/category_sidebar.png" class="responsive-img">
            </div>
          </div>
        </div>
      
        @if(count($weeklyReview) > 0)
          <div class="card horizontal w3-round-large">
            <div class="card-stacked">
              <div class="card-content top-custom-card">
                <h2 style="font-size: 15px; font-family: 'Comic Sans MS'">Ulasan Minggu Ini</h2>
                <p style="color: grey; font-size: 13px;">Temukan objek wisata yang diulas dalam minggu ini.</p>
                <hr>

                <div class="col s12">
                  <div class="row" style="margin-left: -22px;">
                    @foreach($weeklyReview as $wr)
                      <div class="col s4">
                        <a href="{{ URL::to('/place/'.$wr->place_prefix) }}">
                          @if(strpos($wr->image,'https://') !== false)
                            <img src="{{$wr->image}}" class="w3-round-large" style="width:60px;height:60px;">
                          @else
                            <img src="{{ URL::to('/') }}/images/{{$wr->image}}" class="w3-round-large" style="width:60px;height:60px;">
                          @endif
                        </a>
                      </div>
                    @endforeach
                  </div>
                </div>

                <a href="{{ URL::to('/weekly-review') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
              </div>
            </div>
          </div>
        @endif
      
        @if(count($weeklyAdded) > 0)
          <div class="card horizontal w3-round-large">
            <div class="card-stacked">
              <div class="card-content top-custom-card">
                <h2 style="font-size: 15px; font-family: 'Comic Sans MS'">Objek Wisata Baru</h2>
                <p style="color: grey; font-size: 13px;">Objek wisata yang baru ditambahkan dalam satu minggu terakhir.</p>
                <hr>

                <div class="col s12">
                  <div class="row" style="margin-left: -22px;">
                    @foreach($weeklyAdded as $wa)
                      <div class="col s4">
                        <a href="{{ URL::to('/place/'.$wa->place_prefix) }}">
                          @if(strpos($wa->image,'https://') !== false)
                            <img src="{{$wa->image}}" class="w3-round-large" style="width:60px;height:60px;">
                          @else
                            <img src="{{ URL::to('/') }}/images/{{$wa->image}}" class="w3-round-large" style="width:60px;height:60px;">
                          @endif
                        </a>
                      </div>
                    @endforeach
                  </div>
                </div>

                <a href="{{ URL::to('/weekly-added') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
              </div>
            </div>
          </div>
        @endif
      
        @if(count($notReviewedYet) > 0)
          <div class="card horizontal w3-round-large">
            <div class="card-stacked">
              <div class="card-content top-custom-card">
                <h2 style="font-size: 15px; font-family: 'Comic Sans MS'">Belum Diulas</h2>
                <p style="color: grey; font-size: 13px;">Berikan ulasan pada objek wisata yang belum pernah diulas.</p>
                <hr>

                <div class="col s12">
                  <div class="row" style="margin-left: -22px;">
                    @foreach($notReviewedYet as $nry)
                      <div class="col s4">
                        <a href="{{ URL::to('/place/'.$nry->place_prefix) }}">
                          @if(strpos($nry->image,'https://') !== false)
                            <img src="{{$nry->image}}" class="w3-round-large" style="width:60px;height:60px;">
                          @else
                            <img src="{{ URL::to('/') }}/images/{{$nry->image}}" class="w3-round-large" style="width:60px;height:60px;">
                          @endif
                        </a>
                      </div>
                    @endforeach
                  </div>
                </div>

                <a href="{{ URL::to('/not-reviewed-yet') }}" style="float: right; font-weight: bold; font-family: 'Arial';">LIHAT SEMUA<i class="material-icons right" style="margin-left: -2px; margin-top: -1px;">chevron_right</i></a>
              </div>
            </div>
          </div>
        @endif
      </div>

    </div>
  </div>
</div>

<style type="text/css">
  #btnFilterRcm:focus {
      color: red;
  }
  #btnFilterRcm2:focus {
      color: red;
  }
  #btnFilterRcm3:focus {
      color: red;
  }
  #btnFilterRcm4:focus {
      color: red;
  }
  #btnFilterRcm5:focus {
      color: red;
  }
  #btnFilterRcm6:focus {
      color: red;
  }
  #btnFilterRcm7:focus {
      color: red;
  }
  #btnRcmAll:focus {
      color: red;
  }
  #btnFilterRcm:hover {
      color: teal;
  }
  #btnFilterRcm2:hover {
      color: red;
  }
  #btnFilterRcm3:hover {
      color: red;
  }
  #btnFilterRcm4:hover {
      color: red;
  }
  #btnFilterRcm5:hover {
      color: red;
  }
  #btnFilterRcm6:hover {
      color: red;
  }
  #btnFilterRcm7:hover {
      color: red;
  }
  hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 0.5em 0;
    padding: 0; 
  }

  #an {
    -webkit-animation-duration: 3s;
    -webkit-animation-delay: 2s;
    -webkit-animation-iteration-count: infinite;

    -moz-animation-duration: 3s;
    -moz-animation-delay: 2s;
    -moz-animation-iteration-count: infinite;

    -ms-animation-duration: 3s;
    -ms-animation-delay: 2s;
    -ms-animation-iteration-count: infinite;

    -o-animation-duration: 3s;
    -o-animation-delay: 2s;
    -o-animation-iteration-count: infinite;

    animation-duration: 3s;
    animation-delay: 2s;
    animation-iteration-count: infinite;
  }
</style>

  @push('scripts')
  <script>
    $('#an').addClass('animated infinite bounce');
  </script>

  <script>
    // $(document).ready(function () {
      var citiesRcm = [];
      var pricesRcm = [];

      $('input[name="citiesRcm[]"]').on('change', function (e) {
        e.preventDefault();
        citiesRcm = []; // reset 

        $('input[name="citiesRcm[]"]:checked').each(function()
        {
            citiesRcm.push($(this).val());
        });

        $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

        $.ajax({
          type: 'POST',
          url: "{{ URL::to('/category/filter-recommended') }}",
          data: { citiesRcm : citiesRcm, pricesRcm : pricesRcm, _token : <?php echo "'" . csrf_token() . "'"; ?> },
          success: function(data) {
            $('#recommendedAll').html(data);
          },
          error: function(data) {
            console.log(data);
          },
          complete:function(){
            $('.preloader-wrapper').fadeOut();
          }
        });
      });

      $('input[name="pricesRcm[]"]').on('change', function (e) {
        e.preventDefault();
        pricesRcm = []; // reset 

        $('input[name="pricesRcm[]"]:checked').each(function()
        {
            pricesRcm.push($(this).val());
        });

        $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

        $.ajax({
          type: 'POST',
          url: "{{ URL::to('/category/filter-recommended') }}",
          data: { citiesRcm : citiesRcm, pricesRcm : pricesRcm, _token : <?php echo "'" . csrf_token() . "'"; ?> },
          success: function(data) {
            $('#recommendedAll').html(data);
          },
          error: function(data) {
            console.log(data);
          },
          complete:function(){
            $('.preloader-wrapper').fadeOut();
          }
        });
      });

      $("#btnRcmAll").click(function(e){
        $("#filter-cityRcm-1").prop("checked", true);
        $('#filter-cityRcm-2').prop('checked', true);
        $('#filter-cityRcm-3').prop('checked', true);
        $('#filter-cityRcm-4').prop('checked', true);
        $('#filter-cityRcm-5').prop('checked', true);
        $('#filter-priceRcm-1').prop('checked', true);
        $('#filter-priceRcm-2').prop('checked', true);
        $('#filter-priceRcm-3').prop('checked', true);
        $('#filter-priceRcm-4').prop('checked', true);
        $('#filter-priceRcm-5').prop('checked', true);
        $('#filter-priceRcm-6').prop('checked', true);
        $('#filter-priceRcm-7').prop('checked', true);
        $('#filter-priceRcm-8').prop('checked', true);
      });
    // });

    function filterRecommended(id) {
      citiesRcm = [1,2,3,4,5];
      pricesRcm = [1,2,3,4,5,6,7,8];
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-recommended') }}",
        data: { location_id : id, _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#recommendedAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    function filterPrice(id) {
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-recommended') }}",
        data: { price_id : id, _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#recommendedAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    function sortReview(id) {
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-recommended') }}",
        data: { citiesRcm: citiesRcm, pricesRcm: pricesRcm, sort_review_id : id, _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#recommendedAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    function sortCriteria(id) {
      $('#loading').html('<div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-red"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-yellow"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> <div class="spinner-layer spinner-green"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div>');

      $.ajax({
        type: 'POST',
        url: "{{ URL::to('/category/filter-recommended') }}",
        data: { citiesRcm: citiesRcm, pricesRcm: pricesRcm, sort_criteria_id : id, _token : <?php echo "'" . csrf_token() . "'"; ?> },
        success: function(data) {
          $('#recommendedAll').html(data);
        },
        error: function(data) {
          console.log(data);
        },
        complete:function(){
          $('.preloader-wrapper').fadeOut();
        }
      });
    }

    $(document).ready(function() {
      document.getElementById('btnRcmAll').focus();
    });
  </script>
  @endpush

@endsection
<?php
function getExcerpt($str, $startPos=0, $maxLength=100) {
  if(strlen($str) > $maxLength) {
    $excerpt   = substr($str, $startPos, $maxLength-3);
    $lastSpace = strrpos($excerpt, ' ');
    $excerpt   = substr($excerpt, 0, $lastSpace);
    $excerpt  .= '...';
    } else {
        $excerpt = $str;
    }

    return $excerpt;
}
?>