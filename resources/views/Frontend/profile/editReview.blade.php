@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
@include('Frontend.includes.header-profile')

@section('title')
	<title>Ubah Ulasan | BOW</title>
@endsection

@section('content')
  	<div class="container container-fluid">
	    <div class="row">
	    	<div class="card card-profile z-depth-3">
		    	<div class="row subcontent">
		    		<div class="container">
		    		<div class="col-xs-12 col-lg-3 center-align top25 bottom25">
		    			<img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="img-profile"></img>
		    			<br><br>
		    			<div class="profile">
		    				<p>Bergabung {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->format('M d, Y') }}</p>
		    				<li class="divider div-profile"></li>
		    				<table class="table-padding">
								<style>
									.table-padding td{
										padding: 3px 8px;
										font-size: 13.5px;
									}
								</style>
								<tbody>
									<tr>
										<td>Jumlah Ulasan</td>
										@if( $total_reviews > 0)
											<td>{{ $total_reviews }}</td>
											<td><a href="{{ URL::to('account/review') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Komentar</td>
										@if( $total_comments > 0)
											<td>{{ $total_comments }}</td>
											<td><a href="{{ URL::to('account/comment') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Arsip</td>
										@if( $total_bookmarks > 0)
											<td>{{ $total_bookmarks }}</td>
											<td><a href="{{ URL::to('account/bookmark') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
								</tbody>
							</table>
		    			</div>
		    		</div>
					<div class="col-xs-12 col-lg-8 col-lg-offset-1 profile top25">
						<h3>{{ Auth::user()->name }}</h3>
						<p class="text-muted">{{ $display_name }}</p>
						<li class="divider div-profile"></li>

						<div class="section wrapper">
							<div class="row">
								<div class="col s12 m12 l7" style="font-family: 'Calibri'">
									<div class="board-content">
						                  <p style="font-size: 30px; color: teal; font-family: 'Comic Sans MS'" class="site__title">{{ $places->name }}</p><hr>

						                  <form action="/review/edit" method="post">
							                  @if (count($errors->postEditReview) > 0)
							                  <div class="alert alert-danger">
							                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							                    @foreach ($errors->postEditReview->all() as $error)
							                    <p>{{ $error }}</p>
							                    @endforeach
							                  </div>
							                  @endif 
							                  @if (Session::has('message'))
							                  <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
							                  @endif

											  <p class="edit-heading">Kriteria</p>
							                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
							                  <input type="hidden" name="review_id" value="{{ $places->id }}">
							                  <input type="hidden" name="place_id" value="{{ $places->place_id }}">

							                  <div class="rating-block">
							                    <div class="col s4 criteria">Fasilitas</div>

							                    <div class="stars">
							                      @if( $places->facility == 5)
							                      	<input class="star star-fasilitas-5" id="star-fasilitas-5" type="radio" name="fasilitas" value="5" checked="" />
							                      @else
							                      	<input class="star star-fasilitas-5" id="star-fasilitas-5" type="radio" name="fasilitas" value="5" />
							                      @endif
							                      <label class="star star-fasilitas-5" for="star-fasilitas-5"></label>
							                      @if( $places->facility == 4)
							                      	<input class="star star-fasilitas-4" id="star-fasilitas-4" type="radio" name="fasilitas" value="4" checked="" />
							                      @else
							                      	<input class="star star-fasilitas-4" id="star-fasilitas-4" type="radio" name="fasilitas" value="4" />
							                      @endif
							                      <label class="star star-fasilitas-4" for="star-fasilitas-4"></label>
							                      @if( $places->facility == 3)
							                      	<input class="star star-fasilitas-3" id="star-fasilitas-3" type="radio" name="fasilitas" value="3" checked="" />
							                      @else
							                      	<input class="star star-fasilitas-3" id="star-fasilitas-3" type="radio" name="fasilitas" value="3" />
							                      @endif
							                      <label class="star star-fasilitas-3" for="star-fasilitas-3"></label>
							                      @if( $places->facility == 2)
							                      	<input class="star star-fasilitas-2" id="star-fasilitas-2" type="radio" name="fasilitas" value="2" checked="" />
							                      @else
							                      	<input class="star star-fasilitas-2" id="star-fasilitas-2" type="radio" name="fasilitas" value="2" />
							                      @endif
							                      <label class="star star-fasilitas-2" for="star-fasilitas-2"></label>
							                      @if( $places->facility == 1)
							                      	<input class="star star-fasilitas-1" id="star-fasilitas-1" type="radio" name="fasilitas" value="1" checked="" />
							                      @else
							                      	<input class="star star-fasilitas-1" id="star-fasilitas-1" type="radio" name="fasilitas" value="1" />
							                      @endif
							                      <label class="star star-fasilitas-1" for="star-fasilitas-1"></label>
							                    </div>
							                  </div>

							                  <div class="rating-block">
							                    <div class="col s4 criteria">Kebersihan
							                    </div>

							                    <div class="stars">
							                      @if( $places->cleanliness == 5)
							                      	<input class="star star-kebersihan-5" id="star-kebersihan-5" type="radio" name="kebersihan" value="5" checked="" />
							                      @else
							                      	<input class="star star-kebersihan-5" id="star-kebersihan-5" type="radio" name="kebersihan" value="5" />
							                      @endif
							                      <label class="star star-kebersihan-5" for="star-kebersihan-5"></label>
							                      @if( $places->cleanliness == 4)
							                      	<input class="star star-kebersihan-4" id="star-kebersihan-4" type="radio" name="kebersihan" value="4" checked="" />
							                      @else
							                      	<input class="star star-kebersihan-4" id="star-kebersihan-4" type="radio" name="kebersihan" value="4" />
							                      @endif
							                      <label class="star star-kebersihan-4" for="star-kebersihan-4"></label>
							                      @if( $places->cleanliness == 3)
							                      	<input class="star star-kebersihan-3" id="star-kebersihan-3" type="radio" name="kebersihan" value="3" checked="" />
							                      @else
							                      	<input class="star star-kebersihan-3" id="star-kebersihan-3" type="radio" name="kebersihan" value="3" />
							                      @endif
							                      <label class="star star-kebersihan-3" for="star-kebersihan-3"></label>
							                      @if( $places->cleanliness == 2)
							                      	<input class="star star-kebersihan-2" id="star-kebersihan-2" type="radio" name="kebersihan" value="2" checked="" />
							                      @else
							                     	<input class="star star-kebersihan-2" id="star-kebersihan-2" type="radio" name="kebersihan" value="2" />
							                      @endif
							                      <label class="star star-kebersihan-2" for="star-kebersihan-2"></label>
							                      @if( $places->cleanliness == 1)
							                      	<input class="star star-kebersihan-1" id="star-kebersihan-1" type="radio" name="kebersihan" value="1" checked="" />
							                      @else
							                      	<input class="star star-kebersihan-1" id="star-kebersihan-1" type="radio" name="kebersihan" value="1" />
							                      @endif
							                      <label class="star star-kebersihan-1" for="star-kebersihan-1"></label>
							                    </div>
							                  </div>

							                  <div class="rating-block">
							                    <div class="col s4 criteria">Kenyamanan
							                    </div>

							                    <div class="stars">
							                      @if( $places->comfort == 5)
							                      	<input class="star star-kenyamanan-5" id="star-kenyamanan-5" type="radio" name="kenyamanan" value="5" checked="" />
							                      @else
							                      	<input class="star star-kenyamanan-5" id="star-kenyamanan-5" type="radio" name="kenyamanan" value="5" />
							                      @endif
							                      <label class="star star-kenyamanan-5" for="star-kenyamanan-5"></label>
							                      @if( $places->comfort == 4)
							                      	<input class="star star-kenyamanan-4" id="star-kenyamanan-4" type="radio" name="kenyamanan" value="4" checked="" />
							                      @else
							                      	<input class="star star-kenyamanan-4" id="star-kenyamanan-4" type="radio" name="kenyamanan" value="4" />
							                      @endif
							                      <label class="star star-kenyamanan-4" for="star-kenyamanan-4"></label>
							                      @if( $places->comfort == 3)
							                      	<input class="star star-kenyamanan-3" id="star-kenyamanan-3" type="radio" name="kenyamanan" value="3" checked="" />
							                      @else
							                      	<input class="star star-kenyamanan-3" id="star-kenyamanan-3" type="radio" name="kenyamanan" value="3" />
							                      @endif
							                      <label class="star star-kenyamanan-3" for="star-kenyamanan-3"></label>
							                      @if( $places->comfort == 2)
							                      	<input class="star star-kenyamanan-2" id="star-kenyamanan-2" type="radio" name="kenyamanan" value="2" checked="" />
							                      @else
							                      	<input class="star star-kenyamanan-2" id="star-kenyamanan-2" type="radio" name="kenyamanan" value="2" />
							                      @endif
							                      <label class="star star-kenyamanan-2" for="star-kenyamanan-2"></label>
							                      @if( $places->comfort == 1)
							                      	<input class="star star-kenyamanan-1" id="star-kenyamanan-1" type="radio" name="kenyamanan" value="1" checked="" />
							                      @else
							                      	<input class="star star-kenyamanan-1" id="star-kenyamanan-1" type="radio" name="kenyamanan" value="1" />
							                      @endif
							                      <label class="star star-kenyamanan-1" for="star-kenyamanan-1"></label>
							                    </div>
							                  </div>

							                  <div class="rating-block">
							                    <div class="col s4 criteria">Pelayanan
							                    </div>

							                    <div class="stars">
							                      @if( $places->service == 5)
							                      	<input class="star star-pelayanan-5" id="star-pelayanan-5" type="radio" name="pelayanan" value="5" checked="" />
							                      @else
							                      	<input class="star star-pelayanan-5" id="star-pelayanan-5" type="radio" name="pelayanan" value="5" />
							                      @endif
							                      <label class="star star-pelayanan-5" for="star-pelayanan-5"></label>
							                      @if( $places->service == 4)
							                      	<input class="star star-pelayanan-4" id="star-pelayanan-4" type="radio" name="pelayanan" value="4" checked="" />
							                      @else
							                      	<input class="star star-pelayanan-4" id="star-pelayanan-4" type="radio" name="pelayanan" value="4" />
							                      @endif
							                      <label class="star star-pelayanan-4" for="star-pelayanan-4"></label>
							                      @if( $places->service == 3)
							                      	<input class="star star-pelayanan-3" id="star-pelayanan-3" type="radio" name="pelayanan" value="3" checked="" />
							                      @else
							                      	<input class="star star-pelayanan-3" id="star-pelayanan-3" type="radio" name="pelayanan" value="3" />
							                      @endif
							                      <label class="star star-pelayanan-3" for="star-pelayanan-3"></label>
							                      @if( $places->service == 2)
							                      	<input class="star star-pelayanan-2" id="star-pelayanan-2" type="radio" name="pelayanan" value="2" checked="" />
							                      @else
							                      	<input class="star star-pelayanan-2" id="star-pelayanan-2" type="radio" name="pelayanan" value="2" />
							                      @endif
							                      <label class="star star-pelayanan-2" for="star-pelayanan-2"></label>
							                      @if( $places->service == 1)
							                      	<input class="star star-pelayanan-1" id="star-pelayanan-1" type="radio" name="pelayanan" value="1" checked="" />
							                      @else
							                      	<input class="star star-pelayanan-1" id="star-pelayanan-1" type="radio" name="pelayanan" value="1" />
							                      @endif
							                      <label class="star star-pelayanan-1" for="star-pelayanan-1"></label>
							                    </div>
							                  </div>

							                  <div class="rating-block">
							                    <div class="col s4 criteria">Harga
							                    </div>

							                    <div class="stars">
							                      @if( $places->price == 5)
							                      	<input class="star star-harga-5" id="star-harga-5" type="radio" name="harga" value="5" checked="" />
							                      @else
							                      	<input class="star star-harga-5" id="star-harga-5" type="radio" name="harga" value="5" />
							                      @endif
							                      <label class="star star-harga-5" for="star-harga-5"></label>
							                      @if( $places->price == 4)
							                      	<input class="star star-harga-4" id="star-harga-4" type="radio" name="harga" value="4" checked="" />
							                      @else
							                      	<input class="star star-harga-4" id="star-harga-4" type="radio" name="harga" value="4" />
							                      @endif
							                      <label class="star star-harga-4" for="star-harga-4"></label>
							                      @if( $places->price == 3)
							                      	<input class="star star-harga-3" id="star-harga-3" type="radio" name="harga" value="3" checked="" />
							                      @else
							                      	<input class="star star-harga-3" id="star-harga-3" type="radio" name="harga" value="3" />
							                      @endif
							                      <label class="star star-harga-3" for="star-harga-3"></label>
							                      @if( $places->price == 2)
							                      	<input class="star star-harga-2" id="star-harga-2" type="radio" name="harga" value="2" checked="" />
							                      @else
							                      	<input class="star star-harga-2" id="star-harga-2" type="radio" name="harga" value="2" />
							                      @endif
							                      <label class="star star-harga-2" for="star-harga-2"></label>
							                      @if( $places->price == 1)
							                      	<input class="star star-harga-1" id="star-harga-1" type="radio" name="harga" value="1" checked="" />
							                      @else
							                      	<input class="star star-harga-1" id="star-harga-1" type="radio" name="harga" value="1" />
							                      @endif
							                      <label class="star star-harga-1" for="star-harga-1"></label>
							                    </div>
							                  </div>
							                  
							                  <div class="row">
							                     <div class="input-field col s12">
							                      <i class="material-icons prefix">title</i>
							                      <input type="text" class="validate" value="{{ $places->title }}" name="title">
							                      <label>Judul Ulasan</label>
							                    </div>
							                  </div>
							                  
							                  <div class="row">
							                    <div class="input-field col s12">
							                      <i class="material-icons prefix">mode_edit</i>

							                    	<div class="input-field col s12 offset-s1">
								                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="komentar"></label>
							                          <textarea class="materialize-textarea" name="komentar" id="komentar" style="display:none;"></textarea>
								                  	  <div id="komentarEdit" name="komentarEdit" class="editor-wrapper placeholderText" contenteditable="true" required="required">{!! $places->body !!}</div>
								                  	 </div>
							                    </div>
							                  </div>

							                  <button class="btn waves-effect waves-light accent-color" type="submit" id="submitReview" style="color: white; float: right;" name="action">Simpan</button>
							              </form>
									</div>
								</div>
								<div class="col s12 m12 offset-l1 l4">
									<div class="collection">
										<a href="{{ URL::to('account/profile') }}" class="collection-item">Profil</a>
										<a href="{{ URL::to('account/password') }}" class="collection-item">Kata Sandi</a>
										<a href="{{ URL::to('account/review') }}" class="collection-item active">
										@if($total_reviews > 0)
											<span class="new badge orange">{{ $total_reviews }}</span>
										@endif
										Ulasan</a>
										<a href="{{ URL::to('account/comment') }}" class="collection-item">
										@if($total_comments > 0)
											<span class="new badge orange">{{ $total_comments }}</span>
										@endif
										Komentar</a>
										<a href="{{ URL::to('account/bookmark') }}" class="collection-item ">
										@if($total_bookmarks > 0)
											<span class="new badge orange">{{ $total_bookmarks }}</span>
										@endif
										Arsip</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@push('scripts')
  	<!-- Parsley -->
  	<script src="{{ asset("js/parsley.min.js") }}"></script>
  	<script>
	    $(document).ready(function(){
	      $('#submitReview').click(function(){
	        $('#komentar').val($('#komentarEdit').html())
	      });
	    });
  	</script>
	@endpush
@endsection