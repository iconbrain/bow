@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
@include('Frontend.includes.header-profile')

@section('title')
	<title>Ulasan | BOW</title>
@endsection

@section('content')
  	<div class="container container-fluid">
	    <div class="row">
	    	<div class="card card-profile z-depth-3">
		    	<div class="row subcontent">
		    		<div class="container">
			    		<div class="col-xs-12 col-lg-3 center-align top25 bottom25">
			    			<img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="img-profile"></img>
			    			<br><br>
			    			<div class="profile">
			    				<p>Bergabung {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->format('M d, Y') }}</p>
			    				<li class="divider div-profile"></li>
			    				<table class="table-padding">
									<style>
										.table-padding td{
											padding: 3px 8px;
											font-size: 13.5px;
										}
									</style>
									<tbody>
										<tr>
											<td>Jumlah Ulasan</td>
											@if( $total_reviews > 0)
												<td>{{ $total_reviews }}</td>
												<td><a href="{{ URL::to('account/review') }}" class="text-muted">Lihat</a></td>
											@else
												<td>0</td>
											@endif
										</tr>
										<tr>
											<td>Jumlah Komentar</td>
											@if( $total_comments > 0)
												<td>{{ $total_comments }}</td>
												<td><a href="{{ URL::to('account/comment') }}" class="text-muted">Lihat</a></td>
											@else
												<td>0</td>
											@endif
										</tr>
										<tr>
											<td>Jumlah Arsip</td>
											@if( $total_bookmarks > 0)
												<td>{{ $total_bookmarks }}</td>
												<td><a href="{{ URL::to('account/bookmark') }}" class="text-muted">Lihat</a></td>
											@else
												<td>0</td>
											@endif
										</tr>
									</tbody>
								</table>
			    			</div>
			    		</div>
						<div class="col-xs-12 col-lg-8 col-lg-offset-1 profile top25">
							<h3>{{ Auth::user()->name }}</h3>
							<p class="text-muted">{{ $display_name }}</p>
							<li class="divider div-profile"></li>

							<div class="section wrapper">
								<div class="row">
									<div class="col s12 m12 l7">
										<div class="board-content">
											@if (Session::has('message'))
							                  <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
							                @endif
							                
											@if( $total_reviews > 0)
												@foreach($reviews as $r)
												<ul class="messages">
		                                            <li>
								                        <div class="message_date">
														  <div style="display: inline-block; float: right;">
														  	<a href="{{ URL::to('account/edit-review/') }}/{{ $r->id }}"><button style="background-color: #FABE28; border-color:#FABE28; color:white; border-radius: 5px;"><i class="fa fa-edit"></i> Ubah</button></a>
														  	<button style="background-color: red; border-color: red; color:white; border-radius: 5px;" onclick="deleteReview({{ $r->id }}, {{ $r->place_id }});"><i class="fa fa-trash"></i> Hapus</button>
														  </div>

								                          <h3 class="date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('d') }} <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('M,') }} <small>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('Y') }}</small></small></h3>
								                        </div>
								                        <div class="message_wrapper">
								                          	<h5 class="heading" style="overflow-wrap: break-word;">{{ $r->title }}</h5>
								                          	<blockquote class="message blockquote" style="overflow-wrap: break-word;">{!! $r->body !!}</blockquote>
								                          <p class="url blockquote-loc">
								                            <a href="{{ URL::to('place/') }}/{{ $r->place_prefix }}">
								                            	<span class="fs1 text-info" aria-hidden="true"></span>
								                            	<i class="fa fa-map-marker"></i> {{ $r->name }}
								                            </a>
								                          </p>
								                          <br>
								                        </div>
								                    </li>          
		                						</ul>
		                						@endforeach
		                					@else
		                						<div class="center-align" style="margin-bottom: 25px;">
		                							<img src="{{ URL::to('/') }}/images/no_review.png" alt="No Review" class="responsive-img"></img>
		                						</div>
		                					@endif
										</div>
									</div>
									<div class="col s12 m12 offset-l1 l4">
										<div class="collection">
											<a href="{{ URL::to('account/profile') }}" class="collection-item ">Profil</a>
											<a href="{{ URL::to('account/password') }}" class="collection-item ">Kata Sandi</a>
											<a href="{{ URL::to('account/review') }}" class="collection-item active">
											@if($total_reviews > 0)
												<span class="new badge orange">{{ $total_reviews }}</span>
											@endif
											Ulasan</a>
											<a href="{{ URL::to('account/comment') }}" class="collection-item">
											@if($total_comments > 0)
												<span class="new badge orange">{{ $total_comments }}</span>
											@endif
											Komentar</a>
											<a href="{{ URL::to('account/bookmark') }}" class="collection-item ">
											@if($total_bookmarks > 0)
												<span class="new badge orange">{{ $total_bookmarks }}</span>
											@endif
											Arsip</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@push('scripts')
	  <!-- Parsley -->
	  <script src="{{ asset("js/parsley.min.js") }}"></script>
	  <script>
	  	function deleteReview(id, place_id)
	    {
	      if (confirm("Anda yakin ingin menghapus ulasan ini? Semua komentar yang ada pada ulasan ini juga akan ikut terhapus. Lanjutkan?") == true) {
	      	$.ajax({
		      type: "POST",
		      url: "{{ URL::to('review/delete') }}",
		      data: {place_id:place_id, review_id:id, _token:"<?php echo csrf_token(); ?>"},
		      success:
		      function(success)
		      {
		        if(success)
		        {
		        	location.href = "{{ URL::to('account/review') }}";
		        }
		      }
		    });
		  }
	    }
	  </script>
	@endpush
@endsection