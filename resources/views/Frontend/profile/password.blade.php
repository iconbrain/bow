@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
@include('Frontend.includes.header-profile')

@section('title')
	<title>Kata Sandi | BOW</title>
@endsection

@section('content')
  	<div class="container container-fluid">
	    <div class="row">
	    	<div class="card card-profile z-depth-3">
		    	<div class="row subcontent">
		    		<div class="container">
		    		<div class="col-xs-12 col-lg-3 center-align top25 bottom25">
		    			<img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="img-profile"></img>
		    			<br><br>
		    			<div class="profile">
		    				<p>Bergabung {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->format('M d, Y') }}</p>
		    				<li class="divider div-profile"></li>
		    				<table class="table-padding">
								<style>
									.table-padding td{
										padding: 3px 8px;
										font-size: 13.5px;
									}
								</style>
								<tbody>
									<tr>
										<td>Jumlah Ulasan</td>
										@if( $total_reviews > 0)
											<td>{{ $total_reviews }}</td>
											<td><a href="{{ URL::to('account/review') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Komentar</td>
										@if( $total_comments > 0)
											<td>{{ $total_comments }}</td>
											<td><a href="{{ URL::to('account/comment') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Arsip</td>
										@if( $total_bookmarks > 0)
											<td>{{ $total_bookmarks }}</td>
											<td><a href="{{ URL::to('account/bookmark') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
								</tbody>
							</table>
		    			</div>
		    		</div>
					<div class="col-xs-12 col-lg-8 col-lg-offset-1 profile top25">
						<h3>{{ Auth::user()->name }}</h3>
						<p class="text-muted">{{ $display_name }}</p>
						<li class="divider div-profile"></li>

						<div class="section wrapper">
							<div class="row">
								<div class="col s12 m12 l7">
									<div class="board-content">
										<p class="text-muted">
											Anda dapat memperbaharui kata sandi Anda. Kata sandi Anda minimal harus terdiri dari 6 karakter.
										</p>
									
										<form id="demo-form2" data-parsley-validate="" novalidate="" action="/updateUserPassword" class="form-add" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
											<input type="hidden" name="name_prefix" value="{{ $name_prefix }}">

											@if (count($errors->editUserPassword) > 0)
							                  <div class="alert alert-danger">
							                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							                    @foreach ($errors->editUserPassword->all() as $error)
							                    <p>{{ $error }}</p>
							                    @endforeach
							                  </div>
							                @endif
							                @if (Session::has('message'))
								            <div class="alert alert-success" style="margin-bottom: 30px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
								            @endif

											<div class="row">
												<div class="input-field col s12">
													<input type="password" class="input" name="old_password" value="{{{ old('old_password') }}}" required="required" style="color: black;">
													<label class="active prof">Kata Sandi Lama</label>
												</div>
												<div class="input-field col s12">
													<input type="password" class="input" name="new_password" value="{{{ old('new_password') }}}" required="required" style="color: black;">
													<label class="active prof">Kata Sandi Baru</label>
												</div>
												<div class="input-field col s12">
													<input type="password" class="input" name="new_password_confirm" value="{{{ old('new_password_confirm') }}}" required="required" style="color: black;">
													<label class="active prof">Konfirmasi Kata Sandi Baru</label>
												</div>
												<div class="col s12">
													<div id="message"></div>
												</div>
											</div>
											<button class="btn waves-effect waves-light accent-color" type="submit" style="color: white; float: right;" name="action">Simpan</button>
										</form>
									</div>
								</div>
								<div class="col s12 m12 offset-l1 l4">
									<div class="collection">
										<a href="{{ URL::to('account/profile') }}" class="collection-item">Profil</a>
										<a href="{{ URL::to('account/password') }}" class="collection-item active">Kata Sandi</a>
										<a href="{{ URL::to('account/review') }}" class="collection-item ">
										@if($total_reviews > 0)
											<span class="new badge orange">{{ $total_reviews }}</span>
										@endif
										Ulasan</a>
										<a href="{{ URL::to('account/comment') }}" class="collection-item">
										@if($total_comments > 0)
											<span class="new badge orange">{{ $total_comments }}</span>
										@endif
										Komentar</a>
										<a href="{{ URL::to('account/bookmark') }}" class="collection-item ">
										@if($total_bookmarks > 0)
											<span class="new badge orange">{{ $total_bookmarks }}</span>
										@endif
										Arsip</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@push('scripts')
  	<!-- Parsley -->
  	<script src="{{ asset("js/parsley.min.js") }}"></script>
	@endpush
@endsection