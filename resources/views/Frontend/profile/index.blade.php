@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
@include('Frontend.includes.header-profile')

@section('title')
	<title>Profil | BOW</title>
@endsection

@section('content')
  	<div class="container container-fluid">
	    <div class="row">
	    	<div class="card card-profile z-depth-3">
		    	<div class="row subcontent">
		    		<div class="container">
		    		<div class="col-xs-12 col-lg-3 center-align top25 bottom25">
		    			<img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="img-profile"></img>
		    			<br><br>

		    			<div class="profile">
		    				<p>Bergabung {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->format('M d, Y') }}</p>
		    				<li class="divider div-profile"></li>
		    				<table class="table-padding">
								<style>
									.table-padding td{
										padding: 3px 8px;
										font-size: 13.5px;
									}
								</style>
								<tbody>
									<tr>
										<td>Jumlah Ulasan</td>
										@if( $total_reviews > 0)
											<td>{{ $total_reviews }}</td>
											<td><a href="{{ URL::to('account/review') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Komentar</td>
										@if( $total_comments > 0)
											<td>{{ $total_comments }}</td>
											<td><a href="{{ URL::to('account/comment') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Arsip</td>
										@if( $total_bookmarks > 0)
											<td>{{ $total_bookmarks }}</td>
											<td><a href="{{ URL::to('account/bookmark') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
								</tbody>
							</table>
		    			</div>
		    		</div>
					<div class="col-xs-12 col-lg-8 col-lg-offset-1 profile top25">
						<h3>{{ Auth::user()->name }}</h3>
						<p class="text-muted">{{ $display_name }}</p>
						<li class="divider div-profile"></li>

						<div class="section wrapper">
							<div class="row">
								<div class="col s12 m12 l7">
									<div class="board-content">
										<p class="text-muted">
											Anda dapat memperbaharui akun Anda. Silahkan isi dengan informasi yang sah.
										</p>
									
										<form id="demo-form2" data-parsley-validate="" novalidate="" action="/updateUserProfile" class="form-add" method="post" enctype="multipart/form-data">
											{{ csrf_field() }}
											<input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
											<input type="hidden" name="name_prefix" value="{{ $name_prefix }}">

											@if (count($errors->editUserProfile) > 0)
							                  <div class="alert alert-danger">
							                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							                    @foreach ($errors->editUserProfile->all() as $error)
							                    <p>{{ $error }}</p>
							                    @endforeach
							                  </div>
							                @endif
							                @if (Session::has('message'))
								            <div class="alert alert-success" style="margin-bottom: 30px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
								            @endif

											<div class="row">
												<div class="input-field col s12">
													<input type="text" class="input" value="{{ $display_name }}" disabled>
													<label class="active prof">Status</label>
												</div>
												<div class="input-field col s12">
													<input type="text" id="name" class="input" name="name" value="{{ Auth::user()->name }}" required="required" style="color: black;">
													<label class="active prof">Nama Lengkap</label>
												</div>
												<div class="input-field col s12">
													<input type="email" id="email" class="input" name="email" value="{{ Auth::user()->email }}" required="required" style="color: black;">
													<label class="active prof">Email</label>
												</div>
												<div class="input-field col s12">
													<input type="file" id="image" name="image" class="form-control col-xs-12" style="padding-bottom: 30px;"></input><br><br><br>
												</div>
												<div class="col s12">
													<div id="message"></div>
												</div>
											</div>
											<input type="hidden" name="iconOld" value="{{ Auth::user()->avatar }}"></input>
											<button class="btn waves-effect waves-light accent-color" type="submit" style="color: white; float: right;" name="action">Simpan</button>
										</form>
									</div>
								</div>
								<div class="col s12 m12 offset-l1 l4">
									<div class="collection">
										<a href="{{ URL::to('account/profile') }}" class="collection-item active">Profil</a>
										<a href="{{ URL::to('account/password') }}" class="collection-item ">Kata Sandi</a>
										<a href="{{ URL::to('account/review') }}" class="collection-item ">
										@if($total_reviews > 0)
											<span class="new badge orange">{{ $total_reviews }}</span>
										@endif
										Ulasan</a>
										<a href="{{ URL::to('account/comment') }}" class="collection-item">
										@if($total_comments > 0)
											<span class="new badge orange">{{ $total_comments }}</span>
										@endif
										Komentar</a>
										<a href="{{ URL::to('account/bookmark') }}" class="collection-item ">
										@if($total_bookmarks > 0)
											<span class="new badge orange">{{ $total_bookmarks }}</span>
										@endif
										Arsip</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@push('scripts')
	  <!-- Parsley -->
	  <script src="{{ asset("js/parsley.min.js") }}"></script>
	@endpush
@endsection