@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
@include('Frontend.includes.header-profile')

@section('title')
	<title>Ubah Komentar | BOW</title>
@endsection

@section('content')
  	<div class="container container-fluid">
	    <div class="row">
	    	<div class="card card-profile z-depth-3">
		    	<div class="row subcontent">
		    		<div class="container">
		    		<div class="col-xs-12 col-lg-3 center-align top25 bottom25">
		    			<img src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" class="img-profile"></img>
		    			<br><br>
		    			<div class="profile">
		    				<p>Bergabung {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->created_at)->format('M d, Y') }}</p>
		    				<li class="divider div-profile"></li>
		    				<table class="table-padding">
								<style>
									.table-padding td{
										padding: 3px 8px;
										font-size: 13.5px;
									}
								</style>
								<tbody>
									<tr>
										<td>Jumlah Ulasan</td>
										@if( $total_reviews > 0)
											<td>{{ $total_reviews }}</td>
											<td><a href="{{ URL::to('account/review') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Komentar</td>
										@if( $total_comments > 0)
											<td>{{ $total_comments }}</td>
											<td><a href="{{ URL::to('account/comment') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
									<tr>
										<td>Jumlah Arsip</td>
										@if( $total_bookmarks > 0)
											<td>{{ $total_bookmarks }}</td>
											<td><a href="{{ URL::to('account/bookmark') }}" class="text-muted">Lihat</a></td>
										@else
											<td>0</td>
										@endif
									</tr>
								</tbody>
							</table>
		    			</div>
		    		</div>
					<div class="col-xs-12 col-lg-8 col-lg-offset-1 profile top25">
						<h3>{{ Auth::user()->name }}</h3>
						<p class="text-muted">{{ $display_name }}</p>
						<li class="divider div-profile"></li>

						<div class="section wrapper">
							<div class="row">
								<div class="col s12 m12 l7" style="font-family: 'Calibri'">
									<div class="board-content">
						                  @if (count($errors->postEditComment) > 0)
						                  <div class="alert alert-danger">
						                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						                    @foreach ($errors->postEditComment->all() as $error)
						                    <p>{{ $error }}</p>
						                    @endforeach
						                  </div>
						                  @endif 
						                  @if (Session::has('message'))
						                  <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
						                  @endif

										  <p style="font-size: 30px; color: teal; font-family: 'Comic Sans MS'" class="site__title">{{ $places->placeName }}</p>
										  
					                      <p style="font-size: 20px; overflow-wrap: break-word;"><b><i>{!! $places->title !!}</i></b></p>
					                        <p style="overflow-wrap: break-word;">{!! $places->reviewBody !!}</p>

						                  <form action="/comment/edit" method="post">
							                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
							                  <input type="hidden" name="comment_id" value="{{ $places->commentId }}">
							                  
							                  <div class="row">
							                    <div class="input-field col s12">
							                    	<div class="input-field col s12">
								                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="komentar"></label>
								                      <textarea class="materialize-textarea" name="komentar" id="komentar" style="display:none;"></textarea>
							                  		  <blockquote class="message blockquote" style="margin-top: -30px; margin-left: -10px;">
									                  	  <div id="komentarEdit" name="komentarEdit" class="editor-wrapper placeholderText" contenteditable="true" required="required">{!! $places->commentBody !!}</div>
							                  		  </blockquote>

									                  <button type="submit" class="btn send-btn" id="submitComment" style="color: white; float: right;">
						                                <i class="material-icons">send</i>
						                              </button>
								                  	 </div>
							                    </div>
							                  </div>
							              </form>
									</div>
								</div>
								<div class="col s12 m12 offset-l1 l4">
									<div class="collection">
										<a href="{{ URL::to('account/profile') }}" class="collection-item">Profil</a>
										<a href="{{ URL::to('account/password') }}" class="collection-item">Kata Sandi</a>
										<a href="{{ URL::to('account/review') }}" class="collection-item ">
										@if($total_reviews > 0)
											<span class="new badge orange">{{ $total_reviews }}</span>
										@endif
										Ulasan</a>
										<a href="{{ URL::to('account/comment') }}" class="collection-item active">
										@if($total_comments > 0)
											<span class="new badge orange">{{ $total_comments }}</span>
										@endif
										Komentar</a>
										<a href="{{ URL::to('account/bookmark') }}" class="collection-item ">
										@if($total_bookmarks > 0)
											<span class="new badge orange">{{ $total_bookmarks }}</span>
										@endif
										Arsip</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@push('scripts')
  	<!-- Parsley -->
  	<script src="{{ asset("js/parsley.min.js") }}"></script>
  	<script>
	    $(document).ready(function(){
	      $('#submitComment').click(function(){
	        $('#komentar').val($('#komentarEdit').html())
	      });
	    });
  	</script>
	@endpush
@endsection