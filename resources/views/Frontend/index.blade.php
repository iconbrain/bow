@extends('Frontend.layouts.master')

@push('stylesheets')
<link rel="stylesheet" href="{{ asset("css/custom.css") }}">
<link rel="stylesheet" href="{{ asset("css/awesomplete.css") }}" />
@include('Frontend.includes.header-home')
@endpush

@section('title')
<title>Beranda | BOW</title>
@endsection

@section('content')
<div class="flex-center position-ref top70">
    <div class="content">
        <div class="home-banner">
            <div class="top130">
                <h1 class="home-tagline">Temukan objek wisata menarik di Bandung</h1>

                <form action="/search/results" method="post">
                    <div class="col-md-6 col-md-offset-3 top40">
                        <div class="input-group">
                            <div class="input-group-btn search-panel">
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($locations as $loc)
                                    <li><a href="{{$loc->location_id}}"><option>{{$loc->location_name}}</option></a></li>
                                    @endforeach
                                    <li class="divider"></li>
                                    <li><a href="#">Semuanya</a></li>
                                </ul>
                            </div>

                            <input type="hidden" name="search_param" value="all" id="search_param" class="h50">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input class="awesomplete form-control h50" list="mylist" id="myinput" name="placeInput" placeholder="Temukan objek wisata..." type="text" style="border-radius: 10;" />
                            <datalist id="mylist">
                                @foreach($search as $s)
                                <option>{{ $s->name }}</option>
                                @endforeach
                            </datalist>
                            <span class="input-group-btn"">
                                <button class="btn btn-default h50" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container mega-div">
    <div class="row">
        <div class="col-md-12">
            <h3 class="middle">Kategori Wisata</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($categories as $cat)
                    <a href="{{ URL::to('category/'.$cat->prefix) }}" class="cat">
                        <div class="col-md-1-5 cat8">
                            <img src="{{ URL::to('/') }}/images/{{$cat->icon}}" class="category responsive-img"></img><br><br>{{ $cat->category_name }}</div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container little-div">
        <div class="row">
            <div class="col-md-12">
                <h3 class="middle">Direkomendasikan untuk Pergi</h3>
                <p class="w3-text-grey p-middle">Temukan objek wisata yang menarik untuk dikunjungi di Bandung.</p>

                <div class="custom-right">
                    <a href="{{ URL::to('/recommended') }}" class="more-item b-custom"><p class="see-all-home">LIHAT SEMUA</p>
                        <i class="material-icons right">chevron_right</i>
                    </a>
                </div>
            </div>

            @foreach($recommended as $rcm)
            <div class="col-md-4 cards">
                <div class="w3-card-12 w3-round-large w3-hover-shadow">
                    @if($rcm->image == NULL)
                    <img src="/images/no-image.png" class="img-card img-round-top responsive-img">
                    @elseif(strpos($rcm->image,'https://') !== false)
                      <a href="{{ URL::to('/place/'.$rcm->place_prefix) }}"><img src="{{$rcm->image}}" class="img-card img-round-top img-responsive"></a>
                    @else
                      <a href="{{ URL::to('/place/'.$rcm->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$rcm->image}}" class="img-card img-round-top img-responsive"></a>
                    @endif
                    <a href="{{ URL::to('/place/'.$rcm->place_prefix) }}" class="c-custom">
                        <div class="w3-container">
                            <div class="card-title">{{ $rcm->name}}</div>
                            @if(Auth::check())
                              @foreach ($bmk as $bm)
                                @if($bm->place_id == $rcm->place_id)
                                  <i class="material-icons bkmk-home">turned_in</i>
                                @endif
                              @endforeach
                            @endif
                            <div class="card-desc">
                                <?php
                                $excerpt = getExcerpt($rcm->description, 0, 90);
                                ?>
                                {{ $rcm->address }}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach

        </div>
    </div>

    @if(count($weeklyReview) > 0)
    <div class="container little-div">
        <div class="row">
            <div class="col-md-12">
                <h3 class="middle">Ulasan Minggu Ini</h3>
                <p class="w3-text-grey p-middle">Temukan objek wisata yang diulas dalam minggu ini (H-7 dari hari ini).</p>

                <div class="custom-right">
                    <a href="{{ URL::to('/weekly-review') }}" class="more-item b-custom"><p class="see-all-home">LIHAT SEMUA</p>
                        <i class="material-icons right">chevron_right</i>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>

            @foreach($weeklyReview as $wr)
            <div class="col-md-4 cards">
                <div class="w3-card-12 w3-round-large w3-hover-shadow">
                    @if($wr->image == NULL)
                    <img src="/images/no-image.png" class="img-card img-round-top responsive-img">
                    @elseif(strpos($wr->image,'https://') !== false)
                      <a href="{{ URL::to('/place/'.$wr->place_prefix) }}"><img src="{{$wr->image}}" class="img-card img-round-top img-responsive"></a>
                    @else
                      <a href="{{ URL::to('/place/'.$wr->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$wr->image}}" class="img-card img-round-top img-responsive"></a>
                    @endif
                    <a href="{{ URL::to('/place/'.$wr->place_prefix) }}" class="c-custom">
                        <div class="w3-container">
                            <div class="card-title">{{ $wr->name}}</div>
                            @if(Auth::check())
                              @foreach ($bmk as $bm)
                                @if($bm->place_id == $wr->place_id)
                                  <i class="material-icons bkmk-home">turned_in</i>
                                @endif
                              @endforeach
                            @endif
                            <div class="card-desc">
                                <?php
                                $excerpt = getExcerpt($wr->description, 0, 90);
                                ?>
                                {{ $wr->address }}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    @endif

    @if(count($weeklyAdded) > 0)
    <div class="container little-div">
        <div class="row">
            <div class="col-md-12">
                <h3 class="middle">Objek Wisata Baru</h3>
                <p class="w3-text-grey p-middle">Objek wisata yang baru ditambahkan dalam satu minggu terakhir.</p>

                <div class="custom-right">
                    <a href="{{ URL::to('/weekly-added') }}" class="more-item b-custom"><p class="see-all-home">LIHAT SEMUA</p>
                        <i class="material-icons right">chevron_right</i>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>

            @foreach($weeklyAdded as $wa)
            <div class="col-md-4 cards">
                <div class="w3-card-12 w3-round-large w3-hover-shadow">
                    @if($wa->image == NULL)
                    <img src="/images/no-image.png" class="img-card img-round-top responsive-img">
                    @elseif(strpos($wa->image,'https://') !== false)
                      <a href="{{ URL::to('/place/'.$wa->place_prefix) }}"><img src="{{$wa->image}}" class="img-card img-round-top img-responsive"></a>
                    @else
                      <a href="{{ URL::to('/place/'.$wa->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$wa->image}}" class="img-card img-round-top img-responsive"></a>
                    @endif
                    <a href="{{ URL::to('/place/'.$wa->place_prefix) }}" class="c-custom">
                        <div class="w3-container">
                            <div class="card-title">{{ $wa->name}}</div>
                            @if(Auth::check())
                              @foreach ($bmk as $bm)
                                @if($bm->place_id == $wa->place_id)
                                  <i class="material-icons bkmk-home">turned_in</i>
                                @endif
                              @endforeach
                            @endif
                            <div class="card-desc">
                                <?php
                                $excerpt = getExcerpt($wa->description, 0, 90);
                                ?>
                                {{ $wa->address }}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    @endif

    @if(count($notReviewedYet) > 0)
    <div class="container little-div">
        <div class="row">
            <div class="col-md-12">
                <h3 class="middle">Belum Diulas</h3>
                <p class="w3-text-grey p-middle">Berikan ulasan pada objek wisata yang belum pernah diulas di BOW.</p>

                <div class="custom-right">
                    <a href="{{ URL::to('/not-reviewed-yet') }}" class="more-item b-custom"><p class="see-all-home">LIHAT SEMUA</p>
                        <i class="material-icons right">chevron_right</i>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>

            @foreach($notReviewedYet as $nry)
            <div class="col-md-4 cards">
                <div class="w3-card-12 w3-round-large w3-hover-shadow">
                    @if($nry->image == NULL)
                    <img src="/images/no-image.png" class="img-card img-round-top responsive-img">
                    @elseif(strpos($nry->image,'https://') !== false)
                      <a href="{{ URL::to('/place/'.$nry->place_prefix) }}"><img src="{{$nry->image}}" class="img-card img-round-top img-responsive"></a>
                    @else
                      <a href="{{ URL::to('/place/'.$nry->place_prefix) }}"><img src="{{ URL::to('/') }}/images/{{$nry->image}}" class="img-card img-round-top img-responsive"></a>
                    @endif
                    <a href="{{ URL::to('/place/'.$nry->place_prefix) }}" class="c-custom">
                        <div class="w3-container">
                            <div class="card-title">{{ $nry->name}}</div>
                            <div class="card-desc">
                                <?php
                                $excerpt = getExcerpt($nry->description, 0, 90);
                                ?>
                                {{ $nry->address }}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</div>
@endsection

@push('scripts')
    <script src="{{ asset("js/awesomplete.js") }}"></script>
    <script>
        var input = document.getElementById("myinput");
        var awesomplete = new Awesomplete(input, { minChars: 2, maxItems: 200} );
    </script>
@endpush

<style type="text/css">
    input::-webkit-calendar-picker-indicator {
      display: none;
    }
</style>

<?php
function getExcerpt($str, $startPos=0, $maxLength=100) {
  if(strlen($str) > $maxLength) {
    $excerpt   = substr($str, $startPos, $maxLength-3);
    $lastSpace = strrpos($excerpt, ' ');
    $excerpt   = substr($excerpt, 0, $lastSpace);
    $excerpt  .= '...';
    } else {
        $excerpt = $str;
    }

    return $excerpt;
}
?>