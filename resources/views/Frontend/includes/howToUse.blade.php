@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">

@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
<title>Bantuan | BOW</title>
@endsection

@section('content')
  <div class="container container-fluid">
    <div class="row">
      <div style="margin-top: 60px;" class="animated bounceInLeft">
        <h2 style="font-family: 'Comic Sans MS'; height: 38px;" class="site__title1">Bagaimana cara mendapatkan rekomendasi wisata?</h2>
        <div class="col-xs-12 col-lg-6">
          <img src="{{ URL::to('/') }}/images/recommendation_1.png" class="img-responsive" alt=""></img>
        </div>
        <div class="col-xs-12 col-lg-6" style="margin-top: 90px; margin-bottom: 60px;">
            <h2 style="color: #707175">Pilih Kategori Wisata</h2><br>
            <p class="place-card-subtitle">Terdapat 8 kategori wisata yang tersedia di BOW, yaitu Wisata Alam, Wisata Religi, Wisata Sejarah, Wisata Belanja, Wisata Satwa, Wisata Kuliner, Agrowisata dan Wisata Keluarga.</p>
            <p class="place-card-subtitle">Anda dapat memilih kategori wisata sesuai keinginan Anda.</p>
        </div>
      </div>
    </div>
  </div>

  <div style="background-color: #05c443;">
    <div class="container container-fluid">
      <div class="row">
        <div style="margin-top: 20px;" class="animated bounceInLeft">
          <div class="col-xs-12 col-lg-6" style="margin-top: 40px; color: white;">
              <h2>Saring</h2><br>
              <p class="place-card-subtitle">Terdapat dua saringan yang tersedia, yaitu saring kota dan harga.</p><br><br>
              <h2>Sortir</h2><br>
              <p class="place-card-subtitle">Selain saringan, Anda juga dapat melakukan sortir. Anda dapat melakukan sortir ulasan (banyak ke sedikit atau sedikit ke banyak) ataupun sortir kriteria (fasilitas, kebersihan, kenyamanan, pelayanan, harga).</p>
          </div>
          <div class="col-xs-12 col-lg-6">
            <img src="{{ URL::to('/') }}/images/recommendation_2.png" class="img-responsive" alt=""></img>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container container-fluid">
    <div class="row">
      <div style="margin-top: 60px;" class="animated bounceInLeft">
        <h2 style="font-family: 'Comic Sans MS'; height: 38px;" class="site__title1">Bagaimana cara memberikan ulasan?</h2>
        <div class="col-xs-12 col-lg-6" style="margin-top: 50px; margin-bottom: 60px;">
          <img src="{{ URL::to('/') }}/images/review_1.png" class="img-responsive" alt=""></img>
        </div>
        <div class="col-xs-12 col-lg-5 col-lg-offset-1"" style="margin-top: 90px; margin-bottom: 60px;">
            <h2 style="color: #707175">Pilih Objek Wisata</h2><br>
            <p class="place-card-subtitle">Pilihlah objek wisata yang ingin Anda ulas. Anda dapat memilih objek wisata sesuai keinginan Anda.</p>
            <p class="place-card-subtitle">Terdapat lebih dari 200 objek wisata yang ada pada BOW.</p>
            <p class="place-card-subtitle">Jika Anda familiar dengan objek wisata tersebut, maka Anda dapat mengulas pada objek wisata tersebut.</p>
        </div>
      </div>
    </div>
  </div>

  <div style="background-color: #F3F7FA;">
    <div class="container container-fluid">
      <div class="row">
        <div style="margin-top: 20px;" class="animated bounceInLeft">
          <div class="col-xs-12 col-lg-5" style="margin-top: 40px;">
            <h2 style="color: #707175">Pilih Tab Penilaian</h2><br>
            <p class="place-card-subtitle">Untuk dapat memberikan ulasan, pastikan Anda telah berada pada tab PENILAIAN.</p>
            <p class="place-card-subtitle">Pertama-tama, Anda perlu mengisi nilai dari masing-masing kriteria terlebih dahulu. Terdapat 5 kriteria yang tersedia, yaitu sebagai berikut.</p>
            <ol class="rule" style="list-style-type: none; margin-left: -40px;">
              <span><i class="material-icons left">style</i><li><h5 style="margin-top: 15px;">Fasilitas</h5></li></span>
              <p class="help-criteria">Fasilitas dari objek wisata terkait (semakin tinggi nilai fasilitas, maka semakin baik fasilitasnya).</p>
              <span><i class="material-icons left">spa</i><li><h5>Kebersihan</h5></li></span>
              <p class="help-criteria">Kebersihan dari objek wisata terkait (semakin tinggi nilai kebersihan, maka semakin baik kebersihannya).</p>
              <span><i class="material-icons left">favorite</i><li><h5>Kenyamanan</h5></li></span>
              <p class="help-criteria">Kenyamanan dari objek wisata terkait (semakin tinggi nilai kenyamanan, maka semakin baik kenyamanannya).</p>
              <span><i class="material-icons left">room_service</i><li><h5>Pelayanan</h5></li></span>
              <p class="help-criteria">Pelayanan dari objek wisata terkait (semakin tinggi nilai pelayanan, maka semakin baik pelayanannya).</p>
              <span><i class="material-icons left">monetization_on</i><li><h5>Harga</h5></li></span>
              <p class="help-criteria">Harga dari objek wisata terkait (semakin tinggi nilai harga, maka semakin sesuai harga yang ditawarkan dengan pengalaman wisata yang didapatkan).</p>
              <br><br><br>
              <h2 style="color: #707175">Isi Bidang Isian yang Tersedia</h2><br>
              <p class="place-card-subtitle">Selain memberikan penilaian, Anda juga harus mengisi judul beserta isi ulasan Anda.</p>
              <p class="place-card-subtitle">Setelah semua bidang isian diisi, maka klik tombol KIRIM.</p><br>
            </ol>
          </div>
          <div class="col-xs-12 col-lg-6 col-lg-offset-1" style="margin-top: 50px; margin-bottom: 60px;">
            <img src="{{ URL::to('/') }}/images/review_2.png" class="img-responsive"></img>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container container-fluid">
    <div class="row">
      <div style="margin-top: 20px;" class="animated bounceInLeft">
        <div style="padding-top: 60px; margin: auto; width: 60%; text-align: center;">
            <h2 style="color: #707175">Selesai</h2><br>
            <p class="place-card-subtitle">Untuk memastikan ulasan yang Anda berikan telah terkirim, silahkan pilih tab ULASAN. Jika ulasan yang Anda berikan telah muncul pada tab ULASAN, maka proses pemberian ulasan telah selesai.</p><br>
        </div>
        <div style="padding-bottom: 60px; margin: auto; width: 60%;">
          <img src="{{ URL::to('/') }}/images/review_3.png" class="img-responsive" alt=""></img>
        </div>
      </div>
    </div>

    </div>
  </div>
@endsection