@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">

@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
<title>Tentang | BOW</title>
@endsection

@section('content')
  <img src="{{ URL::to('/') }}/images/travel_bg.jpg" class="img-responsive animated fadeIn"></img>
  <div class="container container-fluid">
    <div class="row">
      <div class="table-of-contents">
        <div class="col-xs-12 col-lg-6 col-lg-offset-3" style="text-align: center;">
          <h2 style="color: #1d4971;">BOW</h2><br>
          <p>BOW (Bandung Objek Wisata) adalah sebuah <b>sistem rekomendasi</b> berbasis web yang bertujuan untuk memberikan rekomendasi objek wisata yang ada di <b>Bandung</b>. Sistem ini dibuat oleh <b>Wilson Brain Permana</b> pada tanggal 1 Maret 2017. Algoritma yang digunakan dalam memberikan rekomendasi pada sistem ini adalah <b>AHP dan TOPSIS</b>.<p>
          <p>Sistem ini dibuat untuk keperluan skripsi, guna mendapatkan gelar S.Kom. (Sarjana Komputer) di Universitas Multimedia Nusantara. Adapun dalam menyelesaikan sistem ini, Wilson dibimbing oleh <b>Dennis Gunawan, S.Kom., M.Sc.</b> BOW telah dibuat dengan baik dan benar, dan telah mengimplementasikan algoritma AHP dan TOPSIS dalam memberikan rekomendasi objek wisata di Bandung.</p>
        </div>
      </div>
    </div>
  </div>

  <hr class="hr-about">

  <div class="container container-fluid">
    <div class="row">
      <div class="table-of-contents">
        <div class="col-xs-12 col-lg-4 col-lg-offset-2 center">
          <img src="{{ URL::to('/') }}/images/wilson_brain_permana.png" class="about-profile"><br><br>
        </div>
        <div class="col-xs-12 col-lg-4" style="text-align: center; margin-top: 30px;">
          <h3>Wilson Brain Permana</h3>
          <p style="color: grey; font-size: 14px;">Peneliti</p><br>
          <p style="font-size: 13px;">Wilson Brain Permana adalah mahasiswa jurusan Teknik Informatika di Univeristas Multimedia Nusantara. Wilson Brain Permana merupakan pembuat sistem rekomendasi BOW.</p><br>
          <p>IKUTI</p>
          <a href="https://www.facebook.com/iconbrain96" target="_blank"><img src="{{ URL::to('/') }}/images/fb.png" style="width: 25px; height: 25px;"></a>
          <a href="https://twitter.com/iconbrain96" target="_blank"><img src="{{ URL::to('/') }}/images/twitter.png" style="width: 23px; height: 23px;"></a>
        </div>
      </div>
    </div>
  </div>

  <div class="container container-fluid">
    <div class="row">
      <div class="table-of-contents">
        <div class="col-xs-12 col-lg-4 col-lg-offset-2" style="text-align: center; margin-top: 30px;">
          <h3>Dennis Gunawan</h3>
          <p style="color: grey; font-size: 14px;">Pembimbing</p><br>
          <p style="font-size: 13px;">Dennis Gunawan adalah dosen Teknik Informatika di Universitas Multimedia Nusantara. Dennis Gunawan merupakan pembimbing skripsi yang telah membantu peneliti dalam menyelesaikan skripsi ini.</p><br>
          <p>IKUTI</p>
          <a href="https://www.facebook.com/dennisgunawan90" target="_blank"><img src="{{ URL::to('/') }}/images/fb.png" style="width: 25px; height: 25px;"></img></a>
          <a href="https://twitter.com/dennisgunawan90" target="_blank"><img src="{{ URL::to('/') }}/images/twitter.png" style="width: 23px; height: 23px;"></a>
        </div>
        <div class="col-xs-12 col-lg-4" style="text-align: center; margin-top: 30px;">
          <img src="{{ URL::to('/') }}/images/dennis_gunawan.jpg" class="about-profile"><br><br>
        </div>
      </div>
    </div>
  </div>

  <img src="{{ URL::to('/') }}/images/travel_bg_footer.jpg" class="img-responsive table-of-contents"></img>
@endsection

@push('scripts')
@endpush