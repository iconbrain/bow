@if(Auth::check())
	<li>
		<div class="userView">
            <div class="background">
              <img src="{{ URL::to('/') }}/images/sidenav_bg.png" class="responsive-img">
            </div>
            <a href="#!user"><img class="circle" src="{{ URL::to('/') }}/images/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" ></a>
            <a><span class="white-text name">{{ Auth::user()->name }}</span></a>
            <a><span class="white-text email">{{ Auth::user()->email }}</span></a>
        </div>
    </li>
	<li><a href="{{ URL::to('/tentang') }}" class="b-custom"><i class="material-icons left">contacts</i>Tentang</a></li>
	<li><a href="{{ URL::to('/credits') }}" class="b-custom"><i class="material-icons left">star</i>Kredit</a></li>
	<li><a href="{{ URL::to('/bantuan') }}" class="b-custom"><i class="material-icons left">help_outline</i>Bantuan</a></li>
	<li class="divider"></li>
	<li><a href="{{ URL::to('/account/profile') }}" class="b-custom"><i class="material-icons left">account_circle</i>Profil</a></li>
	<li><a href="{{ URL::to('/account/bookmark') }}" class="b-custom"><i class="material-icons left">bookmark</i>Bookmark</a></li>
	<li class="divider"></li>
	<li><a href="/logout" class="b-custom"><i class="material-icons left">exit_to_app</i>Keluar</a></li>
@else
	<div style="background-color: #33b60f; height: 120px; margin-top: -10px;">
		<h3>
			<img src="{{ URL::to('/') }}/images/logo_sidenav.png" alt="bow" class="responsive-img" style="margin-top: 25px;">
		</h3>
	</div>
	<li><a href="{{ URL::to('/tentang') }}" class="b-custom"><i class="material-icons left">contacts</i>Tentang</a></li>
	<li><a href="{{ URL::to('/credits') }}" class="b-custom"><i class="material-icons left">star</i>Kredit</a></li>
	<li><a href="{{ URL::to('/bantuan') }}" class="b-custom"><i class="material-icons left">help_outline</i>Bantuan</a></li>
	<li class="divider"></li>
	<li><a class="b-custom" id="auth" onclick="document.getElementById('authentication').style.display='block'"><i class="material-icons left">lock_outline</i>Masuk</a></li>
@endif