<footer class="footer-lightgrey">
	<div class="container default mega-div">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12" style="margin-left: -15px">
							<div class="col-md-3">
								<img src="{{ URL::to('/') }}/images/logo.png" class="footer-logo"></img>
							</div>
							<div class="col-md-8">
								<b>© 2017 Bandung Objek Wisata.<br>Hak Cipta dilindungi.</b>
								<br><br><p class="w3-text-grey"><i>"Dan Bandung bagiku bukan cuma masalah geografis, lebih jauh dari itu melibatkan perasaan, yang bersamaku ketika sunyi."</i> - Pidi Baiq</p>
							</div>
						</div>
					</div>
					<br>
				</div>

				<div class="col-md-4">
					<div><b><i class="material-icons" style="font-size:14px;">call</i> Whatsapp / Telepon</b></div><p class="w3-text-grey">0822 9893 8485</p>
					<br>
					<div><b><i class="material-icons" style="font-size:14px;">email</i> Email</b></div><p class="w3-text-grey">wilson.brain@student.umn.ac.id</p>
					<div>
					</div>
					<br>
				</div>

				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<b>Terhubung dengan kami.</b></br></br>
							<a href="https://www.facebook.com/iconbrain96" target="_blank">
								<div class="fbimage w3-round-large"></div>
							</a>
							<a href="https://twitter.com/iconbrain96" target="_blank">
								<div class="twitterimage w3-round-large"></div>
							</a>
							<a href="https://plus.google.com/u/0/111834381677936465150" target="_blank">
								<div class="googleplusimage w3-round-large"></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>