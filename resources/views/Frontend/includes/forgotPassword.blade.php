@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
<title>Lupa Kata Sandi | BOW</title>
@endsection

@section('content')
<div style="padding: 20px;">
  <div class="row" style="margin-top: 40px; margin-bottom: 40px;">
    <div class="col s12 m8 offset-m2 l4 offset-l4">
      <div class="card z-depth-3">
        <div class="card-title" style="color: white; font-size: 24px; font-weight: 300; background-color: #2dbc0d;">
          <h4 style="margin-left: 20px;">Lupa Kata Sandi</h4>
        </div>
        <div class="card-content">
          <p class="grey-text text-muted" style="font-size: 14px; font-family: 'Arial'; text-align: justify;">Silahkan isi alamat email yang sah. Instruksi penggantian kata sandi akan dikirim ke kotak masuk email Anda.</p><br>
          <form role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="input-field">
                <input type="email" name="email" placeholder="Email" id="email">
            </div>
            @if ($errors->has('email'))
              <span class="help-block">
                <p style="color: red; font-size: 14px; font-family: 'Arial'; text-align: justify;">{{ $errors->first('email') }}</p>
              </span>
            @endif
            @if (session('status'))
              <span class="help-block">
                <p style="color: #4DAE4D; font-size: 14px; font-family: 'Arial'; text-align: justify;">{{ session('status') }}</p>
              </span>
            @endif
            <div id="message"></div>
            <br>
            <button class="btn waves-effect waves-light primary-color" style="color: white; width: 100px;" type="submit" name="action">Kirim</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
@endpush