<link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <ul class="w3-left logo-home">
        <a href="{{{ url('/') }}}"><img src="{{ URL::to('/') }}/images/logo_home.png" style="width:130px; height:40px; margin-top:5px;"></img></a>
      </ul>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::user())
        <li>
            <a href="{{ URL::to('tentang') }}" class="d-custom">TENTANG</a>
        </li>
        <li>
            <a href="{{ URL::to('credits') }}" class="d-custom">KREDIT</a>
        </li>
        <li style="margin-right: 10px;">
            <a href="{{ URL::to('bantuan') }}" class="d-custom">BANTUAN</a>
        </li>
        <li class="dropdown">
            <button href="#" class="w3-button w3-green w3-round dropdown-toggle navbar-right-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></button>
            <ul class="dropdown-menu mr30">
                <li><a href="/account/profile">Profil</a></li>
                <li><a href="{{ URL::to('account/bookmark') }}">Arsip</a></li>
                <li><hr class="dropdown-hr"></li>
                <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
            </ul>
        </li>
        @else
        <li>
            <a href="{{ URL::to('tentang') }}" class="d-custom">TENTANG</a>
        </li>
        <li>
            <a href="{{ URL::to('credits') }}" class="d-custom">KREDIT</a>
        </li>
        <li style="margin-right: 10px;">
            <a href="{{ URL::to('bantuan') }}" class="d-custom">BANTUAN</a>
        </li>
        <li>
            <button class="w3-button w3-green w3-round btn-login" id="auth" onclick="document.getElementById('authentication').style.display='block'">Masuk</button>
        </li>
        @endif
      </ul>
    </div><!--/.nav-collapse -->
</nav>

<div id="authentication" class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom w3-round-large w3-centered modal-custom">
        <div class="top15">
            <div id="Login" class="w3-container form">
                <div class="top15">
                    <center>
                        <button type="button" class="close right15" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('authentication').style.display='none'"><span aria-hidden="true">&times;</span></button>
                        <h3><b>Masuk BOW</b></h3>
                    </center>
                </div>
                <div class="w3-border-top"></div>
                <div class="w3-container w3-padding-0">
                    <div class="w3-section">
                        @if (count($errors->login) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->login->all() as $error)
                            <P>{{ $error }}</p>
                            @endforeach
                        </div>
                        @endif 
                        @if (Session::has('message'))
                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif
                        <form action="/login" method="POST">
                            {{ csrf_field() }} <input type="hidden" name="redirurl"
                            value="{{ $_SERVER['REQUEST_URI'] }}">
                            <h6>Email</h6>
                            <input name="email" class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="email" required>
                            <h6>Kata Sandi</h6>
                            <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" name="password" type="password" required>
                            <input class="w3-check" type="checkbox" checked="checked"> Ingat Saya
                            <input type="submit" class="w3-btn w3-btn-block w3-green w3-margin-top w3-round w3-input-height-50" value="MASUK">
                            <p style="text-align: center; margin-top: 15px;"><a href="{{ URL::to('/forgot-password') }}">Lupa Kata Sandi?</a></p>
                        </form>
                    </div>
                </div>
                <div class="w3-container w3-border-top w3-padding-16">
                    <button onclick="document.getElementById('authentication').style.display='none'" type="button" class="w3-btn w3-red w3-round">Batal</button>
                    <span class="w3-right"><a href="#" onclick="openForm('SignUp')">Belum memiliki akun?</a></span>
                </div>
            </div>
        </div>
        <div id="SignUp" class="w3-container form">
            <div class="top15">
                <center>
                    <button type="button" class="close right15" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('authentication').style.display='none'"><span aria-hidden="true">&times;</span></button>
                    <h3><b>Daftar BOW</b></h3>
                </center>
            </div>
            <div class="w3-border-top"></div>
            <div class="w3-container w3-padding-0">
                <div class="w3-section">
                    @if (count($errors->signup) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->signup->all() as $error)
                        <P>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif
                    <form action="/signup" method="POST" id="regForm">
                        {{ csrf_field() }} <input type="hidden" name="redirurl"
                        value="{{ $_SERVER['REQUEST_URI'] }}">
                        <h6>Nama Lengkap</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="text" name="name" required value="{{ old('name') }}">
                        <h6>Email</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="text" name="email" value="{{ old('email') }}" required>
                        <h6>Kata Sandi</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="password" name="password" required>
                        <h6>Konfirmasi Kata Sandi</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" required type="password" name="password_confirmation">
                        <button type="submit" class="w3-btn w3-btn-block w3-green w3-round w3-input-height-50">DAFTAR</button>
                    </form>
                </div>
            </div>
            <div class="w3-container w3-border-top w3-padding-16 ">
                <button onclick="openForm('Login')" type="button" class="w3-btn w3-red w3-round">Kembali</button>
            </div>
        </div>
    </div>
</div>
<div class="fluid-container"></div>
@push('scripts')
    <script>    
        openForm("Login");
        function openForm(formName) {

            var x = document.getElementsByClassName("form");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }
            document.getElementById(formName).style.display = "block";  
        }

        // Get the modal
        var modal = document.getElementById('authentication');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
    @if (Session::has('message'))
    <script>  $('#auth').click(); </script>
    @endif @if($errors->login->any())
    <script>  $('#auth').click();</script>
    @endif @if($errors->signup->any())
    <script>  $('#auth').click(); openForm('SignUp');</script>
    @endif
@endpush