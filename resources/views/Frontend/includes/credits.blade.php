@extends('Frontend.layouts.master')

<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link type="text/css" rel="stylesheet" href="{{ asset("css/materialize.min.css") }}" media="screen,projection"/>
<link rel="stylesheet" href="{{ asset("css/animate.min.css") }}">

@push('stylesheets')
<link type="text/css" rel="stylesheet" href="{{ asset("css/custom.css") }}"/>
@include('Frontend.includes.header')
@endpush

@section('title')
<title>Kredit | BOW</title>
@endsection

@section('content')
  <div class="container container-fluid">
    <div class="row">
      <h1 id="an" style="margin-top: 40px; font-family: 'Comic Sans MS'" class="site__title">Kredit</h1>
    
      <div style="margin-top: 20px;" class="animated bounceInUp">
        <div class="col l4 center-align three-column" style="padding-top: 22px;">
          <img src="{{ URL::to('/') }}/images/gplace_api.png" class="img-responsive" alt="" style="display: inline-block;"></img>
          <h4 style="margin-top: 30px; color: #ff5c33; margin-bottom: 20px;">Google Places API</h4>
          <p class="p-desc">Nama, Alamat, Deskripsi singkat, Jam Operasional, Harga, Nomor Telepon dan Gambar setiap objek wisata yang ada pada BOW didapat melalui API yang disediakan oleh Google Places.</p>
        </div>
        <div class="col l4 center-align three-column" style="padding-top: 22px;">
          <img src="{{ URL::to('/') }}/images/tripadvisor.png" class="img-responsive" alt="" style="display: inline-block;"></img>
          <h4 style="margin-top: 30px; color: #ff5c33; margin-bottom: 20px;">TripAdvisor</h4>
          <p class="p-desc">TripAdvisor digunakan untuk menentukan <i>default rating</i> pada setiap kriteria dari masing-masing objek wisata yang ada pada BOW.</p>
        </div>
        <div class="col l4 center-align three-column" style="padding-top: 22px;">
          <img src="{{ URL::to('/') }}/images/materializecss.png" class="img-responsive" alt="" style="display: inline-block;"></img>
          <h4 style="margin-top: 30px; color: #ff5c33; margin-bottom: 20px;">Materialize</h4>
          <p class="p-desc">Materialize adalah salah satu dari tiga CSS yang digunakan untuk membuat halaman <i>frontend</i> dari BOW. Dokumentasi lengkap dari Materialize dapat ditemukan di <a href="http://materializecss.com" style="color: #2b6aa0;" target="_blank">materializecss.com</a></p>
        </div>
      </div>
    </div>

    <div class="row">
      <div style="margin-top: 60px;" class="animated bounceInUp">
        <div class="col l4 center-align three-column" style="padding-top: 22px;">
          <img src="{{ URL::to('/') }}/images/bootstrap.png" class="img-responsive" alt="" style="display: inline-block;"></img>
          <h4 style="margin-top: 30px; color: #ff5c33; margin-bottom: 20px;">Bootstrap</h4>
          <p class="p-desc">Bootstrap merupakan salah satu dari tiga CSS yang digunakan untuk membuat halaman <i>frontend</i> dari BOW. Dokumentasi lengkap dari Bootstrap dapat ditemukan di <a href="http://getbootstrap.com" style="color: #2b6aa0;" target="_blank">getbootstrap.com</a></p>
        </div>
        <div class="col l4 center-align three-column" style="padding-top: 22px;">
          <img src="{{ URL::to('/') }}/images/w3css.png" class="img-responsive" alt="" style="display: inline-block;"></img>
          <h4 style="margin-top: 30px; color: #ff5c33; margin-bottom: 20px;">W3.CSS</h4>
          <p class="p-desc">W3.CSS juga merupakan salah satu dari tiga CSS yang digunakan untuk membuat halaman <i>frontend</i> dari BOW. Dokumentasi lengkap dari W3.CSS dapat ditemukan di <a href="https://www.w3schools.com/w3css/" style="color: #2b6aa0;" target="_blank">w3schools.com</a></p>
        </div>
        <div class="col l4 center-align three-column" style="padding-top: 22px;">
          <img src="{{ URL::to('/') }}/images/gentelella.png" class="img-responsive" alt="" style="display: inline-block;"></img>
          <h4 style="margin-top: 30px; color: #ff5c33; margin-bottom: 20px;">Gentelella Alela!</h4>
          <p class="p-desc">Gentelella Alela! adalah template yang digunakan untuk membuat halaman  <i>admin</i> dari BOW. Gentelella Alela! dapat Anda temukan pada <a href="https://github.com/FlorientR/laravel-gentelella" style="color: #2b6aa0;" target="_blank">Github</a>.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="container container-fluid">
    <div class="animated bounceInUp" style="margin-bottom: 60px;">
      <p style="text-align: justify; font-family: 'Comic Sans MS'"><b><sup>*</sup>Catatan</b>: Kumpulan data objek wisata yang ada pada BOW didapatkan melalui kumpulan objek-objek wisata di Bandung yang disediakan oleh <a href="http://www.ragamtempatwisata.com/2013/05/daftar-nama-tempat-wisata-di-bandung.html" style="color: #2b6aa0;" target="_blank">ragamtempatwisata.com</a> dan <a href="http://tempatwisatadibandung.info/tempat-wisata-bandung/" style="color: #2b6aa0;" target="_blank">Tempat Wisata Di Bandung</a>. Selanjutnya, data spesifik dari masing-masing objek wisata (seperti alamat, lokasi, nomor telepon, dll) dikumpulkan menggunakan API yang disediakan oleh <a href="https://developers.google.com/places/" style="color: #2b6aa0;" target="_blank">Google Places API</a>.</p>
    </div>
  </div>
@endsection

<style type="text/css">
  #an {
    -webkit-animation-duration: 3s;
    -webkit-animation-delay: 2s;
    -webkit-animation-iteration-count: infinite;

    -moz-animation-duration: 3s;
    -moz-animation-delay: 2s;
    -moz-animation-iteration-count: infinite;

    -ms-animation-duration: 3s;
    -ms-animation-delay: 2s;
    -ms-animation-iteration-count: infinite;

    -o-animation-duration: 3s;
    -o-animation-delay: 2s;
    -o-animation-iteration-count: infinite;

    animation-duration: 3s;
    animation-delay: 2s;
    animation-iteration-count: infinite;
  }
</style>

@push('scripts')
<script>
  $('#an').addClass('animated infinite bounce');
</script>
@endpush