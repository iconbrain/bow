<link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
<link rel="stylesheet" href="{{ asset("css/bootstrap.css") }}">
<link rel="stylesheet" href="{{ asset("css/materialize.css") }}">
<link rel="stylesheet" href="{{ asset("css/materialize.min.css") }}">
<link rel="stylesheet" href="{{ asset("css/w3.css") }}">
<link rel="stylesheet" href="{{ asset("css/custom.css") }}">
<link rel="stylesheet" href="{{ asset("css/awesomplete.css") }}" />
<link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">

<header class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="{{{ url('/') }}}" class="left40"><img src="{{ URL::to('/') }}/images/logo_home.png" style="width:130px; height:40px; margin-top:5px;" class="top15"></img></a>
        <ul class="right hide-on-med-and-down">
          <li>
            <div class="card h50">
              <form action="/search/results" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="input-field">
                  <input class="awesomplete" list="mylist" id="myinput" name="placeInput" placeholder="Temukan objek wisata..." style="color: black; width: 350px;" />
                  <datalist id="mylist">
                    @foreach($search as $s)
                    <option>{{ $s->name }}</option>
                    @endforeach
                  </datalist>
                  <label class="label-icon" for="search"><i class="material-icons" style="color: black; margin-top: 13px; margin-left: 5px;">search</i>
                  </label>
                </div>
              </form>
            </div>
          </li>

          @if (Auth::user())
          <li>
            <a class="login-header" href="{{ URL::to('tentang') }}">TENTANG</a>
          </li>
          <li style="margin-right: -20px; margin-left: -20px;">
            <a class="login-header" href="{{ URL::to('credits') }}">KREDIT</a>
          </li>
          <li style="margin-right: -20px;">
            <a class="login-header" href="{{ URL::to('bantuan') }}">BANTUAN</a>
          </li>
          <li style="margin-right: -20px;">
            <a class="dropdown-button dropdown-login" href="#" data-activates="dropdown1" style="color: grey;">{{Auth::user()->name}}<span class="caret" style="margin-left: 10px;"></span></a>
          </li>
          <li class="mr30">
            <!-- Dropdown Structure -->
            <ul id="dropdown1" class="dropdown-content" style="display: block !important;">
                <li><a href="/account/profile">Profil</a></li>
                <li><a href="{{ URL::to('account/bookmark') }}">Arsip</a></li>
                <li class="divider"></li>
                <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
            </ul>
          </li>
          @else
            <li>
                <a class="login-header" href="{{ URL::to('tentang') }}">TENTANG</a>
            </li>
            <li style="margin-right: -20px; margin-left: -20px;">
                <a class="login-header" href="{{ URL::to('credits') }}">KREDIT</a>
            </li>
            <li style="margin-right: -20px;">
                <a class="login-header" href="{{ URL::to('bantuan') }}">BANTUAN</a>
            </li>
            <li>
                <a class="login-header" id="auth" onclick="document.getElementById('authentication').style.display='block'">MASUK</a>
            </li>
          @endif
        </ul>

        <a href="#" data-activates="slide-out" class="button-collapse" style="color: #28BD07; float: right;"><i class="material-icons">menu</i></a>

        <ul id="slide-out" class="side-nav">
            @include('Frontend.includes.mobile')
        </ul>
      </div>
    </nav>
</header>

<div class="section search-bar hide-on-large-only background-search">
    <div class="container">
        <div class="card">
            <form action="/search/results" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="input-field">
                  <input class="awesomplete" list="mylist" id="myinput" name="placeInput" placeholder="Temukan objek wisata..." style="margin-left: 40px; width: 86%;">
                  <datalist id="mylist">
                    @foreach($search as $s)
                    <option>{{ $s->name }}</option>
                    @endforeach
                  </datalist>
                  <label class="label-icon" for="search"><i class="material-icons" style="color: black; margin-left: 10px;">search</i>
                  </label>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="authentication" class="w3-modal" style="z-index: 999;">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom w3-round-large w3-centered modal-custom">
        <div class="top15">
            <div id="Login" class="w3-container form">
                <div class="top15">
                    <center>
                        <button type="button" class="close right15" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('authentication').style.display='none'"><span aria-hidden="true">&times;</span></button>
                        <h3><b>Masuk BOW</b></h3>
                    </center>
                </div>
                <div class="w3-border-top"></div>
                <div class="w3-container w3-padding-0">
                    <div class="w3-section">
                        @if (count($errors->login) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->login->all() as $error)
                            <P>{{ $error }}</p>
                            @endforeach
                        </div>
                        @endif 
                        @if (Session::has('message'))
                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif
                        <form action="/login" method="POST">
                            {!! csrf_field() !!} <input type="hidden" name="redirurl"
                            value="{{ $_SERVER['REQUEST_URI'] }}">
                            <h6>Email</h6>
                            <input name="email" value="{{ old('email') }}" class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="email" required style="padding-left: 10px; width: 97.5%">
                            <h6>Kata Sandi</h6>
                            <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" name="password" type="password" required style="padding-left: 10px; width: 97.5%">
                            <input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" /><label for="filled-in-box" style="color: black; font-weight: normal;">Ingat Saya</label>
                            <input type="submit" class="w3-btn w3-btn-block w3-green w3-margin-top w3-round w3-input-height-50" value="MASUK">
                            <p style="text-align: center; margin-top: 15px;"><a href="{{ URL::to('/forgot-password') }}">Lupa Kata Sandi?</a></p>
                        </form>
                    </div>
                </div>
                <div class="w3-container w3-border-top w3-padding-16">
                    <button onclick="document.getElementById('authentication').style.display='none'" type="button" class="w3-btn w3-red w3-round">Batal</button>
                    <span class="w3-right"><a href="#" onclick="openForm('SignUp')">Belum memiliki akun?</a></span>
                </div>
            </div>
        </div>
        <div id="SignUp" class="w3-container form">
            <div class="top15">
                <center>
                    <button type="button" class="close right15" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('authentication').style.display='none'"><span aria-hidden="true">&times;</span></button>
                    <h3><b>Daftar BOW</b></h3>
                </center>
            </div>
            <div class="w3-border-top"></div>
            <div class="w3-container w3-padding-0">
                <div class="w3-section">
                    @if (count($errors->signup) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->signup->all() as $error)
                        <P>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif
                    <form action="/signup" method="POST" id="regForm">
                        {{ csrf_field() }} <input type="hidden" name="redirurl"
                        value="{{ $_SERVER['REQUEST_URI'] }}">
                        <h6>Nama Lengkap</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="text" name="name" required value="{{ old('name') }}" style="padding-left: 10px; width: 97.5%">
                        <h6>Email</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="text" name="email" value="{{ old('email') }}" required style="padding-left: 10px; width: 97.5%">
                        <h6>Kata Sandi</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" type="password" name="password" required style="padding-left: 10px; width: 97.5%">
                        <h6>Konfirmasi Kata Sandi</h6>
                        <input class="w3-input w3-border w3-margin-bottom w3-round w3-input-height-50" required type="password" name="password_confirmation" style="padding-left: 10px; width: 97.5%">
                        <button type="submit" class="w3-btn w3-btn-block w3-green w3-round w3-input-height-50">DAFTAR</button>
                    </form>
                </div>
            </div>
            <div class="w3-container w3-border-top w3-padding-16 ">
                <button onclick="openForm('Login')" type="button" class="w3-btn w3-red w3-round">Kembali</button>
            </div>
        </div>
    </div>
</div>
<div class="fluid-container"></div>

<style type="text/css">
    input::-webkit-calendar-picker-indicator {
      display: none;
    }
</style>

@push('scripts')
    <script src="{{ asset("js/awesomplete.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/materialize.min.js") }}"></script>
    <script>
        openForm("Login");
        function openForm(formName) {

            var x = document.getElementsByClassName("form");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }
            document.getElementById(formName).style.display = "block";  
        }

        // Get the modal
        var modal = document.getElementById('authentication');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
    <script>
        $(document).ready(function(){
            $('.parallax').parallax();
            $('.slider').slider({full_width: true, indicators: false});
            $('.dropdown-button').dropdown('close');
            $('.button-collapse').sideNav({menuWidth: 240, activationWidth: 70});
        });
    </script>
    <script>
        var input = document.getElementById("myinput");
        var awesomplete = new Awesomplete(input, { minChars: 2, maxItems: 200} );
    </script>
    @if (Session::has('message'))
    <script>  $('#auth').click(); </script>
    @endif @if($errors->login->any())
    <script>  $('#auth').click();</script>
    @endif @if($errors->signup->any())
    <script>  $('#auth').click(); openForm('SignUp');</script>
    @endif
@endpush