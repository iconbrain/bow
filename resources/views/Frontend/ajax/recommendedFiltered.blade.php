@if(count($places) > 0)
  <div class="col-xs-12 col-lg-6">
    @foreach($places as $item)
      <div class="card horizontal w3-round-large">
        <div class="card-stacked">
          <div class="card-content top-custom-card">
            <a href="{{ URL::to('/place/'.$item->place_prefix) }}">
              @if(strpos($item->image,'https://') !== false)
                <img src="{{$item->image}}" class="w3-round-large img-filtered">
              @else
                <img src="{{ URL::to('/') }}/images/{{$item->image}}" class="w3-round-large img-filtered">
              @endif
            </a>

            @if($item->avgRating > 0 AND $item->avgRating < 1)
              <span  data-position="right" class="badge" style="color: #fff; background-color: #d11717; border-radius: 5px;">{{ $item->avgRating }}</span>
            @elseif($item->avgRating >= 1 AND $item->avgRating < 2)
              <span  data-position="right" class="badge" style="color: #fff; background-color: red; border-radius: 5px;">{{ $item->avgRating }}</span>
            @elseif($item->avgRating >= 2 AND $item->avgRating < 3)
              <span  data-position="right" class="badge" style="color: #fff; background-color: #F62; border-radius: 5px;">{{ $item->avgRating }}</span>
            @elseif($item->avgRating >= 3 AND $item->avgRating < 4)
              <span  data-position="right" class="badge" style="color: #fff; background-color: #ff9e20; border-radius: 5px;">{{ $item->avgRating }}</span>
            @elseif($item->avgRating >= 4 AND $item->avgRating < 5)
              <span  data-position="right" class="badge" style="color: #fff; background-color: #efb915; border-radius: 5px;">{{ $item->avgRating }}</span>
            @elseif($item->avgRating == 5)
              <span  data-position="right" class="badge" style="color: #fff; background-color: #F9BC00; border-radius: 5px;">{{ $item->avgRating }}</span>
            @endif

            <h4 class="categories-title" style="margin-left: 120px;"><a href="{{ URL::to('/place/'.$item->place_prefix) }}" class="a-custom">{{ $item->name }}</a></h4>
            <a href="{{ URL::to('/place/'.$item->place_prefix) }}"><p style="color:gray;margin-left: 120px;">{{ $item->address }}</p></a>

            @if(Auth::check())
              @foreach ($bmk as $bm)
                @if($bm->place_id == $item->place_id)
                  <i class="material-icons" style="float: right; color: #019987">turned_in</i>
                @endif
              @endforeach
            @endif
          </div>
          <div class="card-action">
            <p align="justify">
              <?php
                $excerpt = getExcerpt($item->description, 0, 290);
              ?>
              {!! $excerpt !!}
            </p>
          </div>
          <div class="card-action">
            {{ $item->location_name }}
            <i class="material-icons left" style="color:gray">location_on</i><br>
            <div style="display: inline-flex;">
              <i class="material-icons left" style="color:gray">schedule</i>
              <div class="col s12" style="margin-left:-10px;">{!! $item->operational_hours !!}</div>
            </div><br>
            @if($item->phone_number == NULL) <i class="material-icons left" style="color:gray">perm_phone_msg</i>(Belum Tersedia)
            @else<i class="material-icons left" style="color:gray">perm_phone_msg</i>{{ $item->phone_number }}@endif<br>
            <div class="nulpad">
              <i class="material-icons left" style="color:gray">attach_money</i>
            </div>
            <div class="col s10" style="margin-left:-10px;">{!! $item->price !!}</div><br>
          </div>
          <div class="card-action">
            <div style="float: left;">
              <i class="material-icons left greycolor">rate_review</i>{{ $item->sumReview }} ulasan
            </div>
            <div style="float: right;">
              <i class="material-icons left greycolor">bookmark</i>{{ $item->sumBookmark }} ditandai
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@else
  <div class="col-xs-12 col-lg-6">
    <div class="row">
      <div class="col s12 m9 offset-l2">
        <div class="card-panel teal">
          <span class="white-text">
            <h3 style="text-align:center;">Tidak Tersedia</h3>
            <p style="text-align:center;">Silahkan pilih saringan yang lain.</p>
          </span>
        </div>
        <div style="text-align: center;">
          <p>Terdapat objek wisata yang tidak terdapat dalam sistem kami?</p>
          <p><a style="color: #ff5c33;" href="{{ URL::to('/places/add') }}">Tambah Objek Wisata</a></p>
        </div>
      </div>
    </div>
  </div>
@endif

<?php
function getExcerpt($str, $startPos=0, $maxLength=100) {
  if(strlen($str) > $maxLength) {
    $excerpt   = substr($str, $startPos, $maxLength-3);
    $lastSpace = strrpos($excerpt, ' ');
    $excerpt   = substr($excerpt, 0, $lastSpace);
    $excerpt  .= '...';
    } else {
        $excerpt = $str;
    }

    return $excerpt;
}
?>