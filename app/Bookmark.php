<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $table = 'travel__bookmark';
	public $timestamps = false;
	protected $primaryKey = 'id';
}
