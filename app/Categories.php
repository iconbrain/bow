<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'travel__categories';
	public $timestamps = false;
	protected $primaryKey = 'category_id';
}
