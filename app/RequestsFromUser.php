<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestsFromUser extends Model
{
    protected $table = 'travel__requests';
	public $timestamps = false;
	protected $primaryKey = 'id';
}
