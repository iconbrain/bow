<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Places;
use App\Categories;
use App\Locations;
use App\Users;
use App\Tasks;
use Carbon\Carbon;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function check()
    {
        if(Auth::check()) {
            return Redirect::to('adminpanel/home');
        } else {
            return Redirect::to('adminpanel/login');
        }
    }

    public function login()
    {
        return view('Backend.auth.login');
    }

    public function postLogin(Request $req)
    {
        $rules = array(
            'email'     => 'required|email',
            'password'  => 'required|min:6' 
        );

        $message = array(
            'email.required'    => 'Bidang isian email wajib diisi.',
            'email.email'       => 'Bidang isian email tidak valid.',
            'password.required' => 'Bidang isian password wajib diisi.',
            'password.min'      => 'Bidang isian password minimal 6 karakter.',
        );

        $validator = Validator::make (Input::all (), $rules, $message);

        if ($validator->fails ()) {
            return Redirect::back()->withErrors($validator, 'login')->withInput();
        } else {
            // if (Auth::guard('admin')->attempt(array(
            //     'email'     => $req['email'],
            //     'password'  => $req['password'],
            //     'role_id'   => '1'
            // )))
            if (Auth::attempt(array(
                'email'     => $req['email'],
                'password'  => $req['password'],
                'role_id'   => '1'
            )))
            {
                session ([
                    'email' => $req['email']
                    // 'email' => Auth::user()->email
                ]);
                return Redirect::to('adminpanel/home');
            } else {
                Session::flash('message', "Akun tidak ditemukan. Silahkan coba lagi.");
                return Redirect::back();
            }
        }
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect::to('adminpanel');
    }

    public function index()
    {
        if(Auth::check()) {
            $query = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN tp.facility_criteria
                            ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN tp.cleanliness_criteria
                            ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN tp.comfort_criteria
                            ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN tp.service_criteria
                            ELSE (tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN tp.price_criteria
                            ELSE (tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS price_attr'))
                    ->get();

            $sumFacility = 0;
            $sumCleanliness =0;
            $sumComfort =0;
            $sumService =0;
            $sumPrice = 0;
            foreach($query as $q) {
                $sumFacility += pow($q->facility_attr, 2);
                $sumCleanliness += pow($q->cleanliness_attr, 2);
                $sumComfort += pow($q->comfort_attr, 2);
                $sumService += pow($q->service_attr, 2);
                $sumPrice += pow($q->price_attr, 2);
            }
            $sumFacility = sqrt($sumFacility);
            $sumCleanliness = sqrt($sumCleanliness);
            $sumComfort = sqrt($sumComfort);
            $sumService = sqrt($sumService);
            $sumPrice = sqrt($sumPrice);

            $totalUser = Users::where('role_id','2')->count();
            $totalObjekWisata = Places::count();
            $totalReview = DB::table('reviews')->count();
            $totalKomentar = DB::table('comments')->count();

            $recentReview = DB::table('reviews as r')->select(DB::raw('r.body as body, u.name as name, r.created_at as rcreatedat'))
                            ->leftjoin('users as u','u.id','=','r.author_id')
                            ->orderBy('r.created_at','desc')
                            ->limit(5)
                            ->get();

            $recentComment = DB::table('comments as c')->select(DB::raw('c.body as body, u.name as name, c.created_at as ccreatedat'))
                            ->leftjoin('users as u','u.id','=','c.from_user')
                            ->orderBy('c.created_at','desc')
                            ->limit(5)
                            ->get();

            $tbSelatan  = Places::where('location_id','1')->count();
            $tbBarat    = Places::where('location_id','2')->count();
            $tbUtara    = Places::where('location_id','3')->count();
            $tbKota     = Places::where('location_id','4')->count();
            $tbTimur    = Places::where('location_id','5')->count();

            $newSignUp = Users::where('role_id','2')
                            ->orderBy('created_at','desc')
                            ->limit(3)
                            ->get();
            $sumNewSignUp = Users::select(DB::raw('(SELECT count(*) FROM users WHERE role_id=2) as sum'))
                            ->first();

            $tasks = Tasks::orderBy('ordering','asc')->get();

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            $params = array(
                'query' => $query,
                'sumFacility' => $sumFacility,
                'sumCleanliness' => $sumCleanliness,
                'sumComfort' => $sumComfort,
                'sumService' => $sumService,
                'sumPrice' => $sumPrice,
                'totalUser' => $totalUser,
                'totalObjekWisata' => $totalObjekWisata,
                'totalReview' => $totalReview,
                'totalKomentar' => $totalKomentar,
                'recentReview' => $recentReview,
                'recentComment' => $recentComment,
                'tbSelatan' => $tbSelatan,
                'tbBarat' => $tbBarat,
                'tbUtara' => $tbUtara,
                'tbKota' => $tbKota,
                'tbTimur' => $tbTimur,
                'sumNewSignUp' => $sumNewSignUp,
                'newSignUp' => $newSignUp,
                'tasks' => $tasks,
                'notifSum' => $notifSum,
                'notif' => $notif,
            );  

            return view('Backend.home', $params);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function wisataAlam()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','1')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.wisataAlam', $params);
    }

    public function wisataReligi()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','2')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.wisataReligi', $params);
    }

    public function wisataSejarah()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','3')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.wisataSejarah', $params);
    }

    public function wisataBelanja()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','4')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.wisataBelanja', $params);
    }

    public function wisataSatwa()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','5')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );

        return view('Backend.ahp-topsis.wisataSatwa', $params);
    }

    public function wisataKuliner()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','6')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.wisataKuliner', $params);
    }

    public function agrowisata()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','7')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.agrowisata', $params);
    }

    public function wisataKeluarga()
    {
        $query = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                    ->where('category_id','8')
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);
        $params = array(
            'query' => $query,
            'sumFacility' => $sumFacility,
            'sumCleanliness' => $sumCleanliness,
            'sumComfort' => $sumComfort,
            'sumService' => $sumService,
            'sumPrice' => $sumPrice,
        );  

        return view('Backend.ahp-topsis.wisataKeluarga', $params);
    }

    public function all()
    {
        if(Auth::check()) {
            $query = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

            $sumFacility = 0;
            $sumCleanliness =0;
            $sumComfort =0;
            $sumService =0;
            $sumPrice = 0;
            foreach($query as $q) {
                $sumFacility += pow($q->facility_attr, 2);
                $sumCleanliness += pow($q->cleanliness_attr, 2);
                $sumComfort += pow($q->comfort_attr, 2);
                $sumService += pow($q->service_attr, 2);
                $sumPrice += pow($q->price_attr, 2);
            }
            $sumFacility = sqrt($sumFacility);
            $sumCleanliness = sqrt($sumCleanliness);
            $sumComfort = sqrt($sumComfort);
            $sumService = sqrt($sumService);
            $sumPrice = sqrt($sumPrice);

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            $fasilitas = DB::table('ahp_matrix as am')
                            ->select(DB::raw('bobot'))
                            ->where('id','1')
                        ->first();
            $kebersihan = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','2')
                        ->first();
            $kenyamanan = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','3')
                        ->first();
            $pelayanan = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','4')
                        ->first();
            $harga = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','5')
                        ->first();

            $itemsFacility = array();
            $itemsCleanliness = array();
            $itemsComfort = array();
            $itemsService = array();
            $itemsPrice = array();
            
            foreach($query as $q) {
                $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas->bobot;
                $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan->bobot;
                $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan->bobot;
                $itemsService[] = $q->service_attr/$sumService * $pelayanan->bobot;
                $itemsPrice[] = $q->price_attr/$sumPrice * $harga->bobot;
            }

            foreach ($query as $q) {
                $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*0.101) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*0.202) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*0.151) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*0.234) - max($itemsPrice)),2));
                $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*0.101) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*0.312) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*0.202) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*0.151) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*0.234) - min($itemsPrice)),2));
                $rank = $negSolution/($negSolution+$posSolution);

                DB::update('UPDATE travel__places
                      SET result =?
                      WHERE place_id = ?', [$rank, $q->place_id]);
            }

            DB::statement(DB::raw('SET @row = 0'));
            $query2 = Places::select(DB::raw('@row := @row + 1 as row'),'place_id','name','result','tc.category_name as category_name')
                    ->leftjoin('travel__categories as tc','tc.category_id','=','travel__places.category_id')
                    ->orderBy('result','desc')
                    ->get();

            $query3 = DB::table('ahp_matrix as am')
                        ->select('*')
                        ->get();
            $sumFacilityAHP = DB::table('ahp_matrix as am')->sum('facility');
            $sumCleanlinessAHP = DB::table('ahp_matrix as am')->sum('cleanliness');
            $sumComfortAHP = DB::table('ahp_matrix as am')->sum('comfort');
            $sumServiceAHP = DB::table('ahp_matrix as am')->sum('service');
            $sumPriceAHP = DB::table('ahp_matrix as am')->sum('price');

            $vp = array();
            foreach($query3 as $q) {
                $vp[] = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);
            }

            $hasilkali = array();
            foreach($query3 as $q) {
                $hasilkali[] = round(($q->facility*$vp[0])+($q->cleanliness*$vp[1])+($q->comfort*$vp[2])+($q->service*$vp[3])+($q->price*$vp[4]),3);
            }

            $hasilkali_length = count($hasilkali);
            $sumLambdaMaks = 0;
            for($i=0;$i<$hasilkali_length;$i++) {
                $sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
            }
            $lambdaMaks = round($sumLambdaMaks/5,3);
            $ci = round((round($sumLambdaMaks/5,3)-$i)/($i-1),3);
            $cr = round($ci/1.12,3);

            if($cr <= 0.1) {
                foreach ($query3 as $q) {
                    $priorityVector = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);

                    DB::update('UPDATE ahp_matrix
                          SET bobot =?
                          WHERE name = ?', [$priorityVector, $q->name]);
                }
            }

            $params = array(
                'query'             => $query,
                'query2'            => $query2,
                'sumFacility'       => $sumFacility,
                'sumCleanliness'    => $sumCleanliness,
                'sumComfort'        => $sumComfort,
                'sumService'        => $sumService,
                'sumPrice'          => $sumPrice,
                'notifSum'          => $notifSum,
                'notif'             => $notif,
                'fasilitas'         => $fasilitas->bobot,
                'kebersihan'        => $kebersihan->bobot,
                'kenyamanan'        => $kenyamanan->bobot,
                'pelayanan'         => $pelayanan->bobot,
                'harga'             => $harga->bobot,
            );

            return view('Backend.ahp-topsis.all', $params);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function ahp()
    {
        if(Auth::check()) {
            $query = DB::table('ahp_matrix as am')
                        ->select('*')
                        ->get();
            $sumFacility = DB::table('ahp_matrix as am')->sum('facility');
            $sumCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
            $sumComfort = DB::table('ahp_matrix as am')->sum('comfort');
            $sumService = DB::table('ahp_matrix as am')->sum('service');
            $sumPrice = DB::table('ahp_matrix as am')->sum('price');

            //here
            $vp = array();
            foreach($query as $q) {
                $vp[] = round((round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3))/5,3);
            }

            $hasilkali = array();
            foreach($query as $q) {
                $hasilkali[] = round(($q->facility*$vp[0])+($q->cleanliness*$vp[1])+($q->comfort*$vp[2])+($q->service*$vp[3])+($q->price*$vp[4]),3);
            }

            $hasilkali_length = count($hasilkali);
            $sumLambdaMaks = 0;
            for($i=0;$i<$hasilkali_length;$i++) {
                $sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
            }
            $lambdaMaks = round($sumLambdaMaks/5,3);
            $ci = round((round($sumLambdaMaks/5,3)-$i)/($i-1),3);
            $cr = round($ci/1.12,3);

            if($cr <= 0.1) {
                foreach ($query as $q) {
                    $priorityVector = round((round($q->facility/$sumFacility,3)+round($q->cleanliness/$sumCleanliness,3)+round($q->comfort/$sumComfort,3)+round($q->service/$sumService,3)+round($q->price/$sumPrice,3))/5,3);

                    DB::update('UPDATE ahp_matrix
                          SET bobot =?
                          WHERE name = ?', [$priorityVector, $q->name]);
                }
            }
            //here

            //here
            $query5 = DB::table('travel__places as tp')
            ->select('tp.*',
                DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                    THEN tp.facility_criteria
                    ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                    END AS facility_attr'),
                DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                    THEN tp.cleanliness_criteria
                    ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                    END AS cleanliness_attr'),
                DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                    THEN tp.comfort_criteria
                    ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                    END AS comfort_attr'),
                DB::raw('CASE WHEN ((SELECT SUM(r.service)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id) + tp.service_criteria) IS NULL
                    THEN tp.service_criteria
                    ELSE (tp.service_criteria+((SELECT SUM(r.service)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                    END AS service_attr'),
                DB::raw('CASE WHEN ((SELECT SUM(r.price)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id) + tp.price_criteria) IS NULL
                    THEN tp.price_criteria
                    ELSE (tp.price_criteria+((SELECT SUM(r.price)
                    FROM reviews r
                    WHERE r.place_id = tp.place_id
                    GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                    END AS price_attr'))
            ->get();

            foreach($query5 as $q) {
                $itemsFacility[] = $q->facility_attr/$sumFacility*$vp[0];
                $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1];
                $itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2];
                $itemsService[] = $q->service_attr/$sumService*$vp[3];
                $itemsPrice[] = $q->price_attr/$sumPrice*$vp[4];
            }

            foreach($query5 as $q) {
                $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
                $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
                $rank = $negSolution/($negSolution+$posSolution);
                
                DB::update('UPDATE travel__places
                      SET result =?
                      WHERE place_id = ?', [$rank, $q->place_id]);
            }
            //here

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            $params = array(
                'query' => $query,
                'sumFacility' => $sumFacility,
                'sumCleanliness' => $sumCleanliness,
                'sumComfort' => $sumComfort,
                'sumService' => $sumService,
                'sumPrice' => $sumPrice,
                'notifSum' => $notifSum,
                'notif' => $notif,
            );

            return view('Backend.ahp-topsis.ahp', $params);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function rating()
    {
        if(Auth::check()) {
            $query = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN tp.facility_criteria
                            ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN tp.cleanliness_criteria
                            ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN tp.comfort_criteria
                            ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN tp.service_criteria
                            ELSE (tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN tp.price_criteria
                            ELSE (tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                            END AS price_attr'))
                    ->get();

            $sumFacility = 0;
            $sumCleanliness =0;
            $sumComfort =0;
            $sumService =0;
            $sumPrice = 0;
            foreach($query as $q) {
                $sumFacility += pow($q->facility_attr, 2);
                $sumCleanliness += pow($q->cleanliness_attr, 2);
                $sumComfort += pow($q->comfort_attr, 2);
                $sumService += pow($q->service_attr, 2);
                $sumPrice += pow($q->price_attr, 2);
            }
            $sumFacility = sqrt($sumFacility);
            $sumCleanliness = sqrt($sumCleanliness);
            $sumComfort = sqrt($sumComfort);
            $sumService = sqrt($sumService);
            $sumPrice = sqrt($sumPrice);

            DB::statement(DB::raw('SET @row = 0'));
            $query2 = Places::select(DB::raw('@row := @row + 1 as row'),'place_id','name','result','tc.category_name as category_name')
                    ->leftjoin('travel__categories as tc','tc.category_id','=','travel__places.category_id')
                    ->orderBy('result','desc')
                    ->get();

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            $params = array(
                'query' => $query,
                'query2' => $query2,
                'sumFacility' => $sumFacility,
                'sumCleanliness' => $sumCleanliness,
                'sumComfort' => $sumComfort,
                'sumService' => $sumService,
                'sumPrice' => $sumPrice,
                'notifSum' => $notifSum,
                'notif' => $notif,
            );  

            return view('Backend.rating.index', $params);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getPlacesList()
    {
        if(Auth::check()) {
            $query = Places::select(DB::raw('name,address,description,price,operational_hours,phone_number,image,video_link'))->get();

            $data['active'] = 'placesList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.place.index', $data, ['places' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getPlacesData()
    {
        $data['query'] = Places::select(DB::raw('place_id,name,address,description,price,operational_hours,phone_number,image,video_link'))->get();

        return Datatables::of($data['query'])
                ->rawColumns(['description','operational_hours','address'])
                ->make(true);
    }

    public function getCategoriesList()
    {
        if(Auth::check()) {
            $query = Categories::select(DB::raw('category_id,category_name,prefix,icon'))->get();

            $data['active'] = 'categoriesList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.categories.index', $data, ['categories' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getCategoriesData()
    {
        $data['query'] = Categories::select(DB::raw('category_id,category_name,prefix,icon'))->get();

        return Datatables::of($data['query'])
                ->make(true);
    }

    public function getTasksList() {
        if(Auth::check()) {
            $query = Tasks::select(DB::raw('id,task_name,ordering'))->get();

            $data['active'] = 'tasksList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.tasks.index', $data, ['tasks' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getTasksData()
    {
        $data['query'] = Tasks::select(DB::raw('id,task_name,ordering'))->get();

        return Datatables::of($data['query'])
                ->make(true);
    }

    public function getLocationsList()
    {
        if(Auth::check()) {
            $query = Locations::select(DB::raw('location_id,location_name,prefix'))->get();

            $data['active'] = 'locationsList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.locations.index', $data, ['locations' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getLocationsData()
    {
        $data['query'] = Locations::select(DB::raw('location_id,location_name,prefix'))->get();

        return Datatables::of($data['query'])
                ->make(true);
    }

    public function getReviewsList()
    {
        if(Auth::check()) {
            $query = DB::table('reviews as r')
                    ->select(DB::raw('r.id as id, u.name as username, tp.name as placename, r.title as title, r.body as body, r.facility as facility, r.cleanliness as cleanliness, r.comfort as comfort, r.service as service, r.price as price, r.active as active, r.created_at as created_at, r.updated_at as updated_at'))
                    ->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
                    ->leftjoin('users as u','u.id','=','r.author_id')
                    ->get();

            $data['active'] = 'reviewsList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.reviews.index', $data, ['reviews' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getReviewsData()
    {
        $data['query'] = DB::table('reviews as r')
                        ->select(DB::raw('r.id as id, u.name as username, tp.name as placename, r.title as title, r.body as body, r.facility as facility, r.cleanliness as cleanliness, r.comfort as comfort, r.service as service, r.price as price, r.active as active, r.created_at as created_at, r.updated_at as updated_at'))
                        ->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
                        ->leftjoin('users as u','u.id','=','r.author_id')
                        ->get();

        return Datatables::of($data['query'])
                ->make(true);
    }

    public function viewReviews($id){
        if(Auth::check()) {
            $query = DB::table('reviews as r')
                    ->select(DB::raw('r.id as id, u.name as username, tp.name as placename, r.title as title, r.body as body, r.facility as facility, r.cleanliness as cleanliness, r.comfort as comfort, r.service as service, r.price as price, r.active as active, r.created_at as created_at, r.updated_at as updated_at, u.avatar as avatar, u.email as email'))
                    ->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
                    ->leftjoin('users as u','u.id','=','r.author_id')
                    ->where('r.id',$id)
                    ->get();

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();

            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.reviews.view', ['reviews' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getCommentsList()
    {
        if(Auth::check()) {
            $query = DB::table('comments as c')
                    ->select(DB::raw('c.id as id, u.name as username, r.title as title, c.body as body, c.created_at as created_at'))
                    ->leftjoin('reviews as r','r.id','=','c.on_post')
                    ->leftjoin('users as u','u.id','=','c.from_user')
                    ->get();

            $data['active'] = 'reviewsList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.comments.index', $data, ['comments' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getCommentsData()
    {
        $data['query'] = DB::table('comments as c')
                        ->select(DB::raw('c.id as id, u.name as username, r.title as title, c.body as body, c.created_at as created_at'))
                        ->leftjoin('reviews as r','r.id','=','c.on_post')
                        ->leftjoin('users as u','u.id','=','c.from_user')
                        ->get();

        return Datatables::of($data['query'])
                ->make(true);
    }

    public function viewComments($id){
        if(Auth::check()) {
            $query = DB::table('comments as c')
                        ->select(DB::raw('c.id as id, u.name as username, r.title as title, c.body as body, c.created_at as created_at, tp.name as placename, u.email as email, u.avatar as avatar'))
                        ->leftjoin('reviews as r','r.id','=','c.on_post')
                        ->leftjoin('users as u','u.id','=','c.from_user')
                        ->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
                        ->where('c.id',$id)
                        ->get();

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();

            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.comments.view', ['comments' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getUsersList()
    {
        if(Auth::check()) {
            $query = Users::select('*')
                    ->where('role_id','=','2')
                    ->get();

            $data['active'] = 'usersList';

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.users.index', $data, ['users' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function getUsersData()
    {
        $data['query'] = Users::select('*')
                        ->where('role_id','=','2')
                        ->get();

        return Datatables::of($data['query'])
                ->make(true);
    }

    public function getDecisionMatriksData() {
        $data['query'] = DB::table('travel__places as tp')
                        ->select('tp.*',
                            DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                                THEN round(tp.facility_criteria,1)
                                ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS facility_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                                THEN round(tp.cleanliness_criteria,1)
                                ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS cleanliness_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                                THEN round(tp.comfort_criteria,1)
                                ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS comfort_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.service)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.service_criteria) IS NULL
                                THEN round(tp.service_criteria,1)
                                ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS service_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.price)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.price_criteria) IS NULL
                                THEN round(tp.price_criteria,1)
                                ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS price_attr'))
                        ->get();

        return Datatables::of($data['query'])
                ->make(true);
    }
}
