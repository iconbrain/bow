<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Places;
use App\Locations;
use App\Categories;
use App\Tasks;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class TaskController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function addTask(){
		if(Auth::check()) {
			$lastid = Tasks::select('id')->orderBy('id', 'desc')->first();
			$query = Tasks::all();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.tasks.add', ['tasks' => $query, 'last_id' => intval($lastid['id'])+1, 'notifSum' => $notifSum, 'notif' => $notif]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function postAddTask(Request $request){
		$validate = Validator::make($request->all(), [
	        'task_name' 		=> 'required',
			'ordering' 			=> 'required|numeric|max:9'
	    ]);

	    if ($validate->fails())
	    {
	        return redirect('/adminpanel/tasks/add')->withErrors($validate, 'addTasks')->withInput();
	    }

		$input = $request->all();

		$task = new Tasks;
		$task->id 			= $request->get('id');
		$task->task_name 	= $request->get('task_name');
		$task->ordering 	= $request->get('ordering');
    	$task->save();

    	return redirect('/adminpanel/tasks')->withMessage('Sukses menambah daftar tugas.');
	}

	public function editTask($id){
		if(Auth::check()) {
			$query = Tasks::where('id',$id)->get();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.tasks.edit', ['tasks' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function postEditTask(Request $request, $id){
		$validate = Validator::make($request->all(), [
	        'task_name' 		=> 'required',
			'ordering' 			=> 'required|numeric|max:9'
	    ]);

	    if ($validate->fails())
	    {
	        return redirect('/adminpanel/tasks/edit/' . $id)->withErrors($validate, 'editTasks')->withInput();
	    }

		$input = $request->all();

		$task = Tasks::find($id);
		$task->task_name 	= $request->get('task_name');
		$task->ordering 	= $request->get('ordering');
    	$task->save();

    	return redirect('/adminpanel/tasks')->withMessage('Sukses memperbaharui tugas.');
	}

	public function deleteTask(Request $request)
  	{
  		$id = Input::get('id');
  		$task_name = Input::get('task_name');
        $task = Tasks::where('id', '=', $id)->delete();

        return redirect('/adminpanel/tasks');
  	}
}
