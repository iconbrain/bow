<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use Storage;
use App\Places;
use App\Locations;
use App\Categories;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function editCategory($category_id){
		if(Auth::check()) {
			$query = Categories::where('category_id',$category_id)->get();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.categories.edit', ['categories' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function postEditCategory(Request $request, $category_id){
		$rules = array (
			'category_name' 	=> 'required|alpha_spaces',
	        'image'		 		=> 'mimes:jpg,jpeg,png|max:400',
		);

        $message = array(
        	'category_name.alpha_spaces'		=> 'The category name may only contain letters and spaces.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/categories/edit/' . $category_id)->withErrors($validator, 'editCategories')->withInput();
		}

		$input = $request->all();

		$category = Categories::find($category_id);
		$category_name 	= $request->get('category_name');
		$prefix 		= strtolower(str_replace(array(',', '.', '*', ' '), '-', $request->get('category_name')));
		$image = Input::file('image');
		if($image) {
			$filename  = time() . '.' . $image->getClientOriginalExtension();
	        $path = public_path('images/' . $filename);
	        Image::make($image->getRealPath())->resize(468, 249)->save($path);

			DB::update('UPDATE travel__categories
		          SET category_name =?, prefix=?, icon=?
		          WHERE category_id = ?', [$category_name, $prefix, $filename, $category_id]);
	    	\File::delete(public_path('images/' . $request->get('iconOld')));
	    } else {
	    	DB::update('UPDATE travel__categories
		          SET category_name =?, prefix=?
		          WHERE category_id = ?', [$category_name, $prefix, $category_id]);
	    }

    	return redirect('/adminpanel/categories')->withMessage('Sukses memperbaharui kategori wisata '.$request->get('category_name').'.');
	}
}
