<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Places;
use App\Locations;
use App\Categories;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class LocationController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function editLocation($location_id){
		if(Auth::check()) {
			$query = Locations::where('location_id',$location_id)->get();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.locations.edit', ['locations' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function postEditLocation(Request $request, $location_id){
		$rules = array (
			'location_name' 	=> 'required|alpha_spaces',
		);

        $message = array(
        	'location_name.alpha_spaces'		=> 'The location name may only contain letters and spaces.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/locations/edit/' . $location_id)->withErrors($validator, 'editLocations')->withInput();
		}

		$input = $request->all();

		$location = Locations::find($location_id);
		$location->location_name 	= $request->get('location_name');
		$location->prefix 			= strtolower(str_replace(array(',', '.', '*', ' '), '-', $request->get('location_name')));
    	$location->save();

    	return redirect('/adminpanel/locations')->withMessage('Sukses memperbaharui lokasi wisata '.$request->get('location_name').'.');
	}
}
