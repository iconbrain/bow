<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use View;
use App\Places;
use App\Locations;
use App\Categories;
use App\Users;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class ProfileController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function getProfile(){
		if(Auth::check()) {
			$id = Auth::user()->id;
			$query = Users::where('id',$id)->get();

			$query2 = DB::table('roles')
						->leftjoin('users', 'users.role_id', '=', 'roles.id')
						->where('users.id',$id)
						->get();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.admin.profile', ['admin' => $query, 'roles' => $query2, 'notifSum' => $notifSum, 'notif' => $notif]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function postEditProfile(Request $request){
		$id = Auth::user()->id;

		$rules = array (
			'name' 				=> 'required',
			'email'				=> 'required|email',
		);

        $message = array(
        	'email.email'		=> 'The email field must be an email format.'
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/profile')->withErrors($validator, 'editAdmin')->withInput();
		}

		$input = $request->all();

		$adminProfile = Users::find($id);
		$adminProfile->name 		= $request->get('name');
		$adminProfile->email 		= $request->get('email');
		$adminProfile->updated_at 	= Carbon::now()->addHours(7);
    	$adminProfile->save();

    	return redirect('/adminpanel/profile')->withMessage('Sukses memperbaharui akun admin.');
	}

	public function postEditPassword(Request $request){
		$id = Auth::user()->id;

		$rules = array (
			'old_password' 			=> 'required',
			'new_password' 			=> 'required|min:6',
			'new_password_confirm' 	=> 'required|min:6|same:new_password',
		);

        $message = array(
        	'new_password.min'			=> 'The password field must be at least 6 characters.',
        	'new_password_confirm.min'	=> 'The confirmation password field must be at least 6 characters.',
        	'new_password_confirm.same'	=> 'The confirmation password is not match.',
        );

		$validator = Validator::make ( Input::all (), $rules, $message);

		$old_password		= $request->get('old_password');
		$new_password		= $request->get('new_password');
		$invalidPassword 	= 0;
		if(Hash::check($old_password, Auth::user()->password)) {
			$invalidPassword = 0;
		} else {
			$invalidPassword = 1;
		}
		if($old_password == $new_password) {
			$passwordTidakBerubah = 1;
		} else {
			$passwordTidakBerubah = 0;
		}

		if($invalidPassword == 1) {
			return redirect('/adminpanel/profile')->withErrors('Old password incorrect.', 'editAdminPassword')->withInput();
		}
		else if($passwordTidakBerubah == 1) {
			return redirect('/adminpanel/profile')->withErrors('Password didn\'t change.', 'editAdminPassword')->withInput();
		}
		else if ($validator->fails()) {
			return redirect('/adminpanel/profile')->withErrors($validator, 'editAdminPassword')->withInput();
		}

		$input = $request->all();

		$adminPassword = Users::find($id);
		$adminPassword->password = bcrypt($request['new_password']);
		$adminPassword->updated_at 	= Carbon::now()->addHours(7);
    	$adminPassword->save();

    	return redirect('/adminpanel/profile')->withMessage('Sukses memperbaharui password admin.');
	}
}
