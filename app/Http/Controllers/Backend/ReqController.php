<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\RequestsFromUser;
use Validator;
use View;
use App\Places;
use App\Locations;
use App\Categories;
use App\Users;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\Datatables\Datatables;

class ReqController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function getRequestData(){
		$data['query'] = DB::table('travel__requests as tr')
		    				->select(DB::raw('*'),'tl.location_name as location_name','tc.category_name as category','u.name as username','tr.id as id','tr.name as name','tr.created_at as created_at')
		    				->leftjoin('travel__locations as tl','tl.location_id','=','tr.location_id')
		    				->leftjoin('travel__categories as tc','tc.category_id','=','tr.category_id')
		    				->leftjoin('users as u','u.id','=','tr.from_user')
		    				->get();

        return Datatables::of($data['query'])
        		->rawColumns(['description'])
                ->make(true);
	}

    public function getRequestsList()
    {
        if(Auth::check()) {
        	$query = DB::table('travel__requests as tr')
        				->select(DB::raw('*'),'tl.location_name as location_name','tc.category_name as category','u.name as username','tr.id as id','tr.name as name','tr.created_at as created_at')
        				->leftjoin('travel__locations as tl','tl.location_id','=','tr.location_id')
        				->leftjoin('travel__categories as tc','tc.category_id','=','tr.category_id')
    		    		->leftjoin('users as u','u.id','=','tr.from_user')
        				->get();

            $data['active'] = 'reqList';
            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            return view('Backend.request.index', $data, ['req' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
    }

    public function viewRequest($id){
		if(Auth::check()) {
            $query = DB::table('travel__requests as tr')
        				->select(DB::raw('*'),'tl.location_name as location_name','tc.category_name as category','u.name as username','tr.id as id','tr.name as name','tr.created_at as created_at','pr.price_range')
                        ->leftjoin('price_range as pr','pr.id','=','tr.price_code')
    					->leftjoin('travel__locations as tl','tl.location_id','=','tr.location_id')
        				->leftjoin('travel__categories as tc','tc.category_id','=','tr.category_id')
    		    		->leftjoin('users as u','u.id','=','tr.from_user')
    					->where('tr.id',$id)
    					->get();

    		$notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

    		return View::make('Backend.request.view', ['requests' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
	}

    public function deleteRequest(Request $request)
    {
        $id = Input::get('id');
        $request = DB::table('travel__requests')->where('id', '=', $id)->delete();

        return redirect('/adminpanel/request');
    }
}
