<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Places;
use App\Locations;
use App\RequestsFromUser;
use App\Categories;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class PlaceController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	/*public function addPlace(){
        if(Auth::check()) {
    		$lastid = Places::select('place_id')->orderBy('place_id', 'desc')->first();
    		$query = Locations::all();
    		$query2 = Categories::all();
            $query3 = DB::table('price_range')->get();

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

    		return View::make('Backend.place.add', ['locations' => $query, 'categories' => $query2, 'rangeHarga' => $query3, 'notifSum' => $notifSum, 'notif' => $notif]);
        } else {
            return Redirect::to('/adminpanel');
        }
	}*/

	/*public function postAddPlace(Request $request){
		$rules = array (
			'name' 				=> 'required',
			'lokasi'			=> 'required|integer',
			'kategori'			=> 'required|integer',
            'range_harga'       => 'required|integer',
			'operational_hours'	=> 'required',
			'price' 			=> 'required',
			'phone_number' 		=> 'required',
			'address' 			=> 'required',
			'facility' 			=> 'required|numeric|between:0.1,5',
			'cleanliness' 		=> 'required|numeric|between:0.1,5',
			'comfort' 			=> 'required|numeric|between:0.1,5',
			'service' 			=> 'required|numeric|between:0.1,5',
			'price_criteria'	=> 'required|numeric|between:0.1,5',
	        'image'		 		=> 'required|mimes:jpg,jpeg,png|max:800',
			'video_link' 		=> 'regex:[^https\:\/\/www\.youtube\.com\/embed\/\S*$]',
			'description'		=> 'required',
		);

        $message = array(
        	'lokasi.integer'		=> 'You haven\'t select the location.',
        	'kategori.integer'		=> 'You haven\'t select the category.',
            'range_harga.integer'   => 'You haven\'t select the price range.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/places/add')->withErrors($validator, 'addPlaceHome')->withInput();
		}

		$duplicate = Places::where('name',$request->get('name'))->first();
		if($duplicate)
		{
			return redirect()->back()->withErrors('Attraction name already exist.', 'addPlaceHome')->withInput();
		}

		$input = $request->all();

        $f = DB::table('ahp_matrix')->select('bobot')->where('id','1')->first();
        $k = DB::table('ahp_matrix')->select('bobot')->where('id','2')->first();
        $ken = DB::table('ahp_matrix')->select('bobot')->where('id','3')->first();
        $p = DB::table('ahp_matrix')->select('bobot')->where('id','4')->first();
        $h = DB::table('ahp_matrix')->select('bobot')->where('id','5')->first();
        $avgRating = ($request->get('facility') * $f->bobot) + ($request->get('cleanliness') * $k->bobot) + ($request->get('comfort') * $ken->bobot) + ($request->get('service') * $p->bobot) + ($request->get('price_criteria') * $h->bobot);

		$place = new Places;
		$place->name 					= $request->get('name');
		$place->location_id 			= $request->get('lokasi');
		$place->category_id 			= $request->get('kategori');
        $place->price_code              = $request->get('range_harga');
		$place->place_prefix 		    = strtolower(str_replace(array(',', '.', '*', ' '), '-', $request->get('name')));
		$place->operational_hours 		= nl2br($request->get('operational_hours'));
		$place->price 					= nl2br($request->get('price'));
		$place->phone_number 			= $request->get('phone_number');
		$place->address 				= nl2br($request->get('address'));
		$place->facility_criteria 		= $request->get('facility');
		$place->cleanliness_criteria 	= $request->get('cleanliness');
		$place->comfort_criteria 		= $request->get('comfort');
		$place->service_criteria 		= $request->get('service');
		$place->price_criteria 			= $request->get('price_criteria');
        $place->avgRating               = round($avgRating,1);
		$place->video_link 				= $request->get('video_link');
		$image = Input::file('image');
		if($image) {
			$filename  = time() . '.' . $image->getClientOriginalExtension();
	        $path = public_path('images/' . $filename);
	        Image::make($image->getRealPath())->resize(468, 249)->save($path);
	        $place->image = $filename;
	    }
		$place->description 			= $request->get('description');
		$place->datetime 				= Carbon::now()->addHours(7);
    	$place->save();

    	// update result-nya
        $query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

        $qAHP = DB::table('ahp_matrix as am')
                    ->select('*')
                    ->get();
        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
        $sService = DB::table('ahp_matrix as am')->sum('service');
        $sPrice = DB::table('ahp_matrix as am')->sum('price');

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();
        
        $vp = array();
        foreach($qAHP as $qhp) {
            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
        }

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility*$vp[0];
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1];
            $itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2];
            $itemsService[] = $q->service_attr/$sumService*$vp[3];
            $itemsPrice[] = $q->price_attr/$sumPrice*$vp[4];
        }
        
        foreach($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }

    	return redirect('/adminpanel/places')->withMessage('Sukses menambah objek wisata '.$request->get('name').'.');
	}*/

    public function addPlace(){
        if(Auth::check()) {
            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            $params = array(
                'notifSum'  => $notifSum,
                'notif'     => $notif,
            );

            return View::make('Backend.api.request-api', $params);
        }
        else {
            return Redirect::to('adminpanel/login');
        }
    }

    public function postAddPlace(Request $req){
        // https://maps.googleapis.com/maps/api/place/textsearch/json?query=tangkuban&location=42.3675294,-71.186966&radius=10000&key=AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4

        // https://maps.googleapis.com/maps/api/place/details/json?reference=CmRRAAAAa6_dqk40mgWvficK9I2VVdpGoxrjUZD84Vqwa640eTm8kroRS6HZpvY6Zy4pA568as_Vn6WZu7J0p2sDNQqrHu0MZ-Mo-ALsHEafQd0RZuRfD0gkPVGL1a8Ptp7CJyxKEhBB3sx2t-jq8tn6YKgm703oGhRlDaKj7DgIGEEC79I4NhsFX2JngA&key=AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4

        $api = Input::get('api');

        $ch  = curl_init();

        curl_setopt($ch, CURLOPT_USERAGENT, 'Tes google api');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json', 'user_key:  AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4']);
        curl_setopt($ch, CURLOPT_URL, $api);

        $result = curl_exec($ch);

        $a = json_decode($result, true);
        $result = $a['result'];
        if($result == NULL) {
            return redirect('/adminpanel/request-api')->withErrors('Gagal memperoleh data. Pastikan Anda memasukkan format API yang valid.', 'addAPI')->withInput();
        } else {
            $places = new Places;
            $places->category_id            = 1;
            $places->location_id            = 1;
            $places->price_code             = 1;
            $places->price                  = '-';
            if(isset($result['rating'])) {
                $places->facility_criteria      = $result['rating'];
                $places->cleanliness_criteria   = $result['rating'];
                $places->comfort_criteria       = $result['rating'];
                $places->service_criteria       = $result['rating'];
                $places->price_criteria         = $result['rating'];
                $places->avgRating              = $result['rating'];
            } else {
                $places->facility_criteria      = '0.1';
                $places->cleanliness_criteria   = '0.1';
                $places->comfort_criteria       = '0.1';
                $places->service_criteria       = '0.1';
                $places->price_criteria         = '0.1';
                $places->avgRating              = '0.1';
            }
            $places->place_prefix           = strtolower(str_replace(array(',', '.', '*', ' '), '-', $result['name']));
            $places->description            = '-';
            if(isset($result['opening_hours']['weekday_text'])) {
                $concatenation = '';
                $res_op_hours = $result['opening_hours']['weekday_text'];
                foreach ($res_op_hours as $key3 => $val3) {
                    $concatenation .= $val3 . "<br>";
                }
                $places->operational_hours      = $concatenation;
            } else {
                $places->operational_hours      = '-';
            }
            if(isset($result['formatted_phone_number'])) {
                $places->phone_number           = $result['formatted_phone_number'];
            } else {
                $places->phone_number           = '';
            }
            $places->datetime               = Carbon::now()->addHours(7);
            $places->name                   = $result['name'];
            $places->address                = $result['formatted_address'];
            $places->image                  = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&photoreference=".$result['photos'][0]['photo_reference']."&key=AIzaSyAOzckpjtN_HPedc3MHhi1qZ_qNekcPvHo";
            $places->save();
            curl_close($ch);

            // update result-nya
            $query5 = DB::table('travel__places as tp')
                        ->select('tp.*',
                            DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                                THEN round(tp.facility_criteria,1)
                                ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS facility_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                                THEN round(tp.cleanliness_criteria,1)
                                ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS cleanliness_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                                THEN round(tp.comfort_criteria,1)
                                ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS comfort_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.service)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.service_criteria) IS NULL
                                THEN round(tp.service_criteria,1)
                                ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS service_attr'),
                            DB::raw('CASE WHEN ((SELECT SUM(r.price)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id) + tp.price_criteria) IS NULL
                                THEN round(tp.price_criteria,1)
                                ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                                FROM reviews r
                                WHERE r.place_id = tp.place_id
                                GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                                END AS price_attr'))
                        ->get();

            $sumFacility = 0;
            $sumCleanliness =0;
            $sumComfort =0;
            $sumService =0;
            $sumPrice = 0;
            foreach($query5 as $q) {
                $sumFacility += pow($q->facility_attr, 2);
                $sumCleanliness += pow($q->cleanliness_attr, 2);
                $sumComfort += pow($q->comfort_attr, 2);
                $sumService += pow($q->service_attr, 2);
                $sumPrice += pow($q->price_attr, 2);
            }
            $sumFacility = sqrt($sumFacility);
            $sumCleanliness = sqrt($sumCleanliness);
            $sumComfort = sqrt($sumComfort);
            $sumService = sqrt($sumService);
            $sumPrice = sqrt($sumPrice);

            $qAHP = DB::table('ahp_matrix as am')
                        ->select('*')
                        ->get();
            $sFacility = DB::table('ahp_matrix as am')->sum('facility');
            $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
            $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
            $sService = DB::table('ahp_matrix as am')->sum('service');
            $sPrice = DB::table('ahp_matrix as am')->sum('price');

            $itemsFacility = array();
            $itemsCleanliness = array();
            $itemsComfort = array();
            $itemsService = array();
            $itemsPrice = array();
            
            $vp = array();
            foreach($qAHP as $qhp) {
                $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
            }

            foreach($query5 as $q) {
                $itemsFacility[] = $q->facility_attr/$sumFacility*$vp[0];
                $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1];
                $itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2];
                $itemsService[] = $q->service_attr/$sumService*$vp[3];
                $itemsPrice[] = $q->price_attr/$sumPrice*$vp[4];
            }
            
            foreach($query5 as $q) {
                $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
                $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
                $rank = $negSolution/($negSolution+$posSolution);

                DB::update('UPDATE travel__places
                      SET result =?
                      WHERE place_id = ?', [$rank, $q->place_id]);
            }
            
            return redirect('/adminpanel/places')->withMessage('Sukses mendapatkan data dari API.');
        }
    }

    public function postAddPlaceFromUser(Request $request){
        $validate = Validator::make($request->all(), [
            'name'              => 'required',
            'phone_number'      => 'required',
            'address'           => 'required',
            'operational_hours' => 'required',
            'price'             => 'required',
            'price_range'       => 'required',
            'location'          => 'required',
            'category'          => 'required',
            'description'       => 'required',
        ]);

        if ($validate->fails())
        {
            return redirect()->back()->withErrors($validate, 'placeByUserRequest')->withInput();
        }

        $duplicate = Places::where('name',$request->get('name'))->first();
        if($duplicate)
        {
            return redirect()->back()->withErrors('Attraction name already exist.', 'placeByUserRequest')->withInput();
        }

        $input = $request->all();
        $loc_id = DB::table('travel__locations as tl')
                    ->select(DB::raw('tl.location_id as location_id'))
                    ->where('tl.location_name','=',$request->get('location'))
                    ->first();
        $cat_id = DB::table('travel__categories as tc')
                    ->select(DB::raw('tc.category_id as category_id'))
                    ->where('tc.category_name','=',$request->get('category'))
                    ->first();
        $price_code = DB::table('price_range as pr')
                    ->select(DB::raw('pr.id as price_id'))
                    ->where('pr.price_range','=',$request->get('price_range'))
                    ->first();

        $place = new Places;
        $place->name                    = $request->get('name');
        $place->location_id             = $loc_id->location_id;
        $place->category_id             = $cat_id->category_id;
        $place->price_code              = $price_code->price_id;
        $place->place_prefix            = strtolower(str_replace(array(',', '.', '*', ' '), '-', $request->get('name')));
        $place->operational_hours       = nl2br($request->get('operational_hours'));
        $place->price                   = nl2br($request->get('price'));
        $place->phone_number            = $request->get('phone_number');
        $place->address                 = nl2br($request->get('address'));
        $place->description             = nl2br($request->get('description'));
        $place->facility_criteria       = '0.1';
        $place->cleanliness_criteria    = '0.1';
        $place->comfort_criteria        = '0.1';
        $place->service_criteria        = '0.1';
        $place->price_criteria          = '0.1';
        $place->avgRating               = '0.1';
        $place->image                   = 'noimage.png';
        $place->datetime                = Carbon::now()->addHours(7);
        $place->save();

        DB::table('travel__requests')->where('id', '=', $request->get('id'))->delete();

        // update result nya
        $query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();

        $bobotFasilitas = DB::table('ahp_matrix')->select('bobot')->where('name','=','Fasilitas')->first();
        $bobotKebersihan = DB::table('ahp_matrix')->select('bobot')->where('name','=','Kebersihan')->first();
        $bobotKenyamanan = DB::table('ahp_matrix')->select('bobot')->where('name','=','Kenyamanan')->first();
        $bobotPelayanan = DB::table('ahp_matrix')->select('bobot')->where('name','=','Pelayanan')->first();
        $bobotHarga = DB::table('ahp_matrix')->select('bobot')->where('name','=','Rp (Harga)')->first();

        foreach ($bobotFasilitas as $bFasilitas) {
        }
        foreach ($bobotKebersihan as $bKebersihan) {
        }
        foreach ($bobotKenyamanan as $bKenyamanan) {
        }
        foreach ($bobotPelayanan as $bPelayanan) {
        }
        foreach ($bobotHarga as $bHarga) {
        }

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility*$bFasilitas;
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$bKebersihan;
            $itemsComfort[] = $q->comfort_attr/$sumComfort*$bKenyamanan;
            $itemsService[] = $q->service_attr/$sumService*$bPelayanan;
            $itemsPrice[] = $q->price_attr/$sumPrice*$bHarga;
        }

        foreach($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$bFasilitas) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$bKebersihan) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$bKenyamanan) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$bPelayanan) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$bHarga) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$bFasilitas) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$bKebersihan) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$bKenyamanan) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$bPelayanan) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$bHarga) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }

        return redirect('/adminpanel/places')->withMessage('Sukses menambah objek wisata '.$request->get('name').'. Rating default untuk masing-masing kriteria adalah 0.1, serta objek wisata tidak memiliki gambar dan video. Silahkan update objek wisata tersebut.');
    }

	public function editPlace($place_id){
        if(Auth::check()) {
    		$query = Places::where('place_id',$place_id)->get();
    		$query2 = Locations::all();
    		$query3 = Categories::all();

    		$data = array(
                'category'	=> Places::select(DB::raw('category_name'))
                				->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
                				->where('travel__places.place_id', $place_id)
                				->first(),
                'location'	=> Places::select(DB::raw('location_name'))
                				->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
                				->where('travel__places.place_id', $place_id)
                				->first(),
            );

            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

    		return View::make('Backend.place.edit', ['places' => $query, 'locations' => $query2, 'categories' => $query3, 'notifSum' => $notifSum, 'notif' => $notif])->with($data);
        } else {
            return Redirect::to('/adminpanel');
        }
	}

	public function postEditPlaceHome(Request $request, $place_id){
		$rules = array (
			'name' 				=> 'required',
			'lokasi'			=> 'required|integer',
			'kategori'			=> 'required|integer',
			'operational_hours'	=> 'required',
			'price' 			=> 'required',
			'phone_number' 		=> 'required',
			'address' 			=> 'required',
			'description' 		=> 'required',
		);

        $message = array(
        	'lokasi.integer'		=> 'You haven\'t select the location.',
        	'kategori.integer'		=> 'You haven\'t select the category.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/places/edit/' . $place_id)->withErrors($validator, 'editPlaceHome')->withInput();
		}

		$input = $request->all();

		$placeHome = Places::find($place_id);
		$placeHome->name 					= $request->get('name');
		$placeHome->location_id 			= $request->get('lokasi');
		$placeHome->category_id 			= $request->get('kategori');
		$placeHome->place_prefix			= strtolower(str_replace(array(',', '.', '*', ' '), '-', $request->get('name')));
		$placeHome->operational_hours 		= nl2br($request->get('operational_hours'));
		$placeHome->price 					= nl2br($request->get('price'));
		$placeHome->phone_number 			= $request->get('phone_number');
		$placeHome->address 				= nl2br($request->get('address'));
		$placeHome->description 			= $request->get('description');
    	$placeHome->save();

    	return redirect('/adminpanel/places')->withMessage('Sukses memperbaharui objek wisata '.$request->get('name').'.');
	}

	public function postEditPlaceRating(Request $request, $place_id){
		$validate = Validator::make($request->all(), [
	        'facility' 		=> 'required|numeric|between:0.1,5',
			'cleanliness' 	=> 'required|numeric|between:0.1,5',
			'comfort' 		=> 'required|numeric|between:0.1,5',
			'service' 		=> 'required|numeric|between:0.1,5',
			'price' 		=> 'required|numeric|between:0.1,5'
	    ]);

	    if ($validate->fails())
	    {
	        return redirect('/adminpanel/places/edit/' . $place_id)->withErrors($validate, 'editPlaceRating')->withInput();
	    }

		$input = $request->all();

		$rating = Places::find($place_id);
		$rating->facility_criteria 		= $request->get('facility');
		$rating->cleanliness_criteria 	= $request->get('cleanliness');
		$rating->comfort_criteria 		= $request->get('comfort');
		$rating->service_criteria 		= $request->get('service');
		$rating->price_criteria 		= $request->get('price');
    	$rating->save();

        $query3 = DB::table('ahp_matrix as am')
                    ->select('*')
                    ->get();
        $sumFacilityAHP = DB::table('ahp_matrix as am')->sum('facility');
        $sumCleanlinessAHP = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sumComfortAHP = DB::table('ahp_matrix as am')->sum('comfort');
        $sumServiceAHP = DB::table('ahp_matrix as am')->sum('service');
        $sumPriceAHP = DB::table('ahp_matrix as am')->sum('price');

        $vp = array();
        foreach($query3 as $q) {
            $vp[] = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);
        }

        $hasilkali = array();
        foreach($query3 as $q) {
            $hasilkali[] = round(($q->facility*$vp[0])+($q->cleanliness*$vp[1])+($q->comfort*$vp[2])+($q->service*$vp[3])+($q->price*$vp[4]),3);
        }

        $hasilkali_length = count($hasilkali);
        $sumLambdaMaks = 0;
        for($i=0;$i<$hasilkali_length;$i++) {
            $sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
        }
        $lambdaMaks = round($sumLambdaMaks/5,3);
        $ci = round((round($sumLambdaMaks/5,3)-$i)/($i-1),3);
        $cr = round($ci/1.12,3);

        if($cr <= 0.1) {
            foreach ($query3 as $q) {
                $priorityVector = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);

                DB::update('UPDATE ahp_matrix
                      SET bobot =?
                      WHERE name = ?', [$priorityVector, $q->name]);
            }
        }

        $f = $request->get('facility');
        $k = $request->get('cleanliness');
        $ken = $request->get('comfort');
        $p = $request->get('service');
        $h = $request->get('price');
        $query6 = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN '.$f.'
                        ELSE ('.$f.'+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN '.$k.'
                        ELSE ('.$k.'+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN '.$ken.'
                        ELSE ('.$ken.'+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN '.$p.'
                        ELSE ('.$p.'+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN '.$h.'
                        ELSE ('.$h.'+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                ->where('place_id',$place_id)
                ->get();

        foreach ($query6 as $q6) {
            $avgRate = round((($q6->facility_attr * $vp[0]) + ($q6->cleanliness_attr * $vp[1]) + ($q6->comfort_attr * $vp[2]) + ($q6->service_attr * $vp[3]) + ($q6->price_attr * $vp[4])),1);
        }

        DB::update('UPDATE travel__places
              SET avgRating =?
              WHERE place_id = ?', [$avgRate, $place_id]);

        // update ahp-topsis result-nya
        $query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

        $fasilitas = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','1')
                    ->first();
        $kebersihan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','2')
                    ->first();
        $kenyamanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','3')
                    ->first();
        $pelayanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','4')
                    ->first();
        $harga = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','5')
                    ->first();

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas->bobot;
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan->bobot;
            $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan->bobot;
            $itemsService[] = $q->service_attr/$sumService * $pelayanan->bobot;
            $itemsPrice[] = $q->price_attr/$sumPrice * $harga->bobot;
        }

        foreach ($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }        

    	return redirect('/adminpanel/places')->withMessage('Sukses memperbaharui objek wisata '.$request->get('name').'.');
	}

	public function postEditPlaceAssets(Request $request, $place_id){
		$validate = Validator::make($request->all(), [
			// 'video_link' 	=> 'required|regex:[^https\:\/\/www\.youtube\.com\/embed\/\S*$]',
            'video_link'    => 'sometimes',
	        'image'		 	=> 'mimes:jpg,jpeg,png|max:800'
	    ]);

	    if ($validate->fails())
	    {
	        return redirect('/adminpanel/places/edit/' . $place_id)->withErrors($validate, 'editPlaceAssets')->withInput();
	    }

		$input = $request->all();

		$assets = Places::find($place_id);
        $video_link = $request->get('video_link');
		$image = Input::file('image');
		if($image) {
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('images/' . $filename);
            Image::make($image->getRealPath())->resize(468, 249)->save($path);

            DB::update('UPDATE travel__places
                  SET video_link =?, image=?
                  WHERE place_id = ?', [$video_link, $filename, $place_id]);
            \File::delete(public_path('images/' . $request->get('iconOld')));
	    } else {
            DB::update('UPDATE travel__places
                  SET video_link =?
                  WHERE place_id = ?', [$video_link, $place_id]);
        }

    	return redirect('/adminpanel/places')->withMessage('Sukses memperbaharui objek wisata '.$request->get('name').'.');
	}

	public function deletePlace(Request $request)
  	{
  		$place_id = Input::get('place_id');
  		$name = Input::get('name');

        $imgToDel = Places::where('place_id', '=', $place_id)->first();
        \File::delete(public_path('images/' . $imgToDel->image));

        $comments = DB::table('comments')
                    ->leftjoin('reviews','reviews.id','=','comments.on_post')
                    ->where('reviews.place_id', '=', $place_id)->delete();

        $reviews = DB::table('reviews')
                    ->where('place_id', '=', $place_id)->delete();

        $bookmark = DB::table('travel__bookmark')
                    ->where('place_id', '=', $place_id)->delete();

        $place = Places::where('place_id', '=', $place_id)->delete();

        // update result-nya
        $qAHP = DB::table('ahp_matrix as am')
                ->select('*')
                ->get();
        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
        $sService = DB::table('ahp_matrix as am')->sum('service');
        $sPrice = DB::table('ahp_matrix as am')->sum('price');
        $vp = array();
        foreach($qAHP as $qhp) {
            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
        }

        $query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

        $fasilitas = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','1')
                    ->first();
        $kebersihan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','2')
                    ->first();
        $kenyamanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','3')
                    ->first();
        $pelayanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','4')
                    ->first();
        $harga = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','5')
                    ->first();

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas->bobot;
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan->bobot;
            $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan->bobot;
            $itemsService[] = $q->service_attr/$sumService * $pelayanan->bobot;
            $itemsPrice[] = $q->price_attr/$sumPrice * $harga->bobot;
        }

        foreach ($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }

        return redirect('/adminpanel/places');
  	}
}
