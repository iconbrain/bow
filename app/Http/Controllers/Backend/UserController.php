<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Testing;
use App\Places;
use App\Locations;
use App\Categories;
use App\Users;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class UserController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function viewUser($id){
		if(Auth::check()) {
			$query = Users::where('id',$id)->get();

			$query2 = DB::table('roles')
						->leftjoin('users', 'users.role_id', '=', 'roles.id')
						->where('users.id',$id)
						->get();

			$query3 = DB::table('reviews')
						->select(DB::raw('count(*) as total_review'))
						->leftjoin('users', 'users.id', '=', 'reviews.author_id')
						->where('users.id',$id)
						->get();

			$query4 = DB::table('comments')
						->select(DB::raw('count(*) as total_comment'))
						->leftjoin('users', 'users.id', '=', 'comments.from_user')
						->where('users.id',$id)
						->get();

			$query5 = DB::table('reviews')
						->select('*','reviews.created_at as rca','tp.name as place_name')
						->leftjoin('users', 'users.id', '=', 'reviews.author_id')
						->leftjoin('travel__places as tp', 'tp.place_id', '=', 'reviews.place_id')
						->where('users.id',$id)
						->orderBy('reviews.created_at','desc')
						->limit(4)
						->get();

			$query6 = DB::table('comments')
						->select('*','comments.created_at as cca','r.title as title','comments.body as body')
						->leftjoin('users', 'users.id', '=', 'comments.from_user')
						->leftjoin('reviews as r', 'r.id', '=', 'comments.on_post')
						->where('users.id',$id)
						->orderBy('comments.created_at','desc')
						->limit(4)
						->get();

			$query7 = DB::table('travel__bookmark')
						->select(DB::raw('count(*) as total_bookmark'))
						->leftjoin('users', 'users.id', '=', 'travel__bookmark.user_id')
						->where('users.id',$id)
						->where('travel__bookmark.status','1')
						->get();

			$query8 = DB::table('travel__bookmark as tb')
						->leftjoin('users as u', 'u.id', '=', 'tb.user_id')
						->leftjoin('travel__places as tp', 'tb.place_id', '=', 'tp.place_id')
						->where('u.id',$id)
						->where('tb.status','1')
						->orderBy('tb.datetime','desc')
						->get();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.users.view', ['users' => $query, 'roles' => $query2, 'reviews' => $query3, 'comments' => $query4, 'latest_review' =>$query5, 'latest_comment' => $query6, 'notifSum' => $notifSum, 'notif' => $notif, 'bookmark' => $query7, 'bookmarkPlace' => $query8]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function editUser($id){
		if(Auth::check()) {
			$query = Users::where('id',$id)->get();

			$notifSum = DB::table('travel__requests')
	                        ->select(DB::raw('count(*) as totalNotif'))
	                        ->first();
	        $notif = DB::table('travel__requests as tr')
	                    ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
	                    ->leftjoin('users as u','u.id','=','tr.from_user')
	                    ->orderBy('created_at','desc')
	                    ->limit(3)
	                    ->get();

			return View::make('Backend.users.edit', ['users' => $query, 'notifSum' => $notifSum, 'notif' => $notif]);
		} else {
			return Redirect::to('/adminpanel');
		}
	}

	public function postEditUser(Request $request,$id){
		$rules = array (
			'name' 				=> 'required',
			'email'				=> 'required|email',
		);

        $message = array(
        	'email.email'		=> 'The email field must be an email format.'
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/users/edit/' . $id)->withErrors($validator, 'editUser')->withInput();
		}

		$input = $request->all();

		$userHome = Users::find($id);
		$userHome->name 		= $request->get('name');
		$userHome->email 		= $request->get('email');
		// $userHome->role 		= $request->get('role');
		$userHome->updated_at 	= Carbon::now()->addHours(7);
    	$userHome->save();

    	return redirect('/adminpanel/users')->withMessage('Sukses memperbaharui user '.$request->get('name').'.');
	}

	public function postEditUserPassword(Request $request, $id){
		$rules = array (
			'new_password' 			=> 'required|min:6',
			'new_password_confirm' 	=> 'required|min:6|same:new_password',
		);

        $message = array(
        	'new_password.min'			=> 'The password field must be at least 6 characters.',
        	'new_password_confirm.min'	=> 'The confirmation password field must be at least 6 characters.',
        	'new_password_confirm.same'	=> 'The confirmation password is not match.',
        );

		$validator = Validator::make ( Input::all (), $rules, $message);

		if ($validator->fails()) {
			return redirect('/adminpanel/users/edit/' . $id)->withErrors($validator, 'editUserPassword')->withInput();
		}

		$input = $request->all();

		$userPassword = Users::find($id);
		$userPassword->password = bcrypt($request['new_password']);
		$userPassword->updated_at 	= Carbon::now()->addHours(7);
    	$userPassword->save();

    	return redirect('/adminpanel/users')->withMessage('Sukses memperbaharui password user '.$request->get('name').'.');
	}

	public function requestAPI() {
		if(Auth::check()) {
            $notifSum = DB::table('travel__requests')
                            ->select(DB::raw('count(*) as totalNotif'))
                            ->first();
            $notif = DB::table('travel__requests as tr')
                        ->select('u.name as username','tr.description as description','tr.created_at as created_at','u.avatar as avatar','tr.id as id')
                        ->leftjoin('users as u','u.id','=','tr.from_user')
                        ->orderBy('created_at','desc')
                        ->limit(3)
                        ->get();

            $params = array(
                'notifSum' 	=> $notifSum,
                'notif' 	=> $notif,
            );

			return View::make('Backend.api.request-api', $params);
		}
		else {
            return Redirect::to('adminpanel/login');
        }
	}

	public function requestAPIPost(Request $req) {
		// https://maps.googleapis.com/maps/api/place/textsearch/json?query=tangkuban&location=42.3675294,-71.186966&radius=10000&key=AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4

		// https://maps.googleapis.com/maps/api/place/textsearch/json?query=abc&key=AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4

		// https://maps.googleapis.com/maps/api/place/details/json?reference=CmRRAAAAa6_dqk40mgWvficK9I2VVdpGoxrjUZD84Vqwa640eTm8kroRS6HZpvY6Zy4pA568as_Vn6WZu7J0p2sDNQqrHu0MZ-Mo-ALsHEafQd0RZuRfD0gkPVGL1a8Ptp7CJyxKEhBB3sx2t-jq8tn6YKgm703oGhRlDaKj7DgIGEEC79I4NhsFX2JngA&key=AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4

	    $api = Input::get('api');

	    $ch  = curl_init();

	    curl_setopt($ch, CURLOPT_USERAGENT, 'Tes google api');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json', 'user_key:  AIzaSyCIawlNw19OnaPDVMsOPfvXTV53vQci1-4']);
	    curl_setopt($ch, CURLOPT_URL, $api);

	    $result = curl_exec($ch);

	    $a = json_decode($result, true);
	    $result = $a['result'];
	    if($result == NULL) {
	    	return redirect('/adminpanel/request-api')->withErrors('Gagal memperoleh data. Pastikan Anda memasukkan format API yang valid.', 'addAPI')->withInput();
	    } else {
	    	$places = new Places;
	    	$places->category_id 			= 1;
	    	$places->location_id 			= 1;
	    	$places->price_code 			= 1;
	    	$places->price 					= '-';
	    	if(isset($result['rating'])) {
				$places->facility_criteria 		= $result['rating'];
		    	$places->cleanliness_criteria 	= $result['rating'];
		    	$places->comfort_criteria 		= $result['rating'];
		    	$places->service_criteria 		= $result['rating'];
		    	$places->price_criteria 		= $result['rating'];
		    	$places->avgRating 				= $result['rating'];
	    	} else {
				$places->facility_criteria 		= '0.1';
		    	$places->cleanliness_criteria 	= '0.1';
		    	$places->comfort_criteria 		= '0.1';
		    	$places->service_criteria 		= '0.1';
		    	$places->price_criteria 		= '0.1';
		    	$places->avgRating 				= '0.1';
	    	}
			$places->place_prefix 		    = strtolower(str_replace(array(',', '.', '*', ' '), '-', $result['name']));
	    	$places->description 			= '-';
	    	if(isset($result['opening_hours']['weekday_text'])) {
	    		$concatenation = '';
	    		$res_op_hours = $result['opening_hours']['weekday_text'];
			    foreach ($res_op_hours as $key3 => $val3) {
			    	$concatenation .= $val3 . "<br>";
			    }
	    		$places->operational_hours 		= $concatenation;
	    	} else {
	    		$places->operational_hours 		= '-';
	    	}
	    	if(isset($result['formatted_phone_number'])) {
	    		$places->phone_number 			= $result['formatted_phone_number'];
	    	} else {
	    		$places->phone_number 			= '';
	    	}
	    	$places->datetime 				= Carbon::now()->addHours(7);
		    $places->name 					= $result['name'];
		    $places->address 				= $result['formatted_address'];
		    $places->image 					= "https://maps.googleapis.com/maps/api/place/photo?maxwidth=600&photoreference=".$result['photos'][0]['photo_reference']."&key=AIzaSyAOzckpjtN_HPedc3MHhi1qZ_qNekcPvHo";
		    $places->save();
		    curl_close($ch);

		    // update result-nya
	        $query5 = DB::table('travel__places as tp')
	                    ->select('tp.*',
	                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
	                            THEN round(tp.facility_criteria,1)
	                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
	                            END AS facility_attr'),
	                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
	                            THEN round(tp.cleanliness_criteria,1)
	                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
	                            END AS cleanliness_attr'),
	                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
	                            THEN round(tp.comfort_criteria,1)
	                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
	                            END AS comfort_attr'),
	                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
	                            THEN round(tp.service_criteria,1)
	                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
	                            END AS service_attr'),
	                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
	                            THEN round(tp.price_criteria,1)
	                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
	                            FROM reviews r
	                            WHERE r.place_id = tp.place_id
	                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
	                            END AS price_attr'))
	                    ->get();

	        $sumFacility = 0;
	        $sumCleanliness =0;
	        $sumComfort =0;
	        $sumService =0;
	        $sumPrice = 0;
	        foreach($query5 as $q) {
	            $sumFacility += pow($q->facility_attr, 2);
	            $sumCleanliness += pow($q->cleanliness_attr, 2);
	            $sumComfort += pow($q->comfort_attr, 2);
	            $sumService += pow($q->service_attr, 2);
	            $sumPrice += pow($q->price_attr, 2);
	        }
	        $sumFacility = sqrt($sumFacility);
	        $sumCleanliness = sqrt($sumCleanliness);
	        $sumComfort = sqrt($sumComfort);
	        $sumService = sqrt($sumService);
	        $sumPrice = sqrt($sumPrice);

	        $qAHP = DB::table('ahp_matrix as am')
	                    ->select('*')
	                    ->get();
	        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
	        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
	        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
	        $sService = DB::table('ahp_matrix as am')->sum('service');
	        $sPrice = DB::table('ahp_matrix as am')->sum('price');

	        $itemsFacility = array();
	        $itemsCleanliness = array();
	        $itemsComfort = array();
	        $itemsService = array();
	        $itemsPrice = array();
	        
	        $vp = array();
	        foreach($qAHP as $qhp) {
	            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
	        }

	        foreach($query5 as $q) {
	            $itemsFacility[] = $q->facility_attr/$sumFacility*$vp[0];
	            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1];
	            $itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2];
	            $itemsService[] = $q->service_attr/$sumService*$vp[3];
	            $itemsPrice[] = $q->price_attr/$sumPrice*$vp[4];
	        }
	        
	        foreach($query5 as $q) {
	            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
	            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
	            $rank = $negSolution/($negSolution+$posSolution);

	            DB::update('UPDATE travel__places
	                  SET result =?
	                  WHERE place_id = ?', [$rank, $q->place_id]);
	        }
	    	
	    	return redirect('/adminpanel/places/add')->withMessage('Sukses mendapatkan data dari API.');
		}
	}
}
