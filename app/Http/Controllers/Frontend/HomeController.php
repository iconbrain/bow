<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use View;
use App\Locations;
use App\Categories;
use App\Places;
use App\Reviews;
use Carbon\Carbon;
use DB;
use Auth;

class HomeController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function index(){
		$query = Locations::all();
		$query2 = Categories::all();

		$myTime = Carbon::now();
		$myTime = $myTime->addHours(7);
		$myTimeBeforeAWeek = $myTime->subDays(7);
		
		$query3 = DB::table('travel__places')
                ->where('datetime','>',$myTimeBeforeAWeek)
				->where('datetime','<>',NULL)
                ->limit(3)
                ->get();

		$recommended = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();
		$wrr = Places::select(DB::raw('reviews.place_id'))
				->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->whereIn('travel__places.place_id',$reviewsPlaceId)
				->where('reviews.created_at','>',$myTimeBeforeAWeek)
				->where('reviews.created_at','<>',NULL)
				->get();
		$wkrv = array();
		foreach ($wrr as $wr) {
			$wkrv[] = $wr->place_id;
		}

		$weeklyReview = Places::whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->orderBy('name','asc')
						->limit(3)
						->get();

		$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->where('datetime','>',$myTimeBeforeAWeek)
						->where('datetime','<>',NULL)
						->orderBy('datetime','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();

		$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->limit(3)
						->get();

		$query6 = Places::select(DB::raw('name'))->get();

		if(Auth::check()) {
	        $bmk = DB::table('travel__bookmark as tb')
	        		->select('tb.place_id as place_id')
	        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
	        		->where('tb.user_id',Auth::user()->id)
	        		->where('tb.status','1')
	        		->get();
	    } else {
	    	$bmk = "";
	    }

	    // update ahp-topsis result everytime home page loaded
	    $query3 = DB::table('ahp_matrix as am')
                        ->select('*')
                        ->get();
        $sumFacilityAHP = DB::table('ahp_matrix as am')->sum('facility');
        $sumCleanlinessAHP = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sumComfortAHP = DB::table('ahp_matrix as am')->sum('comfort');
        $sumServiceAHP = DB::table('ahp_matrix as am')->sum('service');
        $sumPriceAHP = DB::table('ahp_matrix as am')->sum('price');

        $vp = array();
        foreach($query3 as $q) {
            $vp[] = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);
        }

        $hasilkali = array();
        foreach($query3 as $q) {
            $hasilkali[] = round(($q->facility*$vp[0])+($q->cleanliness*$vp[1])+($q->comfort*$vp[2])+($q->service*$vp[3])+($q->price*$vp[4]),3);
        }

        $hasilkali_length = count($hasilkali);
        $sumLambdaMaks = 0;
        for($i=0;$i<$hasilkali_length;$i++) {
            $sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
        }
        $lambdaMaks = round($sumLambdaMaks/5,3);
        $ci = round((round($sumLambdaMaks/5,3)-$i)/($i-1),3);
        $cr = round($ci/1.12,3);

        if($cr <= 0.1) {
            foreach ($query3 as $q) {
                $priorityVector = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);

                DB::update('UPDATE ahp_matrix
                      SET bobot =?
                      WHERE name = ?', [$priorityVector, $q->name]);
            }
        }
        
	    $query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

       	$fasilitas = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','1')
                    ->first();
        $kebersihan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','2')
                    ->first();
        $kenyamanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','3')
                    ->first();
        $pelayanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','4')
                    ->first();
        $harga = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','5')
                    ->first();

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas->bobot;
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan->bobot;
            $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan->bobot;
            $itemsService[] = $q->service_attr/$sumService * $pelayanan->bobot;
            $itemsPrice[] = $q->price_attr/$sumPrice * $harga->bobot;
        }

        foreach ($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }

		$params = array(
			'recommended'		=> $recommended,
			'weeklyReview'		=> $weeklyReview,
			'weeklyAdded'		=> $weeklyAdded,
			'notReviewedYet'	=> $notReviewedYet,
			'locations'			=> $query,
			'categories'		=> $query2,
			'places'			=> $query3,
			'my'				=> $myTime,
			'search' 			=> $query6,
			'reviewsPlaceId'	=> $reviewsPlaceId,
			'bmk'				=> $bmk,
		);

		return View::make('Frontend.index', $params);
	}

	public function getHowToUse() {
		$query = Places::select(DB::raw('name'))->get();

		$params = array(
			'search' 			=> $query,
		);

		return View::make('Frontend.includes.howToUse', $params);
	}

	public function getCredits() {
		$query = Places::select(DB::raw('name'))->get();

		$params = array(
			'search' 			=> $query,
		);

		return View::make('Frontend.includes.credits', $params);
	}

	public function getTentang() {
		$query = Places::select(DB::raw('name'))->get();

		$params = array(
			'search' 			=> $query,
		);

		return View::make('Frontend.includes.tentang', $params);
	}

	public function getForgotPassword() {
		if(!Auth::check()) {
			$query = Places::select(DB::raw('name'))->get();

			$params = array(
				'search' 			=> $query,
			);

			return View::make('Frontend.includes.forgotPassword', $params);
		} else {
			return Redirect::to('/');
		}
	}

	public function searchResults(Request $request) {
		$query = $request->get('placeInput');
		$prefix = Places::select(DB::raw('place_prefix'))
					->where('name','LIKE',"%$query%")
					->get();

		foreach ($prefix as $pref) {
			$prefix = $pref->place_prefix;
		}

		return Redirect::to('place/' . $prefix);
	}

	public function getRecommended() {
		$recommended = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
				->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->orderBy('result','desc')
				->paginate(10);

		$query1 = Locations::all();
		$query2 = Places::select(DB::raw('name'))->get();
	    $query3 = DB::table('price_range')->get();
	    $totalCity = Places::select(DB::raw('count(*) as totalCity'))
	    			->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
	    			->first();

	    $myTime = Carbon::now();
		$myTime = $myTime->addHours(7);
		$myTimeBeforeAWeek = $myTime->subDays(7);

	    $rcm = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();
		$wrr = Places::select(DB::raw('reviews.place_id'))
				->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->whereIn('travel__places.place_id',$reviewsPlaceId)
				->where('reviews.created_at','>',$myTimeBeforeAWeek)
				->where('reviews.created_at','<>',NULL)
				->get();
		$wkrv = array();
		foreach ($wrr as $wr) {
			$wkrv[] = $wr->place_id;
		}

		$weeklyReview = Places::whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->orderBy('name','asc')
						->limit(3)
						->get();

		$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->where('datetime','>',$myTimeBeforeAWeek)
						->where('datetime','<>',NULL)
						->orderBy('datetime','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();

		$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->limit(3)
						->get();

		if(Auth::check()) {
	        $bmk = DB::table('travel__bookmark as tb')
	        		->select('tb.place_id as place_id')
	        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
	        		->where('tb.user_id',Auth::user()->id)
	        		->where('tb.status','1')
	        		->get();
	    } else {
	    	$bmk = "";
	    }

	    $sumLocation = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();

		$params = array(
			'recommended'		=> $recommended,
			'locations'			=> $query1,
			'search' 			=> $query2,
			'price_range'		=> $query3,
			'bmk'				=> $bmk,
			'totalCity'			=> $totalCity,
			'rcm'				=> $rcm,
			'weeklyReview'		=> $weeklyReview,
			'weeklyAdded'		=> $weeklyAdded,
			'notReviewedYet'	=> $notReviewedYet,
			'sumloc1'			=> $sumLocation->sumloc1,
			'sumloc2'			=> $sumLocation->sumloc2,
			'sumloc3'			=> $sumLocation->sumloc3,
			'sumloc4'			=> $sumLocation->sumloc4,
			'sumloc5'			=> $sumLocation->sumloc5,
		);

		return View::make('Frontend.home.recommended', $params);
	}

	public function getWeeklyReview() {
		$myTime = Carbon::now();
		$myTime = $myTime->addHours(7);
		$myTimeBeforeAWeek = $myTime->subDays(7);

		$reviewsPlaceId = Reviews::select('place_id')->get();
		$wrr = Places::select(DB::raw('reviews.place_id'))
				->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->whereIn('travel__places.place_id',$reviewsPlaceId)
				->where('reviews.created_at','>',$myTimeBeforeAWeek)
				->where('reviews.created_at','<>',NULL)
				->get();
		$wkrv = array();
		foreach ($wrr as $wr) {
			$wkrv[] = $wr->place_id;
		}

		$weeklyReview = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$wkrv)
						->orderBy('name','asc')
						->paginate(10);

		$wkrvString = implode(",",$wkrv);
	    $sumLocation = Places::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and travel__places.place_id IN('.$wkrvString.')) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and travel__places.place_id IN('.$wkrvString.')) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and travel__places.place_id IN('.$wkrvString.')) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and travel__places.place_id IN('.$wkrvString.')) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and travel__places.place_id IN('.$wkrvString.')) as sumloc5'))
					->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
					->first();

		$query1 = Locations::all();
		$query2 = Places::select(DB::raw('name'))->get();
		$query3 = DB::table('price_range')->get();

		$totalCity = Places::select(DB::raw('count(*) as totalCity'))
						->whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->first();

		if(Auth::check()) {
	        $bmk = DB::table('travel__bookmark as tb')
	        		->select('tb.place_id as place_id')
	        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
	        		->where('tb.user_id',Auth::user()->id)
	        		->where('tb.status','1')
	        		->get();
	    } else {
	    	$bmk = "";
	    }

	    $recommended = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->where('datetime','>',$myTimeBeforeAWeek)
						->where('datetime','<>',NULL)
						->orderBy('datetime','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();

		$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->limit(3)
						->get();

		$params = array(
			'weeklyReview'		=> $weeklyReview,
			'locations'			=> $query1,
			'search' 			=> $query2,
			'price_range'		=> $query3,
			'totalCity'			=> $totalCity,
			'bmk'				=> $bmk,
			'recommended'		=> $recommended,
			'weeklyAdded'		=> $weeklyAdded,
			'notReviewedYet'	=> $notReviewedYet,
			'sumloc1'			=> $sumLocation->sumloc1,
			'sumloc2'			=> $sumLocation->sumloc2,
			'sumloc3'			=> $sumLocation->sumloc3,
			'sumloc4'			=> $sumLocation->sumloc4,
			'sumloc5'			=> $sumLocation->sumloc5,
		);

		return View::make('Frontend.home.weekly-review', $params);
	}

	public function getWeeklyAdded() {
		$myTime = Carbon::now();
		$myTime = $myTime->addHours(7);
		$myTimeBeforeAWeek = $myTime->subDays(7);

		$weeklyAdded = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->where('datetime','>',$myTimeBeforeAWeek)
						->where('datetime','<>',NULL)
						->orderBy('result','desc')
						->paginate(10);

		$query1 = Locations::all();
		$query2 = Places::select(DB::raw('name'))->get();
	    $query3 = DB::table('price_range')->get();
		$totalCity = Places::select(DB::raw('count(*) as totalCity'))
	    			->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
	    			->where('travel__places.datetime','>',$myTimeBeforeAWeek)
	    			->first();

		if(Auth::check()) {
	        $bmk = DB::table('travel__bookmark as tb')
	        		->select('tb.place_id as place_id')
	        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
	        		->where('tb.user_id',Auth::user()->id)
	        		->where('tb.status','1')
	        		->get();
	    } else {
	    	$bmk = "";
	    }

	    $recommended = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();
		$wrr = Places::select(DB::raw('reviews.place_id'))
				->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->whereIn('travel__places.place_id',$reviewsPlaceId)
				->where('reviews.created_at','>',$myTimeBeforeAWeek)
				->where('reviews.created_at','<>',NULL)
				->get();
		$wkrv = array();
		foreach ($wrr as $wr) {
			$wkrv[] = $wr->place_id;
		}

		$weeklyReview = Places::whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->orderBy('name','asc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();

		$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->limit(3)
						->get();
		$myTimeBeforeAWeekString = $myTimeBeforeAWeek->format('Y-m-d H:i:s');
	    $sumLocation = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and travel__places.datetime > "'.$myTimeBeforeAWeekString.'") as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and travel__places.datetime > "'.$myTimeBeforeAWeekString.'") as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and travel__places.datetime > "'.$myTimeBeforeAWeekString.'") as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and travel__places.datetime > "'.$myTimeBeforeAWeekString.'") as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and travel__places.datetime > "'.$myTimeBeforeAWeekString.'") as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();

		$params = array(
			'weeklyAdded'		=> $weeklyAdded,
			'locations'			=> $query1,
			'search' 			=> $query2,
			'bmk'				=> $bmk,
			'totalCity'			=> $totalCity,
			'price_range'		=> $query3,
			'recommended'		=> $recommended,
			'weeklyReview'		=> $weeklyReview,
			'notReviewedYet'	=> $notReviewedYet,
			'sumloc1'			=> $sumLocation->sumloc1,
			'sumloc2'			=> $sumLocation->sumloc2,
			'sumloc3'			=> $sumLocation->sumloc3,
			'sumloc4'			=> $sumLocation->sumloc4,
			'sumloc5'			=> $sumLocation->sumloc5,
		);

		return View::make('Frontend.home.weekly-added', $params);
	}

	public function getNotReviewedYet() {
		$reviewsPlaceId = Reviews::select('place_id')->get();

		$notReviewedYet = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->paginate(10);

		$query1 = Locations::all();
		$query2 = Places::select(DB::raw('name'))->get();
		$query3 = DB::table('price_range')->get();

	    $totalCity = Places::select(DB::raw('count(*) as totalCity'))
	    			->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
					->whereNotIn('travel__places.place_id',$reviewsPlaceId)
	    			->first();
	    foreach ($reviewsPlaceId as $rpi) {
	    	$reviewsPlaceIdString[] = $rpi->place_id;
	    }

		$reviewsPlaceIdStr = implode(",",$reviewsPlaceIdString);
	    $sumLocation = Places::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and travel__places.place_id NOT IN('.$reviewsPlaceIdStr.')) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and travel__places.place_id NOT IN('.$reviewsPlaceIdStr.')) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and travel__places.place_id NOT IN('.$reviewsPlaceIdStr.')) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and travel__places.place_id NOT IN('.$reviewsPlaceIdStr.')) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and travel__places.place_id NOT IN('.$reviewsPlaceIdStr.')) as sumloc5'))
					->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
					->first();

		if(Auth::check()) {
	        $bmk = DB::table('travel__bookmark as tb')
	        		->select('tb.place_id as place_id')
	        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
	        		->where('tb.user_id',Auth::user()->id)
	        		->where('tb.status','1')
	        		->get();
	    } else {
	    	$bmk = "";
	    }

	    $myTime = Carbon::now();
		$myTime = $myTime->addHours(7);
		$myTimeBeforeAWeek = $myTime->subDays(7);

	    $recommended = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();
		$wrr = Places::select(DB::raw('reviews.place_id'))
				->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->whereIn('travel__places.place_id',$reviewsPlaceId)
				->where('reviews.created_at','>',$myTimeBeforeAWeek)
				->where('reviews.created_at','<>',NULL)
				->get();
		$wkrv = array();
		foreach ($wrr as $wr) {
			$wkrv[] = $wr->place_id;
		}

		$weeklyReview = Places::whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->orderBy('name','asc')
						->limit(3)
						->get();

		$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->where('datetime','>',$myTimeBeforeAWeek)
						->where('datetime','<>',NULL)
						->orderBy('datetime','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();

		$nry = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->limit(3)
						->get();

		$params = array(
			'notReviewedYet'	=> $notReviewedYet,
			'locations'			=> $query1,
			'search' 			=> $query2,
			'price_range' 		=> $query3,
			'bmk'				=> $bmk,
			'totalCity'			=> $totalCity,
			'recommended'		=> $recommended,
			'weeklyReview'		=> $weeklyReview,
			'weeklyAdded'		=> $weeklyAdded,
			'nry'				=> $nry,
			'sumloc1'			=> $sumLocation->sumloc1,
			'sumloc2'			=> $sumLocation->sumloc2,
			'sumloc3'			=> $sumLocation->sumloc3,
			'sumloc4'			=> $sumLocation->sumloc4,
			'sumloc5'			=> $sumLocation->sumloc5,
		);

		return View::make('Frontend.home.not-reviewed-yet', $params);
	}

	public function getResetSuccess() {
		$query = Places::select(DB::raw('name'))->get();

		$params = array(
			'search' 			=> $query,
		);

		return View::make('auth.passwords.reset-success', $params);
	}
}
