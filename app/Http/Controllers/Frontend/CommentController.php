<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use View;
use App\Locations;
use App\Categories;
use App\Places;
use App\Comments;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class CommentController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function store(Request $request){
		$komentar = $request->get('body');
		$komen = nl2br($komentar);

		$prefix = $request->get('place_prefix');

		$input['from_user'] 	= $request->user()->id;
		$input['on_post'] 		= $request->input('on_post');
		$input['body'] 			= $komen;
		$input['prefix'] 		= $request->input('prefix');
		$input['created_at'] 	= Carbon::now()->addHours(7);
		$input['updated_at'] 	= Carbon::now()->addHours(7);
		$slug 					= $request->input('slug');
		Comments::create( $input );
 
		return Redirect::to('place/' . $prefix . '#ulasan');
	}
}
