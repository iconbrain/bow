<?php
namespace App\Http\Controllers\Frontend;

use App\User;
use App\Places;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

use Session;
use View;

class UserController extends Controller
{
	public function login(Request $request) {
		$rules = array(
			'email' 	=> 'required|email',
			'password'	=> 'required|min:6'
		);

        $message = array(
        	'email.required'	=> 'Bidang isian email wajib diisi.',
        	'email.email'		=> 'Bidang isian email tidak valid.',
        	'password.required'	=> 'Bidang isian kata sandi wajib diisi.',
        	'password.min'		=> 'Bidang isian kata sandi minimal 6 karakter.',
        );

		$validator = Validator::make (Input::all (), $rules, $message);

		if ($validator->fails ()) {
			return Redirect::back()->withErrors($validator, 'login')->withInput();
		} else {
			if (Auth::attempt(array(
				'email' 	=> $request['email'],
				'password' 	=> $request['password'],
             	'role_id'   => '2'
			)))
			{
				session ([
					'email' => $request['email']
				]);
				return Redirect::back();
			} else {
				Session::flash('message', "Akun tidak ditemukan. Silahkan coba lagi.");
				return Redirect::back();
			}
		}
	}

	public function signup(Request $request) {
		$rules = array (
			'email' 	=> 'required|unique:users|email',
			'name' 		=> 'required|unique:users|alpha_spaces|min:3',
			'password' 	=> 'required|min:6|confirmed'
		);

        $message = array(
        	'email.required'		=> 'Bidang isian email wajib diisi.',
        	'email.unique'			=> 'Email sudah terdaftar.',
        	'email.email'			=> 'Bidang isian email tidak sah.',
        	'name.required'			=> 'Bidang isian nama lengkap wajib diisi.',
        	'name.unique'			=> 'Bidang isian nama lengkap sudah terdaftar.',
        	'name.alpha_spaces'		=> 'Bidang isian nama lengkap hanya boleh berisi huruf dan spasi.',
        	'name.min'				=> 'Bidang isian nama lengkap minimal 3 karakter.',
        	'password.required'		=> 'Bidang isian kata sandi wajib diisi.',
        	'password.min'			=> 'Bidang isian kata sandi minimal 6 karakter.',
        	'password.confirmed'	=> 'Konfirmasi kata sandi tidak cocok.',
        );

		$validator = Validator::make ( Input::all (), $rules, $message);

		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator, 'signup')->withInput();
		} else {
			$date 					= Carbon::now()->addHours(7);

			$user = new User;
			$user->role_id 			= 2;
			$user->avatar 			= 'default.png';
			$user->created_at 		= $date;
			$user->updated_at 		= $date;
			$user->name 			= $request['name'];
			$user->name_prefix		= strtolower(str_replace(array(',', '.', '*', ' '), '-', $request['name']));
			$user->email 			= $request['email'];
			$user->password 		= bcrypt($request['password']);
			$user->remember_token 	= $request['_token'];
			
			$user->save();

			if (Auth::attempt(array(
				'email' 	=> $request['email'],
				'password' 	=> $request['password'],
			))) {
				session ([
					'email' => $request['email']
				]);
				return Redirect::back();
			}
		}
	}

	public function showResetPassword() {
		$query = Places::select(DB::raw('name'))->get();

		return View::make('Frontend.auth.passwords.reset', ['search' => $query]);
	}

	public function logout() {
		Session::flush();
		Auth::logout();
		return Redirect::back();
	}

	public function logoutProfile() {
		Session::flush();
		Auth::logout();
		return Redirect::to('/');
	}
}

