<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use Validator;
use App\Locations;
use App\Categories;
use App\Places;
use App\Comments;
use App\Users;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class ProfileController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function getProfile(){
		if(Auth::check()) {
			$query  = DB::table('roles')
						->select(DB::raw('roles.display_name as display_name'))
						->leftjoin('users','users.role_id','=','roles.id')
						->where('users.id','=',Auth::user()->id)
						->first();
			$query2 = DB::table('reviews')
						->leftjoin('users','users.id','=','reviews.author_id')
						->where('reviews.author_id','=',Auth::user()->id)
						->count();
			$query3 = DB::table('comments')
						->leftjoin('users','users.id','=','comments.from_user')
						->where('comments.from_user','=',Auth::user()->id)
						->count();
			$query4 = DB::table('travel__bookmark as tb')
						->leftjoin('users','users.id','=','tb.user_id')
						->where('tb.status','=','1')
						->where('tb.user_id','=',Auth::user()->id)
						->count();
			$display_name = $query->display_name;
			$query6 = Places::select(DB::raw('name'))->get();
			$query7 = Users::where('name_prefix','=',Auth::user()->name_prefix)->first();
			$name_prefix = $query7->name_prefix;

			$params = array(
				'search' 			=> $query6,
				'display_name'		=> $display_name,
				'total_reviews'		=> $query2,
				'total_comments'	=> $query3,
				'total_bookmarks'	=> $query4,
				'name_prefix'		=> $name_prefix,
			);

			return View::make('Frontend.profile.index', $params);
		} else {
			return Redirect::to('/');
		}
	}

	public function getPassword(){
		if(Auth::check()) {
			$query  = DB::table('roles')
						->select(DB::raw('roles.display_name as display_name'))
						->leftjoin('users','users.role_id','=','roles.id')
						->where('users.id','=',Auth::user()->id)
						->first();
			$query2 = DB::table('reviews')
						->leftjoin('users','users.id','=','reviews.author_id')
						->where('reviews.author_id','=',Auth::user()->id)
						->count();
			$query3 = DB::table('comments')
						->leftjoin('users','users.id','=','comments.from_user')
						->where('comments.from_user','=',Auth::user()->id)
						->count();
			$query4 = DB::table('travel__bookmark as tb')
						->leftjoin('users','users.id','=','tb.user_id')
						->where('tb.status','=','1')
						->where('tb.user_id','=',Auth::user()->id)
						->count();
			$display_name = $query->display_name;
			$query6 = Places::select(DB::raw('name'))->get();
			$query7 = Users::where('name_prefix','=',Auth::user()->name_prefix)->first();
			$name_prefix = $query7->name_prefix;

			$params = array(
				'search' 			=> $query6,
				'display_name'		=> $display_name,
				'total_reviews'		=> $query2,
				'total_comments'	=> $query3,
				'total_bookmarks'	=> $query4,
				'name_prefix'		=> $name_prefix,
			);

			return View::make('Frontend.profile.password', $params);
		} else {
			return Redirect::to('/');
		}
	}

	public function getReview(){
		if(Auth::check()) {
			$query  = DB::table('roles')
						->select(DB::raw('roles.display_name as display_name'))
						->leftjoin('users','users.role_id','=','roles.id')
						->where('users.id','=',Auth::user()->id)
						->first();
			$query2 = DB::table('reviews')
						->leftjoin('users','users.id','=','reviews.author_id')
						->where('reviews.author_id','=',Auth::user()->id)
						->count();
			$query3 = DB::table('comments')
						->leftjoin('users','users.id','=','comments.from_user')
						->where('comments.from_user','=',Auth::user()->id)
						->count();
			$query4 = DB::table('travel__bookmark as tb')
						->leftjoin('users','users.id','=','tb.user_id')
						->where('tb.status','=','1')
						->where('tb.user_id','=',Auth::user()->id)
						->count();
			$query5 = DB::table('reviews as r')
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('r.author_id','=',Auth::user()->id)
						->orderBy('r.created_at','desc')
						->get();
			$display_name = $query->display_name;
			$query6 = Places::select(DB::raw('name'))->get();
			$query7 = Users::where('name_prefix','=',Auth::user()->name_prefix)->first();
			$name_prefix = $query7->name_prefix;

			$params = array(
				'search' 			=> $query6,
				'display_name'		=> $display_name,
				'total_reviews'		=> $query2,
				'total_comments'	=> $query3,
				'total_bookmarks'	=> $query4,
				'name_prefix'		=> $name_prefix,
				'reviews'			=> $query5,
			);

			return View::make('Frontend.profile.review', $params);
		} else {
			return Redirect::to('/');
		}
	}

	public function postDeleteReview(Request $req) {
		$review_id 	= $req->get('review_id');

		$comments  	= DB::table('comments')
						->where('on_post', $review_id)
						->delete();

		$review 	= DB::table('reviews')
						->where('id', $review_id)
						->delete();

		// update avgRating-nya
        $query6 = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                ->where('place_id',$req->get('place_id'))
                ->get();
        foreach ($query6 as $q6) {
        	$fattr = round($q6->facility_attr,1);
        	$cattr = round($q6->cleanliness_attr,1);
        	$comattr = round($q6->comfort_attr,1);
        	$sattr = round($q6->service_attr,1);
        	$pattr = round($q6->price_attr,1);
        }

        $qAHP = DB::table('ahp_matrix as am')
                    ->select('*')
                    ->get();
        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
        $sService = DB::table('ahp_matrix as am')->sum('service');
        $sPrice = DB::table('ahp_matrix as am')->sum('price');
        $vp = array();
        foreach($qAHP as $qhp) {
            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
        }
        foreach ($query6 as $q6) {
	        $avgRate = round((($q6->facility_attr * $vp[0]) + ($q6->cleanliness_attr * $vp[1]) + ($q6->comfort_attr * $vp[2]) + ($q6->service_attr * $vp[3]) + ($q6->price_attr * $vp[4])),1);
        }

        DB::update('UPDATE travel__places
		          SET avgRating =?
		          WHERE place_id = ?', [$avgRate, $req->get('place_id')]);

       	// update result-nya
       	$query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

       	$fasilitas = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','1')
                    ->first();
        $kebersihan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','2')
                    ->first();
        $kenyamanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','3')
                    ->first();
        $pelayanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','4')
                    ->first();
        $harga = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','5')
                    ->first();

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas->bobot;
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan->bobot;
            $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan->bobot;
            $itemsService[] = $q->service_attr/$sumService * $pelayanan->bobot;
            $itemsPrice[] = $q->price_attr/$sumPrice * $harga->bobot;
        }

        foreach ($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }

		return Redirect::to('account/review');
	}

	public function getEditReview($reviewId) {
		if(Auth::check()) {
			$author_id = DB::table('reviews as r')
						->leftjoin('users','users.id','=','r.author_id')
						->where('r.id',$reviewId)
						->first();
			if(Auth::user()->id == $author_id->author_id) {
				$query  = DB::table('roles')
							->select(DB::raw('roles.display_name as display_name'))
							->leftjoin('users','users.role_id','=','roles.id')
							->where('users.id','=',Auth::user()->id)
							->first();
				$query2 = DB::table('reviews')
							->leftjoin('users','users.id','=','reviews.author_id')
							->where('reviews.author_id','=',Auth::user()->id)
							->count();
				$query3 = DB::table('comments')
							->leftjoin('users','users.id','=','comments.from_user')
							->where('comments.from_user','=',Auth::user()->id)
							->count();
				$query4 = DB::table('travel__bookmark as tb')
							->leftjoin('users','users.id','=','tb.user_id')
							->where('tb.status','=','1')
							->where('tb.user_id','=',Auth::user()->id)
							->count();
				$query6 = Places::select(DB::raw('name'))->get();
				$query7 = DB::table('reviews as r')
						->select('*', DB::raw('r.id as id'), DB::raw('r.price as price'))
						->leftjoin('users','users.id','=','r.author_id')
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('r.id',$reviewId)
						->first();
				$display_name = $query->display_name;

				$params = array(
					'display_name'		=> $display_name,
					'total_reviews'		=> $query2,
					'total_comments'	=> $query3,
					'total_bookmarks'	=> $query4,
					'search' 			=> $query6,
					'places'			=> $query7,
				);

				return View::make('Frontend.profile.editReview', $params);
			} else {
				return Redirect::to('/');
			}
		} else {
			return Redirect::to('/');
		}
	}

	public function getEditComment($commentId) {
		if(Auth::check()) {
			$author_id = DB::table('comments as c')
						->leftjoin('users','users.id','=','c.from_user')
						->where('c.id',$commentId)
						->first();
			if(Auth::user()->id == $author_id->from_user) {
				$query  = DB::table('roles')
							->select(DB::raw('roles.display_name as display_name'))
							->leftjoin('users','users.role_id','=','roles.id')
							->where('users.id','=',Auth::user()->id)
							->first();
				$query2 = DB::table('reviews')
							->leftjoin('users','users.id','=','reviews.author_id')
							->where('reviews.author_id','=',Auth::user()->id)
							->count();
				$query3 = DB::table('comments')
							->leftjoin('users','users.id','=','comments.from_user')
							->where('comments.from_user','=',Auth::user()->id)
							->count();
				$query4 = DB::table('travel__bookmark as tb')
							->leftjoin('users','users.id','=','tb.user_id')
							->where('tb.status','=','1')
							->where('tb.user_id','=',Auth::user()->id)
							->count();
				$query6 = Places::select(DB::raw('name'))->get();
				$query7 = DB::table('comments as c')
						->select('*', DB::raw('r.body as reviewBody'), DB::raw('c.body as commentBody'), DB::raw('tp.name as placeName'), DB::raw('c.id as commentId'))
						->leftjoin('users','users.id','=','c.from_user')
						->leftjoin('reviews as r','r.id','=','c.on_post')
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('c.id',$commentId)
						->first();
				$display_name = $query->display_name;

				$params = array(
					'display_name'		=> $display_name,
					'total_reviews'		=> $query2,
					'total_comments'	=> $query3,
					'total_bookmarks'	=> $query4,
					'search' 			=> $query6,
					'places'			=> $query7,
				);

				return View::make('Frontend.profile.editComment', $params);
			} else {
				return Redirect::to('/');
			}
		} else {
			return Redirect::to('/');
		}
	}

	public function postEditComment(Request $req) {
		$rules = array (
			'komentar'			=> 'required',
		);

        $message = array(
        	'komentar.required'		=> 'Bidang isian komentar wajib diisi.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		$comment_id = $req->get('comment_id');

		if ($validator->fails()) {
	    	return Redirect::to('account/edit-comment/' . $comment_id)->withErrors($validator, 'postEditComment')->withInput();
		}

		$komentar 		= $req->get('komentar');
		$komen 			= nl2br($komentar);
		$updated_at		= Carbon::now()->addHours(7);

		DB::update('UPDATE comments
		          SET body=?, updated_at=?
		          WHERE id=?', [$komen, $updated_at, $comment_id]);

		return Redirect::to('account/comment')->withMessage('Sukses memperbaharui komentar.');
	}

	public function postEditReview(Request $req) {
		$rules = array (
			'fasilitas'			=> 'required',
			'kebersihan'		=> 'required',
			'kenyamanan'		=> 'required',
			'pelayanan'			=> 'required',
			'harga'				=> 'required',
			'title' 			=> 'required|min:10|max:191',
			'komentar'			=> 'required|min:20',
		);

        $message = array(
        	'fasilitas.required'	=> 'Rating untuk fasilitas wajib diisi.',
        	'kebersihan.required'	=> 'Rating untuk kebersihan wajib diisi.',
        	'kenyamanan.required'	=> 'Rating untuk kenyamanan wajib diisi.',
        	'pelayanan.required'	=> 'Rating untuk pelayanan wajib diisi.',
        	'harga.required'		=> 'Rating untuk harga wajib diisi.',
        	'title.required'		=> 'Bidang isian judul ulasan wajib diisi.',
        	'title.min'				=> 'Judul kurang dari 10 karakter.',
        	'title.max'				=> 'Judul maksimal 190 karakter.',
        	'komentar.required'		=> 'Bidang isian ulasan wajib diisi.',
        	'komentar.min'			=> 'Ulasan kurang dari 20 karakter.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		$review_id = $req->get('review_id');

		if ($validator->fails()) {
	    	return Redirect::to('account/edit-review/' . $review_id)->withErrors($validator, 'postEditReview')->withInput();
		}

		$komentar 		= $req->get('komentar');
		$komen 			= $komentar;
		$title 			= $req->get('title');
		$facility 		= $req->get('fasilitas');
		$cleanliness 	= $req->get('kebersihan');
		$comfort 		= $req->get('kenyamanan');
		$service 		= $req->get('pelayanan');
		$price 			= $req->get('harga');
		$updated_at		= Carbon::now()->addHours(7);

		DB::update('UPDATE reviews
		          SET title=?, body=?, facility=?, cleanliness=?, comfort=?, service=?, price=?, updated_at=?
		          WHERE id=?', [$title, $komen, $facility, $cleanliness, $comfort, $service, $price, $updated_at, $review_id]);

        // update avgRating-nya
        $query6 = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                ->where('place_id',$req->get('place_id'))
                ->get();
        foreach ($query6 as $q6) {
        	$fattr = round($q6->facility_attr,1);
        	$cattr = round($q6->cleanliness_attr,1);
        	$comattr = round($q6->comfort_attr,1);
        	$sattr = round($q6->service_attr,1);
        	$pattr = round($q6->price_attr,1);
        }

        $qAHP = DB::table('ahp_matrix as am')
                    ->select('*')
                    ->get();
        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
        $sService = DB::table('ahp_matrix as am')->sum('service');
        $sPrice = DB::table('ahp_matrix as am')->sum('price');
        $vp = array();
        foreach($qAHP as $qhp) {
            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
        }
        foreach ($query6 as $q6) {
	        $avgRate = round((($q6->facility_attr * $vp[0]) + ($q6->cleanliness_attr * $vp[1]) + ($q6->comfort_attr * $vp[2]) + ($q6->service_attr * $vp[3]) + ($q6->price_attr * $vp[4])),1);
        }

        DB::update('UPDATE travel__places
		          SET avgRating =?
		          WHERE place_id = ?', [$avgRate, $req->get('place_id')]);

		// update ahp-topsis result-nya
	    $query3 = DB::table('ahp_matrix as am')
                        ->select('*')
                        ->get();
        $sumFacilityAHP = DB::table('ahp_matrix as am')->sum('facility');
        $sumCleanlinessAHP = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sumComfortAHP = DB::table('ahp_matrix as am')->sum('comfort');
        $sumServiceAHP = DB::table('ahp_matrix as am')->sum('service');
        $sumPriceAHP = DB::table('ahp_matrix as am')->sum('price');

        $vp = array();
        foreach($query3 as $q) {
            $vp[] = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);
        }

        $hasilkali = array();
        foreach($query3 as $q) {
            $hasilkali[] = round(($q->facility*$vp[0])+($q->cleanliness*$vp[1])+($q->comfort*$vp[2])+($q->service*$vp[3])+($q->price*$vp[4]),3);
        }

        $hasilkali_length = count($hasilkali);
        $sumLambdaMaks = 0;
        for($i=0;$i<$hasilkali_length;$i++) {
            $sumLambdaMaks += round($hasilkali[$i]/$vp[$i],3);
        }
        $lambdaMaks = round($sumLambdaMaks/5,3);
        $ci = round((round($sumLambdaMaks/5,3)-$i)/($i-1),3);
        $cr = round($ci/1.12,3);

        if($cr <= 0.1) {
            foreach ($query3 as $q) {
                $priorityVector = round((round($q->facility/$sumFacilityAHP,3)+round($q->cleanliness/$sumCleanlinessAHP,3)+round($q->comfort/$sumComfortAHP,3)+round($q->service/$sumServiceAHP,3)+round($q->price/$sumPriceAHP,3))/5,3);

                DB::update('UPDATE ahp_matrix
                      SET bobot =?
                      WHERE name = ?', [$priorityVector, $q->name]);
            }
        }

		$query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

        $sumFacility = 0;
        $sumCleanliness =0;
        $sumComfort =0;
        $sumService =0;
        $sumPrice = 0;
        foreach($query5 as $q) {
            $sumFacility += pow($q->facility_attr, 2);
            $sumCleanliness += pow($q->cleanliness_attr, 2);
            $sumComfort += pow($q->comfort_attr, 2);
            $sumService += pow($q->service_attr, 2);
            $sumPrice += pow($q->price_attr, 2);
        }
        $sumFacility = sqrt($sumFacility);
        $sumCleanliness = sqrt($sumCleanliness);
        $sumComfort = sqrt($sumComfort);
        $sumService = sqrt($sumService);
        $sumPrice = sqrt($sumPrice);

       	$fasilitas = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','1')
                    ->first();
        $kebersihan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','2')
                    ->first();
        $kenyamanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','3')
                    ->first();
        $pelayanan = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','4')
                    ->first();
        $harga = DB::table('ahp_matrix as am')
                    ->select(DB::raw('bobot'))
                    ->where('id','5')
                    ->first();

        $itemsFacility = array();
        $itemsCleanliness = array();
        $itemsComfort = array();
        $itemsService = array();
        $itemsPrice = array();

        foreach($query5 as $q) {
            $itemsFacility[] = $q->facility_attr/$sumFacility * $fasilitas->bobot;
            $itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness * $kebersihan->bobot;
            $itemsComfort[] = $q->comfort_attr/$sumComfort * $kenyamanan->bobot;
            $itemsService[] = $q->service_attr/$sumService * $pelayanan->bobot;
            $itemsPrice[] = $q->price_attr/$sumPrice * $harga->bobot;
        }

        foreach ($query5 as $q) {
            $posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
            $negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
            $rank = $negSolution/($negSolution+$posSolution);

            DB::update('UPDATE travel__places
                  SET result =?
                  WHERE place_id = ?', [$rank, $q->place_id]);
        }

		return Redirect::to('account/review')->withMessage('Sukses memperbaharui ulasan.');
	}

	public function getComment(){
		if(Auth::check()) {
			$query  = DB::table('roles')
						->select(DB::raw('roles.display_name as display_name'))
						->leftjoin('users','users.role_id','=','roles.id')
						->where('users.id','=',Auth::user()->id)
						->first();
			$query2 = DB::table('reviews')
						->leftjoin('users','users.id','=','reviews.author_id')
						->where('reviews.author_id','=',Auth::user()->id)
						->count();
			$query3 = DB::table('comments')
						->leftjoin('users','users.id','=','comments.from_user')
						->where('comments.from_user','=',Auth::user()->id)
						->count();
			$query4 = DB::table('travel__bookmark as tb')
						->leftjoin('users','users.id','=','tb.user_id')
						->where('tb.status','=','1')
						->where('tb.user_id','=',Auth::user()->id)
						->count();
			$query5 = DB::table('comments as c')
						->select('*', DB::raw('c.body as body'), DB::raw('c.created_at as created_at'), DB::raw('c.id as id'))
						->leftjoin('reviews as r','r.id','=','c.on_post')
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('c.from_user','=',Auth::user()->id)
						->orderBy('c.created_at','desc')
						->get();
			$display_name = $query->display_name;
			$query6 = Places::select(DB::raw('name'))->get();
			$query7 = Users::where('name_prefix','=',Auth::user()->name_prefix)->first();
			$name_prefix = $query7->name_prefix;

			$params = array(
				'search' 			=> $query6,
				'display_name'		=> $display_name,
				'total_reviews'		=> $query2,
				'total_comments'	=> $query3,
				'total_bookmarks'	=> $query4,
				'name_prefix'		=> $name_prefix,
				'comments'			=> $query5,
			);

			return View::make('Frontend.profile.comment', $params);
		} else {
			return Redirect::to('/');
		}
	}

	public function postDeleteComment(Request $req) {
		$comment_id 	= $req->get('comment_id');

		$comments  	= DB::table('comments')
						->where('id', $comment_id)
						->delete();

		return Redirect::to('account/comment');
	}

	public function getBookmark(){
		if(Auth::check()) {
			$query  = DB::table('roles')
						->select(DB::raw('roles.display_name as display_name'))
						->leftjoin('users','users.role_id','=','roles.id')
						->where('users.id','=',Auth::user()->id)
						->first();
			$query2 = DB::table('reviews')
						->leftjoin('users','users.id','=','reviews.author_id')
						->where('reviews.author_id','=',Auth::user()->id)
						->count();
			$query3 = DB::table('comments')
						->leftjoin('users','users.id','=','comments.from_user')
						->where('comments.from_user','=',Auth::user()->id)
						->count();
			$query4 = DB::table('travel__bookmark as tb')
						->leftjoin('users','users.id','=','tb.user_id')
						->where('tb.status','=','1')
						->where('tb.user_id','=',Auth::user()->id)
						->count();
			$query5 = DB::table('travel__bookmark as tb')
						->leftjoin('users','users.id','=','tb.user_id')
						->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
						->where('tb.status','=','1')
						->where('tb.user_id','=',Auth::user()->id)
						->orderBy('tb.datetime','desc')
						->get();
			$display_name = $query->display_name;
			$query6 = Places::select(DB::raw('name'))->get();
			$query7 = Users::where('name_prefix','=',Auth::user()->name_prefix)->first();
			$name_prefix = $query7->name_prefix;

			$params = array(
				'search' 			=> $query6,
				'display_name'		=> $display_name,
				'total_reviews'		=> $query2,
				'total_comments'	=> $query3,
				'total_bookmarks'	=> $query4,
				'name_prefix'		=> $name_prefix,
				'bookmark'			=> $query5,
			);

			return View::make('Frontend.profile.bookmark', $params);
		} else {
			return Redirect::to('/');
		}
	}

	public function editUserProfile(Request $request) {
		$rules = array (
			'email' 	=> 'required',
			'name' 		=> 'required|alpha_spaces|min:3',
			'image'		=> 'sometimes|max:2048'
		);

        $message = array(
        	'name.alpha_spaces'	=> 'Bidang isian nama lengkap hanya boleh berisi huruf dan spasi.',
        	'name.min'			=> 'Bidang isian nama lengkap minimal 3 karakter.',
        	'image.max'			=> 'Foto profil maksimal 2 MB.'
        );

		$validator 	= Validator::make(Input::all(), $rules, $message);

		$name 		= $request->get('name');
		$email 		= $request->get('email');

		$emailExist = Users::where('email', $email)->where('id', '<>', Auth::user()->id)->count();
		$nameExist 	= Users::where('name', $name)->where('id', '<>', Auth::user()->id)->count();

		if($emailExist) {
			return Redirect::to('account/profile/')->withErrors('Email sudah terdaftar', 'editUserProfile')->withInput();
		}
		if($nameExist) {
			return Redirect::to('account/profile/')->withErrors('Nama Lengkap sudah terdaftar', 'editUserProfile')->withInput();
		}

		if ($validator->fails()) {
	    	return Redirect::to('account/profile/')->withErrors($validator, 'editUserProfile')->withInput();
		} else {
			$updated_at 	= Carbon::now()->addHours(7);
			$image 			= Input::file('image');
			if($image) {
				$filename  = time() . '.' . $image->getClientOriginalExtension();
		        $path = public_path('images/' . $filename);
		        Image::make($image->getRealPath())->resize(468, 249)->save($path);

				DB::update('UPDATE users
			          SET name =?, email=?, avatar=?, updated_at=?
			          WHERE id = ?', [$name, $email, $filename, $updated_at, Auth::user()->id]);
		    	\File::delete(public_path('images/' . $request->get('iconOld')));
		    } else {
		    	DB::update('UPDATE users
			          SET name =?, email=?, updated_at=?
			          WHERE id = ?', [$name, $email, $updated_at, Auth::user()->id]);
		    }

			return Redirect::to('account/profile/')->withMessage('Profile berhasil diperbaharui.');
		}
	}

	public function editUserPassword(Request $request) {
		$rules = array (
			'old_password' 			=> 'required',
			'new_password' 			=> 'required|min:6',
			'new_password_confirm' 	=> 'required|min:6|same:new_password',
		);

        $message = array(
        	'new_password.min'			=> 'Bidang isian password baru minimal 6 karakter.',
        	'new_password_confirm.min'	=> 'Bidang isian konfirmasi password baru minimal 6 karakter.',
        	'new_password_confirm.same'	=> 'Konfirmasi password tidak cocok.',
        );

		$validator 	= Validator::make(Input::all(), $rules, $message);

		$old_password		= $request->get('old_password');
		$new_password		= $request->get('new_password');
		$invalidPassword 	= 0;
		if(Hash::check($old_password, Auth::user()->password)) {
			$invalidPassword = 0;
		} else {
			$invalidPassword = 1;
		}
		if($old_password == $new_password) {
			$passwordTidakBerubah = 1;
		} else {
			$passwordTidakBerubah = 0;
		}

		$password 		= bcrypt($request->get('new_password'));
		$updated_at 	= Carbon::now()->addHours(7);

		if($invalidPassword == 1) {
			return Redirect::to('/account/password/')->withErrors('Password lama salah.', 'editUserPassword')->withInput();
		}
		else if($passwordTidakBerubah == 1) {
			return Redirect::to('/account/password/')->withErrors('Password tidak berubah.', 'editUserPassword')->withInput();
		}
		else if ($validator->fails()) {
	    	return Redirect::to('/account/password/')->withErrors($validator, 'editUserPassword')->withInput();
		} else {
			DB::update('UPDATE users
		          SET password =?, updated_at=?
		          WHERE id = ?', [$password, $updated_at, Auth::user()->id]);

			return Redirect::to('account/password/')->withMessage('Kata sandi berhasil diperbaharui.');
		}
	}
}
