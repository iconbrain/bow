<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use App\Locations;
use App\Categories;
use App\Places;
use App\Reviews;
use Carbon\Carbon;
use DB;
use Auth;
use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CategoryController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function placeByCategory($prefix){
		$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
				->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->where('travel__categories.prefix', $prefix)
				->orderBy('result','desc')
				->paginate(10);
		$query2 = Categories::where('travel__categories.prefix', $prefix)->get();
		$query3 = Locations::all();

		$sumLocationAlam = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=1) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=1) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=1) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=1) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=1) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationReligi = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=2) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=2) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=2) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=2) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=2) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationSejarah = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=3) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=3) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=3) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=3) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=3) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationBelanja = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=4) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=4) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=4) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=4) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=4) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationSatwa = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=5) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=5) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=5) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=5) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=5) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationKuliner = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=6) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=6) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=6) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=6) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=6) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationAgro = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=7) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=7) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=7) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=7) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=7) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();
		$sumLocationKeluarga = Locations::select(
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=1 and category_id=8) as sumloc1'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=2 and category_id=8) as sumloc2'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=3 and category_id=8) as sumloc3'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=4 and category_id=8) as sumloc4'),
			DB::raw('(SELECT count(*) FROM travel__places WHERE location_id=5 and category_id=8) as sumloc5'))
					->leftjoin('travel__places as tp','tp.location_id','=','travel__locations.location_id')
					->first();

		$query4 = Locations::leftjoin('travel__places', 'travel__places.location_id', '=', 'travel__locations.location_id')
					->get();			
		$query5 = DB::table('price_range')->get();
		$query6 = Places::select(DB::raw('name'))->get();

		if(Auth::check()) {
	        $bmk = DB::table('travel__bookmark as tb')
	        		->select('tb.place_id as place_id')
	        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
	        		->where('tb.user_id',Auth::user()->id)
	        		->where('tb.status','1')
	        		->get();
	    } else {
	    	$bmk = "";
	    }
	    $totalCity = Places::select(DB::raw('count(*) as totalCity'))
	    			->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
	    			->where('travel__categories.prefix', $prefix)
	    			->first();

	    $myTime = Carbon::now();
		$myTime = $myTime->addHours(7);
		$myTimeBeforeAWeek = $myTime->subDays(7);

	    $recommended = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();
		$wrr = Places::select(DB::raw('reviews.place_id'))
				->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
				->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
				->whereIn('travel__places.place_id',$reviewsPlaceId)
				->where('reviews.created_at','>',$myTimeBeforeAWeek)
				->where('reviews.created_at','<>',NULL)
				->get();
		$wkrv = array();
		foreach ($wrr as $wr) {
			$wkrv[] = $wr->place_id;
		}

		$weeklyReview = Places::whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->orderBy('result','desc')
						->limit(3)
						->get();

		$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->where('datetime','>',$myTimeBeforeAWeek)
						->where('datetime','<>',NULL)
						->orderBy('datetime','desc')
						->limit(3)
						->get();

		$reviewsPlaceId = Reviews::select('place_id')->get();

		$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->orderBy('name','asc')
						->limit(3)
						->get();

		$params = array(
			'places' 			=> $query,
			'categories' 		=> $query2,
			'locations' 		=> $query3,
			'locations2' 		=> $query4,
			'price_range'		=> $query5,
			'search'			=> $query6,
			'totalCity'			=> $totalCity,
			'bmk'				=> $bmk,
			'recommended'		=> $recommended,
			'weeklyReview'		=> $weeklyReview,
			'weeklyAdded'		=> $weeklyAdded,
			'notReviewedYet'	=> $notReviewedYet,
			'sumlocAlam1'		=> $sumLocationAlam->sumloc1,
			'sumlocAlam2'		=> $sumLocationAlam->sumloc2,
			'sumlocAlam3'		=> $sumLocationAlam->sumloc3,
			'sumlocAlam4'		=> $sumLocationAlam->sumloc4,
			'sumlocAlam5'		=> $sumLocationAlam->sumloc5,
			'sumlocReligi1'		=> $sumLocationReligi->sumloc1,
			'sumlocReligi2'		=> $sumLocationReligi->sumloc2,
			'sumlocReligi3'		=> $sumLocationReligi->sumloc3,
			'sumlocReligi4'		=> $sumLocationReligi->sumloc4,
			'sumlocReligi5'		=> $sumLocationReligi->sumloc5,
			'sumlocSejarah1'	=> $sumLocationSejarah->sumloc1,
			'sumlocSejarah2'	=> $sumLocationSejarah->sumloc2,
			'sumlocSejarah3'	=> $sumLocationSejarah->sumloc3,
			'sumlocSejarah4'	=> $sumLocationSejarah->sumloc4,
			'sumlocSejarah5'	=> $sumLocationSejarah->sumloc5,
			'sumlocBelanja1'	=> $sumLocationBelanja->sumloc1,
			'sumlocBelanja2'	=> $sumLocationBelanja->sumloc2,
			'sumlocBelanja3'	=> $sumLocationBelanja->sumloc3,
			'sumlocBelanja4'	=> $sumLocationBelanja->sumloc4,
			'sumlocBelanja5'	=> $sumLocationBelanja->sumloc5,
			'sumlocSatwa1'		=> $sumLocationSatwa->sumloc1,
			'sumlocSatwa2'		=> $sumLocationSatwa->sumloc2,
			'sumlocSatwa3'		=> $sumLocationSatwa->sumloc3,
			'sumlocSatwa4'		=> $sumLocationSatwa->sumloc4,
			'sumlocSatwa5'		=> $sumLocationSatwa->sumloc5,
			'sumlocKuliner1'	=> $sumLocationKuliner->sumloc1,
			'sumlocKuliner2'	=> $sumLocationKuliner->sumloc2,
			'sumlocKuliner3'	=> $sumLocationKuliner->sumloc3,
			'sumlocKuliner4'	=> $sumLocationKuliner->sumloc4,
			'sumlocKuliner5'	=> $sumLocationKuliner->sumloc5,
			'sumlocAgro1'		=> $sumLocationAgro->sumloc1,
			'sumlocAgro2'		=> $sumLocationAgro->sumloc2,
			'sumlocAgro3'		=> $sumLocationAgro->sumloc3,
			'sumlocAgro4'		=> $sumLocationAgro->sumloc4,
			'sumlocAgro5'		=> $sumLocationAgro->sumloc5,
			'sumlocKeluarga1'	=> $sumLocationKeluarga->sumloc1,
			'sumlocKeluarga2'	=> $sumLocationKeluarga->sumloc2,
			'sumlocKeluarga3'	=> $sumLocationKeluarga->sumloc3,
			'sumlocKeluarga4'	=> $sumLocationKeluarga->sumloc4,
			'sumlocKeluarga5'	=> $sumLocationKeluarga->sumloc5,
		);

		$validCategoryPrefix = Categories::where('travel__categories.prefix', $prefix)->first();
		if($validCategoryPrefix == null) {
			return View::make('Backend.errors.404');
		} else {
			return View::make('Frontend.category.index', $params);
		}
	}

	public function placeByCategoryAndCity($prefix){
		$query = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
				->where('travel__categories.prefix', $prefix)
				->paginate(10);
		$query2 = Categories::where('travel__categories.prefix', $prefix)->get();
		$query3 = Locations::all();
		$query4 = Locations::leftjoin('travel__places', 'travel__places.location_id', '=', 'travel__locations.location_id')
				->get();

		return View::make('Frontend.category.index', ['places' => $query, 'categories' => $query2, 'locations' => $query3, 'locations2' => $query4]);
	}

	public function postFilterCity(Request $req) {
		if ($req) {
			$location_id 		= filter_var($req->location_id, FILTER_SANITIZE_STRING);
			$prefix 			= filter_var($req->prefix, FILTER_SANITIZE_STRING);
			$cities 			= Input::get('cities');
			$prices 			= Input::get('prices');
			$sort_review_id 	= filter_var($req->sort_review_id, FILTER_SANITIZE_STRING);
			$sort_criteria_id 	= filter_var($req->sort_criteria_id, FILTER_SANITIZE_STRING);
			// \Log::info('cities', [$cities]);
			// \Log::info('prices', [$prices]);

			if($location_id == 100) {
				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
					->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
					->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
					->where('travel__categories.prefix', $prefix)
					->orderBy('result','desc')
					// ->paginate(10);
					->get();
			} else if($prices == null && $cities == null) {
				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->where('travel__categories.prefix', $prefix)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
			} else {
				if($cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->where('travel__categories.prefix', $prefix)
						->whereIn('travel__places.price_code', $prices)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
				} else if($prices == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->where('travel__categories.prefix', $prefix)
						->whereIn('travel__places.location_id', $cities)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
				} else if($prices != null && $cities != null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->where('travel__categories.prefix', $prefix)
						->where(function ($query) use ($cities, $prices) {
	                        $query->whereIn('travel__places.location_id', $cities)
	                                ->whereIn('travel__places.price_code', $prices);
	                    })
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
				}
			}

			if($sort_review_id == 1) {
				if($prices == null && $cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
				} else {
					if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($cities == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('travel__places.price_code', $prices)
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($prices == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('travel__places.location_id', $cities)
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($prices != null && $cities != null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('travel__places.location_id', $cities)
		                                ->whereIn('travel__places.price_code', $prices);
		                    })
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					}
				}
			} else if($sort_review_id == 2) {
				if($prices == null && $cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
				} else {
					if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($cities == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('travel__places.price_code', $prices)
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($prices == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('travel__places.location_id', $cities)
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($prices != null && $cities != null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('travel__places.location_id', $cities)
		                                ->whereIn('travel__places.price_code', $prices);
		                    })
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					}
				}
			}

			if($sort_criteria_id == 1) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->where('travel__categories.prefix', $prefix)
						->orderBy('facility_attr','desc')->orderBy('result','desc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.price_code', $prices)
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.location_id', $cities)
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 2) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.price_code', $prices)
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.location_id', $cities)
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 3) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.price_code', $prices)
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.location_id', $cities)
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 4) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.price_code', $prices)
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.location_id', $cities)
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 5) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.price_code', $prices)
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->whereIn('tp.location_id', $cities)
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where('travel__categories.prefix', $prefix)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			}

			if(Auth::check()) {
		        $bmk = DB::table('travel__bookmark as tb')
		        		->select('tb.place_id as place_id')
		        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
		        		->where('tb.user_id',Auth::user()->id)
		        		->where('tb.status','1')
		        		->get();
		    } else {
		    	$bmk = "";
		    }

			$params = array(
				'places' 			=> $query,
				'bmk'				=> $bmk,
			);

			return View::make('Frontend.ajax.cityFiltered', $params);
        }
	}

	public function postFilterRecommended(Request $req) {
		if ($req) {
			$location_id 		= filter_var($req->location_id, FILTER_SANITIZE_STRING);
			$citiesRcm 			= Input::get('citiesRcm');
			$pricesRcm 			= Input::get('pricesRcm');
			$sort_review_id 	= filter_var($req->sort_review_id, FILTER_SANITIZE_STRING);
			$sort_criteria_id 	= filter_var($req->sort_criteria_id, FILTER_SANITIZE_STRING);

			if($location_id == 100) {
				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
					->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
					->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
					->orderBy('result','desc')
					// ->paginate(10);
					->get();
			} else if($pricesRcm == null && $citiesRcm == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->orderBy('result','desc')
							// ->paginate(10);
							->get();
			} else {
				if($citiesRcm == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->whereIn('travel__places.price_code', $pricesRcm)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
				} else if($pricesRcm == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->whereIn('travel__places.location_id', $citiesRcm)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
				} else if($pricesRcm != null && $citiesRcm != null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->where(function ($query) use ($citiesRcm, $pricesRcm) {
	                        $query->whereIn('travel__places.location_id', $citiesRcm)
	                                ->whereIn('travel__places.price_code', $pricesRcm);
	                    })
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
				}
			}

			if($sort_review_id == 1) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
				} else {
					if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($citiesRcm == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->whereIn('travel__places.price_code', $pricesRcm)
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($pricesRcm == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->whereIn('travel__places.location_id', $citiesRcm)
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($pricesRcm != null && $citiesRcm != null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('travel__places.location_id', $citiesRcm)
		                                ->whereIn('travel__places.price_code', $pricesRcm);
		                    })
							->orderBy('sumReview','desc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					}
				}
			} else if($sort_review_id == 2) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
				} else {
					if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($citiesRcm == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->whereIn('travel__places.price_code', $pricesRcm)
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($pricesRcm == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->whereIn('travel__places.location_id', $citiesRcm)
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					} else if($pricesRcm != null && $citiesRcm != null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('travel__places.location_id', $citiesRcm)
		                                ->whereIn('travel__places.price_code', $pricesRcm);
		                    })
							->orderBy('sumReview','asc')->orderBy('result','desc')
							// ->paginate(10);
							->get();
					}
				}
			}

			if($sort_criteria_id == 1) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->orderBy('facility_attr','desc')->orderBy('result','desc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($citiesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $pricesRcm)
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $citiesRcm)
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm != null && $citiesRcm != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('tp.location_id', $citiesRcm)
		                                ->whereIn('tp.price_code', $pricesRcm);
		                    })
							->orderBy('facility_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 2) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($citiesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $pricesRcm)
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $citiesRcm)
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm != null && $citiesRcm != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('tp.location_id', $citiesRcm)
		                                ->whereIn('tp.price_code', $pricesRcm);
		                    })
							->orderBy('cleanliness_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 3) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->orderBy('comfort_attr','desc')->orderBy('result','desc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($citiesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $pricesRcm)
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $citiesRcm)
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm != null && $citiesRcm != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('tp.location_id', $citiesRcm)
		                                ->whereIn('tp.price_code', $pricesRcm);
		                    })
							->orderBy('comfort_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 4) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->orderBy('service_attr','desc')->orderBy('result','desc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($citiesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $pricesRcm)
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $citiesRcm)
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm != null && $citiesRcm != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('tp.location_id', $citiesRcm)
		                                ->whereIn('tp.price_code', $pricesRcm);
		                    })
							->orderBy('service_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria_id == 5) {
				if($pricesRcm == null && $citiesRcm == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->orderBy('price_attr','desc')->orderBy('result','desc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($pricesRcm == ["1","2","3","4","5"] AND $citiesRcm == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($citiesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $pricesRcm)
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $citiesRcm)
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	} else if($pricesRcm != null && $citiesRcm != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('tp.place_id as place_id'), DB::raw('tp.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($citiesRcm, $pricesRcm) {
		                        $query->whereIn('tp.location_id', $citiesRcm)
		                                ->whereIn('tp.price_code', $pricesRcm);
		                    })
							->orderBy('price_attr','desc')->orderBy('result','desc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			}

			if(Auth::check()) {
		        $bmk = DB::table('travel__bookmark as tb')
		        		->select('tb.place_id as place_id')
		        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
		        		->where('tb.user_id',Auth::user()->id)
		        		->where('tb.status','1')
		        		->get();
		    } else {
		    	$bmk = "";
		    }

			$params = array(
				'places' 			=> $query,
				'bmk'				=> $bmk,
			);

			return View::make('Frontend.ajax.recommendedFiltered', $params);
        }
	}

	public function postFilterNry(Request $req) {
		if ($req) {
			$location_id 	= filter_var($req->location_id, FILTER_SANITIZE_STRING);
			$price_id 		= filter_var($req->price_id, FILTER_SANITIZE_STRING);
			$cities 		= Input::get('cities');
			$prices 		= Input::get('prices');
			$sort_criteria 	= filter_var($req->sort_criteria_id, FILTER_SANITIZE_STRING);
			// \Log::info('cities', [$cities]);
			// \Log::info('prices', [$prices]);

			$reviewsPlaceId = Reviews::select('place_id')->get();

			if($location_id == 100) {
				$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
					->leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
					->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
					->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
					->whereNotIn('travel__places.place_id',$reviewsPlaceId)
					->orderBy('name','asc')
					// ->paginate(10);
					->get();
			} else if($prices == null && $cities == null) {
				$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
					->leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
					->leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
					->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
					->whereNotIn('travel__places.place_id',$reviewsPlaceId)
					->orderBy('name','asc')
					// ->paginate(10);
					->get();
			} else {
				if($cities == null) {
					$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->whereIn('travel__places.price_code', $prices)
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
				} else if($prices == null) {
					$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->whereIn('travel__places.location_id', $cities)
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
				} else if($prices != null && $cities != null) {
					$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
						->leftjoin('travel__locations', 'travel__places.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('travel__places.place_id',$reviewsPlaceId)
						->where(function ($query) use ($cities, $prices) {
	                        $query->whereIn('travel__places.location_id', $cities)
	                                ->whereIn('travel__places.price_code', $prices);
	                    })
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
				}
			}

			if($sort_criteria == 1) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('tp.place_id',$reviewsPlaceId)
						->orderBy('facility_attr','desc')->orderBy('name','asc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('facility_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $prices)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('facility_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $cities)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('facility_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('facility_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria == 2) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('tp.place_id',$reviewsPlaceId)
						->orderBy('cleanliness_attr','desc')->orderBy('name','asc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('cleanliness_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $prices)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('cleanliness_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $cities)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('cleanliness_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('cleanliness_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria == 3) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('tp.place_id',$reviewsPlaceId)
						->orderBy('comfort_attr','desc')->orderBy('name','asc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('comfort_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $prices)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('comfort_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $cities)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('comfort_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('comfort_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria == 4) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('tp.place_id',$reviewsPlaceId)
						->orderBy('service_attr','desc')->orderBy('name','asc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('service_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $prices)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('service_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $cities)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('service_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('service_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			} else if($sort_criteria == 5) {
				if($prices == null && $cities == null) {
					$query = DB::table('travel__places as tp')
		                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
		                        THEN tp.facility_criteria
		                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS facility_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
		                        THEN tp.cleanliness_criteria
		                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS cleanliness_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
		                        THEN tp.comfort_criteria
		                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS comfort_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
		                        THEN tp.service_criteria
		                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS service_attr'),
		                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
		                        THEN tp.price_criteria
		                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
		                        FROM reviews r
		                        WHERE r.place_id = tp.place_id
		                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
		                        END AS price_attr'))
		                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
		                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
						->whereNotIn('tp.place_id',$reviewsPlaceId)
						->orderBy('price_attr','desc')->orderBy('name','asc')
						// ->paginate(10);
		                ->get();
		        } else {
		        	if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('price_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($cities == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.price_code', $prices)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('price_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices == null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->whereIn('tp.location_id', $cities)
							->whereNotIn('tp.place_id',$reviewsPlaceId)
							->orderBy('price_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	} else if($prices != null && $cities != null) {
		        		$query = DB::table('travel__places as tp')
			                ->select('tp.*', DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=tp.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=tp.place_id)) as sumBookmark'), DB::raw('travel__locations.location_name as location_name'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
			                        THEN tp.facility_criteria
			                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS facility_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
			                        THEN tp.cleanliness_criteria
			                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS cleanliness_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
			                        THEN tp.comfort_criteria
			                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS comfort_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
			                        THEN tp.service_criteria
			                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS service_attr'),
			                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
			                        THEN tp.price_criteria
			                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
			                        FROM reviews r
			                        WHERE r.place_id = tp.place_id
			                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
			                        END AS price_attr'))
			                ->leftjoin('travel__categories', 'tp.category_id', '=', 'travel__categories.category_id')
			                ->leftjoin('travel__locations', 'tp.location_id', '=', 'travel__locations.location_id')
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('tp.location_id', $cities)
		                                ->whereIn('tp.price_code', $prices);
		                    })
							->orderBy('price_attr','desc')->orderBy('name','asc')
							// ->paginate(10);
			                ->get();
		        	}
		        }
			}

			if(Auth::check()) {
		        $bmk = DB::table('travel__bookmark as tb')
		        		->select('tb.place_id as place_id')
		        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
		        		->where('tb.user_id',Auth::user()->id)
		        		->where('tb.status','1')
		        		->get();
		    } else {
		    	$bmk = "";
		    }

			$params = array(
				'places' 			=> $query,
				'bmk'				=> $bmk,
			);

			return View::make('Frontend.ajax.nryFiltered', $params);
        }
	}

	public function postFilterWr(Request $req) {
		if ($req) {
			$location_id 	= filter_var($req->location_id, FILTER_SANITIZE_STRING);
			$cities 		= Input::get('cities');
			$prices 		= Input::get('prices');
			$price_id 		= filter_var($req->price_id, FILTER_SANITIZE_STRING);
			$sort_sum 		= filter_var($req->sort_sum_id, FILTER_SANITIZE_STRING);
			// \Log::info('cities', [$cities]);
			// \Log::info('prices', [$prices]);

			$reviewsPlaceId = Reviews::select('place_id')->get();

			$myTime = Carbon::now();
			$myTime = $myTime->addHours(7);
			$myTimeBeforeAWeek = $myTime->subDays(7);

			if($location_id == 100) {
				$reviewsPlaceId = Reviews::select('place_id')->get();
				$wrr = Places::select(DB::raw('reviews.place_id'))
						->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$reviewsPlaceId)
						->where('reviews.created_at','>',$myTimeBeforeAWeek)
						->where('reviews.created_at','<>',NULL)
						->orderBy('name','asc')
						->get();
				$wkrv = array();
				foreach ($wrr as $wr) {
					$wkrv[] = $wr->place_id;
				}

				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$wkrv)
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
			} else if($prices == null && $cities == null) {
				$reviewsPlaceId = Reviews::select('place_id')->get();
				$wrr = Places::select(DB::raw('reviews.place_id'))
						->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$reviewsPlaceId)
						->where('reviews.created_at','>',$myTimeBeforeAWeek)
						->where('reviews.created_at','<>',NULL)
						->orderBy('name','asc')
						->get();
				$wkrv = array();
				foreach ($wrr as $wr) {
					$wkrv[] = $wr->place_id;
				}

				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$wkrv)
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
			} else {
				$reviewsPlaceId = Reviews::select('place_id')->get();
				$wrr = Places::select(DB::raw('reviews.place_id'))
						->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$reviewsPlaceId)
						->where('reviews.created_at','>',$myTimeBeforeAWeek)
						->where('reviews.created_at','<>',NULL)
						->orderBy('name','asc')
						->get();
				$wkrv = array();
				foreach ($wrr as $wr) {
					$wkrv[] = $wr->place_id;
				}

				if($cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$wkrv)
						->whereIn('travel__places.price_code', $prices)
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
				} else if($prices == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$wkrv)
						->whereIn('travel__places.location_id', $cities)
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
				} else if($prices != null && $cities != null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->whereIn('travel__places.place_id',$wkrv)
						->where(function ($query) use ($cities, $prices) {
	                        $query->whereIn('travel__places.location_id', $cities)
	                                ->whereIn('travel__places.price_code', $prices);
	                    })
						->orderBy('name','asc')
						// ->paginate(10);
						->get();
				}
			}

			if($sort_sum == 1) {
				if($prices == null && $cities == null) {
					$reviewsPlaceId = Reviews::select('place_id')->get();
					$wrr = Places::select(DB::raw('reviews.place_id'))
							->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->whereIn('travel__places.place_id',$reviewsPlaceId)
							->where('reviews.created_at','>',$myTimeBeforeAWeek)
							->where('reviews.created_at','<>',NULL)
							->get();
					$wkrv = array();
					foreach ($wrr as $wr) {
						$wkrv[] = $wr->place_id;
					}

					$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->whereIn('travel__places.place_id',$wkrv)
							->orderBy('sumReview','desc')->orderBy('name','asc')
							// ->paginate(10);
							->get();
				} else {
					$reviewsPlaceId = Reviews::select('place_id')->get();
					$wrr = Places::select(DB::raw('reviews.place_id'))
							->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->whereIn('travel__places.place_id',$reviewsPlaceId)
							->where('reviews.created_at','>',$myTimeBeforeAWeek)
							->where('reviews.created_at','<>',NULL)
							->get();
					$wkrv = array();
					foreach ($wrr as $wr) {
						$wkrv[] = $wr->place_id;
					}

					if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->whereIn('travel__places.place_id',$wkrv)
								->orderBy('sumReview','desc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					} else if($cities == null) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->whereIn('travel__places.place_id',$wkrv)
								->whereIn('travel__places.price_code', $prices)
								->orderBy('sumReview','desc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					} else if($prices == null) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->whereIn('travel__places.place_id',$wkrv)
								->whereIn('travel__places.location_id', $cities)
								->orderBy('sumReview','desc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					} else if($prices != null && $cities != null) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->whereIn('travel__places.place_id',$wkrv)
								->where(function ($query) use ($cities, $prices) {
			                        $query->whereIn('travel__places.location_id', $cities)
			                                ->whereIn('travel__places.price_code', $prices);
			                    })
								->orderBy('sumReview','desc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					}
				}
			} else if($sort_sum == 2) {
				if($prices == null && $cities == null) {
					$reviewsPlaceId = Reviews::select('place_id')->get();
					$wrr = Places::select(DB::raw('reviews.place_id'))
							->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->whereIn('travel__places.place_id',$reviewsPlaceId)
							->where('reviews.created_at','>',$myTimeBeforeAWeek)
							->where('reviews.created_at','<>',NULL)
							->get();
					$wkrv = array();
					foreach ($wrr as $wr) {
						$wkrv[] = $wr->place_id;
					}

					$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->whereIn('travel__places.place_id',$wkrv)
							->orderBy('sumReview','asc')->orderBy('name','asc')
							// ->paginate(10);
							->get();
				} else {
					$reviewsPlaceId = Reviews::select('place_id')->get();
					$wrr = Places::select(DB::raw('reviews.place_id'))
							->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->whereIn('travel__places.place_id',$reviewsPlaceId)
							->where('reviews.created_at','>',$myTimeBeforeAWeek)
							->where('reviews.created_at','<>',NULL)
							->get();
					$wkrv = array();
					foreach ($wrr as $wr) {
						$wkrv[] = $wr->place_id;
					}

					if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->whereIn('travel__places.place_id',$wkrv)
								->orderBy('sumReview','asc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					} else if($cities == null) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->whereIn('travel__places.place_id',$wkrv)
								->whereIn('travel__places.price_code', $prices)
								->orderBy('sumReview','asc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					} else if($prices == null) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->whereIn('travel__places.place_id',$wkrv)
								->whereIn('travel__places.location_id', $cities)
								->orderBy('sumReview','asc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					} else if($prices != null && $cities != null) {
						$query = Places::select('*', DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->whereIn('travel__places.place_id',$wkrv)
								->where(function ($query) use ($cities, $prices) {
			                        $query->whereIn('travel__places.location_id', $cities)
			                                ->whereIn('travel__places.price_code', $prices);
			                    })
								->orderBy('sumReview','asc')->orderBy('name','asc')
								// ->paginate(10);
								->get();
					}
				}
			}

			if(Auth::check()) {
		        $bmk = DB::table('travel__bookmark as tb')
		        		->select('tb.place_id as place_id')
		        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
		        		->where('tb.user_id',Auth::user()->id)
		        		->where('tb.status','1')
		        		->get();
		    } else {
		    	$bmk = "";
		    }

			$params = array(
				'places' 			=> $query,
				'bmk'				=> $bmk,
			);

			return View::make('Frontend.ajax.wrFiltered', $params);
        }
	}

	public function postFilterWa(Request $req) {
		if ($req) {
			$location_id 	= filter_var($req->location_id, FILTER_SANITIZE_STRING);
			$price_id 		= filter_var($req->price_id, FILTER_SANITIZE_STRING);
			$cities 		= Input::get('cities');
			$prices 		= Input::get('prices');
			$sort_time 		= filter_var($req->sort_time_id, FILTER_SANITIZE_STRING);
			// \Log::info('cities', [$cities]);
			// \Log::info('prices', [$prices]);

			$reviewsPlaceId = Reviews::select('place_id')->get();

			$myTime = Carbon::now();
			$myTime = $myTime->addHours(7);
			$myTimeBeforeAWeek = $myTime->subDays(7);

			if($location_id == 100) {
				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->where('travel__places.datetime','>',$myTimeBeforeAWeek)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
			} else if($prices == null && $cities == null) {
				$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->where('travel__places.datetime','>',$myTimeBeforeAWeek)
						->orderBy('result','desc')
						// ->paginate(10);
						->get();
			} else {
				if($cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->where('travel__places.datetime','>',$myTimeBeforeAWeek)
							->whereIn('travel__places.price_code', $prices)
							->orderBy('result','desc')
							// ->paginate(10);
							->get();
				} else if($prices == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->where('travel__places.datetime','>',$myTimeBeforeAWeek)
							->whereIn('travel__places.location_id', $cities)
							->orderBy('result','desc')
							// ->paginate(10);
							->get();
				} else if($prices != null && $cities != null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->where('travel__places.datetime','>',$myTimeBeforeAWeek)
							->where(function ($query) use ($cities, $prices) {
		                        $query->whereIn('travel__places.location_id', $cities)
		                                ->whereIn('travel__places.price_code', $prices);
		                    })
							->orderBy('result','desc')
							// ->paginate(10);
							->get();
				}
			}

			if($sort_time == 1) {
				if($prices == null && $cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->where('travel__places.datetime','>',$myTimeBeforeAWeek)
							->orderBy('datetime','desc')
							// ->paginate(10);
							->get();
				} else {
					if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->orderBy('datetime','desc')
								// ->paginate(10);
								->get();
					} else if($cities == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->whereIn('travel__places.price_code', $prices)
								->orderBy('datetime','desc')
								// ->paginate(10);
								->get();
					} else if($prices == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->whereIn('travel__places.location_id', $cities)
								->orderBy('datetime','desc')
								// ->paginate(10);
								->get();
					} else if($prices != null && $cities != null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->where(function ($query) use ($cities, $prices) {
			                        $query->whereIn('travel__places.location_id', $cities)
			                                ->whereIn('travel__places.price_code', $prices);
			                    })
								->orderBy('datetime','desc')
								// ->paginate(10);
								->get();
					}
				}
			} else if($sort_time == 2) {
				if($prices == null && $cities == null) {
					$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
							->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
							->where('travel__places.datetime','>',$myTimeBeforeAWeek)
							->orderBy('datetime','asc')
							// ->paginate(10);
							->get();
				} else {
					if($prices == ["1","2","3","4","5"] AND $cities == ["1","2","3","4","5","6","7","8"]) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->orderBy('datetime','asc')
								// ->paginate(10);
								->get();
					} else if($cities == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->whereIn('travel__places.price_code', $prices)
								->orderBy('datetime','asc')
								// ->paginate(10);
								->get();
					} else if($prices == null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->whereIn('travel__places.location_id', $cities)
								->orderBy('datetime','asc')
								// ->paginate(10);
								->get();
					} else if($prices != null && $cities != null) {
						$query = Places::select('*', DB::raw('travel__places.place_id as place_id'), DB::raw('travel__places.price as price'), DB::raw('(SELECT count(*) FROM reviews WHERE reviews.place_id=travel__places.place_id) as sumReview'), DB::raw('(SELECT count(*) FROM travel__bookmark WHERE (travel__bookmark.status=1 AND travel__bookmark.place_id=travel__places.place_id)) as sumBookmark'))
								->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
								->where('travel__places.datetime','>',$myTimeBeforeAWeek)
								->where(function ($query) use ($cities, $prices) {
			                        $query->whereIn('travel__places.location_id', $cities)
			                                ->whereIn('travel__places.price_code', $prices);
			                    })
								->orderBy('datetime','asc')
								// ->paginate(10);
								->get();
					}
				}
			}

			if(Auth::check()) {
		        $bmk = DB::table('travel__bookmark as tb')
		        		->select('tb.place_id as place_id')
		        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
		        		->where('tb.user_id',Auth::user()->id)
		        		->where('tb.status','1')
		        		->get();
		    } else {
		    	$bmk = "";
		    }

			$params = array(
				'places' 			=> $query,
				'bmk'				=> $bmk,
			);

			return View::make('Frontend.ajax.waFiltered', $params);
        }
	}
}
