<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
use View;
use App\Locations;
use App\Categories;
use App\Places;
use App\RequestsFromUser;
use App\Comments;
use App\Reviews;
use App\Bookmark;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Auth;

class PlaceController {	
	/**
	 * Show Home
	 * @return [view] [Home Page]
	 */
	public function place($prefix){
		$query = Places::where('place_prefix',$prefix)->get();

		$query2 = DB::table('travel__places as tp')
                ->select('tp.*',
                    DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                        THEN tp.facility_criteria
                        ELSE (tp.facility_criteria+((SELECT SUM(r.facility)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS facility_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                        THEN tp.cleanliness_criteria
                        ELSE (tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS cleanliness_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                        THEN tp.comfort_criteria
                        ELSE (tp.comfort_criteria+((SELECT SUM(r.comfort)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS comfort_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.service_criteria) IS NULL
                        THEN tp.service_criteria
                        ELSE (tp.service_criteria+((SELECT SUM(r.service)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS service_attr'),
                    DB::raw('CASE WHEN ((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id) + tp.price_criteria) IS NULL
                        THEN tp.price_criteria
                        ELSE (tp.price_criteria+((SELECT SUM(r.price)
                        FROM reviews r
                        WHERE r.place_id = tp.place_id
                        GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)
                        END AS price_attr'))
                ->where('place_prefix',$prefix)
                ->get();
        foreach ($query2 as $q2) {
        	$fattr = round($q2->facility_attr,1);
        	$cattr = round($q2->cleanliness_attr,1);
        	$comattr = round($q2->comfort_attr,1);
        	$sattr = round($q2->service_attr,1);
        	$pattr = round($q2->price_attr,1);
        }

        $qAHP = DB::table('ahp_matrix as am')
                    ->select('*')
                    ->get();
        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
        $sService = DB::table('ahp_matrix as am')->sum('service');
        $sPrice = DB::table('ahp_matrix as am')->sum('price');
        $vp = array();
        foreach($qAHP as $qhp) {
            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
        }
        foreach ($query2 as $q2) {
	        $avgRate = round((($q2->facility_attr * $vp[0]) + ($q2->cleanliness_attr * $vp[1]) + ($q2->comfort_attr * $vp[2]) + ($q2->service_attr * $vp[3]) + ($q2->price_attr * $vp[4])),1);
        }

        if(!empty($avgRate)){
			$query3 = Reviews::select(DB::raw('author_id'))->where('place_id',$query[0]->place_id)->get();
			$q3_authorId = array();
			foreach ($query3 as $q3) {
				$q3_authorId[] = $q3->author_id;
			}
		}

		$currentUrl = \Request::url();
		$currentUrlSlug = substr($currentUrl, strpos($currentUrl, "/") + 2);
		$currentUrlSlug = explode("/", $currentUrlSlug, 3);
		$currentUrlSlug = $currentUrlSlug[2];

		$reviews = Reviews::where('active','1')
					->where('slug',$currentUrlSlug)
					->orderBy('created_at','desc')
					->get();

		$rvw = Reviews::where('active','1')
				->where('slug',$currentUrlSlug)
				->orderBy('created_at','desc')
				->first();

		$totalReviewer = DB::table('reviews as r')
							->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
							->where('slug',$currentUrlSlug)
							->distinct('author_id')
							->count('author_id');

		$reviewerName = DB::table('reviews as r')
							->select(DB::raw('u.name as name'))
							->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
							->leftjoin('users as u','u.id','=','r.author_id')
							->where('slug',$currentUrlSlug)
							->distinct('author_id')
							->get();

		$location = DB::table('travel__places as tp')
						->select(DB::raw('location_name'))
						->leftjoin('travel__locations as tl','tl.location_id','=','tp.location_id')
						->where('place_prefix',$prefix)
						->get();

		$category = DB::table('travel__places as tp')
						->select(DB::raw('category_name'))
						->leftjoin('travel__categories as tc','tc.category_id','=','tp.category_id')
						->where('place_prefix',$prefix)
						->get();

		$dateEnd = DB::table('reviews as r')
						->select(DB::raw('created_at'))
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('place_prefix',$prefix)
						->orderBy('created_at','desc')
						->limit(1)
						->get();

		$dateStart = DB::table('reviews as r')
						->select(DB::raw('created_at'))
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('place_prefix',$prefix)
						->orderBy('created_at','asc')
						->limit(1)
						->get();
		// var_dump(count($dateEnd));

		if(count($dateEnd) == 0) {
			$dateEnd = '2017-03-24 07:52:04';
		} else {
			foreach ($dateEnd as $de) {
				$dateEnd = $de->created_at;
			}
		}
		if(count($dateStart) == 0) {
			$dateStart = '2017-03-24 07:52:04';
		} else {
			foreach ($dateStart as $ds) {
				$dateStart = $ds->created_at;
			}
		}

		$datetime1 = new \DateTime($dateStart);
		$datetime2 = new \DateTime($dateEnd);
		$interval = $datetime1->diff($datetime2);
		$days = $interval->format('%a');
		$hours = $interval->format('%h');
		$minutes = $interval->format('%i');
		$seconds = $interval->format('%s');

		if($rvw == NULL) {
			$counts = 0;
		} else {
			$counts = DB::table('reviews as r')
						->select(DB::raw('COUNT(*) as sum'))
						->leftjoin('travel__places as tp','tp.place_id','=','r.place_id')
						->where('r.place_id','=',$rvw->place_id)
						->first();
			$counts = $counts->sum;
		}

		$query6 = Places::select(DB::raw('name'))->get();

		if(empty($avgRate)) {
			$myTime = Carbon::now();
			$myTime = $myTime->addHours(7);
			$myTimeBeforeAWeek = $myTime->subDays(7);
			
			$weeklyReview = Reviews::leftjoin('travel__places', 'reviews.place_id', '=', 'travel__places.place_id')
						->where('reviews.created_at','>',$myTimeBeforeAWeek)
						->where('reviews.created_at','<>',NULL)
						->orderBy('reviews.created_at','desc')
						->limit(3)
						->get();

			$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->where('datetime','>',$myTimeBeforeAWeek)
							->where('datetime','<>',NULL)
							->orderBy('datetime','desc')
							->limit(3)
							->get();

			$reviewsPlaceId = Reviews::select('place_id')->get();

			$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
							->whereNotIn('travel__places.place_id',$reviewsPlaceId)
							->orderBy('name','asc')
							->limit(3)
							->get();

			return View::make('Frontend.place.notfound', ['search' => $query6, 'weeklyReview' => $weeklyReview, 'weeklyAdded' => $weeklyAdded, 'notReviewedYet'	=> $notReviewedYet]);
		} else {
			$id_place = Places::where('place_prefix',$prefix)->first();
			$id_place = $id_place->place_id;

			if(Auth::user()) {
				$checkBookmark = DB::table('travel__bookmark')
									->where('user_id',Auth::user()->id)
									->where('place_id',$id_place)
									->first();
				if(empty($checkBookmark)){
					$checkBookmark = 0;
				} else {
					$checkBookmark = 1;
				}
			} else {
				$checkBookmark = 0;
			}

			if($checkBookmark == 1) {
				$cb = DB::table('travel__bookmark')
						->where('user_id',Auth::user()->id)
						->where('place_id',$id_place)
						->first();
				$checkBookmarkStatus = $cb->status;
			} else if($checkBookmark == 0) {
				$checkBookmarkStatus = 0;
			}

			$fasilitas = DB::table('ahp_matrix as am')
                        ->select(DB::raw('bobot'))
                        ->where('id','1')
	                    ->first();
	        $kebersihan = DB::table('ahp_matrix as am')
	                    ->select(DB::raw('bobot'))
	                    ->where('id','2')
	                    ->first();
	        $kenyamanan = DB::table('ahp_matrix as am')
	                    ->select(DB::raw('bobot'))
	                    ->where('id','3')
	                    ->first();
	        $pelayanan = DB::table('ahp_matrix as am')
	                    ->select(DB::raw('bobot'))
	                    ->where('id','4')
	                    ->first();
	        $harga = DB::table('ahp_matrix as am')
	                    ->select(DB::raw('bobot'))
	                    ->where('id','5')
	                    ->first();

	        if(Auth::check()) {
		        $bmk = DB::table('travel__bookmark as tb')
		        		->leftjoin('travel__places as tp','tp.place_id','=','tb.place_id')
		        		->where('tb.user_id',Auth::user()->id)
		        		->where('tb.status','1')
		        		->get();
		       	$countbmk = count($bmk);
		    } else {
		    	$bmk = "";
		    	$countbmk = 0;
		    }
		    
	        DB::update('UPDATE travel__places
		          SET avgRating =?
		          WHERE place_prefix = ?', [$avgRate, $prefix]);

	        $myTime = Carbon::now();
			$myTime = $myTime->addHours(7);
			$myTimeBeforeAWeek = $myTime->subDays(7);

		    $recommended = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->orderBy('result','desc')
							->limit(3)
							->get();

			$reviewsPlaceId = Reviews::select('place_id')->get();
			$wrr = Places::select(DB::raw('reviews.place_id'))
					->leftjoin('reviews', 'reviews.place_id', '=', 'travel__places.place_id')
					->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
					->whereIn('travel__places.place_id',$reviewsPlaceId)
					->where('reviews.created_at','>',$myTimeBeforeAWeek)
					->where('reviews.created_at','<>',NULL)
					->get();
			$wkrv = array();
			foreach ($wrr as $wr) {
				$wkrv[] = $wr->place_id;
			}

			$weeklyReview = Places::whereIn('travel__places.place_id',$wkrv)
						->leftjoin('travel__locations','travel__places.location_id','=','travel__locations.location_id')
						->orderBy('name','asc')
						->limit(3)
						->get();

			$weeklyAdded = Places::leftjoin('travel__categories', 'travel__places.category_id', '=', 'travel__categories.category_id')
							->where('datetime','>',$myTimeBeforeAWeek)
							->where('datetime','<>',NULL)
							->orderBy('datetime','desc')
							->limit(3)
							->get();

			$reviewsPlaceId = Reviews::select('place_id')->get();

			$notReviewedYet = Places::leftjoin('reviews', 'travel__places.place_id', '=', 'reviews.place_id')
							->whereNotIn('travel__places.place_id',$reviewsPlaceId)
							->orderBy('name','asc')
							->limit(3)
							->get();

			return View::make('Frontend.place.index', ['q3_authorId' => $q3_authorId, 'places' => $query, 'avgRate' => $avgRate, 'reviews' => $reviews, 'currentUrlSlug' => $currentUrlSlug, 'counts' => $counts, 'totalReviewer' => $totalReviewer, 'reviewerName' => $reviewerName, 'location' => $location, 'category' => $category, 'days' => $days, 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds, 'search' => $query6, 'checkBookmark' => $checkBookmark, 'id_place' => $id_place, 'checkBookmark' => $checkBookmark, 'checkBookmarkStatus' => $checkBookmarkStatus, 'fattr' => $fattr, 'cattr' => $cattr, 'comattr' => $comattr, 'sattr' => $sattr, 'pattr' => $pattr, 'fasilitas' => $fasilitas->bobot, 'kebersihan' => $kebersihan->bobot, 'kenyamanan' => $kenyamanan->bobot, 'pelayanan' => $pelayanan->bobot, 'harga' => $harga->bobot, 'bmk' => $bmk, 'countbmk' => $countbmk, 'recommended' => $recommended, 'weeklyReview' => $weeklyReview, 'weeklyAdded' => $weeklyAdded, 'notReviewedYet' => $notReviewedYet]);
		}
	}

	public function storeReview(Request $request)
	{
		$rules = array (
			'fasilitas'			=> 'required',
			'kebersihan'		=> 'required',
			'kenyamanan'		=> 'required',
			'pelayanan'			=> 'required',
			'harga'				=> 'required',
			'title' 			=> 'required|min:5',
			'komentar'			=> 'required|min:20',
		);

        $message = array(
        	'fasilitas.required'	=> 'Penilaian untuk fasilitas wajib diisi.',
        	'kebersihan.required'	=> 'Penilaian untuk kebersihan wajib diisi.',
        	'kenyamanan.required'	=> 'Penilaian untuk kenyamanan wajib diisi.',
        	'pelayanan.required'	=> 'Penilaian untuk pelayanan wajib diisi.',
        	'harga.required'		=> 'Penilaian untuk harga wajib diisi.',
        	'title.required'		=> 'Bidang isian judul ulasan wajib diisi.',
        	'title.min'				=> 'Judul kurang dari 5 karakter.',
        	'komentar.required'		=> 'Bidang isian ulasan wajib diisi.',
        	'komentar.min'			=> 'Ulasan kurang dari 20 karakter.',
        );

		$validator = Validator::make(Input::all(), $rules, $message);

		$prefix = $request->get('place_prefix');

		if ($validator->fails()) {
	    	return Redirect::to('place/' . $prefix . '#rating')->withErrors($validator, 'postReview')->withInput();
		}

		$komentar = $request->get('komentar');
		$komen = nl2br($komentar);

		$reviews = new Reviews();
		$reviews->place_id		= $request->get('place_id');
		$reviews->title 		= $request->get('title');
		$reviews->body 			= $komen;
		$reviews->facility 		= $request->get('fasilitas');
		$reviews->cleanliness 	= $request->get('kebersihan');
		$reviews->comfort 		= $request->get('kenyamanan');
		$reviews->service 		= $request->get('pelayanan');
		$reviews->price 		= $request->get('harga');
		$reviews->slug 			= $prefix;
		$reviews->prefix 		= str_slug($reviews->title);
		$reviews->created_at 	= Carbon::now()->addHours(7);
		$reviews->updated_at 	= Carbon::now()->addHours(7);
		
		$reviews->author_id = $request->user()->id;

		if($request->has('save'))
		{
			$reviews->active = 0;
			$message = 'Ulasan berhasil disimpan.';			
		}			
		else 
		{
			$reviews->active = 1;
			$message = 'Ulasan berhasil diterbitkan.';
		}
		$reviews->save();

		// update result-nya
		$query5 = DB::table('travel__places as tp')
                    ->select('tp.*',
                        DB::raw('CASE WHEN ((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.facility_criteria) IS NULL
                            THEN round(tp.facility_criteria,1)
                            ELSE round(((tp.facility_criteria+((SELECT SUM(r.facility)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS facility_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.cleanliness_criteria) IS NULL
                            THEN round(tp.cleanliness_criteria,1)
                            ELSE round(((tp.cleanliness_criteria+((SELECT SUM(r.cleanliness)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS cleanliness_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.comfort_criteria) IS NULL
                            THEN round(tp.comfort_criteria,1)
                            ELSE round(((tp.comfort_criteria+((SELECT SUM(r.comfort)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS comfort_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.service_criteria) IS NULL
                            THEN round(tp.service_criteria,1)
                            ELSE round(((tp.service_criteria+((SELECT SUM(r.service)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS service_attr'),
                        DB::raw('CASE WHEN ((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id) + tp.price_criteria) IS NULL
                            THEN round(tp.price_criteria,1)
                            ELSE round(((tp.price_criteria+((SELECT SUM(r.price)
                            FROM reviews r
                            WHERE r.place_id = tp.place_id
                            GROUP BY r.place_id)))/((SELECT COUNT(*) FROM reviews r WHERE r.place_id = tp.place_id GROUP BY tp.place_id)+1)),1)
                            END AS price_attr'))
                    ->get();

		$sumFacility = 0;
		$sumCleanliness =0;
		$sumComfort =0;
		$sumService =0;
		$sumPrice = 0;
		foreach($query5 as $q) {
			$sumFacility += pow($q->facility_attr, 2);
			$sumCleanliness += pow($q->cleanliness_attr, 2);
			$sumComfort += pow($q->comfort_attr, 2);
			$sumService += pow($q->service_attr, 2);
			$sumPrice += pow($q->price_attr, 2);
		}
		$sumFacility = sqrt($sumFacility);
		$sumCleanliness = sqrt($sumCleanliness);
		$sumComfort = sqrt($sumComfort);
		$sumService = sqrt($sumService);
		$sumPrice = sqrt($sumPrice);

		$qAHP = DB::table('ahp_matrix as am')
                    ->select('*')
                    ->get();
        $sFacility = DB::table('ahp_matrix as am')->sum('facility');
        $sCleanliness = DB::table('ahp_matrix as am')->sum('cleanliness');
        $sComfort = DB::table('ahp_matrix as am')->sum('comfort');
        $sService = DB::table('ahp_matrix as am')->sum('service');
        $sPrice = DB::table('ahp_matrix as am')->sum('price');

		$itemsFacility = array();
		$itemsCleanliness = array();
		$itemsComfort = array();
		$itemsService = array();
		$itemsPrice = array();
		
        $vp = array();
        foreach($qAHP as $qhp) {
            $vp[] = round((round($qhp->facility/$sFacility,3)+round($qhp->cleanliness/$sCleanliness,3)+round($qhp->comfort/$sComfort,3)+round($qhp->service/$sService,3)+round($qhp->price/$sPrice,3))/5,3);
        }

		foreach($query5 as $q) {
			$itemsFacility[] = $q->facility_attr/$sumFacility*$vp[0];
			$itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1];
			$itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2];
			$itemsService[] = $q->service_attr/$sumService*$vp[3];
			$itemsPrice[] = $q->price_attr/$sumPrice*$vp[4];
		}

		foreach($query5 as $q) {
			$posSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - max($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - max($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - max($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - max($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - max($itemsPrice)),2));
			$negSolution = sqrt(pow((($q->facility_attr/$sumFacility*$vp[0]) - min($itemsFacility)),2) + pow((($itemsCleanliness[] = $q->cleanliness_attr/$sumCleanliness*$vp[1]) - min($itemsCleanliness)),2) + pow((($itemsComfort[] = $q->comfort_attr/$sumComfort*$vp[2]) - min($itemsComfort)),2) + pow((($itemsService[] = $q->service_attr/$sumService*$vp[3]) - min($itemsService)),2) + pow((($itemsPrice[] = $q->price_attr/$sumPrice*$vp[4]) - min($itemsPrice)),2));
			$rank = $negSolution/($negSolution+$posSolution);

		    DB::update('UPDATE travel__places
		          SET result =?
		          WHERE place_id = ?', [$rank, $q->place_id]);
		}

	   	return Redirect::to('place/' . $prefix . '#ulasan');
	}

	public function add() {
		$query = Locations::all();
		$query2 = Categories::all();
		$query3 = Places::select(DB::raw('name'))->get();
		$query4 = DB::table('price_range')->get();

		return View::make('Frontend.place.add', ['locations' => $query, 'categories' => $query2, 'search' => $query3, 'priceRange' => $query4]);
	}

	public function addPlaces(Request $request) {
		$rules = array (
			'name' 				=> 'required',
			'phone_number'		=> 'required',
			'address' 			=> 'required',
			'operational_hours'	=> 'required',
			'price' 			=> 'required',
			'price_range'		=> 'required|integer',
			'lokasi'			=> 'required|integer',
			'kategori'			=> 'required|integer',
			'description' 		=> 'required|min:20',
		);

        $message = array(
        	'name.required'			=> 'Nama objek wisata wajib diisi.',
        	'phone_number.required'	=> 'Nomor telepon wisata wajib diisi.',
        	'address.required'		=> 'Alamat wisata wajib diisi.',
        	'price_range.integer'	=> 'Anda belum memilih rentang HTM.',
        	'lokasi.integer'		=> 'Anda belum memilih lokasi.',
        	'kategori.integer'		=> 'Anda belum memilih kategori.',
        	'description.integer'	=> 'Deskripsi wajib diisi.',
        	'description.min'		=> 'Deskripsi minimal 20 karakter.',
        );

        $validator = Validator::make(Input::all(), $rules, $message);

		if ($validator->fails()) {
			return redirect('/places/add')->withErrors($validator, 'addPlace')->withInput();
		}

		$duplicate = Places::where('name',$request->get('name'))->first();
		$duplicate2 = RequestsFromUser::where('name',$request->get('name'))->first();
		if($duplicate)
		{
			return redirect()->back()->withErrors('Objek wisata telah terdapat di BOW.', 'addPlace')->withInput();
		} else if($duplicate2)
		{
			return redirect()->back()->withErrors('Objek wisata ini telah di-request. Kami akan mengevaluasinya dalam waktu maksimal 2x24 jam.', 'addPlace')->withInput();
		}

		$input = $request->all();

		$place = new RequestsFromUser;
		$place->from_user 				= $request->get('from_user');
		$place->name 					= $request->get('name');
		$place->operational_hours 		= $request->get('operational_hours');
		$place->price 					= $request->get('price');
		$place->price_code 				= $request->get('price_range');
		$place->location_id 			= $request->get('lokasi');
		$place->category_id 			= $request->get('kategori');
		$place->phone_number 			= $request->get('phone_number');
		$place->address 				= $request->get('address');
		$place->description 			= $request->get('description');
		$place->created_at 				= Carbon::now()->addHours(7);
    	$place->save();

    	return redirect('/places/add')->withMessage('Permintaan Anda telah dikirim. Terima kasih.');
	}

	public function bookmarkPlace(Request $request)
  	{
  		if(Auth::user()) {
			$checkBookmark = DB::table('travel__bookmark')
								->where('user_id',Auth::user()->id)
								->where('place_id',Input::get('place_id'))
								->first();
			if(empty($checkBookmark)){
				$checkBookmark = 0;
			} else {
				$checkBookmark = 1;
			}
		} else $checkBookmark = 0;

		if($checkBookmark == 1) {
			$cb = DB::table('travel__bookmark')
					->where('user_id',Auth::user()->id)
					->where('place_id',Input::get('place_id'))
					->first();
			$checkBookmarkStatus = $cb->status;
		}

		if($checkBookmark == 0) {
			$bookmark 	= new Bookmark;
	        $bookmark->user_id 		= Input::get('user_id');
			$bookmark->place_id 	= Input::get('place_id');
			$bookmark->status 		= '1';
			$bookmark->datetime 	= Carbon::now()->addHours(7);
	    	$bookmark->save();
		} else if($checkBookmark == 1 && $checkBookmarkStatus == 0) {
			DB::update('UPDATE travel__bookmark
		          SET status = 1
		          WHERE user_id = ? AND place_id = ?', [Input::get('user_id'), Input::get('place_id')]);
		}
  	}

  	public function deleteBookmark(Request $req) {
		$place_id 	= $req->get('place_id');
		
  		DB::update('UPDATE travel__bookmark
		          SET status = 0
		          WHERE user_id = ? AND place_id = ?', [Auth::user()->id, $place_id]);

  		return redirect('/account/bookmark');
  	}
}
