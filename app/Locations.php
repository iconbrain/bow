<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $table = 'travel__locations';
	public $timestamps = false;
	protected $primaryKey = 'location_id';
}
