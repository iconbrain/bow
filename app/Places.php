<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Places extends Model
{
    protected $table = 'travel__places';
	public $timestamps = false;
	protected $primaryKey = 'place_id';

	protected $fillable = ['image'];
}
